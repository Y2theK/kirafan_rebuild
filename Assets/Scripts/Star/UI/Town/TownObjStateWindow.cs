﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x0200104C RID: 4172
	[Token(Token = "0x2000AD6")]
	[StructLayout(3)]
	public class TownObjStateWindow : UIGroup
	{
		// Token: 0x06004FF2 RID: 20466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004976")]
		[Address(RVA = "0x1015A4E84", Offset = "0x15A4E84", VA = "0x1015A4E84")]
		public void Open(long mngID)
		{
		}

		// Token: 0x06004FF3 RID: 20467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004977")]
		[Address(RVA = "0x1015A4F80", Offset = "0x15A4F80", VA = "0x1015A4F80")]
		public void OnClickCloseButtonCallBack()
		{
		}

		// Token: 0x06004FF4 RID: 20468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004978")]
		[Address(RVA = "0x1015A4F8C", Offset = "0x15A4F8C", VA = "0x1015A4F8C")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004FF5 RID: 20469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004979")]
		[Address(RVA = "0x1015A4F98", Offset = "0x15A4F98", VA = "0x1015A4F98")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004FF6 RID: 20470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600497A")]
		[Address(RVA = "0x1015A4F50", Offset = "0x15A4F50", VA = "0x1015A4F50")]
		private void Apply()
		{
		}

		// Token: 0x06004FF7 RID: 20471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600497B")]
		[Address(RVA = "0x1015A4FA4", Offset = "0x15A4FA4", VA = "0x1015A4FA4")]
		public TownObjStateWindow()
		{
		}

		// Token: 0x0400629D RID: 25245
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400452F")]
		private long m_ObjMngID;

		// Token: 0x0400629E RID: 25246
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004530")]
		[SerializeField]
		private TownObjInfo m_ObjInfo;
	}
}
