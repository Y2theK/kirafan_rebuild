﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02001035 RID: 4149
	[Token(Token = "0x2000AC7")]
	[StructLayout(3)]
	public class TownObjectCommon : MonoBehaviour
	{
		// Token: 0x06004F95 RID: 20373 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600491A")]
		[Address(RVA = "0x1015A50A8", Offset = "0x15A50A8", VA = "0x1015A50A8")]
		public static TownObjectCommon CreateCommon(GameObject pobj)
		{
			return null;
		}

		// Token: 0x06004F96 RID: 20374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600491B")]
		[Address(RVA = "0x1015A516C", Offset = "0x15A516C", VA = "0x1015A516C")]
		public void SetUpView(int fobjid)
		{
		}

		// Token: 0x06004F97 RID: 20375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600491C")]
		[Address(RVA = "0x1015A55D4", Offset = "0x15A55D4", VA = "0x1015A55D4")]
		public TownObjectCommon()
		{
		}
	}
}
