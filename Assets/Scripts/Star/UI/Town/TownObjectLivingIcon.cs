﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001047 RID: 4167
	[Token(Token = "0x2000AD3")]
	[StructLayout(3)]
	public class TownObjectLivingIcon : MonoBehaviour
	{
		// Token: 0x06004FD2 RID: 20434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004956")]
		[Address(RVA = "0x1015A55DC", Offset = "0x15A55DC", VA = "0x1015A55DC")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06004FD3 RID: 20435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004957")]
		[Address(RVA = "0x1015A5618", Offset = "0x15A5618", VA = "0x1015A5618")]
		public void SetIcon(Sprite sprite)
		{
		}

		// Token: 0x06004FD4 RID: 20436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004958")]
		[Address(RVA = "0x1015A5650", Offset = "0x15A5650", VA = "0x1015A5650")]
		public TownObjectLivingIcon()
		{
		}

		// Token: 0x04006270 RID: 25200
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004511")]
		[SerializeField]
		private Image m_IconImage;
	}
}
