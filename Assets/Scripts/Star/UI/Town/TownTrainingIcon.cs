﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200104E RID: 4174
	[Token(Token = "0x2000AD8")]
	[StructLayout(3)]
	public class TownTrainingIcon : MonoBehaviour
	{
		// Token: 0x06004FFA RID: 20474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600497E")]
		[Address(RVA = "0x1015A5658", Offset = "0x15A5658", VA = "0x1015A5658")]
		private void Start()
		{
		}

		// Token: 0x06004FFB RID: 20475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600497F")]
		[Address(RVA = "0x1015A5704", Offset = "0x15A5704", VA = "0x1015A5704")]
		public void OnClickIconCallBack()
		{
		}

		// Token: 0x06004FFC RID: 20476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004980")]
		[Address(RVA = "0x1015A5734", Offset = "0x15A5734", VA = "0x1015A5734")]
		public TownTrainingIcon()
		{
		}

		// Token: 0x0400629F RID: 25247
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004531")]
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040062A0 RID: 25248
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004532")]
		private TownMain m_TownMain;

		// Token: 0x040062A1 RID: 25249
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004533")]
		private long m_CharaMngID;
	}
}
