﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200104F RID: 4175
	[Token(Token = "0x2000AD9")]
	[StructLayout(3)]
	public class TownTweet : UIGroup
	{
		// Token: 0x06004FFD RID: 20477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004981")]
		[Address(RVA = "0x1015A573C", Offset = "0x15A573C", VA = "0x1015A573C")]
		public void Setup()
		{
		}

		// Token: 0x06004FFE RID: 20478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004982")]
		[Address(RVA = "0x1015A57D0", Offset = "0x15A57D0", VA = "0x1015A57D0")]
		public void SetStartPosition(Vector2 pos, float size)
		{
		}

		// Token: 0x06004FFF RID: 20479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004983")]
		[Address(RVA = "0x1015A591C", Offset = "0x15A591C", VA = "0x1015A591C", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06005000 RID: 20480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004984")]
		[Address(RVA = "0x1015A5960", Offset = "0x15A5960", VA = "0x1015A5960")]
		private void UpdatePrepare()
		{
		}

		// Token: 0x06005001 RID: 20481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004985")]
		[Address(RVA = "0x1015A5E60", Offset = "0x15A5E60", VA = "0x1015A5E60")]
		public void Open(int charaID, int tweetID)
		{
		}

		// Token: 0x06005002 RID: 20482 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004986")]
		[Address(RVA = "0x1015A5F88", Offset = "0x15A5F88", VA = "0x1015A5F88")]
		private string CalcTweetText(int charaID, int tweetID)
		{
			return null;
		}

		// Token: 0x06005003 RID: 20483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004987")]
		[Address(RVA = "0x1015A5978", Offset = "0x15A5978", VA = "0x1015A5978")]
		private void OpenLoaded()
		{
		}

		// Token: 0x06005004 RID: 20484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004988")]
		[Address(RVA = "0x1015A6048", Offset = "0x15A6048", VA = "0x1015A6048")]
		public void OnCompleteInMove()
		{
		}

		// Token: 0x06005005 RID: 20485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004989")]
		[Address(RVA = "0x1015A6080", Offset = "0x15A6080", VA = "0x1015A6080", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06005006 RID: 20486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600498A")]
		[Address(RVA = "0x1015A65EC", Offset = "0x15A65EC", VA = "0x1015A65EC", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06005007 RID: 20487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600498B")]
		[Address(RVA = "0x1015A6630", Offset = "0x15A6630", VA = "0x1015A6630")]
		public void Destroy()
		{
		}

		// Token: 0x06005008 RID: 20488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600498C")]
		[Address(RVA = "0x1015A6668", Offset = "0x15A6668", VA = "0x1015A6668")]
		public void OnCompleteOutMove()
		{
		}

		// Token: 0x06005009 RID: 20489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600498D")]
		[Address(RVA = "0x1015A66C4", Offset = "0x15A66C4", VA = "0x1015A66C4")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x0600500A RID: 20490 RVA: 0x0001BD08 File Offset: 0x00019F08
		[Token(Token = "0x600498E")]
		[Address(RVA = "0x1015A66D0", Offset = "0x15A66D0", VA = "0x1015A66D0", Slot = "17")]
		protected override bool CheckOutEnd()
		{
			return default(bool);
		}

		// Token: 0x0600500B RID: 20491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600498F")]
		[Address(RVA = "0x1015A6704", Offset = "0x15A6704", VA = "0x1015A6704")]
		public TownTweet()
		{
		}

		// Token: 0x040062A2 RID: 25250
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004534")]
		[SerializeField]
		private GameObject m_IconGameObject;

		// Token: 0x040062A3 RID: 25251
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004535")]
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x040062A4 RID: 25252
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004536")]
		[SerializeField]
		private Text m_TweetTextObj;

		// Token: 0x040062A5 RID: 25253
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004537")]
		[SerializeField]
		private RectTransform m_IconPlace;

		// Token: 0x040062A6 RID: 25254
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004538")]
		[SerializeField]
		private float m_MoveTimeSec;

		// Token: 0x040062A7 RID: 25255
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004539")]
		[SerializeField]
		private GameObject m_Button;

		// Token: 0x040062A8 RID: 25256
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400453A")]
		private bool m_IsDonePrepare;

		// Token: 0x040062A9 RID: 25257
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400453B")]
		private TweetListDB m_TweetListDB;

		// Token: 0x040062AA RID: 25258
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400453C")]
		private bool m_IsMoving;

		// Token: 0x040062AB RID: 25259
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x400453D")]
		private Vector2 m_StartPosition;

		// Token: 0x040062AC RID: 25260
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x400453E")]
		private Vector2 m_StartScale;

		// Token: 0x040062AD RID: 25261
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x400453F")]
		private int m_CharaID;

		// Token: 0x040062AE RID: 25262
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004540")]
		private int m_TweetID;

		// Token: 0x040062AF RID: 25263
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004541")]
		private RectTransform m_IconRectTransform;

		// Token: 0x040062B0 RID: 25264
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004542")]
		private GameObject m_GameObject;
	}
}
