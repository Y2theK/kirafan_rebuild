﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001050 RID: 4176
	[Token(Token = "0x2000ADA")]
	[StructLayout(3)]
	public class TownTweetSpecial : UIGroup
	{
		// Token: 0x0600500C RID: 20492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004990")]
		[Address(RVA = "0x1015A670C", Offset = "0x15A670C", VA = "0x1015A670C")]
		public void Setup()
		{
		}

		// Token: 0x0600500D RID: 20493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004991")]
		[Address(RVA = "0x1015A6774", Offset = "0x15A6774", VA = "0x1015A6774", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600500E RID: 20494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004992")]
		[Address(RVA = "0x1015A6888", Offset = "0x15A6888", VA = "0x1015A6888")]
		private void UpdatePrepare()
		{
		}

		// Token: 0x0600500F RID: 20495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004993")]
		[Address(RVA = "0x1015A68FC", Offset = "0x15A68FC", VA = "0x1015A68FC")]
		public void Open(int charaID, int tweetID)
		{
		}

		// Token: 0x06005010 RID: 20496 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004994")]
		[Address(RVA = "0x1015A6A2C", Offset = "0x15A6A2C", VA = "0x1015A6A2C")]
		private string CalcTweetText(int charaID, int tweetID)
		{
			return null;
		}

		// Token: 0x06005011 RID: 20497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004995")]
		[Address(RVA = "0x1015A6894", Offset = "0x15A6894", VA = "0x1015A6894")]
		private void OpenLoaded()
		{
		}

		// Token: 0x06005012 RID: 20498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004996")]
		[Address(RVA = "0x1015A6AE0", Offset = "0x15A6AE0", VA = "0x1015A6AE0", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06005013 RID: 20499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004997")]
		[Address(RVA = "0x1015A6AD8", Offset = "0x15A6AD8", VA = "0x1015A6AD8")]
		public void Destroy()
		{
		}

		// Token: 0x06005014 RID: 20500 RVA: 0x0001BD20 File Offset: 0x00019F20
		[Token(Token = "0x6004998")]
		[Address(RVA = "0x1015A6B58", Offset = "0x15A6B58", VA = "0x1015A6B58", Slot = "17")]
		protected override bool CheckOutEnd()
		{
			return default(bool);
		}

		// Token: 0x06005015 RID: 20501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004999")]
		[Address(RVA = "0x1015A6BEC", Offset = "0x15A6BEC", VA = "0x1015A6BEC")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06005016 RID: 20502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600499A")]
		[Address(RVA = "0x1015A6BF8", Offset = "0x15A6BF8", VA = "0x1015A6BF8", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06005017 RID: 20503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600499B")]
		[Address(RVA = "0x1015A6C3C", Offset = "0x15A6C3C", VA = "0x1015A6C3C")]
		public TownTweetSpecial()
		{
		}

		// Token: 0x040062B1 RID: 25265
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004543")]
		[SerializeField]
		private Image m_BackGround;

		// Token: 0x040062B2 RID: 25266
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004544")]
		[SerializeField]
		private AnimUIPlayer m_BGAnim;

		// Token: 0x040062B3 RID: 25267
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004545")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x040062B4 RID: 25268
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004546")]
		[SerializeField]
		private AnimUIPlayer m_TweetWindowAnim;

		// Token: 0x040062B5 RID: 25269
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004547")]
		private AnimUIPlayer m_BustAnim;

		// Token: 0x040062B6 RID: 25270
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004548")]
		[SerializeField]
		private Text m_TweetTextObj;

		// Token: 0x040062B7 RID: 25271
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004549")]
		private bool m_IsDonePrepare;

		// Token: 0x040062B8 RID: 25272
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400454A")]
		private TweetListDB m_TweetListDB;

		// Token: 0x040062B9 RID: 25273
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400454B")]
		private int m_CharaID;

		// Token: 0x040062BA RID: 25274
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x400454C")]
		private int m_TweetID;

		// Token: 0x040062BB RID: 25275
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400454D")]
		private RectTransform m_IconRectTransform;
	}
}
