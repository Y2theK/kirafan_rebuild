﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using Star.UI.Schedule;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001051 RID: 4177
	[Token(Token = "0x2000ADB")]
	[StructLayout(3)]
	public class TownUI : MenuUIBase
	{
		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x06005018 RID: 20504 RVA: 0x0001BD38 File Offset: 0x00019F38
		[Token(Token = "0x1700050F")]
		public bool IsMain
		{
			[Token(Token = "0x600499C")]
			[Address(RVA = "0x1015A6C44", Offset = "0x15A6C44", VA = "0x1015A6C44")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06005019 RID: 20505 RVA: 0x0001BD50 File Offset: 0x00019F50
		[Token(Token = "0x17000510")]
		public bool IsSelect
		{
			[Token(Token = "0x600499D")]
			[Address(RVA = "0x1015A6C80", Offset = "0x15A6C80", VA = "0x1015A6C80")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x0600501A RID: 20506 RVA: 0x0001BD68 File Offset: 0x00019F68
		[Token(Token = "0x17000511")]
		public bool IsActive
		{
			[Token(Token = "0x600499E")]
			[Address(RVA = "0x1015A6CBC", Offset = "0x15A6CBC", VA = "0x1015A6CBC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x14000107 RID: 263
		// (add) Token: 0x0600501B RID: 20507 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600501C RID: 20508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000107")]
		public event Action<long[]> OnDecideLiveCharas
		{
			[Token(Token = "0x600499F")]
			[Address(RVA = "0x1015A6CC4", Offset = "0x15A6CC4", VA = "0x1015A6CC4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049A0")]
			[Address(RVA = "0x1015A6DD4", Offset = "0x15A6DD4", VA = "0x1015A6DD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000108 RID: 264
		// (add) Token: 0x0600501D RID: 20509 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600501E RID: 20510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000108")]
		public event Action OnExecuteBuild
		{
			[Token(Token = "0x60049A1")]
			[Address(RVA = "0x1015A6EE4", Offset = "0x15A6EE4", VA = "0x1015A6EE4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049A2")]
			[Address(RVA = "0x1015A6FF4", Offset = "0x15A6FF4", VA = "0x1015A6FF4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000109 RID: 265
		// (add) Token: 0x0600501F RID: 20511 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005020 RID: 20512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000109")]
		public event Action OnExecuteLevelUp
		{
			[Token(Token = "0x60049A3")]
			[Address(RVA = "0x1015A7104", Offset = "0x15A7104", VA = "0x1015A7104")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049A4")]
			[Address(RVA = "0x1015A7214", Offset = "0x15A7214", VA = "0x1015A7214")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010A RID: 266
		// (add) Token: 0x06005021 RID: 20513 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005022 RID: 20514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010A")]
		public event Action OnExecuteQuick
		{
			[Token(Token = "0x60049A5")]
			[Address(RVA = "0x1015A7324", Offset = "0x15A7324", VA = "0x1015A7324")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049A6")]
			[Address(RVA = "0x1015A7434", Offset = "0x15A7434", VA = "0x1015A7434")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010B RID: 267
		// (add) Token: 0x06005023 RID: 20515 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005024 RID: 20516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010B")]
		public event Action OnExecuteSell
		{
			[Token(Token = "0x60049A7")]
			[Address(RVA = "0x1015A7544", Offset = "0x15A7544", VA = "0x1015A7544")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049A8")]
			[Address(RVA = "0x1015A7654", Offset = "0x15A7654", VA = "0x1015A7654")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010C RID: 268
		// (add) Token: 0x06005025 RID: 20517 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005026 RID: 20518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010C")]
		public event Action OnExecuteStore
		{
			[Token(Token = "0x60049A9")]
			[Address(RVA = "0x1015A7764", Offset = "0x15A7764", VA = "0x1015A7764")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049AA")]
			[Address(RVA = "0x1015A7874", Offset = "0x15A7874", VA = "0x1015A7874")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010D RID: 269
		// (add) Token: 0x06005027 RID: 20519 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005028 RID: 20520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010D")]
		public event Action OnClickBuildPointCancelButton
		{
			[Token(Token = "0x60049AB")]
			[Address(RVA = "0x1015A7984", Offset = "0x15A7984", VA = "0x1015A7984")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049AC")]
			[Address(RVA = "0x1015A7A94", Offset = "0x15A7A94", VA = "0x1015A7A94")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010E RID: 270
		// (add) Token: 0x06005029 RID: 20521 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600502A RID: 20522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010E")]
		public event Action OnClickMovePointCancelButton
		{
			[Token(Token = "0x60049AD")]
			[Address(RVA = "0x1015A7BA4", Offset = "0x15A7BA4", VA = "0x1015A7BA4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049AE")]
			[Address(RVA = "0x1015A7CB4", Offset = "0x15A7CB4", VA = "0x1015A7CB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400010F RID: 271
		// (add) Token: 0x0600502B RID: 20523 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600502C RID: 20524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400010F")]
		public event Action OnClosedScheduleWindow
		{
			[Token(Token = "0x60049AF")]
			[Address(RVA = "0x1015A7DC4", Offset = "0x15A7DC4", VA = "0x1015A7DC4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049B0")]
			[Address(RVA = "0x1015A7ED4", Offset = "0x15A7ED4", VA = "0x1015A7ED4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000110 RID: 272
		// (add) Token: 0x0600502D RID: 20525 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600502E RID: 20526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000110")]
		public event Action OnClosedTweetWindow
		{
			[Token(Token = "0x60049B1")]
			[Address(RVA = "0x1015A7FE4", Offset = "0x15A7FE4", VA = "0x1015A7FE4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049B2")]
			[Address(RVA = "0x1015A80F4", Offset = "0x15A80F4", VA = "0x1015A80F4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000111 RID: 273
		// (add) Token: 0x0600502F RID: 20527 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005030 RID: 20528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000111")]
		public event Action OnEnd
		{
			[Token(Token = "0x60049B3")]
			[Address(RVA = "0x1015A8204", Offset = "0x15A8204", VA = "0x1015A8204")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60049B4")]
			[Address(RVA = "0x1015A8314", Offset = "0x15A8314", VA = "0x1015A8314")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06005031 RID: 20529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049B5")]
		[Address(RVA = "0x1015A8424", Offset = "0x15A8424", VA = "0x1015A8424")]
		private void Start()
		{
		}

		// Token: 0x06005032 RID: 20530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049B6")]
		[Address(RVA = "0x1015A8428", Offset = "0x15A8428", VA = "0x1015A8428")]
		public void Setup(TownMain owner)
		{
		}

		// Token: 0x06005033 RID: 20531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049B7")]
		[Address(RVA = "0x1015A8AC8", Offset = "0x15A8AC8", VA = "0x1015A8AC8")]
		private void Update()
		{
		}

		// Token: 0x06005034 RID: 20532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049B8")]
		[Address(RVA = "0x1015A98FC", Offset = "0x15A98FC", VA = "0x1015A98FC")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x06005035 RID: 20533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049B9")]
		[Address(RVA = "0x1015A990C", Offset = "0x15A990C", VA = "0x1015A990C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06005036 RID: 20534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049BA")]
		[Address(RVA = "0x1015A9AC0", Offset = "0x15A9AC0", VA = "0x1015A9AC0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06005037 RID: 20535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049BB")]
		[Address(RVA = "0x1015A9CE8", Offset = "0x15A9CE8", VA = "0x1015A9CE8", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06005038 RID: 20536 RVA: 0x0001BD80 File Offset: 0x00019F80
		[Token(Token = "0x60049BC")]
		[Address(RVA = "0x1015A9D24", Offset = "0x15A9D24", VA = "0x1015A9D24", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06005039 RID: 20537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049BD")]
		[Address(RVA = "0x1015A9D60", Offset = "0x15A9D60", VA = "0x1015A9D60")]
		public void SetMainGroupButtonInteractable(bool flg)
		{
		}

		// Token: 0x0600503A RID: 20538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049BE")]
		[Address(RVA = "0x1015A8A78", Offset = "0x15A8A78", VA = "0x1015A8A78")]
		public void SetEnableObjInput(bool flg)
		{
		}

		// Token: 0x0600503B RID: 20539 RVA: 0x0001BD98 File Offset: 0x00019F98
		[Token(Token = "0x60049BF")]
		[Address(RVA = "0x1015A9E98", Offset = "0x15A9E98", VA = "0x1015A9E98")]
		public bool CheckRaycast()
		{
			return default(bool);
		}

		// Token: 0x0600503C RID: 20540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C0")]
		[Address(RVA = "0x1015A9EC8", Offset = "0x15A9EC8", VA = "0x1015A9EC8")]
		public void SetDarkBG(bool flg)
		{
		}

		// Token: 0x0600503D RID: 20541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C1")]
		[Address(RVA = "0x1015A9158", Offset = "0x15A9158", VA = "0x1015A9158")]
		public void UpdateKRRPoint()
		{
		}

		// Token: 0x0600503E RID: 20542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C2")]
		[Address(RVA = "0x1015A9434", Offset = "0x15A9434", VA = "0x1015A9434")]
		public void OpenMainGroup()
		{
		}

		// Token: 0x0600503F RID: 20543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C3")]
		[Address(RVA = "0x1015A9ED4", Offset = "0x15A9ED4", VA = "0x1015A9ED4")]
		public void CloseMainGroup()
		{
		}

		// Token: 0x06005040 RID: 20544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C4")]
		[Address(RVA = "0x1015A9F18", Offset = "0x15A9F18", VA = "0x1015A9F18")]
		public void OnClickBuildLvInfoButtonCallBack()
		{
		}

		// Token: 0x06005041 RID: 20545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C5")]
		[Address(RVA = "0x1015AA050", Offset = "0x15AA050", VA = "0x1015AA050")]
		public void OpenBuildTopWindow()
		{
		}

		// Token: 0x06005042 RID: 20546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C6")]
		[Address(RVA = "0x1015AA184", Offset = "0x15AA184", VA = "0x1015AA184")]
		public void OnCloseBuildTopWindowCallBack()
		{
		}

		// Token: 0x06005043 RID: 20547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C7")]
		[Address(RVA = "0x1015AA28C", Offset = "0x15AA28C", VA = "0x1015AA28C")]
		public void OpenSelectWindow()
		{
		}

		// Token: 0x06005044 RID: 20548 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60049C8")]
		[Address(RVA = "0x1015AA46C", Offset = "0x15AA46C", VA = "0x1015AA46C")]
		public TownObjSelectWindow GetSelectWindow()
		{
			return null;
		}

		// Token: 0x06005045 RID: 20549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049C9")]
		[Address(RVA = "0x1015AA474", Offset = "0x15AA474", VA = "0x1015AA474")]
		public void CloseSelectWindow()
		{
		}

		// Token: 0x06005046 RID: 20550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CA")]
		[Address(RVA = "0x1015AA4F0", Offset = "0x15AA4F0", VA = "0x1015AA4F0")]
		public void OnCompleteObjSelectWindow()
		{
		}

		// Token: 0x06005047 RID: 20551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CB")]
		[Address(RVA = "0x1015A95A4", Offset = "0x15A95A4", VA = "0x1015A95A4")]
		public void OpenInfoWindow(long mngID)
		{
		}

		// Token: 0x06005048 RID: 20552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CC")]
		[Address(RVA = "0x1015AA4F4", Offset = "0x15AA4F4", VA = "0x1015AA4F4")]
		public void OpenBuildWindow(int objID)
		{
		}

		// Token: 0x06005049 RID: 20553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CD")]
		[Address(RVA = "0x1015AA5F8", Offset = "0x15AA5F8", VA = "0x1015AA5F8")]
		public void OpenPlacementWindow(long mngID)
		{
		}

		// Token: 0x0600504A RID: 20554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CE")]
		[Address(RVA = "0x1015AA690", Offset = "0x15AA690", VA = "0x1015AA690")]
		public void OpenSwapAreaWindow(int objID)
		{
		}

		// Token: 0x0600504B RID: 20555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049CF")]
		[Address(RVA = "0x1015A961C", Offset = "0x15A961C", VA = "0x1015A961C")]
		public void OpenLevelUpWindow(long mngID)
		{
		}

		// Token: 0x0600504C RID: 20556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D0")]
		[Address(RVA = "0x1015A96B4", Offset = "0x15A96B4", VA = "0x1015A96B4")]
		public void OpenQuick(long mngID)
		{
		}

		// Token: 0x0600504D RID: 20557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D1")]
		[Address(RVA = "0x1015AA728", Offset = "0x15AA728", VA = "0x1015AA728")]
		public void OpenSell(long mngID)
		{
		}

		// Token: 0x0600504E RID: 20558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D2")]
		[Address(RVA = "0x1015AA7C0", Offset = "0x15AA7C0", VA = "0x1015AA7C0")]
		public void OnCloseConfirmWindowCallBack()
		{
		}

		// Token: 0x0600504F RID: 20559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D3")]
		[Address(RVA = "0x1015AAA70", Offset = "0x15AAA70", VA = "0x1015AAA70")]
		private void OnConfirmQuick(int btn)
		{
		}

		// Token: 0x06005050 RID: 20560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D4")]
		[Address(RVA = "0x1015AAB10", Offset = "0x15AAB10", VA = "0x1015AAB10")]
		private void OnCloseObjStateWindow()
		{
		}

		// Token: 0x06005051 RID: 20561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D5")]
		[Address(RVA = "0x1015AAB5C", Offset = "0x15AAB5C", VA = "0x1015AAB5C")]
		public void OpenChangeWindow(long mngID, int targetID)
		{
		}

		// Token: 0x06005052 RID: 20562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D6")]
		[Address(RVA = "0x1015A974C", Offset = "0x15A974C", VA = "0x1015A974C")]
		public void OpenBuildPointGroup()
		{
		}

		// Token: 0x06005053 RID: 20563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D7")]
		[Address(RVA = "0x1015AABA0", Offset = "0x15AABA0", VA = "0x1015AABA0")]
		public void CloseBuildPointGroup()
		{
		}

		// Token: 0x06005054 RID: 20564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D8")]
		[Address(RVA = "0x1015AABD4", Offset = "0x15AABD4", VA = "0x1015AABD4")]
		public void OnClickCancelBuildButtonCallBack()
		{
		}

		// Token: 0x06005055 RID: 20565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049D9")]
		[Address(RVA = "0x1015AAC84", Offset = "0x15AAC84", VA = "0x1015AAC84")]
		public void OpenMovePointGroup()
		{
		}

		// Token: 0x06005056 RID: 20566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DA")]
		[Address(RVA = "0x1015A9804", Offset = "0x15A9804", VA = "0x1015A9804")]
		public void OpenBuildAreaWindow(bool swapMode)
		{
		}

		// Token: 0x06005057 RID: 20567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DB")]
		[Address(RVA = "0x1015AACF8", Offset = "0x15AACF8", VA = "0x1015AACF8")]
		public void OnCloseContentAreaWindowCallBack()
		{
		}

		// Token: 0x06005058 RID: 20568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DC")]
		[Address(RVA = "0x1015AAEB8", Offset = "0x15AAEB8", VA = "0x1015AAEB8")]
		public void OnCloseBuildSelectWindowCallBack()
		{
		}

		// Token: 0x06005059 RID: 20569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DD")]
		[Address(RVA = "0x1015AB20C", Offset = "0x15AB20C", VA = "0x1015AB20C")]
		private void OnConfirmStore(int btn)
		{
		}

		// Token: 0x0600505A RID: 20570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DE")]
		[Address(RVA = "0x1015AB270", Offset = "0x15AB270", VA = "0x1015AB270")]
		public void OnClickTownBufButtonCallBack()
		{
		}

		// Token: 0x0600505B RID: 20571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049DF")]
		[Address(RVA = "0x1015AB308", Offset = "0x15AB308", VA = "0x1015AB308")]
		public void OnCloseTownBufWindowCallBack()
		{
		}

		// Token: 0x0600505C RID: 20572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E0")]
		[Address(RVA = "0x1015AB35C", Offset = "0x15AB35C", VA = "0x1015AB35C")]
		public void OnClickScheduleButtonCallBack()
		{
		}

		// Token: 0x0600505D RID: 20573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E1")]
		[Address(RVA = "0x1015A9528", Offset = "0x15A9528", VA = "0x1015A9528")]
		public void OpenScheduleWindow(int roomIdx, long[] liveCharaMngID)
		{
		}

		// Token: 0x0600505E RID: 20574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E2")]
		[Address(RVA = "0x1015AB42C", Offset = "0x15AB42C", VA = "0x1015AB42C")]
		public void OnDecideLiveMember()
		{
		}

		// Token: 0x0600505F RID: 20575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E3")]
		[Address(RVA = "0x1015AB4A0", Offset = "0x15AB4A0", VA = "0x1015AB4A0")]
		public void DecideScheduleCharaChange()
		{
		}

		// Token: 0x06005060 RID: 20576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E4")]
		[Address(RVA = "0x1015AB4D4", Offset = "0x15AB4D4", VA = "0x1015AB4D4")]
		public void OnCloseScheduleCallBack()
		{
		}

		// Token: 0x06005061 RID: 20577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E5")]
		[Address(RVA = "0x1015AB52C", Offset = "0x15AB52C", VA = "0x1015AB52C")]
		public void OpenCharaTweetSpecial(long charaMngID, int tweetID, List<TownPartsGetItem> popupList)
		{
		}

		// Token: 0x06005062 RID: 20578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E6")]
		[Address(RVA = "0x1015AB664", Offset = "0x15AB664", VA = "0x1015AB664")]
		public void OnCompleteTweet()
		{
		}

		// Token: 0x06005063 RID: 20579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E7")]
		[Address(RVA = "0x1015AB7DC", Offset = "0x15AB7DC", VA = "0x1015AB7DC")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x06005064 RID: 20580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E8")]
		[Address(RVA = "0x1015AA140", Offset = "0x15AA140", VA = "0x1015AA140")]
		public void ShowNpc()
		{
		}

		// Token: 0x06005065 RID: 20581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049E9")]
		[Address(RVA = "0x1015A9ECC", Offset = "0x15A9ECC", VA = "0x1015A9ECC")]
		public void HideNpc()
		{
		}

		// Token: 0x06005066 RID: 20582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049EA")]
		[Address(RVA = "0x1015AB8AC", Offset = "0x15AB8AC", VA = "0x1015AB8AC")]
		public void OnAddBuildComEnd(long manageID)
		{
		}

		// Token: 0x06005067 RID: 20583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60049EB")]
		[Address(RVA = "0x1015AB8B0", Offset = "0x15AB8B0", VA = "0x1015AB8B0")]
		public TownUI()
		{
		}

		// Token: 0x040062BC RID: 25276
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400454E")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x040062BD RID: 25277
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400454F")]
		[SerializeField]
		private Text m_KRRPointValueText;

		// Token: 0x040062BE RID: 25278
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004550")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040062BF RID: 25279
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004551")]
		[SerializeField]
		private TownObjSelectWindow m_SelectWindow;

		// Token: 0x040062C0 RID: 25280
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004552")]
		[SerializeField]
		private UIGroup m_BuildPointGroup;

		// Token: 0x040062C1 RID: 25281
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004553")]
		[SerializeField]
		private CustomButton m_BuildPointCancelButton;

		// Token: 0x040062C2 RID: 25282
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004554")]
		[SerializeField]
		private TownBufWindow m_TownBufWindow;

		// Token: 0x040062C3 RID: 25283
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004555")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E64C", Offset = "0x12E64C")]
		private BuildTopWindow m_BuildTopWindow;

		// Token: 0x040062C4 RID: 25284
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004556")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E698", Offset = "0x12E698")]
		private BuildListWindow m_BuildListWindow;

		// Token: 0x040062C5 RID: 25285
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004557")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E6E4", Offset = "0x12E6E4")]
		private ContentAreaListWindow m_ContentAreaWindow;

		// Token: 0x040062C6 RID: 25286
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004558")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E730", Offset = "0x12E730")]
		private ScheduleWindow m_ScheduleWindow;

		// Token: 0x040062C7 RID: 25287
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004559")]
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x040062C8 RID: 25288
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400455A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E78C", Offset = "0x12E78C")]
		private TownObjChangeWindow m_TownObjChangeWindow;

		// Token: 0x040062C9 RID: 25289
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400455B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E7D8", Offset = "0x12E7D8")]
		private TownObjConfirmWindow m_ConfirmWindow;

		// Token: 0x040062CA RID: 25290
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400455C")]
		[SerializeField]
		private TownObjStateWindow m_StateWindow;

		// Token: 0x040062CB RID: 25291
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400455D")]
		[SerializeField]
		private SpecialTweet m_SpecialTweet;

		// Token: 0x040062CC RID: 25292
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400455E")]
		[SerializeField]
		private NpcIllust m_Npc;

		// Token: 0x040062CD RID: 25293
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400455F")]
		[SerializeField]
		private AnimUIPlayer m_NpcAnim;

		// Token: 0x040062CE RID: 25294
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004560")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040062CF RID: 25295
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004561")]
		[SerializeField]
		private GameObject m_Invisible;

		// Token: 0x040062D0 RID: 25296
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004562")]
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x040062D1 RID: 25297
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004563")]
		[SerializeField]
		private CustomButton m_BuildLvInfoBtn;

		// Token: 0x040062D2 RID: 25298
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004564")]
		private TownMain m_Owner;

		// Token: 0x040062D3 RID: 25299
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004565")]
		private bool m_IsActive;

		// Token: 0x040062D4 RID: 25300
		[Cpp2IlInjected.FieldOffset(Offset = "0xF9")]
		[Token(Token = "0x4004566")]
		private bool m_IsShowNpc;

		// Token: 0x040062D6 RID: 25302
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004568")]
		public Action OnRoomIn;

		// Token: 0x040062E1 RID: 25313
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4004573")]
		private List<TownPartsGetItem> m_HoldPopupList;

		// Token: 0x02001052 RID: 4178
		[Token(Token = "0x2001238")]
		public enum eUIState
		{
			// Token: 0x040062E3 RID: 25315
			[Token(Token = "0x40070A6")]
			Hide,
			// Token: 0x040062E4 RID: 25316
			[Token(Token = "0x40070A7")]
			In,
			// Token: 0x040062E5 RID: 25317
			[Token(Token = "0x40070A8")]
			Out,
			// Token: 0x040062E6 RID: 25318
			[Token(Token = "0x40070A9")]
			End,
			// Token: 0x040062E7 RID: 25319
			[Token(Token = "0x40070AA")]
			Main,
			// Token: 0x040062E8 RID: 25320
			[Token(Token = "0x40070AB")]
			Select,
			// Token: 0x040062E9 RID: 25321
			[Token(Token = "0x40070AC")]
			BuildPoint,
			// Token: 0x040062EA RID: 25322
			[Token(Token = "0x40070AD")]
			MovePoint,
			// Token: 0x040062EB RID: 25323
			[Token(Token = "0x40070AE")]
			Tweet,
			// Token: 0x040062EC RID: 25324
			[Token(Token = "0x40070AF")]
			Num
		}
	}
}
