﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CFE RID: 3326
	[Token(Token = "0x20008E6")]
	[StructLayout(3)]
	public class TownObjectIcon : ASyncImage
	{
		// Token: 0x06003CB9 RID: 15545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003776")]
		[Address(RVA = "0x10159C7EC", Offset = "0x159C7EC", VA = "0x10159C7EC")]
		public void Apply(int townObjectId, int townObjectLv = 1)
		{
		}

		// Token: 0x06003CBA RID: 15546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003777")]
		[Address(RVA = "0x1015AB920", Offset = "0x15AB920", VA = "0x1015AB920", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CBB RID: 15547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003778")]
		[Address(RVA = "0x1015AB94C", Offset = "0x15AB94C", VA = "0x1015AB94C")]
		public TownObjectIcon()
		{
		}

		// Token: 0x04004C04 RID: 19460
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400356C")]
		protected int m_TownObjectId;

		// Token: 0x04004C05 RID: 19461
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400356D")]
		protected int m_TownObjectLv;
	}
}
