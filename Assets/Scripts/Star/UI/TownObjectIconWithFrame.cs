﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CFF RID: 3327
	[Token(Token = "0x20008E7")]
	[StructLayout(3)]
	public class TownObjectIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003CBC RID: 15548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003779")]
		[Address(RVA = "0x1015A5590", Offset = "0x15A5590", VA = "0x1015A5590")]
		public void Apply(int townObjectID, int lv)
		{
		}

		// Token: 0x06003CBD RID: 15549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377A")]
		[Address(RVA = "0x1015AB95C", Offset = "0x15AB95C", VA = "0x1015AB95C")]
		public void Destroy()
		{
		}

		// Token: 0x06003CBE RID: 15550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377B")]
		[Address(RVA = "0x1015AB9FC", Offset = "0x15AB9FC", VA = "0x1015AB9FC")]
		public TownObjectIconWithFrame()
		{
		}

		// Token: 0x04004C06 RID: 19462
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400356E")]
		[SerializeField]
		private TownObjectIcon m_Icon;
	}
}
