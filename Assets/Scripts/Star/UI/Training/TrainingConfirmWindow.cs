﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E4B RID: 3659
	[Token(Token = "0x20009C2")]
	[StructLayout(3)]
	public class TrainingConfirmWindow : UIGroup
	{
		// Token: 0x060043D0 RID: 17360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E46")]
		[Address(RVA = "0x1015ABA04", Offset = "0x15ABA04", VA = "0x1015ABA04")]
		private void Awake()
		{
		}

		// Token: 0x060043D1 RID: 17361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E47")]
		[Address(RVA = "0x1015ABAD8", Offset = "0x15ABAD8", VA = "0x1015ABAD8", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x060043D2 RID: 17362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E48")]
		[Address(RVA = "0x1015ABB04", Offset = "0x15ABB04", VA = "0x1015ABB04", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060043D3 RID: 17363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E49")]
		[Address(RVA = "0x1015ABB8C", Offset = "0x15ABB8C", VA = "0x1015ABB8C")]
		public void SetDataRetire(int index, Action<int> callback)
		{
		}

		// Token: 0x060043D4 RID: 17364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4A")]
		[Address(RVA = "0x1015ABE58", Offset = "0x15ABE58", VA = "0x1015ABE58")]
		public void SetDataReStart(List<TrainingManager.TrainingRequestData> outRequestList, List<TrainingManager.TrainingRequestData> requestList, Action<int, List<TrainingManager.TrainingRequestData>> callback, long gold, long KRRPoint)
		{
		}

		// Token: 0x060043D5 RID: 17365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4B")]
		[Address(RVA = "0x1015AC3F8", Offset = "0x15AC3F8", VA = "0x1015AC3F8")]
		public void SetDataSkip(int index, Action<int> callback)
		{
		}

		// Token: 0x060043D6 RID: 17366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4C")]
		[Address(RVA = "0x1015AC84C", Offset = "0x15AC84C", VA = "0x1015AC84C")]
		public void OnClickYesButton()
		{
		}

		// Token: 0x060043D7 RID: 17367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4D")]
		[Address(RVA = "0x1015AC8F0", Offset = "0x15AC8F0", VA = "0x1015AC8F0")]
		public void OnClickNoButton()
		{
		}

		// Token: 0x060043D8 RID: 17368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4E")]
		[Address(RVA = "0x1015AC994", Offset = "0x15AC994", VA = "0x1015AC994")]
		public TrainingConfirmWindow()
		{
		}

		// Token: 0x04005482 RID: 21634
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B7C")]
		[SerializeField]
		private GameObject m_goSelectedSlotData;

		// Token: 0x04005483 RID: 21635
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003B7D")]
		[SerializeField]
		private GameObject m_goViewAreaRetire;

		// Token: 0x04005484 RID: 21636
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003B7E")]
		[SerializeField]
		private GameObject m_goViewAreaRestart;

		// Token: 0x04005485 RID: 21637
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003B7F")]
		[SerializeField]
		private GameObject m_goViewAreaSkip;

		// Token: 0x04005486 RID: 21638
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003B80")]
		[SerializeField]
		private CustomButton m_btnYes;

		// Token: 0x04005487 RID: 21639
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003B81")]
		[SerializeField]
		private CustomButton m_btnNo;

		// Token: 0x04005488 RID: 21640
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003B82")]
		[SerializeField]
		private Text m_txtTitle;

		// Token: 0x04005489 RID: 21641
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003B83")]
		[SerializeField]
		private Text m_txtPartyName;

		// Token: 0x0400548A RID: 21642
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003B84")]
		[SerializeField]
		private Text m_txtTrainingName;

		// Token: 0x0400548B RID: 21643
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003B85")]
		[SerializeField]
		private Text m_txtEndAt;

		// Token: 0x0400548C RID: 21644
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003B86")]
		[SerializeField]
		private GameObject[] m_goPartyList;

		// Token: 0x0400548D RID: 21645
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003B87")]
		[SerializeField]
		private Text[] m_txtPartyNameList;

		// Token: 0x0400548E RID: 21646
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003B88")]
		[SerializeField]
		private Text[] m_txtTrainingNameList;

		// Token: 0x0400548F RID: 21647
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003B89")]
		[SerializeField]
		private Text m_txtGold;

		// Token: 0x04005490 RID: 21648
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003B8A")]
		[SerializeField]
		private Text m_txtKRRPoint;

		// Token: 0x04005491 RID: 21649
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003B8B")]
		[SerializeField]
		private Text m_txtGemBefore;

		// Token: 0x04005492 RID: 21650
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003B8C")]
		[SerializeField]
		private Text m_txtGemAfter;

		// Token: 0x04005493 RID: 21651
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003B8D")]
		private TrainingConfirmWindow.eWindowType m_windowType;

		// Token: 0x04005494 RID: 21652
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003B8E")]
		private Action<int> m_CBOnClickRetireWindowBtn;

		// Token: 0x04005495 RID: 21653
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003B8F")]
		private Action<int, List<TrainingManager.TrainingRequestData>> m_CBOnClickReStartWindowBtn;

		// Token: 0x04005496 RID: 21654
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003B90")]
		private Action<int> m_CBOnClickSkipWindowBtn;

		// Token: 0x04005497 RID: 21655
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003B91")]
		private TrainingManager.SlotData m_slotData;

		// Token: 0x04005498 RID: 21656
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003B92")]
		private List<TrainingManager.TrainingRequestData> m_RequestTrainingData;

		// Token: 0x02000E4C RID: 3660
		[Token(Token = "0x200114F")]
		private enum eWindowType
		{
			// Token: 0x0400549A RID: 21658
			[Token(Token = "0x4006C50")]
			Type_RetireConfirm,
			// Token: 0x0400549B RID: 21659
			[Token(Token = "0x4006C51")]
			Type_ReStartConfirm,
			// Token: 0x0400549C RID: 21660
			[Token(Token = "0x4006C52")]
			Type_SkipConfirm
		}
	}
}
