﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E4D RID: 3661
	[Token(Token = "0x20009C3")]
	[StructLayout(3)]
	public class TrainingContent : ScrollItemIcon
	{
		// Token: 0x060043D9 RID: 17369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E4F")]
		[Address(RVA = "0x1015AC99C", Offset = "0x15AC99C", VA = "0x1015AC99C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060043DA RID: 17370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E50")]
		[Address(RVA = "0x1015AD01C", Offset = "0x15AD01C", VA = "0x1015AD01C")]
		public void OnClickPanel(ScrollItemIcon icon)
		{
		}

		// Token: 0x060043DB RID: 17371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E51")]
		[Address(RVA = "0x1015AD094", Offset = "0x15AD094", VA = "0x1015AD094")]
		public TrainingContent()
		{
		}

		// Token: 0x0400549D RID: 21661
		[Token(Token = "0x4003B93")]
		private const int TRAINING_CLEAR = 0;

		// Token: 0x0400549E RID: 21662
		[Token(Token = "0x4003B94")]
		private const int TRAINING_NOTCLEAR = 1;

		// Token: 0x0400549F RID: 21663
		[Token(Token = "0x4003B95")]
		private const int TRAINING_COST_GOLD = 0;

		// Token: 0x040054A0 RID: 21664
		[Token(Token = "0x4003B96")]
		private const int TRAINING_COST_KRR = 1;

		// Token: 0x040054A1 RID: 21665
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B97")]
		[SerializeField]
		private Text m_txtTrainingTitle;

		// Token: 0x040054A2 RID: 21666
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003B98")]
		[SerializeField]
		private Text m_txtCost;

		// Token: 0x040054A3 RID: 21667
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003B99")]
		[SerializeField]
		private Image m_imgCostIcon;

		// Token: 0x040054A4 RID: 21668
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003B9A")]
		[SerializeField]
		private Sprite[] m_spCostIcon;

		// Token: 0x040054A5 RID: 21669
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003B9B")]
		[SerializeField]
		private Text m_txtTimer;

		// Token: 0x040054A6 RID: 21670
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003B9C")]
		[SerializeField]
		private Image m_imgCrown;

		// Token: 0x040054A7 RID: 21671
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B9D")]
		[SerializeField]
		private Sprite[] m_spCrown;

		// Token: 0x040054A8 RID: 21672
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003B9E")]
		[SerializeField]
		private Image m_imgMore;

		// Token: 0x040054A9 RID: 21673
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003B9F")]
		[SerializeField]
		protected PrefabCloner[] m_dropItemIconCloner;

		// Token: 0x040054AA RID: 21674
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003BA0")]
		private TrainingContentData m_TrainingData;

		// Token: 0x040054AB RID: 21675
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003BA1")]
		private int m_TrainingID;
	}
}
