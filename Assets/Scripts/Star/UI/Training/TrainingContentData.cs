﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Training
{
	// Token: 0x02000E4E RID: 3662
	[Token(Token = "0x20009C4")]
	[StructLayout(3)]
	public class TrainingContentData : ScrollItemData
	{
		// Token: 0x060043DC RID: 17372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E52")]
		[Address(RVA = "0x1015AD09C", Offset = "0x15AD09C", VA = "0x1015AD09C")]
		public TrainingContentData(TrainingManager.TrainingData data)
		{
		}

		// Token: 0x040054AC RID: 21676
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BA2")]
		public TrainingManager.TrainingData data;

		// Token: 0x040054AD RID: 21677
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BA3")]
		public Action<int> m_CBOnclickPanel;
	}
}
