﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E4F RID: 3663
	[Token(Token = "0x20009C5")]
	[StructLayout(3)]
	public class TrainingContentList : MonoBehaviour
	{
		// Token: 0x060043DD RID: 17373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E53")]
		[Address(RVA = "0x1015AD0C8", Offset = "0x15AD0C8", VA = "0x1015AD0C8")]
		public void SetTab()
		{
		}

		// Token: 0x060043DE RID: 17374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E54")]
		[Address(RVA = "0x1015AD180", Offset = "0x15AD180", VA = "0x1015AD180")]
		public void SetData()
		{
		}

		// Token: 0x060043DF RID: 17375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E55")]
		[Address(RVA = "0x1015AD2DC", Offset = "0x15AD2DC", VA = "0x1015AD2DC")]
		public void Open(int index, Action<int> OnClickCallback)
		{
		}

		// Token: 0x060043E0 RID: 17376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E56")]
		[Address(RVA = "0x1015AD2E8", Offset = "0x15AD2E8", VA = "0x1015AD2E8")]
		private void LateUpdate()
		{
		}

		// Token: 0x060043E1 RID: 17377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E57")]
		[Address(RVA = "0x1015AD458", Offset = "0x15AD458", VA = "0x1015AD458")]
		public void ChangeTab(int type)
		{
		}

		// Token: 0x060043E2 RID: 17378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E58")]
		[Address(RVA = "0x1015AD820", Offset = "0x15AD820", VA = "0x1015AD820")]
		protected void CallbackChangeTab(int type)
		{
		}

		// Token: 0x060043E3 RID: 17379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E59")]
		[Address(RVA = "0x1015AD8D4", Offset = "0x15AD8D4", VA = "0x1015AD8D4")]
		public void ListClear()
		{
		}

		// Token: 0x060043E4 RID: 17380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5A")]
		[Address(RVA = "0x1015AD55C", Offset = "0x15AD55C", VA = "0x1015AD55C")]
		private void SetList(TrainingDefine.eCostType type)
		{
		}

		// Token: 0x060043E5 RID: 17381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5B")]
		[Address(RVA = "0x1015AD908", Offset = "0x15AD908", VA = "0x1015AD908")]
		public void SetScrollValue(float value)
		{
		}

		// Token: 0x060043E6 RID: 17382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5C")]
		[Address(RVA = "0x1015AD948", Offset = "0x15AD948", VA = "0x1015AD948")]
		public void SetScrollPosFromTrainingID(int trainingID)
		{
		}

		// Token: 0x060043E7 RID: 17383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5D")]
		[Address(RVA = "0x1015ADCA0", Offset = "0x15ADCA0", VA = "0x1015ADCA0")]
		public TrainingContentList()
		{
		}

		// Token: 0x040054AE RID: 21678
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BA4")]
		[SerializeField]
		protected GameObject m_TrainingContentPrefab;

		// Token: 0x040054AF RID: 21679
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BA5")]
		[SerializeField]
		protected TabGroup m_TabGroup;

		// Token: 0x040054B0 RID: 21680
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003BA6")]
		[SerializeField]
		protected ScrollViewBase m_ScrollView;

		// Token: 0x040054B1 RID: 21681
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003BA7")]
		[SerializeField]
		protected Transform m_tfContent;

		// Token: 0x040054B2 RID: 21682
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003BA8")]
		[SerializeField]
		protected Text m_txtValueGold;

		// Token: 0x040054B3 RID: 21683
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003BA9")]
		[SerializeField]
		protected Text m_txtValueKRRPoint;

		// Token: 0x040054B4 RID: 21684
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003BAA")]
		protected Action<int> m_CBOnclickPanel;

		// Token: 0x040054B5 RID: 21685
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003BAB")]
		protected int m_SelectedPartyIndex;

		// Token: 0x040054B6 RID: 21686
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003BAC")]
		protected float m_bufScrollValue;
	}
}
