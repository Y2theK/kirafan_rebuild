﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using UnityEngine;

namespace Star.UI.Training
{
	// Token: 0x02000E50 RID: 3664
	[Token(Token = "0x20009C6")]
	[StructLayout(3)]
	public class TrainingEditWindow : UIGroup
	{
		// Token: 0x060043E8 RID: 17384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5E")]
		[Address(RVA = "0x1015ADCA8", Offset = "0x15ADCA8", VA = "0x1015ADCA8")]
		private void Awake()
		{
		}

		// Token: 0x060043E9 RID: 17385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E5F")]
		[Address(RVA = "0x1015ADD7C", Offset = "0x15ADD7C", VA = "0x1015ADD7C")]
		public void SetData(int index, Action<List<long>> callback)
		{
		}

		// Token: 0x060043EA RID: 17386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E60")]
		[Address(RVA = "0x1015AE2E8", Offset = "0x15AE2E8", VA = "0x1015AE2E8")]
		private void OnClickIcon(int iconIndex, long mngID)
		{
		}

		// Token: 0x060043EB RID: 17387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E61")]
		[Address(RVA = "0x1015AE7F4", Offset = "0x15AE7F4", VA = "0x1015AE7F4")]
		private void OnClickChara(long mngID)
		{
		}

		// Token: 0x060043EC RID: 17388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E62")]
		[Address(RVA = "0x1015AE954", Offset = "0x15AE954", VA = "0x1015AE954")]
		private void OnClickRemove()
		{
		}

		// Token: 0x060043ED RID: 17389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E63")]
		[Address(RVA = "0x1015AEA38", Offset = "0x15AEA38", VA = "0x1015AEA38")]
		private void CharaListOnClose()
		{
		}

		// Token: 0x060043EE RID: 17390 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003E64")]
		[Address(RVA = "0x1015AE6F4", Offset = "0x15AE6F4", VA = "0x1015AE6F4")]
		private List<long> GetNowCharaMngIDList()
		{
			return null;
		}

		// Token: 0x060043EF RID: 17391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E65")]
		[Address(RVA = "0x1015AED74", Offset = "0x15AED74", VA = "0x1015AED74")]
		private void OnClickDecide()
		{
		}

		// Token: 0x060043F0 RID: 17392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E66")]
		[Address(RVA = "0x1015AEDFC", Offset = "0x15AEDFC", VA = "0x1015AEDFC")]
		private void OnClickCancel()
		{
		}

		// Token: 0x060043F1 RID: 17393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E67")]
		[Address(RVA = "0x1015AEE08", Offset = "0x15AEE08", VA = "0x1015AEE08")]
		public TrainingEditWindow()
		{
		}

		// Token: 0x040054B7 RID: 21687
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003BAD")]
		[SerializeField]
		private TrainingCharaIcon[] m_icon;

		// Token: 0x040054B8 RID: 21688
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003BAE")]
		[SerializeField]
		private CustomButton m_btnDecide;

		// Token: 0x040054B9 RID: 21689
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003BAF")]
		[SerializeField]
		private CustomButton m_btnCancel;

		// Token: 0x040054BA RID: 21690
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003BB0")]
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x040054BB RID: 21691
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003BB1")]
		private int m_SelectedPartyIndex;

		// Token: 0x040054BC RID: 21692
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4003BB2")]
		private int m_SelectedIconIndex;

		// Token: 0x040054BD RID: 21693
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003BB3")]
		private Action<List<long>> m_CBPartyDecide;
	}
}
