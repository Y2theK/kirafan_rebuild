﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E51 RID: 3665
	[Token(Token = "0x20009C7")]
	[StructLayout(3)]
	public class TrainingPartyInfo : MonoBehaviour
	{
		// Token: 0x060043F2 RID: 17394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E68")]
		[Address(RVA = "0x1015AEE38", Offset = "0x15AEE38", VA = "0x1015AEE38")]
		private void OnDestroy()
		{
		}

		// Token: 0x060043F3 RID: 17395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E69")]
		[Address(RVA = "0x1015AEE44", Offset = "0x15AEE44", VA = "0x1015AEE44")]
		public void SetData(TrainingManager.SlotData data, int index)
		{
		}

		// Token: 0x060043F4 RID: 17396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6A")]
		[Address(RVA = "0x1015AF740", Offset = "0x15AF740", VA = "0x1015AF740")]
		private void Update()
		{
		}

		// Token: 0x060043F5 RID: 17397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6B")]
		[Address(RVA = "0x1015AF7F0", Offset = "0x15AF7F0", VA = "0x1015AF7F0")]
		public void OnClickPanel()
		{
		}

		// Token: 0x060043F6 RID: 17398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6C")]
		[Address(RVA = "0x1015AF8E8", Offset = "0x15AF8E8", VA = "0x1015AF8E8")]
		private void SetSelctinPanel()
		{
		}

		// Token: 0x060043F7 RID: 17399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6D")]
		[Address(RVA = "0x1015AF17C", Offset = "0x15AF17C", VA = "0x1015AF17C")]
		private void SetLockPanel()
		{
		}

		// Token: 0x060043F8 RID: 17400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6E")]
		[Address(RVA = "0x1015AF5CC", Offset = "0x15AF5CC", VA = "0x1015AF5CC")]
		private void SetComplatePanel()
		{
		}

		// Token: 0x060043F9 RID: 17401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E6F")]
		[Address(RVA = "0x1015AF408", Offset = "0x15AF408", VA = "0x1015AF408")]
		public void ResetPanel()
		{
		}

		// Token: 0x060043FA RID: 17402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E70")]
		[Address(RVA = "0x1015AF9C0", Offset = "0x15AF9C0", VA = "0x1015AF9C0")]
		public TrainingPartyInfo()
		{
		}

		// Token: 0x040054BE RID: 21694
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BB4")]
		[SerializeField]
		private GameObject m_SelectionPanel;

		// Token: 0x040054BF RID: 21695
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BB5")]
		[SerializeField]
		private GameObject m_CompletePanel;

		// Token: 0x040054C0 RID: 21696
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003BB6")]
		[SerializeField]
		private GameObject m_LockPanel;

		// Token: 0x040054C1 RID: 21697
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003BB7")]
		[SerializeField]
		private GameObject m_Cover;

		// Token: 0x040054C2 RID: 21698
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003BB8")]
		[SerializeField]
		private GameObject m_CoverSelect;

		// Token: 0x040054C3 RID: 21699
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003BB9")]
		[SerializeField]
		private GameObject m_InfoPanel;

		// Token: 0x040054C4 RID: 21700
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003BBA")]
		[SerializeField]
		private Text m_textConditionMsg;

		// Token: 0x040054C5 RID: 21701
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003BBB")]
		[SerializeField]
		private Image m_imgText;

		// Token: 0x040054C6 RID: 21702
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003BBC")]
		[SerializeField]
		private CustomButton m_button;

		// Token: 0x040054C7 RID: 21703
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003BBD")]
		[SerializeField]
		private Text m_Timer;

		// Token: 0x040054C8 RID: 21704
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003BBE")]
		[SerializeField]
		private Text m_TrainingName;

		// Token: 0x040054C9 RID: 21705
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003BBF")]
		[SerializeField]
		private Sprite[] m_spPartyNo;

		// Token: 0x040054CA RID: 21706
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003BC0")]
		private int m_indexNo;

		// Token: 0x040054CB RID: 21707
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003BC1")]
		private TrainingManager.SlotData m_slotData;

		// Token: 0x040054CC RID: 21708
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003BC2")]
		public Action<int> m_CBSelect;

		// Token: 0x040054CD RID: 21709
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003BC3")]
		public Action<int> m_CBTraining;

		// Token: 0x040054CE RID: 21710
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003BC4")]
		public Action<int> m_CBComplete;
	}
}
