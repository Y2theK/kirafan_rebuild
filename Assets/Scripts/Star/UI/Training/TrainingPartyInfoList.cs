﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Training
{
	// Token: 0x02000E52 RID: 3666
	[Token(Token = "0x20009C8")]
	[StructLayout(3)]
	public class TrainingPartyInfoList : MonoBehaviour
	{
		// Token: 0x060043FB RID: 17403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E71")]
		[Address(RVA = "0x1015AF9C8", Offset = "0x15AF9C8", VA = "0x1015AF9C8")]
		public void Open(Action<int> CBComplete, Action<int> CBTraining, Action<int> CBSelect)
		{
		}

		// Token: 0x060043FC RID: 17404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E72")]
		[Address(RVA = "0x1015AFD40", Offset = "0x15AFD40", VA = "0x1015AFD40")]
		public void ResetSelectedParty()
		{
		}

		// Token: 0x060043FD RID: 17405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E73")]
		[Address(RVA = "0x1015AFC0C", Offset = "0x15AFC0C", VA = "0x1015AFC0C")]
		private void ResetPartyList()
		{
		}

		// Token: 0x060043FE RID: 17406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E74")]
		[Address(RVA = "0x1015AFE64", Offset = "0x15AFE64", VA = "0x1015AFE64")]
		public TrainingPartyInfoList()
		{
		}

		// Token: 0x040054CF RID: 21711
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BC5")]
		[SerializeField]
		protected Transform m_tfContent;

		// Token: 0x040054D0 RID: 21712
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BC6")]
		[SerializeField]
		protected GameObject m_TrainingPartyInfoPrefab;

		// Token: 0x040054D1 RID: 21713
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003BC7")]
		private List<TrainingPartyInfo> m_partyInfoList;
	}
}
