﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E53 RID: 3667
	[Token(Token = "0x20009C9")]
	[StructLayout(3)]
	public class TrainingResultCharaPanel : MonoBehaviour
	{
		// Token: 0x060043FF RID: 17407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E75")]
		[Address(RVA = "0x1015AFED4", Offset = "0x15AFED4", VA = "0x1015AFED4")]
		public void Setup(TrainingResultUI.TrainingResultCharaArg arg, int index, int slotID)
		{
		}

		// Token: 0x06004400 RID: 17408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E76")]
		[Address(RVA = "0x1015B0454", Offset = "0x15B0454", VA = "0x1015B0454")]
		public void SetSimulate(TrainingResultUI.TrainingResultCharaArg arg, int index, int partyIndex, Action<int> panelCallback)
		{
		}

		// Token: 0x06004401 RID: 17409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E77")]
		[Address(RVA = "0x1015B08F0", Offset = "0x15B08F0", VA = "0x1015B08F0")]
		public void SetEmpty(int index, Action<int> panelCallback)
		{
		}

		// Token: 0x06004402 RID: 17410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E78")]
		[Address(RVA = "0x1015B0A18", Offset = "0x15B0A18", VA = "0x1015B0A18")]
		private void OnClickCharaIcon(int index, long mngId)
		{
		}

		// Token: 0x06004403 RID: 17411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E79")]
		[Address(RVA = "0x1015B0A78", Offset = "0x15B0A78", VA = "0x1015B0A78")]
		public void PlayExp()
		{
		}

		// Token: 0x06004404 RID: 17412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E7A")]
		[Address(RVA = "0x1015B0ADC", Offset = "0x15B0ADC", VA = "0x1015B0ADC")]
		public void SkipExp(bool popup = true)
		{
		}

		// Token: 0x06004405 RID: 17413 RVA: 0x00019968 File Offset: 0x00017B68
		[Token(Token = "0x6003E7B")]
		[Address(RVA = "0x1015B0B50", Offset = "0x15B0B50", VA = "0x1015B0B50")]
		public bool IsPlayingExp()
		{
			return default(bool);
		}

		// Token: 0x06004406 RID: 17414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E7C")]
		[Address(RVA = "0x1015B0BB0", Offset = "0x15B0BB0", VA = "0x1015B0BB0")]
		public void PlayFriendship()
		{
		}

		// Token: 0x06004407 RID: 17415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E7D")]
		[Address(RVA = "0x1015B0DC0", Offset = "0x15B0DC0", VA = "0x1015B0DC0")]
		public void SkipFriendship(bool popup = true)
		{
		}

		// Token: 0x06004408 RID: 17416 RVA: 0x00019980 File Offset: 0x00017B80
		[Token(Token = "0x6003E7E")]
		[Address(RVA = "0x1015B0E34", Offset = "0x15B0E34", VA = "0x1015B0E34")]
		public bool IsPlayingFriendship()
		{
			return default(bool);
		}

		// Token: 0x06004409 RID: 17417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E7F")]
		[Address(RVA = "0x1015B0E94", Offset = "0x15B0E94", VA = "0x1015B0E94")]
		public TrainingResultCharaPanel()
		{
		}

		// Token: 0x040054D2 RID: 21714
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BC8")]
		[SerializeField]
		private TrainingCharaIcon m_CharaIcon;

		// Token: 0x040054D3 RID: 21715
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BC9")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040054D4 RID: 21716
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003BCA")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x040054D5 RID: 21717
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003BCB")]
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x040054D6 RID: 21718
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003BCC")]
		[SerializeField]
		private GameObject m_goInfo;

		// Token: 0x040054D7 RID: 21719
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003BCD")]
		private TrainingResultUI.TrainingResultCharaArg m_Arg;

		// Token: 0x040054D8 RID: 21720
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003BCE")]
		private Action<int> m_OnClickPanel;
	}
}
