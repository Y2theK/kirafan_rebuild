﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E54 RID: 3668
	[Token(Token = "0x20009CA")]
	[StructLayout(3)]
	public class TrainingResultUI : UIGroup
	{
		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x0600440A RID: 17418 RVA: 0x00019998 File Offset: 0x00017B98
		[Token(Token = "0x1700049A")]
		public TrainingResultUI.eBtn DecidedBtn
		{
			[Token(Token = "0x6003E80")]
			[Address(RVA = "0x1015B0E9C", Offset = "0x15B0E9C", VA = "0x1015B0E9C")]
			get
			{
				return TrainingResultUI.eBtn.Ready_Cancel;
			}
		}

		// Token: 0x0600440B RID: 17419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E81")]
		[Address(RVA = "0x1015B0EA4", Offset = "0x15B0EA4", VA = "0x1015B0EA4", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x0600440C RID: 17420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E82")]
		[Address(RVA = "0x1015B10BC", Offset = "0x15B10BC", VA = "0x1015B10BC")]
		public void Init()
		{
		}

		// Token: 0x0600440D RID: 17421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E83")]
		[Address(RVA = "0x1015B1714", Offset = "0x15B1714", VA = "0x1015B1714")]
		public void ClearResultUIData()
		{
		}

		// Token: 0x0600440E RID: 17422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E84")]
		[Address(RVA = "0x1015B1774", Offset = "0x15B1774", VA = "0x1015B1774")]
		public void StackResultData(int slotID, int trainingID, TrainingResultUI.TrainingResultCharaArg[] charaArgs, TrainingResultUI.TrainingResultItemArg[] itemArgs, long rewardExp, long rewardFriendship, long rewardGold, long rewardKP, long overKP, int firstGem)
		{
		}

		// Token: 0x0600440F RID: 17423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E85")]
		[Address(RVA = "0x1015B1B90", Offset = "0x15B1B90", VA = "0x1015B1B90")]
		private void SetRewardPlayerList()
		{
		}

		// Token: 0x06004410 RID: 17424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E86")]
		[Address(RVA = "0x1015B1E30", Offset = "0x15B1E30", VA = "0x1015B1E30")]
		private void SetResultWindowText(bool flgComplete)
		{
		}

		// Token: 0x06004411 RID: 17425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E87")]
		[Address(RVA = "0x1015B1FE0", Offset = "0x15B1FE0", VA = "0x1015B1FE0")]
		private void SetupResult(TrainingResultUI.TrainingResultUIData data, bool skip)
		{
		}

		// Token: 0x06004412 RID: 17426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E88")]
		[Address(RVA = "0x1015B2874", Offset = "0x15B2874", VA = "0x1015B2874")]
		public void SetupReady(int index, int trainningID)
		{
		}

		// Token: 0x06004413 RID: 17427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E89")]
		[Address(RVA = "0x1015B3588", Offset = "0x15B3588", VA = "0x1015B3588")]
		private void OnClickPanelCharaIcon(int iconIndex)
		{
		}

		// Token: 0x06004414 RID: 17428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8A")]
		[Address(RVA = "0x1015B3878", Offset = "0x15B3878", VA = "0x1015B3878")]
		private void OnClickChara(long mngID)
		{
		}

		// Token: 0x06004415 RID: 17429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8B")]
		[Address(RVA = "0x1015B3BC0", Offset = "0x15B3BC0", VA = "0x1015B3BC0")]
		private void OnClickRemove()
		{
		}

		// Token: 0x06004416 RID: 17430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8C")]
		[Address(RVA = "0x1015B3D70", Offset = "0x15B3D70", VA = "0x1015B3D70")]
		private void OnCharaListClose()
		{
		}

		// Token: 0x06004417 RID: 17431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8D")]
		[Address(RVA = "0x1015B3F90", Offset = "0x15B3F90", VA = "0x1015B3F90", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004418 RID: 17432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8E")]
		[Address(RVA = "0x1015B4298", Offset = "0x15B4298", VA = "0x1015B4298")]
		private void ChangeStep(TrainingResultUI.eStep step)
		{
		}

		// Token: 0x06004419 RID: 17433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E8F")]
		[Address(RVA = "0x1015B4B1C", Offset = "0x15B4B1C", VA = "0x1015B4B1C", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x0600441A RID: 17434 RVA: 0x000199B0 File Offset: 0x00017BB0
		[Token(Token = "0x6003E90")]
		[Address(RVA = "0x1015B53BC", Offset = "0x15B53BC", VA = "0x1015B53BC")]
		public bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600441B RID: 17435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E91")]
		[Address(RVA = "0x1015B53CC", Offset = "0x15B53CC", VA = "0x1015B53CC")]
		public void PlayInResult(bool flgComplete)
		{
		}

		// Token: 0x0600441C RID: 17436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E92")]
		[Address(RVA = "0x1015B5550", Offset = "0x15B5550", VA = "0x1015B5550")]
		public void PlayIn()
		{
		}

		// Token: 0x0600441D RID: 17437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E93")]
		[Address(RVA = "0x1015B55FC", Offset = "0x15B55FC", VA = "0x1015B55FC")]
		public void PlayOut()
		{
		}

		// Token: 0x0600441E RID: 17438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E94")]
		[Address(RVA = "0x1015B56A8", Offset = "0x15B56A8", VA = "0x1015B56A8")]
		private void PlayExp()
		{
		}

		// Token: 0x0600441F RID: 17439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E95")]
		[Address(RVA = "0x1015B57F0", Offset = "0x15B57F0", VA = "0x1015B57F0")]
		private void PlayFriendship()
		{
		}

		// Token: 0x06004420 RID: 17440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E96")]
		[Address(RVA = "0x1015B4398", Offset = "0x15B4398", VA = "0x1015B4398")]
		private void UpdateSeq()
		{
		}

		// Token: 0x06004421 RID: 17441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E97")]
		[Address(RVA = "0x1015B5938", Offset = "0x15B5938", VA = "0x1015B5938")]
		private void OnEndFirstRewardInfo()
		{
		}

		// Token: 0x06004422 RID: 17442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E98")]
		[Address(RVA = "0x1015B59D8", Offset = "0x15B59D8", VA = "0x1015B59D8")]
		public void OnClickPartyRecommend()
		{
		}

		// Token: 0x06004423 RID: 17443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E99")]
		[Address(RVA = "0x1015B5DD4", Offset = "0x15B5DD4", VA = "0x1015B5DD4")]
		private void OnClickCancelButton()
		{
		}

		// Token: 0x06004424 RID: 17444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9A")]
		[Address(RVA = "0x1015B5DDC", Offset = "0x15B5DDC", VA = "0x1015B5DDC")]
		private void OnClickDecideButton()
		{
		}

		// Token: 0x06004425 RID: 17445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9B")]
		[Address(RVA = "0x1015B6510", Offset = "0x15B6510", VA = "0x1015B6510")]
		private void OnClickCloseButton()
		{
		}

		// Token: 0x06004426 RID: 17446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9C")]
		[Address(RVA = "0x1015B651C", Offset = "0x15B651C", VA = "0x1015B651C")]
		public void OnClickReStartButton()
		{
		}

		// Token: 0x06004427 RID: 17447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9D")]
		[Address(RVA = "0x1015B6828", Offset = "0x15B6828", VA = "0x1015B6828")]
		public void OnClickNextButtonCallBack()
		{
		}

		// Token: 0x06004428 RID: 17448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9E")]
		[Address(RVA = "0x1015B691C", Offset = "0x15B691C", VA = "0x1015B691C")]
		private void CallbackChangeTab(int tabIndex)
		{
		}

		// Token: 0x06004429 RID: 17449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E9F")]
		[Address(RVA = "0x1015B69D8", Offset = "0x15B69D8", VA = "0x1015B69D8")]
		public void StopVoice()
		{
		}

		// Token: 0x0600442A RID: 17450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA0")]
		[Address(RVA = "0x1015B6A7C", Offset = "0x15B6A7C", VA = "0x1015B6A7C")]
		public TrainingResultUI()
		{
		}

		// Token: 0x040054D9 RID: 21721
		[Token(Token = "0x4003BCF")]
		private const float DISPLAY_TIME = 1f;

		// Token: 0x040054DA RID: 21722
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003BD0")]
		private TrainingResultUI.eStep m_Step;

		// Token: 0x040054DB RID: 21723
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003BD1")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040054DC RID: 21724
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003BD2")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x040054DD RID: 21725
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003BD3")]
		[SerializeField]
		private GameObject m_CharasObj;

		// Token: 0x040054DE RID: 21726
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003BD4")]
		[SerializeField]
		private ScrollViewBase m_ItemScrollResult;

		// Token: 0x040054DF RID: 21727
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003BD5")]
		[SerializeField]
		private ScrollViewBase m_ItemScrollReady;

		// Token: 0x040054E0 RID: 21728
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003BD6")]
		[SerializeField]
		private Image m_GoldIcon;

		// Token: 0x040054E1 RID: 21729
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003BD7")]
		[SerializeField]
		private Image m_KPIcon;

		// Token: 0x040054E2 RID: 21730
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003BD8")]
		[SerializeField]
		private Text m_CurrencyText;

		// Token: 0x040054E3 RID: 21731
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003BD9")]
		[SerializeField]
		private GameObject m_goBtnAreaReady;

		// Token: 0x040054E4 RID: 21732
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003BDA")]
		[SerializeField]
		private GameObject m_goBtnAreaResult;

		// Token: 0x040054E5 RID: 21733
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003BDB")]
		[SerializeField]
		private CustomButton m_btnClose;

		// Token: 0x040054E6 RID: 21734
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003BDC")]
		[SerializeField]
		private CustomButton m_btnCancel;

		// Token: 0x040054E7 RID: 21735
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003BDD")]
		[SerializeField]
		private CustomButton m_btnReStart;

		// Token: 0x040054E8 RID: 21736
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003BDE")]
		[SerializeField]
		private CustomButton m_btnRecommend;

		// Token: 0x040054E9 RID: 21737
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003BDF")]
		[SerializeField]
		private CustomButton m_btnDecide;

		// Token: 0x040054EA RID: 21738
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003BE0")]
		[SerializeField]
		private TrainingResultCharaPanel[] m_CharaPanels;

		// Token: 0x040054EB RID: 21739
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003BE1")]
		[SerializeField]
		private Text m_TxtBtnReStart;

		// Token: 0x040054EC RID: 21740
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003BE2")]
		[SerializeField]
		private CharaListWindow m_charaListWindow;

		// Token: 0x040054ED RID: 21741
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003BE3")]
		[SerializeField]
		private GameObject m_goSkipPanel;

		// Token: 0x040054EE RID: 21742
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003BE4")]
		private TrainingResultUI.eSequence m_Seq;

		// Token: 0x040054EF RID: 21743
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4003BE5")]
		private TrainingResultUI.eBtn m_DecidedBtn;

		// Token: 0x040054F0 RID: 21744
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003BE6")]
		private eCharaNamedType m_VoiceNamed;

		// Token: 0x040054F1 RID: 21745
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4003BE7")]
		private int m_VoiceReqID;

		// Token: 0x040054F2 RID: 21746
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003BE8")]
		private SoundHandler m_SoundHandler;

		// Token: 0x040054F3 RID: 21747
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4003BE9")]
		private long m_OverKP;

		// Token: 0x040054F4 RID: 21748
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4003BEA")]
		private int m_selectedSlotID;

		// Token: 0x040054F5 RID: 21749
		[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
		[Token(Token = "0x4003BEB")]
		private int m_selectedPartyIndex;

		// Token: 0x040054F6 RID: 21750
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4003BEC")]
		private int m_selectedTrainingID;

		// Token: 0x040054F7 RID: 21751
		[Cpp2IlInjected.FieldOffset(Offset = "0x154")]
		[Token(Token = "0x4003BED")]
		private int m_selectedCharaIndex;

		// Token: 0x040054F8 RID: 21752
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4003BEE")]
		private List<long> m_charaMngIDs;

		// Token: 0x040054F9 RID: 21753
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4003BEF")]
		public Action<List<TrainingManager.TrainingRequestData>> m_OnClickOrderCallback;

		// Token: 0x040054FA RID: 21754
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4003BF0")]
		private Dictionary<int, TrainingResultUI.TrainingResultUIData> m_dicResultUIData;

		// Token: 0x040054FB RID: 21755
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4003BF1")]
		private List<RewardPlayer.TrainingReward> m_FirstRewardList;

		// Token: 0x02000E55 RID: 3669
		[Token(Token = "0x2001150")]
		private enum eStep
		{
			// Token: 0x040054FD RID: 21757
			[Token(Token = "0x4006C54")]
			Hide,
			// Token: 0x040054FE RID: 21758
			[Token(Token = "0x4006C55")]
			InNpc,
			// Token: 0x040054FF RID: 21759
			[Token(Token = "0x4006C56")]
			In,
			// Token: 0x04005500 RID: 21760
			[Token(Token = "0x4006C57")]
			Idle,
			// Token: 0x04005501 RID: 21761
			[Token(Token = "0x4006C58")]
			Out,
			// Token: 0x04005502 RID: 21762
			[Token(Token = "0x4006C59")]
			End
		}

		// Token: 0x02000E56 RID: 3670
		[Token(Token = "0x2001151")]
		private enum eSequence
		{
			// Token: 0x04005504 RID: 21764
			[Token(Token = "0x4006C5B")]
			None,
			// Token: 0x04005505 RID: 21765
			[Token(Token = "0x4006C5C")]
			Exp_Friendship,
			// Token: 0x04005506 RID: 21766
			[Token(Token = "0x4006C5D")]
			Stop,
			// Token: 0x04005507 RID: 21767
			[Token(Token = "0x4006C5E")]
			TapWait,
			// Token: 0x04005508 RID: 21768
			[Token(Token = "0x4006C5F")]
			KPOver,
			// Token: 0x04005509 RID: 21769
			[Token(Token = "0x4006C60")]
			CheckUnlock,
			// Token: 0x0400550A RID: 21770
			[Token(Token = "0x4006C61")]
			UnlockWait,
			// Token: 0x0400550B RID: 21771
			[Token(Token = "0x4006C62")]
			ClearFirst,
			// Token: 0x0400550C RID: 21772
			[Token(Token = "0x4006C63")]
			WaitClearFirst,
			// Token: 0x0400550D RID: 21773
			[Token(Token = "0x4006C64")]
			End
		}

		// Token: 0x02000E57 RID: 3671
		[Token(Token = "0x2001152")]
		public enum eBtn
		{
			// Token: 0x0400550F RID: 21775
			[Token(Token = "0x4006C66")]
			None = -1,
			// Token: 0x04005510 RID: 21776
			[Token(Token = "0x4006C67")]
			Ready_Cancel,
			// Token: 0x04005511 RID: 21777
			[Token(Token = "0x4006C68")]
			Ready_Decide,
			// Token: 0x04005512 RID: 21778
			[Token(Token = "0x4006C69")]
			Result_Close,
			// Token: 0x04005513 RID: 21779
			[Token(Token = "0x4006C6A")]
			Result_ReStart
		}

		// Token: 0x02000E58 RID: 3672
		[Token(Token = "0x2001153")]
		public class TrainingResultCharaArg
		{
			// Token: 0x0600442C RID: 17452 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600623D")]
			[Address(RVA = "0x1015B6B70", Offset = "0x15B6B70", VA = "0x1015B6B70")]
			public TrainingResultCharaArg()
			{
			}

			// Token: 0x0600442D RID: 17453 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600623E")]
			[Address(RVA = "0x1015B6B94", Offset = "0x15B6B94", VA = "0x1015B6B94")]
			public TrainingResultCharaArg(long charaMngID, int beforeLv, long beforeExp, int afterLv, long afterExp, int beforeFriendshipLv, long beforeFriendshipExp, int afterFriendshipLv, long afterFriendshipExp)
			{
			}

			// Token: 0x0600442E RID: 17454 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600623F")]
			[Address(RVA = "0x1015B3214", Offset = "0x15B3214", VA = "0x1015B3214")]
			public TrainingResultCharaArg(long charaMngId, TrainingManager.TrainingData trainingData, int charaIndex)
			{
			}

			// Token: 0x04005514 RID: 21780
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006C6B")]
			public long m_CharaMngID;

			// Token: 0x04005515 RID: 21781
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006C6C")]
			public int m_BeforeLv;

			// Token: 0x04005516 RID: 21782
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006C6D")]
			public long m_BeforeExp;

			// Token: 0x04005517 RID: 21783
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006C6E")]
			public int m_AfterLv;

			// Token: 0x04005518 RID: 21784
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006C6F")]
			public long m_AfterExp;

			// Token: 0x04005519 RID: 21785
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006C70")]
			public int m_BeforeFriendshipLv;

			// Token: 0x0400551A RID: 21786
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006C71")]
			public long m_BeforeFriendshipExp;

			// Token: 0x0400551B RID: 21787
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006C72")]
			public int m_AfterFriendshipLv;

			// Token: 0x0400551C RID: 21788
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006C73")]
			public long m_AfterFriendshipExp;
		}

		// Token: 0x02000E59 RID: 3673
		[Token(Token = "0x2001154")]
		public class TrainingResultItemArg
		{
			// Token: 0x0600442F RID: 17455 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006240")]
			[Address(RVA = "0x1015B6C3C", Offset = "0x15B6C3C", VA = "0x1015B6C3C")]
			private TrainingResultItemArg()
			{
			}

			// Token: 0x06004430 RID: 17456 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006241")]
			[Address(RVA = "0x1015B6C44", Offset = "0x15B6C44", VA = "0x1015B6C44")]
			public TrainingResultItemArg(int itemID, int itemNum)
			{
			}

			// Token: 0x0400551D RID: 21789
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006C74")]
			public int m_ItemID;

			// Token: 0x0400551E RID: 21790
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006C75")]
			public int m_ItemNum;
		}

		// Token: 0x02000E5A RID: 3674
		[Token(Token = "0x2001155")]
		public class TrainingResultUIData
		{
			// Token: 0x06004431 RID: 17457 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006242")]
			[Address(RVA = "0x1015B1B04", Offset = "0x15B1B04", VA = "0x1015B1B04")]
			public TrainingResultUIData(int slotID, int trainingID, TrainingResultUI.TrainingResultCharaArg[] charaArgs, TrainingResultUI.TrainingResultItemArg[] itemArgs, long rewardExp, long rewardFriendship, long rewardGold, long rewardKP, long overKP, int firstGem)
			{
			}

			// Token: 0x06004432 RID: 17458 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006243")]
			[Address(RVA = "0x1015B6718", Offset = "0x15B6718", VA = "0x1015B6718")]
			public long[] GetCharaMngIDs()
			{
				return null;
			}

			// Token: 0x0400551F RID: 21791
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006C76")]
			public int m_slotID;

			// Token: 0x04005520 RID: 21792
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006C77")]
			public int m_trainingID;

			// Token: 0x04005521 RID: 21793
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006C78")]
			public TrainingResultUI.TrainingResultCharaArg[] m_charaArgs;

			// Token: 0x04005522 RID: 21794
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006C79")]
			public TrainingResultUI.TrainingResultItemArg[] m_itemArgs;

			// Token: 0x04005523 RID: 21795
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006C7A")]
			public long m_rewardExp;

			// Token: 0x04005524 RID: 21796
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006C7B")]
			public long m_rewardFriendship;

			// Token: 0x04005525 RID: 21797
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006C7C")]
			public long m_rewardGold;

			// Token: 0x04005526 RID: 21798
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006C7D")]
			public long m_rewardKP;

			// Token: 0x04005527 RID: 21799
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006C7E")]
			public long m_overKP;

			// Token: 0x04005528 RID: 21800
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006C7F")]
			public int m_firstGem;
		}
	}
}
