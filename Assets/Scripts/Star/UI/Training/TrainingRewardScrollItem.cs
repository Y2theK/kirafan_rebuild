﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Training
{
	// Token: 0x02000E5B RID: 3675
	[Token(Token = "0x20009CB")]
	[StructLayout(3)]
	public class TrainingRewardScrollItem : ScrollItemIcon
	{
		// Token: 0x06004433 RID: 17459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA2")]
		[Address(RVA = "0x1015B6C7C", Offset = "0x15B6C7C", VA = "0x1015B6C7C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004434 RID: 17460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA3")]
		[Address(RVA = "0x1015B6DBC", Offset = "0x15B6DBC", VA = "0x1015B6DBC")]
		public TrainingRewardScrollItem()
		{
		}

		// Token: 0x04005529 RID: 21801
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003BF2")]
		[SerializeField]
		private PrefabCloner m_ItemIcon;

		// Token: 0x02000E5C RID: 3676
		[Token(Token = "0x2001156")]
		public class TrainingRewardScrollItemData : ScrollItemData
		{
			// Token: 0x06004435 RID: 17461 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006244")]
			[Address(RVA = "0x1015B6DC4", Offset = "0x15B6DC4", VA = "0x1015B6DC4")]
			private TrainingRewardScrollItemData()
			{
			}

			// Token: 0x06004436 RID: 17462 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006245")]
			[Address(RVA = "0x1015B355C", Offset = "0x15B355C", VA = "0x1015B355C")]
			public TrainingRewardScrollItemData(int itemID)
			{
			}

			// Token: 0x06004437 RID: 17463 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006246")]
			[Address(RVA = "0x1015B6DCC", Offset = "0x15B6DCC", VA = "0x1015B6DCC", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x0400552A RID: 21802
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006C80")]
			public int m_ItemID;
		}
	}
}
