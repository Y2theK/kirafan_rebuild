﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Training
{
	// Token: 0x02000E5D RID: 3677
	[Token(Token = "0x20009CC")]
	[StructLayout(3)]
	public class TrainingSubInfo : MonoBehaviour
	{
		// Token: 0x06004438 RID: 17464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA4")]
		[Address(RVA = "0x1015B6E5C", Offset = "0x15B6E5C", VA = "0x1015B6E5C")]
		private void Awake()
		{
		}

		// Token: 0x06004439 RID: 17465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA5")]
		[Address(RVA = "0x1015B6EEC", Offset = "0x15B6EEC", VA = "0x1015B6EEC")]
		private void Update()
		{
		}

		// Token: 0x0600443A RID: 17466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA6")]
		[Address(RVA = "0x1015B7078", Offset = "0x15B7078", VA = "0x1015B7078")]
		public void SetCompleteAllBtnCallback(Action callback)
		{
		}

		// Token: 0x0600443B RID: 17467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA7")]
		[Address(RVA = "0x1015B7080", Offset = "0x15B7080", VA = "0x1015B7080")]
		public void SetCharaTalkMode()
		{
		}

		// Token: 0x0600443C RID: 17468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA8")]
		[Address(RVA = "0x1015B72FC", Offset = "0x15B72FC", VA = "0x1015B72FC")]
		public void SetPartyInfoMode(int index, Action callbackEdit)
		{
		}

		// Token: 0x0600443D RID: 17469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EA9")]
		[Address(RVA = "0x1015B7A28", Offset = "0x15B7A28", VA = "0x1015B7A28")]
		public void SelectPartyInOrderMode(int index, Action callbackRetire, Action callbackSkip)
		{
		}

		// Token: 0x0600443E RID: 17470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EAA")]
		[Address(RVA = "0x1015B760C", Offset = "0x15B760C", VA = "0x1015B760C")]
		public void SetPartyInfo(int index, bool flgIconInput)
		{
		}

		// Token: 0x0600443F RID: 17471 RVA: 0x000199C8 File Offset: 0x00017BC8
		[Token(Token = "0x6003EAB")]
		[Address(RVA = "0x1015B71D0", Offset = "0x15B71D0", VA = "0x1015B71D0")]
		private eText_MessageDB GetTrainingMessage()
		{
			return eText_MessageDB.Dummy_0;
		}

		// Token: 0x06004440 RID: 17472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EAC")]
		[Address(RVA = "0x1015B7D34", Offset = "0x15B7D34", VA = "0x1015B7D34")]
		private void OnClickRecommendParty()
		{
		}

		// Token: 0x06004441 RID: 17473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EAD")]
		[Address(RVA = "0x1015B7E70", Offset = "0x15B7E70", VA = "0x1015B7E70")]
		private void OnClickPartyEdit()
		{
		}

		// Token: 0x06004442 RID: 17474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EAE")]
		[Address(RVA = "0x1015B7E7C", Offset = "0x15B7E7C", VA = "0x1015B7E7C")]
		private void OnClickRetire()
		{
		}

		// Token: 0x06004443 RID: 17475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EAF")]
		[Address(RVA = "0x1015B7E88", Offset = "0x15B7E88", VA = "0x1015B7E88")]
		private void OnClickTrainingSkip()
		{
		}

		// Token: 0x06004444 RID: 17476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB0")]
		[Address(RVA = "0x1015B7E94", Offset = "0x15B7E94", VA = "0x1015B7E94")]
		private void OnClickCompleteAll()
		{
		}

		// Token: 0x06004445 RID: 17477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB1")]
		[Address(RVA = "0x1015B7EA0", Offset = "0x15B7EA0", VA = "0x1015B7EA0")]
		private void OnClickCharaIcon(int index, long mngID)
		{
		}

		// Token: 0x06004446 RID: 17478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB2")]
		[Address(RVA = "0x1015B7EAC", Offset = "0x15B7EAC", VA = "0x1015B7EAC")]
		public TrainingSubInfo()
		{
		}

		// Token: 0x0400552B RID: 21803
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003BF3")]
		[SerializeField]
		private GameObject m_goPartyList;

		// Token: 0x0400552C RID: 21804
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003BF4")]
		[SerializeField]
		private Text m_TalkText;

		// Token: 0x0400552D RID: 21805
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003BF5")]
		[SerializeField]
		private GameObject[] CharacterInfo;

		// Token: 0x0400552E RID: 21806
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003BF6")]
		[SerializeField]
		private CustomButton m_btnRight;

		// Token: 0x0400552F RID: 21807
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003BF7")]
		[SerializeField]
		private CustomButton m_btnLeft;

		// Token: 0x04005530 RID: 21808
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003BF8")]
		[SerializeField]
		private Text m_txtBtnRight;

		// Token: 0x04005531 RID: 21809
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003BF9")]
		[SerializeField]
		private Text m_txtBtnLeft;

		// Token: 0x04005532 RID: 21810
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003BFA")]
		[SerializeField]
		private CustomButton m_btnCompleteAll;

		// Token: 0x04005533 RID: 21811
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003BFB")]
		[SerializeField]
		private Text m_txtPartyName;

		// Token: 0x04005534 RID: 21812
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003BFC")]
		private int m_SelectedPartyIndex;

		// Token: 0x04005535 RID: 21813
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003BFD")]
		private List<TrainingCharaIcon> CharaIcon;

		// Token: 0x04005536 RID: 21814
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003BFE")]
		private Action m_CBRetire;

		// Token: 0x04005537 RID: 21815
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003BFF")]
		private Action m_CBSkip;

		// Token: 0x04005538 RID: 21816
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003C00")]
		private Action m_CBEdit;

		// Token: 0x04005539 RID: 21817
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C01")]
		private Action m_CBCompleteAll;

		// Token: 0x0400553A RID: 21818
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C02")]
		private TrainingSubInfo.eMode m_mode;

		// Token: 0x02000E5E RID: 3678
		[Token(Token = "0x2001157")]
		private enum eMode
		{
			// Token: 0x0400553C RID: 21820
			[Token(Token = "0x4006C82")]
			Mode_NPCTalk,
			// Token: 0x0400553D RID: 21821
			[Token(Token = "0x4006C83")]
			Mode_PartyInfo,
			// Token: 0x0400553E RID: 21822
			[Token(Token = "0x4006C84")]
			Mode_PartyInfoInOrder
		}
	}
}
