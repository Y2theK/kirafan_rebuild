﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Training
{
	// Token: 0x02000E5F RID: 3679
	[Token(Token = "0x20009CD")]
	[StructLayout(3)]
	public class TrainingUI : MonoBehaviour
	{
		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x06004447 RID: 17479 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700049B")]
		public UIGroup mainUIGroup
		{
			[Token(Token = "0x6003EB3")]
			[Address(RVA = "0x1015AE7EC", Offset = "0x15AE7EC", VA = "0x1015AE7EC")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x06004448 RID: 17480 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700049C")]
		private TrainingManager TrainingMng
		{
			[Token(Token = "0x6003EB4")]
			[Address(RVA = "0x1015B7F24", Offset = "0x15B7F24", VA = "0x1015B7F24")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004449 RID: 17481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB5")]
		[Address(RVA = "0x1015B7F84", Offset = "0x15B7F84", VA = "0x1015B7F84")]
		public void Update()
		{
		}

		// Token: 0x0600444A RID: 17482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB6")]
		[Address(RVA = "0x1015B8564", Offset = "0x15B8564", VA = "0x1015B8564")]
		private void GotoDefault()
		{
		}

		// Token: 0x0600444B RID: 17483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB7")]
		[Address(RVA = "0x1015B8644", Offset = "0x15B8644", VA = "0x1015B8644")]
		private void GotoGlobalShortcut()
		{
		}

		// Token: 0x0600444C RID: 17484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB8")]
		[Address(RVA = "0x1015B8300", Offset = "0x15B8300", VA = "0x1015B8300")]
		private void GotoADV()
		{
		}

		// Token: 0x0600444D RID: 17485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EB9")]
		[Address(RVA = "0x1015B8308", Offset = "0x15B8308", VA = "0x1015B8308")]
		private void GotoCharaQuest()
		{
		}

		// Token: 0x0600444E RID: 17486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EBA")]
		[Address(RVA = "0x1015B864C", Offset = "0x15B864C", VA = "0x1015B864C")]
		private void GotoReOpen()
		{
		}

		// Token: 0x0600444F RID: 17487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EBB")]
		[Address(RVA = "0x1015B856C", Offset = "0x15B856C", VA = "0x1015B856C")]
		private void _GotoMenuEnd(TrainingUI.eTransit transit)
		{
		}

		// Token: 0x06004450 RID: 17488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EBC")]
		[Address(RVA = "0x1015B8654", Offset = "0x15B8654", VA = "0x1015B8654")]
		public void Setup()
		{
		}

		// Token: 0x06004451 RID: 17489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EBD")]
		[Address(RVA = "0x1015B8680", Offset = "0x15B8680", VA = "0x1015B8680")]
		public void Open()
		{
		}

		// Token: 0x06004452 RID: 17490 RVA: 0x000199E0 File Offset: 0x00017BE0
		[Token(Token = "0x6003EBE")]
		[Address(RVA = "0x1015B895C", Offset = "0x15B895C", VA = "0x1015B895C")]
		public bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004453 RID: 17491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EBF")]
		[Address(RVA = "0x1015B896C", Offset = "0x15B896C", VA = "0x1015B896C")]
		public void Release()
		{
		}

		// Token: 0x06004454 RID: 17492 RVA: 0x000199F8 File Offset: 0x00017BF8
		[Token(Token = "0x6003EC0")]
		[Address(RVA = "0x1015B8A14", Offset = "0x15B8A14", VA = "0x1015B8A14")]
		public TrainingUI.eTransit GetTransit()
		{
			return TrainingUI.eTransit.Default;
		}

		// Token: 0x06004455 RID: 17493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC1")]
		[Address(RVA = "0x1015B8A1C", Offset = "0x15B8A1C", VA = "0x1015B8A1C")]
		private void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06004456 RID: 17494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC2")]
		[Address(RVA = "0x1015B8860", Offset = "0x15B8860", VA = "0x1015B8860")]
		private void ReturnToPartyInfo(bool isUnlockStart)
		{
		}

		// Token: 0x06004457 RID: 17495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC3")]
		[Address(RVA = "0x1015B8B34", Offset = "0x15B8B34", VA = "0x1015B8B34")]
		private void ActivatePartyInfo()
		{
		}

		// Token: 0x06004458 RID: 17496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC4")]
		[Address(RVA = "0x1015B8CE0", Offset = "0x15B8CE0", VA = "0x1015B8CE0")]
		private void ActivateContentsList()
		{
		}

		// Token: 0x06004459 RID: 17497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC5")]
		[Address(RVA = "0x1015B9008", Offset = "0x15B9008", VA = "0x1015B9008")]
		private void ActivateTrainingReadyInfo()
		{
		}

		// Token: 0x0600445A RID: 17498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC6")]
		[Address(RVA = "0x1015B90E8", Offset = "0x15B90E8", VA = "0x1015B90E8")]
		private void ActivateTrainingResultInfo()
		{
		}

		// Token: 0x0600445B RID: 17499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC7")]
		[Address(RVA = "0x1015B9624", Offset = "0x15B9624", VA = "0x1015B9624")]
		private void CB_Retire()
		{
		}

		// Token: 0x0600445C RID: 17500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC8")]
		[Address(RVA = "0x1015B96E4", Offset = "0x15B96E4", VA = "0x1015B96E4")]
		private void CB_Skip()
		{
		}

		// Token: 0x0600445D RID: 17501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EC9")]
		[Address(RVA = "0x1015B97A4", Offset = "0x15B97A4", VA = "0x1015B97A4")]
		private void CB_Edit()
		{
		}

		// Token: 0x0600445E RID: 17502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECA")]
		[Address(RVA = "0x1015B9860", Offset = "0x15B9860", VA = "0x1015B9860")]
		private void CB_PartyEditDecide(List<long> partyList)
		{
		}

		// Token: 0x0600445F RID: 17503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECB")]
		[Address(RVA = "0x1015B98E4", Offset = "0x15B98E4", VA = "0x1015B98E4")]
		private void PartySelect(int index)
		{
		}

		// Token: 0x06004460 RID: 17504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECC")]
		[Address(RVA = "0x1015B98EC", Offset = "0x15B98EC", VA = "0x1015B98EC")]
		private void PartySelectInOrder(int index)
		{
		}

		// Token: 0x06004461 RID: 17505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECD")]
		[Address(RVA = "0x1015B99D0", Offset = "0x15B99D0", VA = "0x1015B99D0")]
		private void TrainingSelect(int trainingID)
		{
		}

		// Token: 0x06004462 RID: 17506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECE")]
		[Address(RVA = "0x1015B99D8", Offset = "0x15B99D8", VA = "0x1015B99D8")]
		private void OnClickOrder(List<TrainingManager.TrainingRequestData> requestList)
		{
		}

		// Token: 0x06004463 RID: 17507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ECF")]
		[Address(RVA = "0x1015B9EA8", Offset = "0x15B9EA8", VA = "0x1015B9EA8")]
		private void OnResponseTrainingStart(TrainingDefine.eOrderResult result, string errMsg)
		{
		}

		// Token: 0x06004464 RID: 17508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED0")]
		[Address(RVA = "0x1015BA014", Offset = "0x15BA014", VA = "0x1015BA014")]
		private void OnConfirmOutOfPeriod(int btn)
		{
		}

		// Token: 0x06004465 RID: 17509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED1")]
		[Address(RVA = "0x1015BA01C", Offset = "0x15BA01C", VA = "0x1015BA01C")]
		private void OnClickReStart(List<TrainingManager.TrainingRequestData> requestList)
		{
		}

		// Token: 0x06004466 RID: 17510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED2")]
		[Address(RVA = "0x1015BA384", Offset = "0x15BA384", VA = "0x1015BA384")]
		private void OnConfirmReStartFailed(int answer)
		{
		}

		// Token: 0x06004467 RID: 17511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED3")]
		[Address(RVA = "0x1015BA3B0", Offset = "0x15BA3B0", VA = "0x1015BA3B0")]
		private void OnConfirmReStart(int answer, List<TrainingManager.TrainingRequestData> requestList)
		{
		}

		// Token: 0x06004468 RID: 17512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED4")]
		[Address(RVA = "0x1015BA3E8", Offset = "0x15BA3E8", VA = "0x1015BA3E8")]
		private void OnClickComplete(int index)
		{
		}

		// Token: 0x06004469 RID: 17513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED5")]
		[Address(RVA = "0x1015BA550", Offset = "0x15BA550", VA = "0x1015BA550")]
		private void OnClickCompleteAll()
		{
		}

		// Token: 0x0600446A RID: 17514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED6")]
		[Address(RVA = "0x1015BA7C0", Offset = "0x15BA7C0", VA = "0x1015BA7C0")]
		private void OnResponseComplete(TrainingDefine.eCompleteResult result, string errMsg)
		{
		}

		// Token: 0x0600446B RID: 17515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED7")]
		[Address(RVA = "0x1015BAA60", Offset = "0x15BAA60", VA = "0x1015BAA60")]
		private void OnClosedResultUI()
		{
		}

		// Token: 0x0600446C RID: 17516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED8")]
		[Address(RVA = "0x1015BAAD0", Offset = "0x15BAAD0", VA = "0x1015BAAD0")]
		private void OnConfirmSkip(int answer)
		{
		}

		// Token: 0x0600446D RID: 17517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ED9")]
		[Address(RVA = "0x1015BAD70", Offset = "0x15BAD70", VA = "0x1015BAD70")]
		private void OnConfirmShortOfGem(int btn)
		{
		}

		// Token: 0x0600446E RID: 17518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDA")]
		[Address(RVA = "0x1015BADEC", Offset = "0x15BADEC", VA = "0x1015BADEC")]
		private void OnConfirmRetire(int answer)
		{
		}

		// Token: 0x0600446F RID: 17519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDB")]
		[Address(RVA = "0x1015BAF08", Offset = "0x15BAF08", VA = "0x1015BAF08")]
		private void OnResponseRetire(TrainingDefine.eCancelResult result, string errMsg)
		{
		}

		// Token: 0x06004470 RID: 17520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDC")]
		[Address(RVA = "0x1015BA8F0", Offset = "0x15BA8F0", VA = "0x1015BA8F0")]
		private void ShowAlreadyComplete()
		{
		}

		// Token: 0x06004471 RID: 17521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDD")]
		[Address(RVA = "0x1015BAFE8", Offset = "0x15BAFE8", VA = "0x1015BAFE8")]
		private void OnConfirmShowAlreadyComplete(int answer)
		{
		}

		// Token: 0x06004472 RID: 17522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDE")]
		[Address(RVA = "0x1015B8970", Offset = "0x15B8970", VA = "0x1015B8970")]
		private void StopTrainingVoice()
		{
		}

		// Token: 0x06004473 RID: 17523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EDF")]
		[Address(RVA = "0x1015B8310", Offset = "0x15B8310", VA = "0x1015B8310")]
		private void RequestTrainingVoice_Top()
		{
		}

		// Token: 0x06004474 RID: 17524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EE0")]
		[Address(RVA = "0x1015B83B8", Offset = "0x15B83B8", VA = "0x1015B83B8")]
		private void RequestTrainingVoice_Complete(bool isRareItem)
		{
		}

		// Token: 0x06004475 RID: 17525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EE1")]
		[Address(RVA = "0x1015B9A8C", Offset = "0x15B9A8C", VA = "0x1015B9A8C")]
		private void RequestTrainingVoice_Order(List<TrainingManager.TrainingRequestData> requestList)
		{
		}

		// Token: 0x06004476 RID: 17526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003EE2")]
		[Address(RVA = "0x1015BAFF0", Offset = "0x15BAFF0", VA = "0x1015BAFF0")]
		public TrainingUI()
		{
		}

		// Token: 0x0400553F RID: 21823
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C03")]
		[SerializeField]
		private NpcIllust.eNpc m_NaviNpcCharaId;

		// Token: 0x04005540 RID: 21824
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C04")]
		[SerializeField]
		private NpcIllust m_imgNavi;

		// Token: 0x04005541 RID: 21825
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003C05")]
		[SerializeField]
		private TrainingPartyInfoList m_PartyList;

		// Token: 0x04005542 RID: 21826
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003C06")]
		[SerializeField]
		private TrainingContentList m_ContentList;

		// Token: 0x04005543 RID: 21827
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003C07")]
		[SerializeField]
		private TrainingResultUI m_ResultUI;

		// Token: 0x04005544 RID: 21828
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003C08")]
		[SerializeField]
		private TrainingSubInfo m_SubInfo;

		// Token: 0x04005545 RID: 21829
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003C09")]
		[SerializeField]
		private TrainingConfirmWindow m_Confirm;

		// Token: 0x04005546 RID: 21830
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003C0A")]
		[SerializeField]
		private TrainingEditWindow m_Edit;

		// Token: 0x04005547 RID: 21831
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003C0B")]
		[SerializeField]
		private UIGroup m_mainUIGroup;

		// Token: 0x04005548 RID: 21832
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003C0C")]
		[SerializeField]
		private AnimUIPlayer m_BG;

		// Token: 0x04005549 RID: 21833
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003C0D")]
		private int m_SelectedPartyIndex;

		// Token: 0x0400554A RID: 21834
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4003C0E")]
		private int m_SelectedTrainingID;

		// Token: 0x0400554B RID: 21835
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003C0F")]
		private TrainingUI.eStep m_Step;

		// Token: 0x0400554C RID: 21836
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003C10")]
		private TrainingUI.eLocalState m_LocalState;

		// Token: 0x0400554D RID: 21837
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003C11")]
		private Dictionary<TrainingUI.eLocalState, GlobalUI.SceneInfoStack> m_dicSceneInfoStack;

		// Token: 0x0400554E RID: 21838
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003C12")]
		private TrainingUI.eTransit m_Transit;

		// Token: 0x0400554F RID: 21839
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003C13")]
		private bool m_IsPlayout;

		// Token: 0x04005550 RID: 21840
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C14")]
		private TrainingUI.eRequestRineVoice m_ReserveRineVoiceType;

		// Token: 0x04005551 RID: 21841
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003C15")]
		private TrainingUI.eRequestRineVoice m_RequestedRineVoiceType;

		// Token: 0x04005552 RID: 21842
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C16")]
		private int m_VoiceReqID;

		// Token: 0x04005553 RID: 21843
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003C17")]
		private int m_OrderVoiceReqID;

		// Token: 0x04005554 RID: 21844
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003C18")]
		private bool m_flgCompleteAll;

		// Token: 0x02000E60 RID: 3680
		[Token(Token = "0x2001158")]
		public enum eStep
		{
			// Token: 0x04005556 RID: 21846
			[Token(Token = "0x4006C86")]
			None = -1,
			// Token: 0x04005557 RID: 21847
			[Token(Token = "0x4006C87")]
			Top_In,
			// Token: 0x04005558 RID: 21848
			[Token(Token = "0x4006C88")]
			Unlock,
			// Token: 0x04005559 RID: 21849
			[Token(Token = "0x4006C89")]
			Main,
			// Token: 0x0400555A RID: 21850
			[Token(Token = "0x4006C8A")]
			End
		}

		// Token: 0x02000E61 RID: 3681
		[Token(Token = "0x2001159")]
		private enum eLocalState
		{
			// Token: 0x0400555C RID: 21852
			[Token(Token = "0x4006C8C")]
			None = -1,
			// Token: 0x0400555D RID: 21853
			[Token(Token = "0x4006C8D")]
			TOP,
			// Token: 0x0400555E RID: 21854
			[Token(Token = "0x4006C8E")]
			TrainingList
		}

		// Token: 0x02000E62 RID: 3682
		[Token(Token = "0x200115A")]
		public enum eTransit
		{
			// Token: 0x04005560 RID: 21856
			[Token(Token = "0x4006C90")]
			Default,
			// Token: 0x04005561 RID: 21857
			[Token(Token = "0x4006C91")]
			GlobalShortcut,
			// Token: 0x04005562 RID: 21858
			[Token(Token = "0x4006C92")]
			ADV,
			// Token: 0x04005563 RID: 21859
			[Token(Token = "0x4006C93")]
			CharaQuest,
			// Token: 0x04005564 RID: 21860
			[Token(Token = "0x4006C94")]
			ReOpen
		}

		// Token: 0x02000E63 RID: 3683
		[Token(Token = "0x200115B")]
		public enum eRequestRineVoice
		{
			// Token: 0x04005566 RID: 21862
			[Token(Token = "0x4006C96")]
			None = -1,
			// Token: 0x04005567 RID: 21863
			[Token(Token = "0x4006C97")]
			First,
			// Token: 0x04005568 RID: 21864
			[Token(Token = "0x4006C98")]
			GotItem,
			// Token: 0x04005569 RID: 21865
			[Token(Token = "0x4006C99")]
			GotRareItem
		}
	}
}
