﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E21 RID: 3617
	[Token(Token = "0x20009AF")]
	[StructLayout(3)]
	public class TrainingCharaIcon : MonoBehaviour
	{
		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x060042F7 RID: 17143 RVA: 0x00019620 File Offset: 0x00017820
		[Token(Token = "0x17000492")]
		public long CharaMngID
		{
			[Token(Token = "0x6003D6F")]
			[Address(RVA = "0x1015AED6C", Offset = "0x15AED6C", VA = "0x1015AED6C")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x060042F8 RID: 17144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D70")]
		[Address(RVA = "0x1015AE10C", Offset = "0x15AE10C", VA = "0x1015AE10C")]
		public void SetData(int index, long charaMngID, bool viewInfoIcon = true)
		{
		}

		// Token: 0x060042F9 RID: 17145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D71")]
		[Address(RVA = "0x1015AE1D0", Offset = "0x15AE1D0", VA = "0x1015AE1D0")]
		public void SetBtnCallback(Action<int, long> callback)
		{
		}

		// Token: 0x060042FA RID: 17146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D72")]
		[Address(RVA = "0x1015B0398", Offset = "0x15B0398", VA = "0x1015B0398")]
		public void RemoveBtnCallback()
		{
		}

		// Token: 0x060042FB RID: 17147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D73")]
		[Address(RVA = "0x1015B0360", Offset = "0x15B0360", VA = "0x1015B0360")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x060042FC RID: 17148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D74")]
		[Address(RVA = "0x1015BB078", Offset = "0x15BB078", VA = "0x1015BB078")]
		public void UpdateCharaIcon(long charaMngID)
		{
		}

		// Token: 0x060042FD RID: 17149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D75")]
		[Address(RVA = "0x1015AE120", Offset = "0x15AE120", VA = "0x1015AE120")]
		public void SetTrainningTag(int index)
		{
		}

		// Token: 0x060042FE RID: 17150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D76")]
		[Address(RVA = "0x1015BB440", Offset = "0x15BB440", VA = "0x1015BB440")]
		public void OnClickPanel()
		{
		}

		// Token: 0x060042FF RID: 17151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D77")]
		[Address(RVA = "0x1015BB4A8", Offset = "0x15BB4A8", VA = "0x1015BB4A8")]
		public TrainingCharaIcon()
		{
		}

		// Token: 0x0400534E RID: 21326
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003AEC")]
		[SerializeField]
		private PrefabCloner m_CharaIcon;

		// Token: 0x0400534F RID: 21327
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003AED")]
		[SerializeField]
		private Image m_imgEmptyIcon;

		// Token: 0x04005350 RID: 21328
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003AEE")]
		[SerializeField]
		private Image m_imgTrainingTag;

		// Token: 0x04005351 RID: 21329
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003AEF")]
		[SerializeField]
		private Sprite[] m_TrainingSprites;

		// Token: 0x04005352 RID: 21330
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003AF0")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04005353 RID: 21331
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003AF1")]
		[SerializeField]
		private GameObject m_TrainingCover;

		// Token: 0x04005354 RID: 21332
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003AF2")]
		private int m_Index;

		// Token: 0x04005355 RID: 21333
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003AF3")]
		private long m_CharaMngID;

		// Token: 0x04005356 RID: 21334
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003AF4")]
		private bool m_viewInfoIcon;

		// Token: 0x04005357 RID: 21335
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003AF5")]
		private Action<int, long> m_callbackBtn;
	}
}
