﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E22 RID: 3618
	[Token(Token = "0x20009B0")]
	[StructLayout(3)]
	public class TutorialMessage : MonoBehaviour
	{
		// Token: 0x06004300 RID: 17152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D78")]
		[Address(RVA = "0x1015BB4B8", Offset = "0x15BB4B8", VA = "0x1015BB4B8")]
		public void Open(eTutorialMessageListDB tutorialMessageID)
		{
		}

		// Token: 0x06004301 RID: 17153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D79")]
		[Address(RVA = "0x1015BBCD8", Offset = "0x15BBCD8", VA = "0x1015BBCD8")]
		public void Clear()
		{
		}

		// Token: 0x06004302 RID: 17154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D7A")]
		[Address(RVA = "0x1015BB5B4", Offset = "0x15BB5B4", VA = "0x1015BB5B4")]
		public void SetEnableDark(bool flg, float alpha = 1f, bool raycastTarget = true)
		{
		}

		// Token: 0x06004303 RID: 17155 RVA: 0x00019638 File Offset: 0x00017838
		[Token(Token = "0x6003D7B")]
		[Address(RVA = "0x1015BBDC8", Offset = "0x15BBDC8", VA = "0x1015BBDC8")]
		public int GetDarkCanvasSortingOrder()
		{
			return 0;
		}

		// Token: 0x06004304 RID: 17156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D7C")]
		[Address(RVA = "0x1015BB818", Offset = "0x15BB818", VA = "0x1015BB818")]
		public void SetEnableMessage(bool flg)
		{
		}

		// Token: 0x06004305 RID: 17157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D7D")]
		[Address(RVA = "0x1015BBEB4", Offset = "0x15BBEB4", VA = "0x1015BBEB4")]
		public void SetMessage(eTutorialMessageListDB tutorialMessageID)
		{
		}

		// Token: 0x06004306 RID: 17158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D7E")]
		[Address(RVA = "0x1015BB89C", Offset = "0x15BB89C", VA = "0x1015BB89C")]
		public void SetMessage(string message)
		{
		}

		// Token: 0x06004307 RID: 17159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D7F")]
		[Address(RVA = "0x1015BB8DC", Offset = "0x15BB8DC", VA = "0x1015BB8DC")]
		public void SetMessagePosition(TutorialMessage.eMessagePosition pos)
		{
		}

		// Token: 0x06004308 RID: 17160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D80")]
		[Address(RVA = "0x1015BBC54", Offset = "0x15BBC54", VA = "0x1015BBC54")]
		public void SetEnableArrow(bool flg)
		{
		}

		// Token: 0x06004309 RID: 17161 RVA: 0x00019650 File Offset: 0x00017850
		[Token(Token = "0x6003D81")]
		[Address(RVA = "0x1015BBF84", Offset = "0x15BBF84", VA = "0x1015BBF84")]
		public bool GetEnableArrow()
		{
			return default(bool);
		}

		// Token: 0x0600430A RID: 17162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D82")]
		[Address(RVA = "0x1015BBFB4", Offset = "0x15BBFB4", VA = "0x1015BBFB4")]
		public void SetArrowPosition(Vector3 position)
		{
		}

		// Token: 0x0600430B RID: 17163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D83")]
		[Address(RVA = "0x1015BC054", Offset = "0x15BC054", VA = "0x1015BC054")]
		public void SetArrowWorldPosition(Vector3 position)
		{
		}

		// Token: 0x0600430C RID: 17164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D84")]
		[Address(RVA = "0x1015BC248", Offset = "0x15BC248", VA = "0x1015BC248")]
		public void SetArrowOffset(GameObject obj, Vector3 offset)
		{
		}

		// Token: 0x0600430D RID: 17165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D85")]
		[Address(RVA = "0x1015BC3D4", Offset = "0x15BC3D4", VA = "0x1015BC3D4")]
		public void SetArrowDirection(TutorialMessage.eArrowDirection direction)
		{
		}

		// Token: 0x0600430E RID: 17166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D86")]
		[Address(RVA = "0x1015BBD44", Offset = "0x15BBD44", VA = "0x1015BBD44")]
		public void SetEnableTrace(bool flg)
		{
		}

		// Token: 0x0600430F RID: 17167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D87")]
		[Address(RVA = "0x1015BC4B0", Offset = "0x15BC4B0", VA = "0x1015BC4B0")]
		public void SetTracePosition(Vector2 startPos, Vector2 endPos)
		{
		}

		// Token: 0x06004310 RID: 17168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D88")]
		[Address(RVA = "0x1015BC69C", Offset = "0x15BC69C", VA = "0x1015BC69C")]
		public void SetTracePositionOffset(GameObject obj, Vector2 offset, Vector2 move)
		{
		}

		// Token: 0x06004311 RID: 17169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D89")]
		[Address(RVA = "0x1015BC8D8", Offset = "0x15BC8D8", VA = "0x1015BC8D8")]
		public TutorialMessage()
		{
		}

		// Token: 0x04005358 RID: 21336
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003AF6")]
		private float MESSAGE_OFFSET;

		// Token: 0x04005359 RID: 21337
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003AF7")]
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x0400535A RID: 21338
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003AF8")]
		[SerializeField]
		private GameObject m_DarkObj;

		// Token: 0x0400535B RID: 21339
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003AF9")]
		[SerializeField]
		private GameObject m_MessageObj;

		// Token: 0x0400535C RID: 21340
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003AFA")]
		[SerializeField]
		private GameObject m_ArrowObj;

		// Token: 0x0400535D RID: 21341
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003AFB")]
		[SerializeField]
		private Text m_MessageText;

		// Token: 0x0400535E RID: 21342
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003AFC")]
		[SerializeField]
		private GameObject m_TraceObj;

		// Token: 0x0400535F RID: 21343
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003AFD")]
		private float m_DardDefaultAlpha;

		// Token: 0x02000E23 RID: 3619
		[Token(Token = "0x2001138")]
		public enum eArrowDirection
		{
			// Token: 0x04005361 RID: 21345
			[Token(Token = "0x4006BAC")]
			RightTop,
			// Token: 0x04005362 RID: 21346
			[Token(Token = "0x4006BAD")]
			RightBottom,
			// Token: 0x04005363 RID: 21347
			[Token(Token = "0x4006BAE")]
			LeftTop,
			// Token: 0x04005364 RID: 21348
			[Token(Token = "0x4006BAF")]
			LeftBottom
		}

		// Token: 0x02000E24 RID: 3620
		[Token(Token = "0x2001139")]
		public enum eMessagePosition
		{
			// Token: 0x04005366 RID: 21350
			[Token(Token = "0x4006BB1")]
			RightTop,
			// Token: 0x04005367 RID: 21351
			[Token(Token = "0x4006BB2")]
			RightBottom,
			// Token: 0x04005368 RID: 21352
			[Token(Token = "0x4006BB3")]
			LeftTop,
			// Token: 0x04005369 RID: 21353
			[Token(Token = "0x4006BB4")]
			LeftBottom
		}
	}
}
