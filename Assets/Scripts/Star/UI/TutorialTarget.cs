﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000E25 RID: 3621
	[Token(Token = "0x20009B1")]
	[StructLayout(3)]
	public class TutorialTarget : MonoBehaviour
	{
		// Token: 0x06004312 RID: 17170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D8A")]
		[Address(RVA = "0x1015BC8F0", Offset = "0x15BC8F0", VA = "0x1015BC8F0")]
		public void SetEnable(bool flg)
		{
		}

		// Token: 0x06004313 RID: 17171 RVA: 0x00019668 File Offset: 0x00017868
		[Token(Token = "0x6003D8B")]
		[Address(RVA = "0x1015BC8F8", Offset = "0x15BC8F8", VA = "0x1015BC8F8")]
		public bool GetEnable()
		{
			return default(bool);
		}

		// Token: 0x06004314 RID: 17172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D8C")]
		[Address(RVA = "0x1015BC900", Offset = "0x15BC900", VA = "0x1015BC900")]
		private void Start()
		{
		}

		// Token: 0x06004315 RID: 17173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D8D")]
		[Address(RVA = "0x1015BC928", Offset = "0x15BC928", VA = "0x1015BC928")]
		private void Update()
		{
		}

		// Token: 0x06004316 RID: 17174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D8E")]
		[Address(RVA = "0x1015BC9FC", Offset = "0x15BC9FC", VA = "0x1015BC9FC")]
		public TutorialTarget()
		{
		}

		// Token: 0x0400536A RID: 21354
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003AFE")]
		private bool m_Enable;

		// Token: 0x0400536B RID: 21355
		[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
		[Token(Token = "0x4003AFF")]
		private bool m_IsAttached;

		// Token: 0x0400536C RID: 21356
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B00")]
		private GameObject m_GameObject;
	}
}
