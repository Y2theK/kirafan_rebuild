﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E26 RID: 3622
	[Token(Token = "0x20009B2")]
	[StructLayout(3)]
	public class TutorialTipsScroll : InfiniteScrollEx
	{
		// Token: 0x06004317 RID: 17175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D8F")]
		[Address(RVA = "0x1015BCA04", Offset = "0x15BCA04", VA = "0x1015BCA04", Slot = "7")]
		public override void Setup()
		{
		}

		// Token: 0x06004318 RID: 17176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D90")]
		[Address(RVA = "0x1015BCA3C", Offset = "0x15BCA3C", VA = "0x1015BCA3C", Slot = "8")]
		protected override void Update()
		{
		}

		// Token: 0x06004319 RID: 17177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D91")]
		[Address(RVA = "0x1015BCCA8", Offset = "0x15BCCA8", VA = "0x1015BCCA8")]
		public void InitializeMainImage()
		{
		}

		// Token: 0x0600431A RID: 17178 RVA: 0x00019680 File Offset: 0x00017880
		[Token(Token = "0x6003D92")]
		[Address(RVA = "0x1015BCCDC", Offset = "0x15BCCDC", VA = "0x1015BCCDC")]
		public bool IsEndPage()
		{
			return default(bool);
		}

		// Token: 0x0600431B RID: 17179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D93")]
		[Address(RVA = "0x1015BCDCC", Offset = "0x15BCDCC", VA = "0x1015BCDCC")]
		public TutorialTipsScroll()
		{
		}

		// Token: 0x0400536D RID: 21357
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003B01")]
		public Image m_Image;
	}
}
