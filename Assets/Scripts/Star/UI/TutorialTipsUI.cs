﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E27 RID: 3623
	[Token(Token = "0x20009B3")]
	[StructLayout(3)]
	public class TutorialTipsUI : MonoBehaviour
	{
		// Token: 0x0600431C RID: 17180 RVA: 0x00019698 File Offset: 0x00017898
		[Token(Token = "0x6003D94")]
		[Address(RVA = "0x1015BCDD4", Offset = "0x15BCDD4", VA = "0x1015BCDD4")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x0600431D RID: 17181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D95")]
		[Address(RVA = "0x1015BCDE4", Offset = "0x15BCDE4", VA = "0x1015BCDE4")]
		private void Start()
		{
		}

		// Token: 0x0600431E RID: 17182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D96")]
		[Address(RVA = "0x1015BCF00", Offset = "0x15BCF00", VA = "0x1015BCF00")]
		private void Initialize()
		{
		}

		// Token: 0x0600431F RID: 17183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D97")]
		[Address(RVA = "0x1015BD200", Offset = "0x15BD200", VA = "0x1015BD200")]
		public void OnDestroy()
		{
		}

		// Token: 0x06004320 RID: 17184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D98")]
		[Address(RVA = "0x1015BD210", Offset = "0x15BD210", VA = "0x1015BD210")]
		public void Destroy()
		{
		}

		// Token: 0x06004321 RID: 17185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D99")]
		[Address(RVA = "0x1015BD258", Offset = "0x15BD258", VA = "0x1015BD258")]
		public void Abort()
		{
		}

		// Token: 0x06004322 RID: 17186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9A")]
		[Address(RVA = "0x1015BD2A4", Offset = "0x15BD2A4", VA = "0x1015BD2A4")]
		public void Open(eTutorialTipsListDB tutorialTipsID, Action onOpened, Action onClose)
		{
		}

		// Token: 0x06004323 RID: 17187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9B")]
		[Address(RVA = "0x1015BD6B4", Offset = "0x15BD6B4", VA = "0x1015BD6B4")]
		public void Open(eRetireTipsListDB retireTipsID, Action onOpened, Action onClose)
		{
		}

		// Token: 0x06004324 RID: 17188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9C")]
		[Address(RVA = "0x1015BD58C", Offset = "0x15BD58C", VA = "0x1015BD58C")]
		private void Open()
		{
		}

		// Token: 0x06004325 RID: 17189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9D")]
		[Address(RVA = "0x1015BD990", Offset = "0x15BD990", VA = "0x1015BD990")]
		public void Close()
		{
		}

		// Token: 0x06004326 RID: 17190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9E")]
		[Address(RVA = "0x1015BD9E4", Offset = "0x15BD9E4", VA = "0x1015BD9E4")]
		private void Update()
		{
		}

		// Token: 0x06004327 RID: 17191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D9F")]
		[Address(RVA = "0x1015BDBD8", Offset = "0x15BDBD8", VA = "0x1015BDBD8")]
		private void UpdateIn()
		{
		}

		// Token: 0x06004328 RID: 17192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA0")]
		[Address(RVA = "0x1015BDC28", Offset = "0x15BDC28", VA = "0x1015BDC28")]
		private void UpdateIdle()
		{
		}

		// Token: 0x06004329 RID: 17193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA1")]
		[Address(RVA = "0x1015BDE14", Offset = "0x15BDE14", VA = "0x1015BDE14")]
		private void UpdateCloseButtonActive()
		{
		}

		// Token: 0x0600432A RID: 17194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA2")]
		[Address(RVA = "0x1015BDDD0", Offset = "0x15BDDD0", VA = "0x1015BDDD0")]
		private void UpdateOut()
		{
		}

		// Token: 0x0600432B RID: 17195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA3")]
		[Address(RVA = "0x1015BD2D0", Offset = "0x15BD2D0", VA = "0x1015BD2D0")]
		private void Prepare(eTutorialTipsListDB tutorialTipsID)
		{
		}

		// Token: 0x0600432C RID: 17196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA4")]
		[Address(RVA = "0x1015BD6E8", Offset = "0x15BD6E8", VA = "0x1015BD6E8")]
		private void Prepare(eRetireTipsListDB retireTipsID)
		{
		}

		// Token: 0x0600432D RID: 17197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA5")]
		[Address(RVA = "0x1015BDA4C", Offset = "0x15BDA4C", VA = "0x1015BDA4C")]
		private void UpdatePrepare()
		{
		}

		// Token: 0x0600432E RID: 17198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA6")]
		[Address(RVA = "0x1015BD02C", Offset = "0x15BD02C", VA = "0x1015BD02C")]
		private void DestroySpriteHndls()
		{
		}

		// Token: 0x0600432F RID: 17199 RVA: 0x000196B0 File Offset: 0x000178B0
		[Token(Token = "0x6003DA7")]
		[Address(RVA = "0x1015BDF50", Offset = "0x15BDF50", VA = "0x1015BDF50")]
		private bool IsSpriteHandlerAvailable()
		{
			return default(bool);
		}

		// Token: 0x06004330 RID: 17200 RVA: 0x000196C8 File Offset: 0x000178C8
		[Token(Token = "0x6003DA8")]
		[Address(RVA = "0x1015BE044", Offset = "0x15BE044", VA = "0x1015BE044")]
		private bool IsOpenTips()
		{
			return default(bool);
		}

		// Token: 0x06004331 RID: 17201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DA9")]
		[Address(RVA = "0x1015BE0C0", Offset = "0x15BE0C0", VA = "0x1015BE0C0")]
		public void OnClickClose()
		{
		}

		// Token: 0x06004332 RID: 17202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAA")]
		[Address(RVA = "0x1015BE0C4", Offset = "0x15BE0C4", VA = "0x1015BE0C4")]
		public TutorialTipsUI()
		{
		}

		// Token: 0x0400536E RID: 21358
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B02")]
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x0400536F RID: 21359
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B03")]
		[SerializeField]
		private TutorialTipsScroll m_Scroll;

		// Token: 0x04005370 RID: 21360
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B04")]
		[SerializeField]
		private Text m_DestTitleText;

		// Token: 0x04005371 RID: 21361
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003B05")]
		private TutorialTipsUI.eMode m_Mode;

		// Token: 0x04005372 RID: 21362
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003B06")]
		private eTutorialTipsListDB m_TipsID;

		// Token: 0x04005373 RID: 21363
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003B07")]
		private TutorialTipsListDB_Param m_TTLDBP;

		// Token: 0x04005374 RID: 21364
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B08")]
		private RetireTipsListDB_Param m_RetireDBParam;

		// Token: 0x04005375 RID: 21365
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003B09")]
		private eRetireTipsListDB m_RetireTipsID;

		// Token: 0x04005376 RID: 21366
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003B0A")]
		private List<SpriteHandler> m_TipsImageSpriteHndls;

		// Token: 0x04005377 RID: 21367
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003B0B")]
		private List<TutorialTipsUIItemData> m_ScrollItemData;

		// Token: 0x04005378 RID: 21368
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003B0C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100128030", Offset = "0x128030")]
		private CustomButton m_CloseButton;

		// Token: 0x04005379 RID: 21369
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B0D")]
		private Action m_OnOpenedCallback;

		// Token: 0x0400537A RID: 21370
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003B0E")]
		private Action m_OnCloseCallback;

		// Token: 0x0400537B RID: 21371
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003B0F")]
		private bool m_Destroyed;

		// Token: 0x0400537C RID: 21372
		[Cpp2IlInjected.FieldOffset(Offset = "0x99")]
		[Token(Token = "0x4003B10")]
		private bool m_Preparing;

		// Token: 0x0400537D RID: 21373
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4003B11")]
		private TutorialTipsUI.eStep m_Step;

		// Token: 0x0400537E RID: 21374
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003B12")]
		private int m_CurrentIdx;

		// Token: 0x02000E28 RID: 3624
		[Token(Token = "0x200113A")]
		public enum eMode
		{
			// Token: 0x04005380 RID: 21376
			[Token(Token = "0x4006BB6")]
			Tutorial,
			// Token: 0x04005381 RID: 21377
			[Token(Token = "0x4006BB7")]
			Retire
		}

		// Token: 0x02000E29 RID: 3625
		[Token(Token = "0x200113B")]
		public enum eStep
		{
			// Token: 0x04005383 RID: 21379
			[Token(Token = "0x4006BB9")]
			None = -1,
			// Token: 0x04005384 RID: 21380
			[Token(Token = "0x4006BBA")]
			In,
			// Token: 0x04005385 RID: 21381
			[Token(Token = "0x4006BBB")]
			Idle,
			// Token: 0x04005386 RID: 21382
			[Token(Token = "0x4006BBC")]
			Out
		}
	}
}
