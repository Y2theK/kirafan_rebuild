﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E2B RID: 3627
	[Token(Token = "0x20009B5")]
	[StructLayout(3)]
	public class TutorialTipsUIItem : InfiniteScrollItem
	{
		// Token: 0x06004336 RID: 17206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAE")]
		[Address(RVA = "0x1015BE0D4", Offset = "0x15BE0D4", VA = "0x1015BE0D4", Slot = "5")]
		public override void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06004337 RID: 17207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAF")]
		[Address(RVA = "0x1015BE1FC", Offset = "0x15BE1FC", VA = "0x1015BE1FC", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004338 RID: 17208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB0")]
		[Address(RVA = "0x1015BE29C", Offset = "0x15BE29C", VA = "0x1015BE29C")]
		private void Update()
		{
		}

		// Token: 0x06004339 RID: 17209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB1")]
		[Address(RVA = "0x1015BE2A0", Offset = "0x15BE2A0", VA = "0x1015BE2A0")]
		public TutorialTipsUIItem()
		{
		}

		// Token: 0x0400538B RID: 21387
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003B17")]
		[SerializeField]
		private Image m_ImageImage;

		// Token: 0x0400538C RID: 21388
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003B18")]
		[SerializeField]
		private Text m_Text;
	}
}
