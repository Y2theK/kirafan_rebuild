﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E2A RID: 3626
	[Token(Token = "0x20009B4")]
	[StructLayout(3)]
	public class TutorialTipsUIItemData : InfiniteScrollItemData
	{
		// Token: 0x06004333 RID: 17203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAB")]
		[Address(RVA = "0x1015BDF04", Offset = "0x15BDF04", VA = "0x1015BDF04")]
		public TutorialTipsUIItemData(Text destTitleText, string titleText, Sprite imageSprite, string message)
		{
		}

		// Token: 0x06004334 RID: 17204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAC")]
		[Address(RVA = "0x1015BE2A8", Offset = "0x15BE2A8", VA = "0x1015BE2A8")]
		public TutorialTipsUIItemData()
		{
		}

		// Token: 0x06004335 RID: 17205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DAD")]
		[Address(RVA = "0x1015BD024", Offset = "0x15BD024", VA = "0x1015BD024")]
		public void Destroy()
		{
		}

		// Token: 0x04005387 RID: 21383
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003B13")]
		public Text m_DestTitleText;

		// Token: 0x04005388 RID: 21384
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B14")]
		public string m_TitleText;

		// Token: 0x04005389 RID: 21385
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B15")]
		public Sprite m_ImageSprite;

		// Token: 0x0400538A RID: 21386
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B16")]
		public string m_Message;
	}
}
