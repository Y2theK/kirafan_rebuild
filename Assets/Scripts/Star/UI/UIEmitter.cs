﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D4D RID: 3405
	[Token(Token = "0x2000922")]
	[StructLayout(3)]
	public class UIEmitter : MonoBehaviour
	{
		// Token: 0x06003E83 RID: 16003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393A")]
		[Address(RVA = "0x1015BE2B0", Offset = "0x15BE2B0", VA = "0x1015BE2B0")]
		private void Start()
		{
		}

		// Token: 0x06003E84 RID: 16004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393B")]
		[Address(RVA = "0x1015BE2C4", Offset = "0x15BE2C4", VA = "0x1015BE2C4")]
		public void Play()
		{
		}

		// Token: 0x06003E85 RID: 16005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393C")]
		[Address(RVA = "0x1015BE2D0", Offset = "0x15BE2D0", VA = "0x1015BE2D0")]
		public void Stop()
		{
		}

		// Token: 0x06003E86 RID: 16006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393D")]
		[Address(RVA = "0x1015BE2DC", Offset = "0x15BE2DC", VA = "0x1015BE2DC")]
		private void Update()
		{
		}

		// Token: 0x06003E87 RID: 16007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393E")]
		[Address(RVA = "0x1015BEAF4", Offset = "0x15BEAF4", VA = "0x1015BEAF4")]
		public UIEmitter()
		{
		}

		// Token: 0x04004DC5 RID: 19909
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036C1")]
		[SerializeField]
		private bool m_AutoStart;

		// Token: 0x04004DC6 RID: 19910
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036C2")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100125388", Offset = "0x125388")]
		private Sprite[] m_ParticleBaseSprites;

		// Token: 0x04004DC7 RID: 19911
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40036C3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001253D4", Offset = "0x1253D4")]
		private UIParticle m_ParticleBasePrefab;

		// Token: 0x04004DC8 RID: 19912
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40036C4")]
		[SerializeField]
		private float m_InstantiateIntervalTime;

		// Token: 0x04004DC9 RID: 19913
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40036C5")]
		private float m_WaitTime;

		// Token: 0x04004DCA RID: 19914
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40036C6")]
		private int m_CreateSpriteIndex;

		// Token: 0x04004DCB RID: 19915
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40036C7")]
		[SerializeField]
		private UIParticle.ImageAxisDefaultSizeInformationValue m_ImageAxisDefaultSizeInformationValue;

		// Token: 0x04004DCC RID: 19916
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40036C8")]
		[SerializeField]
		private float m_ChildActionTime;

		// Token: 0x04004DCD RID: 19917
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40036C9")]
		[SerializeField]
		private UIEmitter.UIEmitterChildActionInformation m_ChildActionX;

		// Token: 0x04004DCE RID: 19918
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40036CA")]
		[SerializeField]
		private UIEmitter.UIEmitterChildActionInformation m_ChildActionY;

		// Token: 0x04004DCF RID: 19919
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40036CB")]
		private UIEmitter.eState m_State;

		// Token: 0x02000D4E RID: 3406
		[Token(Token = "0x20010F2")]
		[Serializable]
		public class UIEmitterChildActionInformation
		{
			// Token: 0x06003E88 RID: 16008 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061FC")]
			[Address(RVA = "0x1015BEB0C", Offset = "0x15BEB0C", VA = "0x1015BEB0C")]
			public UIEmitterChildActionInformation()
			{
			}

			// Token: 0x04004DD0 RID: 19920
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A53")]
			[SerializeField]
			public float m_Move;

			// Token: 0x04004DD1 RID: 19921
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006A54")]
			[SerializeField]
			public iTween.EaseType m_MoveEaseType;

			// Token: 0x04004DD2 RID: 19922
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A55")]
			[SerializeField]
			public iTween.LoopType m_MoveLoopType;
		}

		// Token: 0x02000D4F RID: 3407
		[Token(Token = "0x20010F3")]
		public enum eState
		{
			// Token: 0x04004DD4 RID: 19924
			[Token(Token = "0x4006A57")]
			Error,
			// Token: 0x04004DD5 RID: 19925
			[Token(Token = "0x4006A58")]
			NotActive,
			// Token: 0x04004DD6 RID: 19926
			[Token(Token = "0x4006A59")]
			Active
		}
	}
}
