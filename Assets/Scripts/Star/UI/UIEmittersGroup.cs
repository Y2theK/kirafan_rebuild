﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D50 RID: 3408
	[Token(Token = "0x2000923")]
	[StructLayout(3)]
	public class UIEmittersGroup : PlayInterfaceMonoBehaviour
	{
		// Token: 0x06003E89 RID: 16009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600393F")]
		[Address(RVA = "0x1015BEB1C", Offset = "0x15BEB1C", VA = "0x1015BEB1C", Slot = "4")]
		public override void Play()
		{
		}

		// Token: 0x06003E8A RID: 16010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003940")]
		[Address(RVA = "0x1015BEBBC", Offset = "0x15BEBBC", VA = "0x1015BEBBC", Slot = "5")]
		public override void Stop()
		{
		}

		// Token: 0x06003E8B RID: 16011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003941")]
		[Address(RVA = "0x1015BEC5C", Offset = "0x15BEC5C", VA = "0x1015BEC5C")]
		public UIEmittersGroup()
		{
		}

		// Token: 0x04004DD7 RID: 19927
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036CC")]
		[SerializeField]
		private UIEmitter[] m_Emitters;
	}
}
