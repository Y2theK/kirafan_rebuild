﻿using Star.UI.Window;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI {
    [RequireComponent(typeof(CustomButtonGroup))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UIGroup : MonoBehaviour {
        [SerializeField] private bool m_AutoEnableInput = true;
        [SerializeField] private bool m_InputBlock = true;
        [SerializeField] private bool m_EnableGlobalUI;
        [SerializeField] protected AnimUIPlayer[] m_AnimsPrepareStart;
        [SerializeField] protected AnimUIPlayer[] m_Anims;
        [SerializeField] protected CustomWindow m_CustomWindowPrefab;
        [SerializeField] protected CustomWindow m_CustomWindow;
        [SerializeField] protected InstantiateWindowStatus m_WindowStatus;

        private eState m_State;
        private CustomButtonGroup m_CustomButtonGroup;
        private CanvasGroup m_CanvasGroup;
        private bool m_IsEnableInputSelf;
        private bool m_IsEnableInputParent;
        protected bool m_AutoDisableInHide;
        private GameObject m_GameObject;

        public event Action OnIdle;
        public event Action OnClose;
        public event Action OnEnd;

        public eState State => m_State;
        public int LastPressedFooterButtonID => m_CustomWindow == null ? -1 : m_CustomWindow.LastPressedFooterButtonID;
        public CustomWindow Window => m_CustomWindow;

        public bool IsOpen() {
            return m_State != eState.Hide;
        }

        public bool IsIdle() {
            return m_State == eState.Idle;
        }

        public bool IsOpenOrIn() {
            return m_State == eState.In || m_State == eState.Idle;
        }

        public bool IsHideOrOut() {
            return m_State == eState.Hide || m_State == eState.Out;
        }

        public bool IsPlayingInOut => m_State == eState.In || m_State == eState.Out;
        public bool IsDonePlayIn => m_State == eState.Idle;
        public bool IsDonePlayOut => m_State == eState.Hide;

        public CustomButtonGroup CustomButtonGroup {
            get {
                if (m_CustomButtonGroup == null) {
                    m_CustomButtonGroup = GetComponent<CustomButtonGroup>();
                }
                return m_CustomButtonGroup;
            }
        }

        public CanvasGroup CanvasGroup {
            get {
                if (m_CanvasGroup == null) {
                    m_CanvasGroup = GetComponent<CanvasGroup>();
                }
                return m_CanvasGroup;
            }
        }

        public GameObject GameObject {
            get {
                if (m_GameObject == null) {
                    m_GameObject = gameObject;
                }
                return m_GameObject;
            }
        }

        public virtual void PrepareStart() {
            m_State = eState.Prepare;
            if (m_AnimsPrepareStart != null) {
                for (int i = 0; i < m_AnimsPrepareStart.Length; i++) {
                    if (m_AnimsPrepareStart[i] != null) {
                        m_AnimsPrepareStart[i].PlayIn();
                    }
                }
            }
            if (m_WindowStatus.m_ContentObj != null) {
                m_WindowStatus.m_ContentObj.gameObject.SetActive(false);
            }
        }

        public virtual void Open() {
            if (m_WindowStatus.m_ContentObj != null) {
                m_WindowStatus.m_ContentObj.gameObject.SetActive(true);
            }
            if (m_CustomWindow == null && m_CustomWindowPrefab != null) {
                m_CustomWindow = Instantiate(m_CustomWindowPrefab, GetComponent<RectTransform>());
                m_CustomWindow.RectTransform.localPosition = Vector2.zero;
                m_CustomWindow.RectTransform.localScale = Vector3.one;
                m_CustomWindow.RectTransform.sizeDelta = Vector2.zero;
                if (m_CustomWindow.BGDark != null) {
                    m_CustomWindow.BGDark.GetComponent<Image>().enabled = m_WindowStatus.m_BGDark;
                }
                m_CustomWindow.SetSortOrder(m_WindowStatus.m_SortOrderTypeID, m_WindowStatus.m_SortOrderOffset);
                if (m_WindowStatus.m_FitWindowRect != null) {
                    m_CustomWindow.WindowRectTransform.localPosition = m_WindowStatus.m_FitWindowRect.localPosition;
                    m_CustomWindow.WindowRectTransform.sizeDelta = m_WindowStatus.m_FitWindowRect.sizeDelta;
                }
                if (m_WindowStatus.m_TitleText != null) {
                    m_CustomWindow.SetTitle(m_WindowStatus.m_TitleText.text);
                    m_WindowStatus.m_TitleText.gameObject.SetActive(false);
                } else {
                    m_CustomWindow.SetTitle(null);
                }
                m_CustomWindow.SetContent(m_WindowStatus.m_ContentObj);
                switch (m_WindowStatus.m_FooterType) {
                    case InstantiateWindowStatus.eFooterType.OK:
                        m_CustomWindow.SetFooterOK();
                        break;
                    case InstantiateWindowStatus.eFooterType.Close:
                        m_CustomWindow.SetFooterClose();
                        break;
                    case InstantiateWindowStatus.eFooterType.YesNo:
                        m_CustomWindow.SetFooterYesNo();
                        break;
                    case InstantiateWindowStatus.eFooterType.Cancel:
                        m_CustomWindow.SetFooterCancel();
                        break;
                }
                m_CustomWindow.OnClickFooterButton += OnClickFooterButtonCallBack;
            }
            GameObject.SetActive(true);
            if (m_CustomWindow != null) {
                m_CustomWindow.Open();
            }
            for (int i = 0; i < m_Anims.Length; i++) {
                if (m_Anims[i] != null) {
                    m_Anims[i].PlayIn();
                }
            }
            OpenChangeStateProcess();
        }

        protected virtual void OpenChangeStateProcess() {
            SetEnableInput(false);
            m_State = eState.In;
            if (m_InputBlock) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameObject, true);
            }
        }

        public virtual void Close() {
            GameObject.SetActive(true);
            if (m_CustomWindow != null) {
                m_CustomWindow.Close();
            }
            if (m_AnimsPrepareStart != null) {
                for (int i = 0; i < m_AnimsPrepareStart.Length; i++) {
                    if (m_AnimsPrepareStart[i] != null) {
                        m_AnimsPrepareStart[i].PlayOut();
                    }
                }
            }
            for (int j = 0; j < m_Anims.Length; j++) {
                if (m_Anims[j] != null) {
                    m_Anims[j].PlayOut();
                }
            }
            SetEnableInput(false);
            OnClose.Call();
            m_State = eState.Out;
            if (m_InputBlock) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameObject, true);
            }
        }

        public virtual void Hide() {
            for (int i = 0; i < m_AnimsPrepareStart.Length; i++) {
                if (m_AnimsPrepareStart[i] != null) {
                    m_AnimsPrepareStart[i].Hide();
                }
            }
            for (int j = 0; j < m_Anims.Length; j++) {
                if (m_Anims[j] != null) {
                    m_Anims[j].Hide();
                }
            }
            m_State = eState.Hide;
            if (m_InputBlock) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameObject, false);
            }
        }

        public virtual void Update() {
            switch (m_State) {
                case eState.Hide:
                    if (m_AutoDisableInHide && GameObject.activeSelf) {
                        GameObject.SetActive(false);
                    }
                    break;
                case eState.In:
                    if (CheckInEnd()) {
                        m_State = eState.Idle;
                        OnFinishPlayIn();
                        if (m_AutoEnableInput) {
                            SetEnableInput(true);
                        }
                        OnIdle.Call();
                    }
                    break;
                case eState.Out:
                    if (CheckOutEnd()) {
                        m_State = eState.WaitFinalize;
                        OnFinishPlayOut();
                    }
                    break;
                case eState.WaitFinalize:
                    if (IsCompleteFinalize()) {
                        m_State = eState.Hide;
                        OnFinishFinalize();
                    }
                    break;
            }
        }

        public virtual void LateUpdate() {
            if (m_State == eState.Hide && m_AutoDisableInHide && GameObject.activeSelf) {
                GameObject.SetActive(false);
            }
        }

        protected virtual void OnDestroy() {
            m_CustomWindowPrefab = null;
            if (m_CustomWindow != null) {
                Destroy(m_CustomWindow);
                m_CustomWindow = null;
            }
            GameSystem inst = GameSystem.Inst;
            if (inst != null && inst.IsAvailable()) {
                inst.InputBlock.SetBlockObj(GameObject, false);
            }
        }

        public virtual void OnFinishPlayIn() {
            if (m_InputBlock) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameObject, false);
            }
        }

        public virtual void OnFinishPlayOut() { }

        public virtual bool IsCompleteFinalize() {
            return true;
        }

        public virtual void OnFinishFinalize() {
            if (m_AutoDisableInHide) {
                GameObject.SetActive(false);
            }
            if (m_InputBlock) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameObject, false);
            }
            OnEnd.Call();
        }

        public void SetEnableInput(bool flg) {
            m_IsEnableInputSelf = flg;
            if (m_IsEnableInputSelf && m_IsEnableInputParent) {
                CustomButtonGroup.IsEnableInput = true;
                CanvasGroup.blocksRaycasts = true;
                if (m_EnableGlobalUI) {
                    GameSystem.Inst.GlobalUI.SetEnableInputFromUIGroup(this, true);
                }
            } else if (m_EnableGlobalUI) {
                CustomButtonGroup.IsEnableInput = false;
                CanvasGroup.blocksRaycasts = false;
                GameSystem.Inst.GlobalUI.SetEnableInputFromUIGroup(this, false);
            }
        }

        public void SetEnableInputParent(bool flg) {
            m_IsEnableInputParent = flg;
            if (m_IsEnableInputSelf && m_IsEnableInputParent) {
                CustomButtonGroup.IsEnableInput = true;
                CanvasGroup.blocksRaycasts = true;
                if (m_EnableGlobalUI) {
                    GameSystem.Inst.GlobalUI.SetEnableInputFromUIGroup(this, true);
                }
            } else {
                CustomButtonGroup.IsEnableInput = false;
                CanvasGroup.blocksRaycasts = false;
                if (m_EnableGlobalUI) {
                    GameSystem.Inst.GlobalUI.SetEnableInputFromUIGroup(this, false);
                }
            }
        }

        public bool IsEnableInput() {
            return m_IsEnableInputParent && m_IsEnableInputSelf && CustomButtonGroup.IsEnableInput;
        }

        public void SetEnableGlobalUIMode(bool flg) {
            m_EnableGlobalUI = flg;
        }

        protected void AddPlayAnim(AnimUIPlayer anim) {
            int num = -1;
            for (int i = 0; i < m_Anims.Length; i++) {
                if (m_Anims[i] == anim) {
                    return;
                }
                if (num == -1 && m_Anims[i] == null) {
                    num = i;
                }
            }
            if (num == -1) {
                AnimUIPlayer[] array = new AnimUIPlayer[m_Anims.Length + 1];
                for (int j = 0; j < m_Anims.Length; j++) {
                    array[j] = m_Anims[j];
                }
                m_Anims = array;
                m_Anims[m_Anims.Length - 1] = anim;
            } else {
                m_Anims[num] = anim;
            }
        }

        protected void RemovePlayAnim(AnimUIPlayer anim) {
            for (int i = 0; i < m_Anims.Length; i++) {
                if (m_Anims[i] == anim) {
                    m_Anims[i] = null;
                    return;
                }
            }
        }

        protected virtual bool CheckInEnd() {
            bool flag = true;
            if (m_CustomWindow != null && m_CustomWindow.IsPlayingAnim()) {
                flag = false;
            }
            if (flag && m_AnimsPrepareStart != null) {
                for (int i = 0; i < m_AnimsPrepareStart.Length; i++) {
                    if (m_AnimsPrepareStart[i] != null) {
                        if (m_AnimsPrepareStart[i].State == 1) {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            if (flag && m_Anims != null) {
                for (int j = 0; j < m_Anims.Length; j++) {
                    if (m_Anims[j] != null) {
                        if (m_Anims[j].State == 1) {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            return flag;
        }

        protected virtual bool CheckOutEnd() {
            bool flag = true;
            if (m_CustomWindow != null && m_CustomWindow.IsPlayingAnim()) {
                flag = false;
            }
            if (flag && m_AnimsPrepareStart != null) {
                for (int i = 0; i < m_AnimsPrepareStart.Length; i++) {
                    if (m_AnimsPrepareStart[i] != null) {
                        if (m_AnimsPrepareStart[i].State == 3) {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            if (flag && m_Anims != null) {
                for (int j = 0; j < m_Anims.Length; j++) {
                    if (m_Anims[j] != null) {
                        if (m_Anims[j].isActiveAndEnabled && m_Anims[j].State == 3) {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            return flag;
        }

        private void OnClickFooterButtonCallBack() {
            m_WindowStatus.m_OnClickFooterButton.Invoke();
        }

        [Serializable]
        public class InstantiateWindowStatus {
            [Tooltip("描画順の設定")]
            [SerializeField] public UIDefine.eSortOrderTypeID m_SortOrderTypeID;

            [Tooltip("描画順の設定にオフセットをかける")]
            [SerializeField] public int m_SortOrderOffset;

            [Tooltip("後ろを暗くするかどうか")]
            [SerializeField] public bool m_BGDark;

            [Tooltip("指定したRectTransformにウィンドウサイズをあわせる")]
            [SerializeField] public RectTransform m_FitWindowRect;

            [Tooltip("指定したTextのtextをタイトルに表示")]
            [SerializeField] public Text m_TitleText;

            [Tooltip("指定したRectTransformをウィンドウ内へ移動")]
            [SerializeField] public RectTransform m_ContentObj;

            [SerializeField] public eFooterType m_FooterType;
            [SerializeField] public UnityEvent m_OnClickFooterButton;

            public InstantiateWindowStatus() {
                m_SortOrderTypeID = UIDefine.eSortOrderTypeID.UniqueWindow;
            }

            public enum eFooterType {
                None,
                OK,
                Close,
                YesNo,
                Cancel
            }
        }

        public enum eState {
            Hide,
            Prepare,
            In,
            Idle,
            Out,
            WaitFinalize
        }
    }
}
