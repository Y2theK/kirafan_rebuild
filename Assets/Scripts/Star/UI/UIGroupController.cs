﻿using UnityEngine;

namespace Star.UI {
    public class UIGroupController : MonoBehaviour {
        private UIGroup[] m_UIGroups;
        private UIGroupController[] m_UIGroupControllers;
        private bool m_IsEnableInputSelf = true;
        private bool m_IsEnableInputParent = true;
        private bool m_IsDonePrepare;
        private RectTransform m_RectTransform;

        private void Prepare() {
            m_RectTransform = base.GetComponent<RectTransform>();
            m_UIGroups = m_RectTransform.GetComponentsInChildren<UIGroup>();
            m_UIGroupControllers = m_RectTransform.GetComponentsInChildren<UIGroupController>();
            m_IsDonePrepare = true;
        }

        public void SetEnableInput(bool flg) {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            m_IsEnableInputSelf = flg;
            if (m_UIGroupControllers != null) {
                for (int i = 0; i < m_UIGroupControllers.Length; i++) {
                    if (i != 0) {
                        m_UIGroupControllers[i].SetEnableInputParent(IsEnableInput());
                    }
                }
            }
            if (m_UIGroups != null) {
                for (int j = 0; j < m_UIGroups.Length; j++) {
                    m_UIGroups[j].SetEnableInputParent(flg);
                }
            }
        }

        public void SetEnableInputParent(bool flg) {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            m_IsEnableInputParent = flg;
            if (m_UIGroupControllers != null) {
                for (int i = 0; i < m_UIGroupControllers.Length; i++) {
                    if (i != 0) {
                        m_UIGroupControllers[i].SetEnableInputParent(IsEnableInput());
                    }
                }
            }
            if (m_UIGroups != null) {
                for (int j = 0; j < m_UIGroups.Length; j++) {
                    m_UIGroups[j].SetEnableInputParent(flg);
                }
            }
        }

        public bool IsEnableInput() {
            return m_IsEnableInputSelf && m_IsEnableInputParent;
        }
    }
}
