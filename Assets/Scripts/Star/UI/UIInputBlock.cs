﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D5D RID: 3421
	[Token(Token = "0x2000928")]
	[StructLayout(3)]
	public class UIInputBlock : MonoBehaviour
	{
		// Token: 0x06003ED6 RID: 16086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003985")]
		[Address(RVA = "0x1015C0D7C", Offset = "0x15C0D7C", VA = "0x1015C0D7C")]
		private void Start()
		{
		}

		// Token: 0x06003ED7 RID: 16087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003986")]
		[Address(RVA = "0x1015C0DB8", Offset = "0x15C0DB8", VA = "0x1015C0DB8")]
		private void Update()
		{
		}

		// Token: 0x06003ED8 RID: 16088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003987")]
		[Address(RVA = "0x1015C0F34", Offset = "0x15C0F34", VA = "0x1015C0F34")]
		public void Clear()
		{
		}

		// Token: 0x06003ED9 RID: 16089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003988")]
		[Address(RVA = "0x1015BF5D4", Offset = "0x15BF5D4", VA = "0x1015BF5D4")]
		public void SetBlockObj(GameObject blockObj, bool flg)
		{
		}

		// Token: 0x06003EDA RID: 16090 RVA: 0x00018BE8 File Offset: 0x00016DE8
		[Token(Token = "0x6003989")]
		[Address(RVA = "0x1015C0DFC", Offset = "0x15C0DFC", VA = "0x1015C0DFC")]
		public bool IsBlockInput()
		{
			return default(bool);
		}

		// Token: 0x06003EDB RID: 16091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600398A")]
		[Address(RVA = "0x1015C0FBC", Offset = "0x15C0FBC", VA = "0x1015C0FBC")]
		public UIInputBlock()
		{
		}

		// Token: 0x04004E20 RID: 20000
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036EF")]
		[SerializeField]
		private InvisibleGraphic m_BlockObj;

		// Token: 0x04004E21 RID: 20001
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036F0")]
		private List<GameObject> m_BlockObjStackList;
	}
}
