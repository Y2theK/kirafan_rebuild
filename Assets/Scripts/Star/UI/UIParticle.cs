﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D52 RID: 3410
	[Token(Token = "0x2000925")]
	[StructLayout(3)]
	public class UIParticle : MonoBehaviour
	{
		// Token: 0x06003E8C RID: 16012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003942")]
		[Address(RVA = "0x1015C102C", Offset = "0x15C102C", VA = "0x1015C102C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003E8D RID: 16013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003943")]
		[Address(RVA = "0x1015BE664", Offset = "0x15BE664", VA = "0x1015BE664")]
		public void SetImageDefaultSizeInformation(UIParticle.ImageAxisDefaultSizeInformation imageDefaultSizeInformation)
		{
		}

		// Token: 0x06003E8E RID: 16014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003944")]
		[Address(RVA = "0x1015C109C", Offset = "0x15C109C", VA = "0x1015C109C")]
		public void SetImageDefaultSizeInformationValue(UIParticle.ImageAxisDefaultSizeInformationValue imageDefaultSizeInformationValue)
		{
		}

		// Token: 0x06003E8F RID: 16015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003945")]
		[Address(RVA = "0x1015C111C", Offset = "0x15C111C", VA = "0x1015C111C")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x06003E90 RID: 16016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003946")]
		[Address(RVA = "0x1015C119C", Offset = "0x15C119C", VA = "0x1015C119C")]
		public void SetDefaultImageSizeType(UIParticle.eImageAxisDefaultSizeType defaultSizeType)
		{
		}

		// Token: 0x06003E91 RID: 16017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003947")]
		[Address(RVA = "0x1015C1240", Offset = "0x15C1240", VA = "0x1015C1240")]
		public void SetDefaultImageSize(float x, float y)
		{
		}

		// Token: 0x06003E92 RID: 16018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003948")]
		[Address(RVA = "0x1015BE66C", Offset = "0x15BE66C", VA = "0x1015BE66C")]
		public void SetActionTime(float actionTime)
		{
		}

		// Token: 0x06003E93 RID: 16019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003949")]
		[Address(RVA = "0x1015BE674", Offset = "0x15BE674", VA = "0x1015BE674")]
		public void SetMoveX(float move)
		{
		}

		// Token: 0x06003E94 RID: 16020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394A")]
		[Address(RVA = "0x1015BE6AC", Offset = "0x15BE6AC", VA = "0x1015BE6AC")]
		public void SetMoveY(float move)
		{
		}

		// Token: 0x06003E95 RID: 16021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394B")]
		[Address(RVA = "0x1015BE6E4", Offset = "0x15BE6E4", VA = "0x1015BE6E4")]
		public void SetMoveEaseTypeX(iTween.EaseType easeType)
		{
		}

		// Token: 0x06003E96 RID: 16022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394C")]
		[Address(RVA = "0x1015BE714", Offset = "0x15BE714", VA = "0x1015BE714")]
		public void SetMoveEaseTypeY(iTween.EaseType easeType)
		{
		}

		// Token: 0x06003E97 RID: 16023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394D")]
		[Address(RVA = "0x1015BE744", Offset = "0x15BE744", VA = "0x1015BE744")]
		public void SetMoveLoopTypeX(iTween.LoopType loopType)
		{
		}

		// Token: 0x06003E98 RID: 16024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394E")]
		[Address(RVA = "0x1015BE774", Offset = "0x15BE774", VA = "0x1015BE774")]
		public void SetMoveLoopTypeY(iTween.LoopType loopType)
		{
		}

		// Token: 0x06003E99 RID: 16025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600394F")]
		[Address(RVA = "0x1015BE7A4", Offset = "0x15BE7A4", VA = "0x1015BE7A4")]
		public void Play()
		{
		}

		// Token: 0x06003E9A RID: 16026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003950")]
		[Address(RVA = "0x1015C12DC", Offset = "0x15C12DC", VA = "0x1015C12DC")]
		private void ApplyBeforePlay()
		{
		}

		// Token: 0x06003E9B RID: 16027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003951")]
		[Address(RVA = "0x1015C1628", Offset = "0x15C1628", VA = "0x1015C1628")]
		private void SetImageAxisDefaultSizeConstant(eAxis axis, float value)
		{
		}

		// Token: 0x06003E9C RID: 16028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003952")]
		[Address(RVA = "0x1015C1844", Offset = "0x15C1844", VA = "0x1015C1844")]
		private void SetSizeFitterActive(bool active)
		{
		}

		// Token: 0x06003E9D RID: 16029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003953")]
		[Address(RVA = "0x1015C1538", Offset = "0x15C1538", VA = "0x1015C1538")]
		private void SetSizeFitterFitMode(eAxis axis, ContentSizeFitter.FitMode fitMode)
		{
		}

		// Token: 0x06003E9E RID: 16030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003954")]
		[Address(RVA = "0x1015C1668", Offset = "0x15C1668", VA = "0x1015C1668")]
		private void SetImageSize(eAxis axis, float value)
		{
		}

		// Token: 0x06003E9F RID: 16031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003955")]
		[Address(RVA = "0x1015C18F4", Offset = "0x15C18F4", VA = "0x1015C18F4")]
		public UIParticle()
		{
		}

		// Token: 0x04004DDC RID: 19932
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036D1")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100125480", Offset = "0x125480")]
		private Image m_Image;

		// Token: 0x04004DDD RID: 19933
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036D2")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001254CC", Offset = "0x1254CC")]
		private ContentSizeFitter m_SizeFitter;

		// Token: 0x04004DDE RID: 19934
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40036D3")]
		[SerializeField]
		private float m_ActionTime;

		// Token: 0x04004DDF RID: 19935
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40036D4")]
		[SerializeField]
		private UIParticle.UIParticleActionInformation m_MoveX;

		// Token: 0x04004DE0 RID: 19936
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40036D5")]
		[SerializeField]
		private UIParticle.UIParticleActionInformation m_MoveY;

		// Token: 0x04004DE1 RID: 19937
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40036D6")]
		[SerializeField]
		private UIParticle.ImageAxisDefaultSizeInformation m_ImageDefaultSizeInformation;

		// Token: 0x02000D53 RID: 3411
		[Token(Token = "0x20010F4")]
		[Serializable]
		public enum eImageAxisDefaultSizeType
		{
			// Token: 0x04004DE3 RID: 19939
			[Token(Token = "0x4006A5B")]
			Error,
			// Token: 0x04004DE4 RID: 19940
			[Token(Token = "0x4006A5C")]
			SpriteSize,
			// Token: 0x04004DE5 RID: 19941
			[Token(Token = "0x4006A5D")]
			Constant,
			// Token: 0x04004DE6 RID: 19942
			[Token(Token = "0x4006A5E")]
			ParentSize
		}

		// Token: 0x02000D54 RID: 3412
		[Token(Token = "0x20010F5")]
		[Serializable]
		public class ImageAxisDefaultSizeInformation
		{
			// Token: 0x06003EA1 RID: 16033 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061FD")]
			[Address(RVA = "0x1015BE65C", Offset = "0x15BE65C", VA = "0x1015BE65C")]
			public ImageAxisDefaultSizeInformation()
			{
			}

			// Token: 0x04004DE7 RID: 19943
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A5F")]
			[SerializeField]
			public RectTransform m_ParentRectTransform;

			// Token: 0x04004DE8 RID: 19944
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A60")]
			[SerializeField]
			public Sprite m_Sprite;

			// Token: 0x04004DE9 RID: 19945
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006A61")]
			[SerializeField]
			public UIParticle.ImageAxisDefaultSizeInformationValue m_Value;
		}

		// Token: 0x02000D55 RID: 3413
		[Token(Token = "0x20010F6")]
		[Serializable]
		public class ImageAxisDefaultSizeInformationValue
		{
			// Token: 0x06003EA2 RID: 16034 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061FE")]
			[Address(RVA = "0x1015C1230", Offset = "0x15C1230", VA = "0x1015C1230")]
			public ImageAxisDefaultSizeInformationValue()
			{
			}

			// Token: 0x04004DEA RID: 19946
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A62")]
			[SerializeField]
			public UIParticle.eImageAxisDefaultSizeType m_ImageDefaultSizeType;

			// Token: 0x04004DEB RID: 19947
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006A63")]
			[SerializeField]
			public float m_X;

			// Token: 0x04004DEC RID: 19948
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A64")]
			[SerializeField]
			public float m_Y;
		}

		// Token: 0x02000D56 RID: 3414
		[Token(Token = "0x20010F7")]
		[Serializable]
		public class UIParticleActionInformation
		{
			// Token: 0x06003EA3 RID: 16035 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061FF")]
			[Address(RVA = "0x1015C19E4", Offset = "0x15C19E4", VA = "0x1015C19E4")]
			public UIParticleActionInformation()
			{
			}

			// Token: 0x04004DED RID: 19949
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A65")]
			[SerializeField]
			public iTweenMoveToWrapper m_Tween;

			// Token: 0x04004DEE RID: 19950
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A66")]
			[SerializeField]
			public float m_Move;

			// Token: 0x04004DEF RID: 19951
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006A67")]
			[SerializeField]
			public iTween.EaseType m_EaseType;

			// Token: 0x04004DF0 RID: 19952
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006A68")]
			[SerializeField]
			public iTween.LoopType m_LoopType;
		}
	}
}
