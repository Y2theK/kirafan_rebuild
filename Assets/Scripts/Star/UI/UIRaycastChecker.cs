﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000E2C RID: 3628
	[Token(Token = "0x20009B6")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D36C", Offset = "0x11D36C")]
	[StructLayout(3)]
	public class UIRaycastChecker : MonoBehaviour
	{
		// Token: 0x0600433A RID: 17210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB2")]
		[Address(RVA = "0x1015C19F4", Offset = "0x15C19F4", VA = "0x1015C19F4")]
		private void Start()
		{
		}

		// Token: 0x0600433B RID: 17211 RVA: 0x000196E0 File Offset: 0x000178E0
		[Token(Token = "0x6003DB3")]
		[Address(RVA = "0x1015C1A98", Offset = "0x15C1A98", VA = "0x1015C1A98")]
		public bool Check()
		{
			return default(bool);
		}

		// Token: 0x0600433C RID: 17212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB4")]
		[Address(RVA = "0x1015C1DC0", Offset = "0x15C1DC0", VA = "0x1015C1DC0")]
		public UIRaycastChecker()
		{
		}

		// Token: 0x0400538D RID: 21389
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B19")]
		private InvisibleGraphic m_Graph;

		// Token: 0x0400538E RID: 21390
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B1A")]
		private RectTransform m_Rect;
	}
}
