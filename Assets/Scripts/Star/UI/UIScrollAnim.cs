﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D5E RID: 3422
	[Token(Token = "0x2000929")]
	[StructLayout(3)]
	public class UIScrollAnim : MonoBehaviour
	{
		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x06003EDC RID: 16092 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000474")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x600398B")]
			[Address(RVA = "0x1015C1DC8", Offset = "0x15C1DC8", VA = "0x1015C1DC8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003EDD RID: 16093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600398C")]
		[Address(RVA = "0x1015C1E60", Offset = "0x15C1E60", VA = "0x1015C1E60")]
		private void Start()
		{
		}

		// Token: 0x06003EDE RID: 16094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600398D")]
		[Address(RVA = "0x1015C1E9C", Offset = "0x15C1E9C", VA = "0x1015C1E9C")]
		private void Update()
		{
		}

		// Token: 0x06003EDF RID: 16095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600398E")]
		[Address(RVA = "0x1015C204C", Offset = "0x15C204C", VA = "0x1015C204C")]
		public UIScrollAnim()
		{
		}

		// Token: 0x04004E22 RID: 20002
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036F1")]
		[SerializeField]
		private float m_Duration;

		// Token: 0x04004E23 RID: 20003
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40036F2")]
		[SerializeField]
		private bool m_Reverse;

		// Token: 0x04004E24 RID: 20004
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036F3")]
		private float m_TimeCount;

		// Token: 0x04004E25 RID: 20005
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40036F4")]
		private Vector2 m_StartPos;

		// Token: 0x04004E26 RID: 20006
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40036F5")]
		private RectTransform m_RectTransform;
	}
}
