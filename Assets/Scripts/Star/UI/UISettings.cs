﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000E2D RID: 3629
	[Token(Token = "0x20009B7")]
	[StructLayout(3)]
	public class UISettings
	{
		// Token: 0x0600433D RID: 17213 RVA: 0x000196F8 File Offset: 0x000178F8
		[Token(Token = "0x6003DB5")]
		[Address(RVA = "0x1015C205C", Offset = "0x15C205C", VA = "0x1015C205C")]
		public static int GetTitleFilterMax()
		{
			return 0;
		}

		// Token: 0x0600433E RID: 17214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB6")]
		[Address(RVA = "0x1015C2170", Offset = "0x15C2170", VA = "0x1015C2170")]
		public void Setup(LocalSaveData localSaveData)
		{
		}

		// Token: 0x0600433F RID: 17215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB7")]
		[Address(RVA = "0x1015C2844", Offset = "0x15C2844", VA = "0x1015C2844")]
		public void SetFilterDefault(UISettings.eUsingType_Filter usingType)
		{
		}

		// Token: 0x06004340 RID: 17216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB8")]
		[Address(RVA = "0x1015C2944", Offset = "0x15C2944", VA = "0x1015C2944")]
		public void SetSortDefault(UISettings.eUsingType_Sort usingType)
		{
		}

		// Token: 0x06004341 RID: 17217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DB9")]
		[Address(RVA = "0x1015C2830", Offset = "0x15C2830", VA = "0x1015C2830")]
		public void Save()
		{
		}

		// Token: 0x06004342 RID: 17218 RVA: 0x00019710 File Offset: 0x00017910
		[Token(Token = "0x6003DBA")]
		[Address(RVA = "0x1015C29E8", Offset = "0x15C29E8", VA = "0x1015C29E8")]
		public ushort GetSortParamValue(UISettings.eUsingType_Sort type)
		{
			return 0;
		}

		// Token: 0x06004343 RID: 17219 RVA: 0x00019728 File Offset: 0x00017928
		[Token(Token = "0x6003DBB")]
		[Address(RVA = "0x1015C2AC8", Offset = "0x15C2AC8", VA = "0x1015C2AC8")]
		public ushort GetSortParamOrderType(UISettings.eUsingType_Sort type)
		{
			return 0;
		}

		// Token: 0x06004344 RID: 17220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DBC")]
		[Address(RVA = "0x1015C2B80", Offset = "0x15C2B80", VA = "0x1015C2B80")]
		public void SetSortParamValue(UISettings.eUsingType_Sort type, ushort value)
		{
		}

		// Token: 0x06004345 RID: 17221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DBD")]
		[Address(RVA = "0x1015C2C14", Offset = "0x15C2C14", VA = "0x1015C2C14")]
		public void SetSortParamOrderType(UISettings.eUsingType_Sort type, ushort orderType)
		{
		}

		// Token: 0x06004346 RID: 17222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DBE")]
		[Address(RVA = "0x1015C2CA8", Offset = "0x15C2CA8", VA = "0x1015C2CA8")]
		public void SetSortParamOrderType(UISettings.eUsingType_Sort usingType, UISettings.eSortOrderType orderType)
		{
		}

		// Token: 0x06004347 RID: 17223 RVA: 0x00019740 File Offset: 0x00017940
		[Token(Token = "0x6003DBF")]
		[Address(RVA = "0x1015C2CB0", Offset = "0x15C2CB0", VA = "0x1015C2CB0")]
		public UISettings.UsingSortMap GetUsingSortMap(UISettings.eUsingType_Sort type)
		{
			return default(UISettings.UsingSortMap);
		}

		// Token: 0x06004348 RID: 17224 RVA: 0x00019758 File Offset: 0x00017958
		[Token(Token = "0x6003DC0")]
		[Address(RVA = "0x1015C2D00", Offset = "0x15C2D00", VA = "0x1015C2D00")]
		public bool CheckForceAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category)
		{
			return default(bool);
		}

		// Token: 0x06004349 RID: 17225 RVA: 0x00019770 File Offset: 0x00017970
		[Token(Token = "0x6003DC1")]
		[Address(RVA = "0x1015C2F94", Offset = "0x15C2F94", VA = "0x1015C2F94")]
		public bool GetFilterFlagIsAll(UISettings.eUsingType_Filter usingType, int typeIndex)
		{
			return default(bool);
		}

		// Token: 0x0600434A RID: 17226 RVA: 0x00019788 File Offset: 0x00017988
		[Token(Token = "0x6003DC2")]
		[Address(RVA = "0x1015C3048", Offset = "0x15C3048", VA = "0x1015C3048")]
		public bool GetFilterFlagIsAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category)
		{
			return default(bool);
		}

		// Token: 0x0600434B RID: 17227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DC3")]
		[Address(RVA = "0x1015C3180", Offset = "0x15C3180", VA = "0x1015C3180")]
		public void SetFilterFlagIsAll(UISettings.eUsingType_Filter usingType, int typeIndex, bool isOn)
		{
		}

		// Token: 0x0600434C RID: 17228 RVA: 0x000197A0 File Offset: 0x000179A0
		[Token(Token = "0x6003DC4")]
		[Address(RVA = "0x1015C3244", Offset = "0x15C3244", VA = "0x1015C3244")]
		public bool GetFilterFlag(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex)
		{
			return default(bool);
		}

		// Token: 0x0600434D RID: 17229 RVA: 0x000197B8 File Offset: 0x000179B8
		[Token(Token = "0x6003DC5")]
		[Address(RVA = "0x1015C330C", Offset = "0x15C330C", VA = "0x1015C330C")]
		public bool GetFilterFlag(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category, int bitIndex)
		{
			return default(bool);
		}

		// Token: 0x0600434E RID: 17230 RVA: 0x000197D0 File Offset: 0x000179D0
		[Token(Token = "0x6003DC6")]
		[Address(RVA = "0x1015C33FC", Offset = "0x15C33FC", VA = "0x1015C33FC")]
		public bool GetFilterFlagWithAll(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex)
		{
			return default(bool);
		}

		// Token: 0x0600434F RID: 17231 RVA: 0x000197E8 File Offset: 0x000179E8
		[Token(Token = "0x6003DC7")]
		[Address(RVA = "0x1015C3458", Offset = "0x15C3458", VA = "0x1015C3458")]
		public bool GetFilterFlagWithAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category, int bitIndex)
		{
			return default(bool);
		}

		// Token: 0x06004350 RID: 17232 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003DC8")]
		[Address(RVA = "0x1015C3548", Offset = "0x15C3548", VA = "0x1015C3548")]
		public bool[] GetFilterFlagsWithAll(UISettings.eUsingType_Filter usingType, int typeIndex, UISettings.eFilterCategory category)
		{
			return null;
		}

		// Token: 0x06004351 RID: 17233 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003DC9")]
		[Address(RVA = "0x1015C2EDC", Offset = "0x15C2EDC", VA = "0x1015C2EDC")]
		public bool[] GetFilterFlagsWithAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category)
		{
			return null;
		}

		// Token: 0x06004352 RID: 17234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DCA")]
		[Address(RVA = "0x1015C3740", Offset = "0x15C3740", VA = "0x1015C3740")]
		public void SetFilterFlag(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex, bool isOn)
		{
		}

		// Token: 0x06004353 RID: 17235 RVA: 0x00019800 File Offset: 0x00017A00
		[Token(Token = "0x6003DCB")]
		[Address(RVA = "0x1015C3130", Offset = "0x15C3130", VA = "0x1015C3130")]
		public UISettings.UsingFilterMap GetUsingFilterMap(UISettings.eUsingType_Filter usingType)
		{
			return default(UISettings.UsingFilterMap);
		}

		// Token: 0x06004354 RID: 17236 RVA: 0x00019818 File Offset: 0x00017A18
		[Token(Token = "0x6003DCC")]
		[Address(RVA = "0x1015C36F0", Offset = "0x15C36F0", VA = "0x1015C36F0")]
		public int GetFilterCategoryMax(UISettings.eFilterCategory category)
		{
			return 0;
		}

		// Token: 0x06004355 RID: 17237 RVA: 0x00019830 File Offset: 0x00017A30
		[Token(Token = "0x6003DCD")]
		[Address(RVA = "0x1015C380C", Offset = "0x15C380C", VA = "0x1015C380C")]
		public int GetFilterCategoryButtonNum(UISettings.eFilterCategory category)
		{
			return 0;
		}

		// Token: 0x06004356 RID: 17238 RVA: 0x00019848 File Offset: 0x00017A48
		[Token(Token = "0x6003DCE")]
		[Address(RVA = "0x1015C391C", Offset = "0x15C391C", VA = "0x1015C391C")]
		public bool CheckFilterFlg(UISettings.eUsingType_Filter usingType)
		{
			return default(bool);
		}

		// Token: 0x06004357 RID: 17239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DCF")]
		[Address(RVA = "0x1015C39B4", Offset = "0x15C39B4", VA = "0x1015C39B4")]
		public void Clear()
		{
		}

		// Token: 0x06004358 RID: 17240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DD0")]
		[Address(RVA = "0x1015C3B3C", Offset = "0x15C3B3C", VA = "0x1015C3B3C")]
		public UISettings()
		{
		}

		// Token: 0x0400538F RID: 21391
		[Token(Token = "0x4003B1B")]
		public static eRare RARE_CHARA_MIN;

		// Token: 0x04005390 RID: 21392
		[Token(Token = "0x4003B1C")]
		public static eRare RARE_CHARA_MAX;

		// Token: 0x04005391 RID: 21393
		[Token(Token = "0x4003B1D")]
		public static eRare RARE_WEAPON_MIN;

		// Token: 0x04005392 RID: 21394
		[Token(Token = "0x4003B1E")]
		public static eRare RARE_WEAPON_MAX;

		// Token: 0x04005393 RID: 21395
		[Token(Token = "0x4003B1F")]
		public static eRare HIGH_RARE_CHARA_MIN;

		// Token: 0x04005394 RID: 21396
		[Token(Token = "0x4003B20")]
		public static eRare HIGH_RARE_CHARA_MAX;

		// Token: 0x04005395 RID: 21397
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003B21")]
		private LocalSaveData m_LocalSaveData;

		// Token: 0x04005396 RID: 21398
		[Token(Token = "0x4003B22")]
		public const int USING_TYPE_SORT_NUM = 3;

		// Token: 0x04005397 RID: 21399
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B23")]
		private readonly UISettings.UsingSortMap[] UsingSortMaps;

		// Token: 0x04005398 RID: 21400
		[Token(Token = "0x4003B24")]
		public const int FILTER_CATEGORY_NUM = 16;

		// Token: 0x04005399 RID: 21401
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B25")]
		private int[] FilterCategoryMaxs;

		// Token: 0x0400539A RID: 21402
		[Token(Token = "0x4003B26")]
		public const int USING_TYPE_FILTER_NUM = 12;

		// Token: 0x0400539B RID: 21403
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B27")]
		private readonly UISettings.UsingFilterMap[] UsingFilterMaps;

		// Token: 0x02000E2E RID: 3630
		[Token(Token = "0x200113C")]
		public enum eSortOrderType
		{
			// Token: 0x0400539D RID: 21405
			[Token(Token = "0x4006BBE")]
			Ascending,
			// Token: 0x0400539E RID: 21406
			[Token(Token = "0x4006BBF")]
			Descending,
			// Token: 0x0400539F RID: 21407
			[Token(Token = "0x4006BC0")]
			Num
		}

		// Token: 0x02000E2F RID: 3631
		[Token(Token = "0x200113D")]
		public enum eSortValue_Chara
		{
			// Token: 0x040053A1 RID: 21409
			[Token(Token = "0x4006BC2")]
			Level,
			// Token: 0x040053A2 RID: 21410
			[Token(Token = "0x4006BC3")]
			Rare,
			// Token: 0x040053A3 RID: 21411
			[Token(Token = "0x4006BC4")]
			Cost,
			// Token: 0x040053A4 RID: 21412
			[Token(Token = "0x4006BC5")]
			Title,
			// Token: 0x040053A5 RID: 21413
			[Token(Token = "0x4006BC6")]
			GetTime,
			// Token: 0x040053A6 RID: 21414
			[Token(Token = "0x4006BC7")]
			Hp,
			// Token: 0x040053A7 RID: 21415
			[Token(Token = "0x4006BC8")]
			Atk,
			// Token: 0x040053A8 RID: 21416
			[Token(Token = "0x4006BC9")]
			Mgc,
			// Token: 0x040053A9 RID: 21417
			[Token(Token = "0x4006BCA")]
			Def,
			// Token: 0x040053AA RID: 21418
			[Token(Token = "0x4006BCB")]
			MDef,
			// Token: 0x040053AB RID: 21419
			[Token(Token = "0x4006BCC")]
			Spd,
			// Token: 0x040053AC RID: 21420
			[Token(Token = "0x4006BCD")]
			EventDrop,
			// Token: 0x040053AD RID: 21421
			[Token(Token = "0x4006BCE")]
			FriendShip,
			// Token: 0x040053AE RID: 21422
			[Token(Token = "0x4006BCF")]
			Arousal,
			// Token: 0x040053AF RID: 21423
			[Token(Token = "0x4006BD0")]
			Num
		}

		// Token: 0x02000E30 RID: 3632
		[Token(Token = "0x200113E")]
		public enum eSortValue_Weapon
		{
			// Token: 0x040053B1 RID: 21425
			[Token(Token = "0x4006BD2")]
			Level,
			// Token: 0x040053B2 RID: 21426
			[Token(Token = "0x4006BD3")]
			Rare,
			// Token: 0x040053B3 RID: 21427
			[Token(Token = "0x4006BD4")]
			Cost,
			// Token: 0x040053B4 RID: 21428
			[Token(Token = "0x4006BD5")]
			HaveNum,
			// Token: 0x040053B5 RID: 21429
			[Token(Token = "0x4006BD6")]
			Get,
			// Token: 0x040053B6 RID: 21430
			[Token(Token = "0x4006BD7")]
			Atk,
			// Token: 0x040053B7 RID: 21431
			[Token(Token = "0x4006BD8")]
			Mgc,
			// Token: 0x040053B8 RID: 21432
			[Token(Token = "0x4006BD9")]
			Def,
			// Token: 0x040053B9 RID: 21433
			[Token(Token = "0x4006BDA")]
			MDef,
			// Token: 0x040053BA RID: 21434
			[Token(Token = "0x4006BDB")]
			Num
		}

		// Token: 0x02000E31 RID: 3633
		[Token(Token = "0x200113F")]
		public enum eSortValue_Support
		{
			// Token: 0x040053BC RID: 21436
			[Token(Token = "0x4006BDD")]
			Level,
			// Token: 0x040053BD RID: 21437
			[Token(Token = "0x4006BDE")]
			Rare,
			// Token: 0x040053BE RID: 21438
			[Token(Token = "0x4006BDF")]
			Cost,
			// Token: 0x040053BF RID: 21439
			[Token(Token = "0x4006BE0")]
			HP,
			// Token: 0x040053C0 RID: 21440
			[Token(Token = "0x4006BE1")]
			Atk,
			// Token: 0x040053C1 RID: 21441
			[Token(Token = "0x4006BE2")]
			Mgc,
			// Token: 0x040053C2 RID: 21442
			[Token(Token = "0x4006BE3")]
			Def,
			// Token: 0x040053C3 RID: 21443
			[Token(Token = "0x4006BE4")]
			MDef,
			// Token: 0x040053C4 RID: 21444
			[Token(Token = "0x4006BE5")]
			Spd,
			// Token: 0x040053C5 RID: 21445
			[Token(Token = "0x4006BE6")]
			Num
		}

		// Token: 0x02000E32 RID: 3634
		[Token(Token = "0x2001140")]
		public enum eUsingType_Sort
		{
			// Token: 0x040053C7 RID: 21447
			[Token(Token = "0x4006BE8")]
			None = -1,
			// Token: 0x040053C8 RID: 21448
			[Token(Token = "0x4006BE9")]
			CharaListView,
			// Token: 0x040053C9 RID: 21449
			[Token(Token = "0x4006BEA")]
			WeaponList,
			// Token: 0x040053CA RID: 21450
			[Token(Token = "0x4006BEB")]
			SupportSelect,
			// Token: 0x040053CB RID: 21451
			[Token(Token = "0x4006BEC")]
			Num
		}

		// Token: 0x02000E33 RID: 3635
		[Token(Token = "0x2001141")]
		public struct UsingSortMap
		{
			// Token: 0x0600435A RID: 17242 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600623B")]
			[Address(RVA = "0x100038550", Offset = "0x38550", VA = "0x100038550")]
			public UsingSortMap(ushort sortValueMax)
			{
			}

			// Token: 0x040053CC RID: 21452
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006BED")]
			public ushort m_SortValueMax;
		}

		// Token: 0x02000E34 RID: 3636
		[Token(Token = "0x2001142")]
		public enum eFilterFlag_DeadLine
		{
			// Token: 0x040053CE RID: 21454
			[Token(Token = "0x4006BEF")]
			Exist,
			// Token: 0x040053CF RID: 21455
			[Token(Token = "0x4006BF0")]
			NoExist,
			// Token: 0x040053D0 RID: 21456
			[Token(Token = "0x4006BF1")]
			Num
		}

		// Token: 0x02000E35 RID: 3637
		[Token(Token = "0x2001143")]
		public enum eFilterFlag_PresentType
		{
			// Token: 0x040053D2 RID: 21458
			[Token(Token = "0x4006BF3")]
			Chara,
			// Token: 0x040053D3 RID: 21459
			[Token(Token = "0x4006BF4")]
			Weapon,
			// Token: 0x040053D4 RID: 21460
			[Token(Token = "0x4006BF5")]
			Item,
			// Token: 0x040053D5 RID: 21461
			[Token(Token = "0x4006BF6")]
			Gold,
			// Token: 0x040053D6 RID: 21462
			[Token(Token = "0x4006BF7")]
			Etc,
			// Token: 0x040053D7 RID: 21463
			[Token(Token = "0x4006BF8")]
			Num
		}

		// Token: 0x02000E36 RID: 3638
		[Token(Token = "0x2001144")]
		public enum eFilterFlag_AbilitySphereDisplay
		{
			// Token: 0x040053D9 RID: 21465
			[Token(Token = "0x4006BFA")]
			Equippable,
			// Token: 0x040053DA RID: 21466
			[Token(Token = "0x4006BFB")]
			Num
		}

		// Token: 0x02000E37 RID: 3639
		[Token(Token = "0x2001145")]
		public enum eFilterFlag_AbilitySphere
		{
			// Token: 0x040053DC RID: 21468
			[Token(Token = "0x4006BFD")]
			Hp,
			// Token: 0x040053DD RID: 21469
			[Token(Token = "0x4006BFE")]
			Atk,
			// Token: 0x040053DE RID: 21470
			[Token(Token = "0x4006BFF")]
			MAtk,
			// Token: 0x040053DF RID: 21471
			[Token(Token = "0x4006C00")]
			Def,
			// Token: 0x040053E0 RID: 21472
			[Token(Token = "0x4006C01")]
			MDef,
			// Token: 0x040053E1 RID: 21473
			[Token(Token = "0x4006C02")]
			Skill,
			// Token: 0x040053E2 RID: 21474
			[Token(Token = "0x4006C03")]
			Num
		}

		// Token: 0x02000E38 RID: 3640
		[Token(Token = "0x2001146")]
		public enum eFilterCategory
		{
			// Token: 0x040053E4 RID: 21476
			[Token(Token = "0x4006C05")]
			Class,
			// Token: 0x040053E5 RID: 21477
			[Token(Token = "0x4006C06")]
			RareChara,
			// Token: 0x040053E6 RID: 21478
			[Token(Token = "0x4006C07")]
			RareWeapon,
			// Token: 0x040053E7 RID: 21479
			[Token(Token = "0x4006C08")]
			Element,
			// Token: 0x040053E8 RID: 21480
			[Token(Token = "0x4006C09")]
			Title,
			// Token: 0x040053E9 RID: 21481
			[Token(Token = "0x4006C0A")]
			DeadLine,
			// Token: 0x040053EA RID: 21482
			[Token(Token = "0x4006C0B")]
			PresentType,
			// Token: 0x040053EB RID: 21483
			[Token(Token = "0x4006C0C")]
			StatusPriority,
			// Token: 0x040053EC RID: 21484
			[Token(Token = "0x4006C0D")]
			TitleSelect,
			// Token: 0x040053ED RID: 21485
			[Token(Token = "0x4006C0E")]
			RoomObj_Season,
			// Token: 0x040053EE RID: 21486
			[Token(Token = "0x4006C0F")]
			RoomObj_Motif,
			// Token: 0x040053EF RID: 21487
			[Token(Token = "0x4006C10")]
			RoomObj_CharaAction,
			// Token: 0x040053F0 RID: 21488
			[Token(Token = "0x4006C11")]
			RoomObj_Category,
			// Token: 0x040053F1 RID: 21489
			[Token(Token = "0x4006C12")]
			HighRareChara,
			// Token: 0x040053F2 RID: 21490
			[Token(Token = "0x4006C13")]
			AbilitySphereDisplay,
			// Token: 0x040053F3 RID: 21491
			[Token(Token = "0x4006C14")]
			AbilitySphere,
			// Token: 0x040053F4 RID: 21492
			[Token(Token = "0x4006C15")]
			Num
		}

		// Token: 0x02000E39 RID: 3641
		[Token(Token = "0x2001147")]
		public enum eUsingType_Filter
		{
			// Token: 0x040053F6 RID: 21494
			[Token(Token = "0x4006C17")]
			None = -1,
			// Token: 0x040053F7 RID: 21495
			[Token(Token = "0x4006C18")]
			CharaListView,
			// Token: 0x040053F8 RID: 21496
			[Token(Token = "0x4006C19")]
			WeaponList,
			// Token: 0x040053F9 RID: 21497
			[Token(Token = "0x4006C1A")]
			WeaponEquip,
			// Token: 0x040053FA RID: 21498
			[Token(Token = "0x4006C1B")]
			SupportSelect,
			// Token: 0x040053FB RID: 21499
			[Token(Token = "0x4006C1C")]
			SupportTitle,
			// Token: 0x040053FC RID: 21500
			[Token(Token = "0x4006C1D")]
			Present,
			// Token: 0x040053FD RID: 21501
			[Token(Token = "0x4006C1E")]
			Recommend,
			// Token: 0x040053FE RID: 21502
			[Token(Token = "0x4006C1F")]
			RoomObjectShop,
			// Token: 0x040053FF RID: 21503
			[Token(Token = "0x4006C20")]
			RoomObjectStrage,
			// Token: 0x04005400 RID: 21504
			[Token(Token = "0x4006C21")]
			ContentRoomMember,
			// Token: 0x04005401 RID: 21505
			[Token(Token = "0x4006C22")]
			Ability,
			// Token: 0x04005402 RID: 21506
			[Token(Token = "0x4006C23")]
			AbilitySphere,
			// Token: 0x04005403 RID: 21507
			[Token(Token = "0x4006C24")]
			Num
		}

		// Token: 0x02000E3A RID: 3642
		[Token(Token = "0x2001148")]
		public struct UsingFilterMap
		{
			// Token: 0x0600435B RID: 17243 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600623C")]
			[Address(RVA = "0x100038548", Offset = "0x38548", VA = "0x100038548")]
			public UsingFilterMap(UISettings.eFilterCategory[] categories)
			{
			}

			// Token: 0x04005404 RID: 21508
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006C25")]
			public UISettings.eFilterCategory[] m_Categories;
		}
	}
}
