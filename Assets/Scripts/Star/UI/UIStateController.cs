﻿using System;
using UnityEngine;

namespace Star.UI {
    public class UIStateController : MonoBehaviour {
        [SerializeField] private UIGroup[] m_UIGroups;

        private StateData[] m_StateDatas;
        private int m_NowState = -1;

        public int NowState => m_NowState;

        public void Setup(int stateNum, int startState = -1, bool transit = false) {
            m_StateDatas = new StateData[stateNum];
            for (int i = 0; i < stateNum; i++) {
                m_StateDatas[i].m_TransitDest = new bool[stateNum];
                for (int j = 0; j < stateNum; j++) {
                    m_StateDatas[i].m_TransitDest[j] = transit;
                }
            }
            m_NowState = startState;
        }

        public void AddTransit(int sourceStateID, int destStateID, bool flg = true) {
            if (sourceStateID < m_StateDatas.Length && destStateID < m_StateDatas.Length) {
                m_StateDatas[sourceStateID].m_TransitDest[destStateID] = flg;
            }
        }

        public void AddTransitEach(int sourceStateID, int destStateID, bool flg = true) {
            if (sourceStateID < m_StateDatas.Length && destStateID < m_StateDatas.Length) {
                m_StateDatas[sourceStateID].m_TransitDest[destStateID] = flg;
                m_StateDatas[destStateID].m_TransitDest[sourceStateID] = flg;
            }
        }

        public void AddOnEnter(int stateID, Action OnEnter) {
            m_StateDatas[stateID].OnEnter += OnEnter;
        }

        public void AddOnExit(int stateID, Action OnExit) {
            m_StateDatas[stateID].OnExit += OnExit;
        }

        public bool ChangeState(int state) {
            if (state >= 0 && state < m_StateDatas.Length && m_StateDatas[m_NowState].m_TransitDest[state]) {
                DoTransit(state);
                return true;
            }
            return false;
        }

        public void ForceChangeState(int state) {
            if (state >= 0 && state < m_StateDatas.Length && m_NowState != state) {
                DoTransit(state);
            }
        }

        private void DoTransit(int state) {
            m_StateDatas[m_NowState].OnExit.Call();
            SetEnableUIGroups(false);
            m_NowState = state;
            m_StateDatas[state].OnEnter.Call();
        }

        public void SetEnableUIGroups(bool flg) {
            if (m_UIGroups != null && m_UIGroups.Length > 0) {
                for (int i = 0; i < m_UIGroups.Length; i++) {
                    m_UIGroups[i].SetEnableInput(flg);
                }
            }
        }

        public bool IsPlayingInOut() {
            for (int i = 0; i < m_UIGroups.Length; i++) {
                if (m_UIGroups[i].IsPlayingInOut) {
                    return true;
                }
            }
            return false;
        }

        private struct StateData {
            public bool[] m_TransitDest;
            public Action OnEnter;
            public Action OnExit;
        }
    }
}
