﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D00 RID: 3328
	[Token(Token = "0x20008E8")]
	[StructLayout(3)]
	public class VariableIcon : MonoBehaviour
	{
		// Token: 0x06003CBF RID: 15551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377C")]
		[Address(RVA = "0x1015C4B50", Offset = "0x15C4B50", VA = "0x1015C4B50")]
		private void Prepare()
		{
		}

		// Token: 0x06003CC0 RID: 15552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377D")]
		[Address(RVA = "0x1015C4C98", Offset = "0x15C4C98", VA = "0x1015C4C98")]
		private void DestroyAllIcon()
		{
		}

		// Token: 0x06003CC1 RID: 15553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377E")]
		[Address(RVA = "0x1015C4E78", Offset = "0x15C4E78", VA = "0x1015C4E78")]
		public void ApplyChara(int charaID)
		{
		}

		// Token: 0x06003CC2 RID: 15554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600377F")]
		[Address(RVA = "0x1015C4F18", Offset = "0x15C4F18", VA = "0x1015C4F18")]
		public void ApplyItem(int itemID)
		{
		}

		// Token: 0x06003CC3 RID: 15555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003780")]
		[Address(RVA = "0x1015C4FB8", Offset = "0x15C4FB8", VA = "0x1015C4FB8")]
		public void ApplyWeapon(int weaponID)
		{
		}

		// Token: 0x06003CC4 RID: 15556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003781")]
		[Address(RVA = "0x1015C5164", Offset = "0x15C5164", VA = "0x1015C5164")]
		public void ApplyTownObject(int townObjectID, int lv = 1)
		{
		}

		// Token: 0x06003CC5 RID: 15557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003782")]
		[Address(RVA = "0x1015C521C", Offset = "0x15C521C", VA = "0x1015C521C")]
		public void ApplyRoomObject(eRoomObjectCategory category, int roomObjectID)
		{
		}

		// Token: 0x06003CC6 RID: 15558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003783")]
		[Address(RVA = "0x1015C52D4", Offset = "0x15C52D4", VA = "0x1015C52D4")]
		public void ApplyOrb(int orbID)
		{
		}

		// Token: 0x06003CC7 RID: 15559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003784")]
		[Address(RVA = "0x1015C5374", Offset = "0x15C5374", VA = "0x1015C5374")]
		public void ApplyGold()
		{
		}

		// Token: 0x06003CC8 RID: 15560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003785")]
		[Address(RVA = "0x1015C5408", Offset = "0x15C5408", VA = "0x1015C5408")]
		public void ApplyGem()
		{
		}

		// Token: 0x06003CC9 RID: 15561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003786")]
		[Address(RVA = "0x1015C549C", Offset = "0x15C549C", VA = "0x1015C549C")]
		public void ApplyKirara()
		{
		}

		// Token: 0x06003CCA RID: 15562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003787")]
		[Address(RVA = "0x1015C5530", Offset = "0x15C5530", VA = "0x1015C5530")]
		public void ApplyAchievement()
		{
		}

		// Token: 0x06003CCB RID: 15563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003788")]
		[Address(RVA = "0x1015C55C0", Offset = "0x15C55C0", VA = "0x1015C55C0")]
		public void Destroy()
		{
		}

		// Token: 0x06003CCC RID: 15564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003789")]
		[Address(RVA = "0x1015C55D0", Offset = "0x15C55D0", VA = "0x1015C55D0")]
		public VariableIcon()
		{
		}

		// Token: 0x04004C07 RID: 19463
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400356F")]
		[SerializeField]
		private Sprite m_GoldSprite;

		// Token: 0x04004C08 RID: 19464
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003570")]
		[SerializeField]
		private Sprite m_GemSprite;

		// Token: 0x04004C09 RID: 19465
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003571")]
		[SerializeField]
		private Sprite m_KPSprite;

		// Token: 0x04004C0A RID: 19466
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003572")]
		[SerializeField]
		private Sprite m_AchievementSprite;

		// Token: 0x04004C0B RID: 19467
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003573")]
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x04004C0C RID: 19468
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003574")]
		private VariableIcon.eType m_Type;

		// Token: 0x04004C0D RID: 19469
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003575")]
		private int m_ID;

		// Token: 0x04004C0E RID: 19470
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003576")]
		private int m_SubID;

		// Token: 0x04004C0F RID: 19471
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003577")]
		private CharaIcon m_CharaIcon;

		// Token: 0x04004C10 RID: 19472
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003578")]
		private ItemIcon m_ItemIcon;

		// Token: 0x04004C11 RID: 19473
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003579")]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04004C12 RID: 19474
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400357A")]
		private TownObjectIcon m_TownObjectIcon;

		// Token: 0x04004C13 RID: 19475
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400357B")]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x04004C14 RID: 19476
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400357C")]
		private OrbIcon m_OrbIcon;

		// Token: 0x04004C15 RID: 19477
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400357D")]
		private bool m_IsDonePrepare;

		// Token: 0x02000D01 RID: 3329
		[Token(Token = "0x20010DF")]
		public enum eType
		{
			// Token: 0x04004C17 RID: 19479
			[Token(Token = "0x40069E8")]
			None,
			// Token: 0x04004C18 RID: 19480
			[Token(Token = "0x40069E9")]
			Chara,
			// Token: 0x04004C19 RID: 19481
			[Token(Token = "0x40069EA")]
			Item,
			// Token: 0x04004C1A RID: 19482
			[Token(Token = "0x40069EB")]
			Weapon,
			// Token: 0x04004C1B RID: 19483
			[Token(Token = "0x40069EC")]
			TownObject,
			// Token: 0x04004C1C RID: 19484
			[Token(Token = "0x40069ED")]
			RoomObject,
			// Token: 0x04004C1D RID: 19485
			[Token(Token = "0x40069EE")]
			Orb,
			// Token: 0x04004C1E RID: 19486
			[Token(Token = "0x40069EF")]
			Gem,
			// Token: 0x04004C1F RID: 19487
			[Token(Token = "0x40069F0")]
			Gold,
			// Token: 0x04004C20 RID: 19488
			[Token(Token = "0x40069F1")]
			Kirara,
			// Token: 0x04004C21 RID: 19489
			[Token(Token = "0x40069F2")]
			Achievement
		}
	}
}
