﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D02 RID: 3330
	[Token(Token = "0x20008E9")]
	[StructLayout(3)]
	public class VariableIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003CCD RID: 15565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378A")]
		[Address(RVA = "0x1015C55E0", Offset = "0x15C55E0", VA = "0x1015C55E0")]
		private void DestroyAllIcon()
		{
		}

		// Token: 0x06003CCE RID: 15566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378B")]
		[Address(RVA = "0x1015C58C8", Offset = "0x15C58C8", VA = "0x1015C58C8")]
		private void ResetParam()
		{
		}

		// Token: 0x06003CCF RID: 15567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378C")]
		[Address(RVA = "0x1015C58E4", Offset = "0x15C58E4", VA = "0x1015C58E4")]
		public void Destroy()
		{
		}

		// Token: 0x06003CD0 RID: 15568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378D")]
		[Address(RVA = "0x1015C591C", Offset = "0x15C591C", VA = "0x1015C591C")]
		public void ApplyChara(int charaID)
		{
		}

		// Token: 0x06003CD1 RID: 15569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378E")]
		[Address(RVA = "0x1015C5A24", Offset = "0x15C5A24", VA = "0x1015C5A24")]
		public void ApplyCharaMngID(long charaMngID)
		{
		}

		// Token: 0x06003CD2 RID: 15570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600378F")]
		[Address(RVA = "0x1015C5B2C", Offset = "0x15C5B2C", VA = "0x1015C5B2C")]
		public void ApplyItem(int itemID, bool appeal = false, bool visibleRarity = true)
		{
		}

		// Token: 0x06003CD3 RID: 15571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003790")]
		[Address(RVA = "0x1015C5C50", Offset = "0x15C5C50", VA = "0x1015C5C50")]
		public void ApplyWeapon(int weaponID, bool visibleRarity = true)
		{
		}

		// Token: 0x06003CD4 RID: 15572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003791")]
		[Address(RVA = "0x1015C5DD4", Offset = "0x15C5DD4", VA = "0x1015C5DD4")]
		public void ApplyTownObject(int townObjectID, int lv = 1)
		{
		}

		// Token: 0x06003CD5 RID: 15573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003792")]
		[Address(RVA = "0x1015C5EEC", Offset = "0x15C5EEC", VA = "0x1015C5EEC")]
		public void ApplyRoomObject(eRoomObjectCategory category, int roomObjectID)
		{
		}

		// Token: 0x06003CD6 RID: 15574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003793")]
		[Address(RVA = "0x1015C6004", Offset = "0x15C6004", VA = "0x1015C6004")]
		public void ApplyRoomObject(int accessKey)
		{
		}

		// Token: 0x06003CD7 RID: 15575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003794")]
		[Address(RVA = "0x1015C6044", Offset = "0x15C6044", VA = "0x1015C6044")]
		public void ApplyOrb(int orbID)
		{
		}

		// Token: 0x06003CD8 RID: 15576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003795")]
		[Address(RVA = "0x1015C614C", Offset = "0x15C614C", VA = "0x1015C614C")]
		public void ApplyGold()
		{
		}

		// Token: 0x06003CD9 RID: 15577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003796")]
		[Address(RVA = "0x1015C61E4", Offset = "0x15C61E4", VA = "0x1015C61E4")]
		public void ApplyGem()
		{
		}

		// Token: 0x06003CDA RID: 15578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003797")]
		[Address(RVA = "0x1015C627C", Offset = "0x15C627C", VA = "0x1015C627C")]
		public void ApplyKirara()
		{
		}

		// Token: 0x06003CDB RID: 15579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003798")]
		[Address(RVA = "0x1015C6314", Offset = "0x15C6314", VA = "0x1015C6314")]
		public void ApplyPackageItem(int packageID)
		{
		}

		// Token: 0x06003CDC RID: 15580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003799")]
		[Address(RVA = "0x1015C641C", Offset = "0x15C641C", VA = "0x1015C641C")]
		public void ApplyAchievement()
		{
		}

		// Token: 0x06003CDD RID: 15581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600379A")]
		[Address(RVA = "0x1015C6514", Offset = "0x15C6514", VA = "0x1015C6514")]
		public void ApplyPresent(ePresentType presentType, int id)
		{
		}

		// Token: 0x06003CDE RID: 15582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600379B")]
		[Address(RVA = "0x1015C6668", Offset = "0x15C6668", VA = "0x1015C6668")]
		public VariableIconWithFrame()
		{
		}

		// Token: 0x04004C22 RID: 19490
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400357E")]
		[SerializeField]
		private PrefabCloner m_CharaIconWithFrameCloner;

		// Token: 0x04004C23 RID: 19491
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400357F")]
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04004C24 RID: 19492
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003580")]
		[SerializeField]
		private PrefabCloner m_WeaponIconWithFrameCloner;

		// Token: 0x04004C25 RID: 19493
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003581")]
		[SerializeField]
		private PrefabCloner m_TownObjectIconWithFrameCloner;

		// Token: 0x04004C26 RID: 19494
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003582")]
		[SerializeField]
		private PrefabCloner m_RoomObjectIconWithFrameCloner;

		// Token: 0x04004C27 RID: 19495
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003583")]
		[SerializeField]
		private PrefabCloner m_MasterOrbIconWithFrameCloner;

		// Token: 0x04004C28 RID: 19496
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003584")]
		[SerializeField]
		private PrefabCloner m_GoldCloner;

		// Token: 0x04004C29 RID: 19497
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003585")]
		[SerializeField]
		private PrefabCloner m_GemCloner;

		// Token: 0x04004C2A RID: 19498
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003586")]
		[SerializeField]
		private PrefabCloner m_KPCloner;

		// Token: 0x04004C2B RID: 19499
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003587")]
		[SerializeField]
		private PrefabCloner m_PackageIconWithFrameCloner;

		// Token: 0x04004C2C RID: 19500
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003588")]
		[SerializeField]
		private PrefabCloner m_AchievementIconWithFrameCloner;

		// Token: 0x04004C2D RID: 19501
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003589")]
		private VariableIconWithFrame.eType m_Type;

		// Token: 0x04004C2E RID: 19502
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x400358A")]
		private int m_ID;

		// Token: 0x04004C2F RID: 19503
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400358B")]
		private int m_SubID;

		// Token: 0x04004C30 RID: 19504
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400358C")]
		private long m_MngID;

		// Token: 0x02000D03 RID: 3331
		[Token(Token = "0x20010E0")]
		public enum eType
		{
			// Token: 0x04004C32 RID: 19506
			[Token(Token = "0x40069F4")]
			None,
			// Token: 0x04004C33 RID: 19507
			[Token(Token = "0x40069F5")]
			Chara,
			// Token: 0x04004C34 RID: 19508
			[Token(Token = "0x40069F6")]
			Item,
			// Token: 0x04004C35 RID: 19509
			[Token(Token = "0x40069F7")]
			Weapon,
			// Token: 0x04004C36 RID: 19510
			[Token(Token = "0x40069F8")]
			TownObject,
			// Token: 0x04004C37 RID: 19511
			[Token(Token = "0x40069F9")]
			RoomObject,
			// Token: 0x04004C38 RID: 19512
			[Token(Token = "0x40069FA")]
			Orb,
			// Token: 0x04004C39 RID: 19513
			[Token(Token = "0x40069FB")]
			Gem,
			// Token: 0x04004C3A RID: 19514
			[Token(Token = "0x40069FC")]
			Gold,
			// Token: 0x04004C3B RID: 19515
			[Token(Token = "0x40069FD")]
			Kirara,
			// Token: 0x04004C3C RID: 19516
			[Token(Token = "0x40069FE")]
			Package,
			// Token: 0x04004C3D RID: 19517
			[Token(Token = "0x40069FF")]
			Achievement
		}
	}
}
