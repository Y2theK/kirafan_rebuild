﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D61 RID: 3425
	[Token(Token = "0x200092B")]
	[StructLayout(3)]
	public class VerticalText : UIBehaviour, IMeshModifier
	{
		// Token: 0x06003EEC RID: 16108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600399B")]
		[Address(RVA = "0x1015C6680", Offset = "0x15C6680", VA = "0x1015C6680", Slot = "6")]
		protected override void Start()
		{
		}

		// Token: 0x06003EED RID: 16109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600399C")]
		[Address(RVA = "0x1015C6B18", Offset = "0x15C6B18", VA = "0x1015C6B18", Slot = "17")]
		public void ModifyMesh(Mesh mesh)
		{
		}

		// Token: 0x06003EEE RID: 16110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600399D")]
		[Address(RVA = "0x1015C6B1C", Offset = "0x15C6B1C", VA = "0x1015C6B1C", Slot = "18")]
		public void ModifyMesh(VertexHelper verts)
		{
		}

		// Token: 0x06003EEF RID: 16111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600399E")]
		[Address(RVA = "0x1015C6BD8", Offset = "0x15C6BD8", VA = "0x1015C6BD8")]
		private void Modify(List<UIVertex> vertexList)
		{
		}

		// Token: 0x06003EF0 RID: 16112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600399F")]
		[Address(RVA = "0x1015C7114", Offset = "0x15C7114", VA = "0x1015C7114")]
		public VerticalText()
		{
		}

		// Token: 0x04004E2D RID: 20013
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036F9")]
		private Text m_CacheText;

		// Token: 0x04004E2E RID: 20014
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036FA")]
		private List<char> m_NoRotateCharacter;

		// Token: 0x04004E2F RID: 20015
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40036FB")]
		private List<char> m_ShiftCharacter;

		// Token: 0x04004E30 RID: 20016
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40036FC")]
		private List<UIVertex> m_VertexList;
	}
}
