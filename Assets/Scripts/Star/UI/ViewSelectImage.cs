﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D88 RID: 3464
	[Token(Token = "0x2000946")]
	[StructLayout(3)]
	public class ViewSelectImage : MonoBehaviour
	{
		// Token: 0x06003FE7 RID: 16359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8C")]
		[Address(RVA = "0x1015C711C", Offset = "0x15C711C", VA = "0x1015C711C")]
		private void Awake()
		{
		}

		// Token: 0x06003FE8 RID: 16360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8D")]
		[Address(RVA = "0x1015C71B0", Offset = "0x15C71B0", VA = "0x1015C71B0")]
		public void Apply(int charaID)
		{
		}

		// Token: 0x06003FE9 RID: 16361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8E")]
		[Address(RVA = "0x1015C71F0", Offset = "0x15C71F0", VA = "0x1015C71F0")]
		public void OnDestroy()
		{
		}

		// Token: 0x06003FEA RID: 16362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8F")]
		[Address(RVA = "0x1015C7224", Offset = "0x15C7224", VA = "0x1015C7224")]
		private void Update()
		{
		}

		// Token: 0x06003FEB RID: 16363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A90")]
		[Address(RVA = "0x1015C731C", Offset = "0x15C731C", VA = "0x1015C731C")]
		public void UpdateScaling()
		{
		}

		// Token: 0x06003FEC RID: 16364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A91")]
		[Address(RVA = "0x1015C7644", Offset = "0x15C7644", VA = "0x1015C7644")]
		public void RequestScaling(float scale)
		{
		}

		// Token: 0x06003FED RID: 16365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A92")]
		[Address(RVA = "0x1015C766C", Offset = "0x15C766C", VA = "0x1015C766C")]
		public void OnClick()
		{
		}

		// Token: 0x06003FEE RID: 16366 RVA: 0x00018EE8 File Offset: 0x000170E8
		[Token(Token = "0x6003A93")]
		[Address(RVA = "0x1015C74EC", Offset = "0x15C74EC", VA = "0x1015C74EC")]
		private Vector2 GetImageSize(float scale)
		{
			return default(Vector2);
		}

		// Token: 0x06003FEF RID: 16367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A94")]
		[Address(RVA = "0x1015C7678", Offset = "0x15C7678", VA = "0x1015C7678")]
		public void SetAnimTimer(float timer)
		{
		}

		// Token: 0x06003FF0 RID: 16368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A95")]
		[Address(RVA = "0x1015C7680", Offset = "0x15C7680", VA = "0x1015C7680")]
		public ViewSelectImage()
		{
		}

		// Token: 0x04004F53 RID: 20307
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40037E3")]
		private readonly Rect IMAGE_SIZE_BASE;

		// Token: 0x04004F54 RID: 20308
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037E4")]
		[SerializeField]
		private CharaIllust m_imgCharaCard;

		// Token: 0x04004F55 RID: 20309
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40037E5")]
		[SerializeField]
		private Image m_imgLoading;

		// Token: 0x04004F56 RID: 20310
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40037E6")]
		[SerializeField]
		private AnimationCurve m_animCurveScaling;

		// Token: 0x04004F57 RID: 20311
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40037E7")]
		[SerializeField]
		private AnimationCurve m_animCurveImgColor;

		// Token: 0x04004F58 RID: 20312
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40037E8")]
		[SerializeField]
		private CustomButton m_button;

		// Token: 0x04004F59 RID: 20313
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40037E9")]
		[SerializeField]
		private float m_Velocity;

		// Token: 0x04004F5A RID: 20314
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40037EA")]
		private float m_AnimTimer;

		// Token: 0x04004F5B RID: 20315
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40037EB")]
		private float m_ScalingFactor;

		// Token: 0x04004F5C RID: 20316
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40037EC")]
		public Action m_OnClickCallBack;
	}
}
