﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D89 RID: 3465
	[Token(Token = "0x2000947")]
	[StructLayout(3)]
	public class ViewSelectPanel : MonoBehaviour
	{
		// Token: 0x06003FF1 RID: 16369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A96")]
		[Address(RVA = "0x1015C76E8", Offset = "0x15C76E8", VA = "0x1015C76E8")]
		private void Awake()
		{
		}

		// Token: 0x06003FF2 RID: 16370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A97")]
		[Address(RVA = "0x1015C76F0", Offset = "0x15C76F0", VA = "0x1015C76F0")]
		private void Update()
		{
		}

		// Token: 0x06003FF3 RID: 16371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A98")]
		[Address(RVA = "0x1015C7704", Offset = "0x15C7704", VA = "0x1015C7704")]
		public void UpdateMove()
		{
		}

		// Token: 0x06003FF4 RID: 16372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A99")]
		[Address(RVA = "0x1015C77C4", Offset = "0x15C77C4", VA = "0x1015C77C4")]
		public void RequestMoving(float dir)
		{
		}

		// Token: 0x06003FF5 RID: 16373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A9A")]
		[Address(RVA = "0x1015C77EC", Offset = "0x15C77EC", VA = "0x1015C77EC")]
		public void SetAnimTimer(float timer)
		{
		}

		// Token: 0x06003FF6 RID: 16374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A9B")]
		[Address(RVA = "0x1015C77F4", Offset = "0x15C77F4", VA = "0x1015C77F4")]
		public ViewSelectPanel()
		{
		}

		// Token: 0x04004F5D RID: 20317
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40037ED")]
		[SerializeField]
		private AnimationCurve m_moveAnimCurve;

		// Token: 0x04004F5E RID: 20318
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40037EE")]
		[SerializeField]
		private float m_Velocity;

		// Token: 0x04004F5F RID: 20319
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40037EF")]
		private float m_AnimTimer;

		// Token: 0x04004F60 RID: 20320
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037F0")]
		private float m_MovingFactor;
	}
}
