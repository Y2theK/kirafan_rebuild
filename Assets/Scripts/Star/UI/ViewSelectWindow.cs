﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D8A RID: 3466
	[Token(Token = "0x2000948")]
	[StructLayout(3)]
	public class ViewSelectWindow : UIGroup
	{
		// Token: 0x06003FF7 RID: 16375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A9C")]
		[Address(RVA = "0x1015C7804", Offset = "0x15C7804", VA = "0x1015C7804")]
		public void Setup(long managedCharaId)
		{
		}

		// Token: 0x06003FF8 RID: 16376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A9D")]
		[Address(RVA = "0x1015C7CB8", Offset = "0x15C7CB8", VA = "0x1015C7CB8")]
		public void OnClickNormalImage()
		{
		}

		// Token: 0x06003FF9 RID: 16377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A9E")]
		[Address(RVA = "0x1015C7D5C", Offset = "0x15C7D5C", VA = "0x1015C7D5C")]
		public void OnClickEvoImage()
		{
		}

		// Token: 0x06003FFA RID: 16378 RVA: 0x00018F00 File Offset: 0x00017100
		[Token(Token = "0x6003A9F")]
		[Address(RVA = "0x1015C7D4C", Offset = "0x15C7D4C", VA = "0x1015C7D4C")]
		public bool IsChangedViewCharaID()
		{
			return default(bool);
		}

		// Token: 0x06003FFB RID: 16379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA0")]
		[Address(RVA = "0x1015C7DF4", Offset = "0x15C7DF4", VA = "0x1015C7DF4")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06003FFC RID: 16380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA1")]
		[Address(RVA = "0x1015C7EC8", Offset = "0x15C7EC8", VA = "0x1015C7EC8")]
		public void RequestSetCharaView(long[] managedIDs, int[] viewEvolutions)
		{
		}

		// Token: 0x06003FFD RID: 16381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA2")]
		[Address(RVA = "0x1015C7FDC", Offset = "0x15C7FDC", VA = "0x1015C7FDC")]
		public void OnResponceRequestSetCharaView(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003FFE RID: 16382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA3")]
		[Address(RVA = "0x1015C8160", Offset = "0x15C8160", VA = "0x1015C8160")]
		public ViewSelectWindow()
		{
		}

		// Token: 0x04004F61 RID: 20321
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40037F1")]
		[SerializeField]
		private ViewSelectPanel m_selectPanel;

		// Token: 0x04004F62 RID: 20322
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40037F2")]
		[SerializeField]
		private ViewSelectImage m_charaNormal;

		// Token: 0x04004F63 RID: 20323
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40037F3")]
		[SerializeField]
		private ViewSelectImage m_charaEvolution;

		// Token: 0x04004F64 RID: 20324
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40037F4")]
		[SerializeField]
		private Text m_charaName;

		// Token: 0x04004F65 RID: 20325
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40037F5")]
		[SerializeField]
		private ElementIcon m_elementIcon;

		// Token: 0x04004F66 RID: 20326
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40037F6")]
		[SerializeField]
		private ClassIcon m_classIcon;

		// Token: 0x04004F67 RID: 20327
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40037F7")]
		[SerializeField]
		private CustomButton m_btnDecide;

		// Token: 0x04004F68 RID: 20328
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40037F8")]
		private long m_managedCharaID;

		// Token: 0x04004F69 RID: 20329
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40037F9")]
		private int m_baseCharaViewType;

		// Token: 0x04004F6A RID: 20330
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x40037FA")]
		private int m_selectedCharaViewType;

		// Token: 0x04004F6B RID: 20331
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40037FB")]
		public Action m_callbackSet;
	}
}
