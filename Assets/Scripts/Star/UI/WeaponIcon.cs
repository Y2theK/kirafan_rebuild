﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D04 RID: 3332
	[Token(Token = "0x20008EA")]
	[StructLayout(3)]
	public class WeaponIcon : ASyncImage
	{
		// Token: 0x06003CDF RID: 15583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600379C")]
		[Address(RVA = "0x1015C5054", Offset = "0x15C5054", VA = "0x1015C5054")]
		public void Apply(int weaponID)
		{
		}

		// Token: 0x06003CE0 RID: 15584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600379D")]
		[Address(RVA = "0x1015C817C", Offset = "0x15C817C", VA = "0x1015C817C", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CE1 RID: 15585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600379E")]
		[Address(RVA = "0x1015C81A8", Offset = "0x15C81A8", VA = "0x1015C81A8")]
		public WeaponIcon()
		{
		}

		// Token: 0x04004C3E RID: 19518
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400358D")]
		protected int m_WeaponID;
	}
}
