﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D05 RID: 3333
	[Token(Token = "0x20008EB")]
	[StructLayout(3)]
	public class WeaponIconWithFrame : MonoBehaviour
	{
		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x06003CE2 RID: 15586 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700044A")]
		private GameObject GameObject
		{
			[Token(Token = "0x600379F")]
			[Address(RVA = "0x1015C81B8", Offset = "0x15C81B8", VA = "0x1015C81B8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003CE3 RID: 15587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A0")]
		[Address(RVA = "0x1015C5D5C", Offset = "0x15C5D5C", VA = "0x1015C5D5C")]
		public void Apply(int weaponID, bool visibleRarity = true)
		{
		}

		// Token: 0x06003CE4 RID: 15588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A1")]
		[Address(RVA = "0x1015C8758", Offset = "0x15C8758", VA = "0x1015C8758")]
		public void Apply(int id, int lv)
		{
		}

		// Token: 0x06003CE5 RID: 15589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A2")]
		[Address(RVA = "0x1015C8AEC", Offset = "0x15C8AEC", VA = "0x1015C8AEC")]
		public void Apply(UserWeaponData wpnData)
		{
		}

		// Token: 0x06003CE6 RID: 15590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A3")]
		[Address(RVA = "0x1015C8D44", Offset = "0x15C8D44", VA = "0x1015C8D44")]
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06003CE7 RID: 15591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A4")]
		[Address(RVA = "0x1015C8248", Offset = "0x15C8248", VA = "0x1015C8248")]
		private void ApplyWeaponID(int weaponID, bool visibleRarity = true)
		{
		}

		// Token: 0x06003CE8 RID: 15592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A5")]
		[Address(RVA = "0x1015C87D4", Offset = "0x15C87D4", VA = "0x1015C87D4")]
		public void SetEnableRenderDetailText(bool flg)
		{
		}

		// Token: 0x06003CE9 RID: 15593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A6")]
		[Address(RVA = "0x1015C884C", Offset = "0x15C884C", VA = "0x1015C884C")]
		public void ApplyDetailText(UISettings.eSortValue_Weapon sortValue)
		{
		}

		// Token: 0x06003CEA RID: 15594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A7")]
		[Address(RVA = "0x1015C92E8", Offset = "0x15C92E8", VA = "0x1015C92E8")]
		private void ChangeEmptyState()
		{
		}

		// Token: 0x06003CEB RID: 15595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A8")]
		[Address(RVA = "0x1015C92EC", Offset = "0x15C92EC", VA = "0x1015C92EC")]
		public void Destroy()
		{
		}

		// Token: 0x06003CEC RID: 15596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037A9")]
		[Address(RVA = "0x1015C938C", Offset = "0x15C938C", VA = "0x1015C938C")]
		public void SetMode(WeaponIconWithFrame.eMode mode)
		{
		}

		// Token: 0x06003CED RID: 15597 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AA")]
		[Address(RVA = "0x1015C9248", Offset = "0x15C9248", VA = "0x1015C9248")]
		private Sprite GetExistBackgroundSprite()
		{
			return null;
		}

		// Token: 0x06003CEE RID: 15598 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AB")]
		[Address(RVA = "0x1015C9250", Offset = "0x15C9250", VA = "0x1015C9250")]
		private Sprite GetExistSwapBackgroundSprite()
		{
			return null;
		}

		// Token: 0x06003CEF RID: 15599 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AC")]
		[Address(RVA = "0x1015C9394", Offset = "0x15C9394", VA = "0x1015C9394")]
		private Sprite GetExistSwapBackgroundSpriteNaked()
		{
			return null;
		}

		// Token: 0x06003CF0 RID: 15600 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AD")]
		[Address(RVA = "0x1015C8FDC", Offset = "0x15C8FDC", VA = "0x1015C8FDC")]
		private Sprite GetNoExistBackgroundSprite()
		{
			return null;
		}

		// Token: 0x06003CF1 RID: 15601 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AE")]
		[Address(RVA = "0x1015C9024", Offset = "0x15C9024", VA = "0x1015C9024")]
		private Sprite GetNoExistSwapBackgroundSprite()
		{
			return null;
		}

		// Token: 0x06003CF2 RID: 15602 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037AF")]
		[Address(RVA = "0x1015C93B4", Offset = "0x15C93B4", VA = "0x1015C93B4")]
		private Sprite GetNoExistSwapBackgroundSpriteNaked()
		{
			return null;
		}

		// Token: 0x06003CF3 RID: 15603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B0")]
		[Address(RVA = "0x1015C9118", Offset = "0x15C9118", VA = "0x1015C9118")]
		private void SetNoExistDisableModeHideImagesActive(bool active)
		{
		}

		// Token: 0x06003CF4 RID: 15604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B1")]
		[Address(RVA = "0x1015C93FC", Offset = "0x15C93FC", VA = "0x1015C93FC")]
		public WeaponIconWithFrame()
		{
		}

		// Token: 0x04004C3F RID: 19519
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400358E")]
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04004C40 RID: 19520
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400358F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001241E8", Offset = "0x1241E8")]
		private Image m_Background;

		// Token: 0x04004C41 RID: 19521
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003590")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124234", Offset = "0x124234")]
		private Image m_SwapBackground;

		// Token: 0x04004C42 RID: 19522
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003591")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124280", Offset = "0x124280")]
		private Sprite m_ExistBackground;

		// Token: 0x04004C43 RID: 19523
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003592")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001242CC", Offset = "0x1242CC")]
		private Sprite m_ExistSwapBackground;

		// Token: 0x04004C44 RID: 19524
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003593")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124318", Offset = "0x124318")]
		private Sprite m_NoExistBackground;

		// Token: 0x04004C45 RID: 19525
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003594")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124364", Offset = "0x124364")]
		private Sprite m_NoExistSwapBackground;

		// Token: 0x04004C46 RID: 19526
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003595")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001243B0", Offset = "0x1243B0")]
		private Sprite m_NoExistAddModeBackground;

		// Token: 0x04004C47 RID: 19527
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003596")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001243FC", Offset = "0x1243FC")]
		private Sprite m_NoExistAddModeSwapBackground;

		// Token: 0x04004C48 RID: 19528
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003597")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124448", Offset = "0x124448")]
		private Sprite m_NoExistNakedSkipLoadModeBackground;

		// Token: 0x04004C49 RID: 19529
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003598")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124494", Offset = "0x124494")]
		private Sprite m_NoExistNakedSkipLoadModeSwapBackground;

		// Token: 0x04004C4A RID: 19530
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003599")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001244E0", Offset = "0x1244E0")]
		private GameObject[] m_NoExistDisableModeHideImages;

		// Token: 0x04004C4B RID: 19531
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400359A")]
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x04004C4C RID: 19532
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400359B")]
		[SerializeField]
		private Image m_FrameGold;

		// Token: 0x04004C4D RID: 19533
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400359C")]
		[SerializeField]
		private GameObject m_DetailObj;

		// Token: 0x04004C4E RID: 19534
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400359D")]
		[SerializeField]
		private IconStatusTitle m_DetailStatusTitle;

		// Token: 0x04004C4F RID: 19535
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400359E")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04004C50 RID: 19536
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400359F")]
		private WeaponIconWithFrame.eMode m_Mode;

		// Token: 0x04004C51 RID: 19537
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40035A0")]
		private WeaponIconWithFrame.WeaponDetailStatus m_DetailStatus;

		// Token: 0x04004C52 RID: 19538
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40035A1")]
		private UISettings.eSortValue_Weapon m_CurrentDetail;

		// Token: 0x04004C53 RID: 19539
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40035A2")]
		private GameObject m_GameObject;

		// Token: 0x02000D06 RID: 3334
		[Token(Token = "0x20010E1")]
		public enum eMode
		{
			// Token: 0x04004C55 RID: 19541
			[Token(Token = "0x4006A01")]
			Normal,
			// Token: 0x04004C56 RID: 19542
			[Token(Token = "0x4006A02")]
			Add,
			// Token: 0x04004C57 RID: 19543
			[Token(Token = "0x4006A03")]
			Disable,
			// Token: 0x04004C58 RID: 19544
			[Token(Token = "0x4006A04")]
			IgnoreNaked,
			// Token: 0x04004C59 RID: 19545
			[Token(Token = "0x4006A05")]
			NakedBGOnly
		}

		// Token: 0x02000D07 RID: 3335
		[Token(Token = "0x20010E2")]
		public class WeaponDetailStatus
		{
			// Token: 0x06003CF5 RID: 15605 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061F6")]
			[Address(RVA = "0x1015C9464", Offset = "0x15C9464", VA = "0x1015C9464")]
			public WeaponDetailStatus()
			{
			}

			// Token: 0x04004C5A RID: 19546
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A06")]
			public int m_Lv;

			// Token: 0x04004C5B RID: 19547
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006A07")]
			public int m_Cost;

			// Token: 0x04004C5C RID: 19548
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A08")]
			public int m_Atk;

			// Token: 0x04004C5D RID: 19549
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006A09")]
			public int m_Mgc;

			// Token: 0x04004C5E RID: 19550
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006A0A")]
			public int m_Def;

			// Token: 0x04004C5F RID: 19551
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006A0B")]
			public int m_MDef;
		}
	}
}
