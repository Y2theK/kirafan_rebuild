﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CAE RID: 3246
	[Token(Token = "0x20008B4")]
	[StructLayout(3)]
	public class WeaponInfo : MonoBehaviour
	{
		// Token: 0x06003B7C RID: 15228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600363E")]
		[Address(RVA = "0x1015C946C", Offset = "0x15C946C", VA = "0x1015C946C")]
		public void SetCharaCost(int cost)
		{
		}

		// Token: 0x06003B7D RID: 15229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600363F")]
		[Address(RVA = "0x1015C9474", Offset = "0x15C9474", VA = "0x1015C9474")]
		public void Apply(int weaponID, int num = -1, bool applySkillInfo = true)
		{
		}

		// Token: 0x06003B7E RID: 15230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003640")]
		[Address(RVA = "0x1015CA848", Offset = "0x15CA848", VA = "0x1015CA848")]
		public void Apply(UserWeaponData weaponData)
		{
		}

		// Token: 0x06003B7F RID: 15231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003641")]
		[Address(RVA = "0x1015CB624", Offset = "0x15CB624", VA = "0x1015CB624")]
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06003B80 RID: 15232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003642")]
		[Address(RVA = "0x1015CC028", Offset = "0x15CC028", VA = "0x1015CC028")]
		public void ApplyEmpty()
		{
		}

		// Token: 0x06003B81 RID: 15233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003643")]
		[Address(RVA = "0x1015CC164", Offset = "0x15CC164", VA = "0x1015CC164")]
		public void ApplyNoEquip()
		{
		}

		// Token: 0x06003B82 RID: 15234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003644")]
		[Address(RVA = "0x1015CC390", Offset = "0x15CC390", VA = "0x1015CC390")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06003B83 RID: 15235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003645")]
		[Address(RVA = "0x1015CA0A0", Offset = "0x15CA0A0", VA = "0x1015CA0A0")]
		private void AddStatus(int[] status, int[] add)
		{
		}

		// Token: 0x06003B84 RID: 15236 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003646")]
		[Address(RVA = "0x1015CA270", Offset = "0x15CA270", VA = "0x1015CA270")]
		private SkillInfo AddActiveSkill(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, long remainExp = -1L, bool changeLevelColor = false)
		{
			return null;
		}

		// Token: 0x06003B85 RID: 15237 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003647")]
		[Address(RVA = "0x1015CA47C", Offset = "0x15CA47C", VA = "0x1015CA47C")]
		private SkillInfo AddPassiveSkill(int skillID, eElementType ownerElement)
		{
			return null;
		}

		// Token: 0x06003B86 RID: 15238 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003648")]
		[Address(RVA = "0x1015CA588", Offset = "0x15CA588", VA = "0x1015CA588")]
		public SkillInfo AddPassiveSkillLocked(int evolutionCount)
		{
			return null;
		}

		// Token: 0x06003B87 RID: 15239 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003649")]
		[Address(RVA = "0x1015CA704", Offset = "0x15CA704", VA = "0x1015CA704")]
		public SkillInfo AddNoSkill()
		{
			return null;
		}

		// Token: 0x06003B88 RID: 15240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364A")]
		[Address(RVA = "0x1015C9FEC", Offset = "0x15C9FEC", VA = "0x1015C9FEC")]
		private void AddSeparatorWeaponEffect()
		{
		}

		// Token: 0x06003B89 RID: 15241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364B")]
		[Address(RVA = "0x1015CA1BC", Offset = "0x15CA1BC", VA = "0x1015CA1BC")]
		private void AddSeparatorWeaponSkill()
		{
		}

		// Token: 0x06003B8A RID: 15242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364C")]
		[Address(RVA = "0x1015CA3C8", Offset = "0x15CA3C8", VA = "0x1015CA3C8")]
		private void AddSeparatorPassiveSkill()
		{
		}

		// Token: 0x06003B8B RID: 15243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364D")]
		[Address(RVA = "0x1015CC4E8", Offset = "0x15CC4E8", VA = "0x1015CC4E8")]
		public WeaponInfo()
		{
		}

		// Token: 0x04004A20 RID: 18976
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400343D")]
		[SerializeField]
		private WeaponNamePlate m_WeaponNamePlate;

		// Token: 0x04004A21 RID: 18977
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400343E")]
		[SerializeField]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x04004A22 RID: 18978
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400343F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100123600", Offset = "0x123600")]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04004A23 RID: 18979
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003440")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004A24 RID: 18980
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003441")]
		[SerializeField]
		private Text m_LvText;

		// Token: 0x04004A25 RID: 18981
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003442")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x04004A26 RID: 18982
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003443")]
		[SerializeField]
		private GameObject m_TotalCost;

		// Token: 0x04004A27 RID: 18983
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003444")]
		[SerializeField]
		private Text m_TotalCostText;

		// Token: 0x04004A28 RID: 18984
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003445")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04004A29 RID: 18985
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003446")]
		[SerializeField]
		private Text m_AtkText;

		// Token: 0x04004A2A RID: 18986
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003447")]
		[SerializeField]
		private Text m_MgcText;

		// Token: 0x04004A2B RID: 18987
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003448")]
		[SerializeField]
		private Text m_DefText;

		// Token: 0x04004A2C RID: 18988
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003449")]
		[SerializeField]
		private Text m_MDefText;

		// Token: 0x04004A2D RID: 18989
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400344A")]
		[SerializeField]
		private PrefabCloner m_SkillInfoPrefabClone;

		// Token: 0x04004A2E RID: 18990
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400344B")]
		private SkillInfo m_SkillInfo;

		// Token: 0x04004A2F RID: 18991
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400344C")]
		[SerializeField]
		private ContainerScrollRect m_ContainerScroll;

		// Token: 0x04004A30 RID: 18992
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400344D")]
		[SerializeField]
		private WeaponStatus m_WeaponStatusPrefab;

		// Token: 0x04004A31 RID: 18993
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400344E")]
		[SerializeField]
		private SkillInfo m_SkillInfoPrefab;

		// Token: 0x04004A32 RID: 18994
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400344F")]
		[SerializeField]
		private GameObject m_SeparatorWeaponEffectPrefab;

		// Token: 0x04004A33 RID: 18995
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003450")]
		[SerializeField]
		private GameObject m_SeparatorWeaponSkillPrefab;

		// Token: 0x04004A34 RID: 18996
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003451")]
		[SerializeField]
		private GameObject m_SeparatorWeaponSkillAutoPrefab;

		// Token: 0x04004A35 RID: 18997
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003452")]
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x04004A36 RID: 18998
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003453")]
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x04004A37 RID: 18999
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003454")]
		[SerializeField]
		private GameObject m_NoSkillExist;

		// Token: 0x04004A38 RID: 19000
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003455")]
		private int m_CharaCost;
	}
}
