﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E3F RID: 3647
	[Token(Token = "0x20009BB")]
	[StructLayout(3)]
	public class WeaponDetailGroup : UIGroup
	{
		// Token: 0x1400007E RID: 126
		// (add) Token: 0x0600437B RID: 17275 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600437C RID: 17276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007E")]
		public event Action<long> OnEquip
		{
			[Token(Token = "0x6003DF1")]
			[Address(RVA = "0x1015CC4F8", Offset = "0x15CC4F8", VA = "0x1015CC4F8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003DF2")]
			[Address(RVA = "0x1015CC604", Offset = "0x15CC604", VA = "0x1015CC604")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600437D RID: 17277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DF3")]
		[Address(RVA = "0x1015CC710", Offset = "0x15CC710", VA = "0x1015CC710")]
		public void Open(UserWeaponData wpnData, bool equipMode, int charaCost = -1)
		{
		}

		// Token: 0x0600437E RID: 17278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DF4")]
		[Address(RVA = "0x1015CC85C", Offset = "0x15CC85C", VA = "0x1015CC85C")]
		public void OnClickEquip()
		{
		}

		// Token: 0x0600437F RID: 17279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DF5")]
		[Address(RVA = "0x1015CC8AC", Offset = "0x15CC8AC", VA = "0x1015CC8AC")]
		public WeaponDetailGroup()
		{
		}

		// Token: 0x0400541C RID: 21532
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B3A")]
		[SerializeField]
		private WeaponInfo m_Info;

		// Token: 0x0400541D RID: 21533
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003B3B")]
		private long m_WpnMngID;

		// Token: 0x0400541F RID: 21535
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003B3D")]
		[SerializeField]
		private GameObject m_CloseButton;

		// Token: 0x04005420 RID: 21536
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003B3E")]
		[SerializeField]
		private GameObject m_EquipButton;

		// Token: 0x04005421 RID: 21537
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003B3F")]
		[SerializeField]
		private GameObject m_CancelButton;
	}
}
