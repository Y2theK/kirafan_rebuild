﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E40 RID: 3648
	[Token(Token = "0x20009BC")]
	[StructLayout(3)]
	public class WeaponListPlayer : SingletonMonoBehaviour<WeaponListPlayer>
	{
		// Token: 0x1400007F RID: 127
		// (add) Token: 0x06004380 RID: 17280 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004381 RID: 17281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007F")]
		public event Action OnClose
		{
			[Token(Token = "0x6003DF6")]
			[Address(RVA = "0x1015CC8B4", Offset = "0x15CC8B4", VA = "0x1015CC8B4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003DF7")]
			[Address(RVA = "0x1015CC9C0", Offset = "0x15CC9C0", VA = "0x1015CC9C0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004382 RID: 17282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DF8")]
		[Address(RVA = "0x1015CCACC", Offset = "0x15CCACC", VA = "0x1015CCACC", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06004383 RID: 17283 RVA: 0x00019878 File Offset: 0x00017A78
		[Token(Token = "0x6003DF9")]
		[Address(RVA = "0x1015CCB1C", Offset = "0x15CCB1C", VA = "0x1015CCB1C")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06004384 RID: 17284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFA")]
		[Address(RVA = "0x1015CCB2C", Offset = "0x15CCB2C", VA = "0x1015CCB2C")]
		public void Open(UserBattlePartyData partyData, int slotIdx)
		{
		}

		// Token: 0x06004385 RID: 17285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFB")]
		[Address(RVA = "0x1015CCD2C", Offset = "0x15CCD2C", VA = "0x1015CCD2C")]
		public void Open(UserSupportPartyData supportPartyData, int slotIdx)
		{
		}

		// Token: 0x06004386 RID: 17286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFC")]
		[Address(RVA = "0x1015CCB44", Offset = "0x15CCB44", VA = "0x1015CCB44")]
		private void Open()
		{
		}

		// Token: 0x06004387 RID: 17287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFD")]
		[Address(RVA = "0x1015CCD44", Offset = "0x15CCD44", VA = "0x1015CCD44")]
		public void Close()
		{
		}

		// Token: 0x06004388 RID: 17288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFE")]
		[Address(RVA = "0x1015CCE8C", Offset = "0x15CCE8C", VA = "0x1015CCE8C")]
		public void ForceHide()
		{
		}

		// Token: 0x06004389 RID: 17289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DFF")]
		[Address(RVA = "0x1015CCE94", Offset = "0x15CCE94", VA = "0x1015CCE94")]
		private void Update()
		{
		}

		// Token: 0x0600438A RID: 17290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E00")]
		[Address(RVA = "0x1015CD74C", Offset = "0x15CD74C", VA = "0x1015CD74C")]
		private void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600438B RID: 17291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E01")]
		[Address(RVA = "0x1015CD754", Offset = "0x15CD754", VA = "0x1015CD754")]
		private void OnClickButton()
		{
		}

		// Token: 0x0600438C RID: 17292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E02")]
		[Address(RVA = "0x1015CD750", Offset = "0x15CD750", VA = "0x1015CD750")]
		private void OnExit()
		{
		}

		// Token: 0x0600438D RID: 17293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E03")]
		[Address(RVA = "0x1015CE560", Offset = "0x15CE560", VA = "0x1015CE560")]
		private void OnConfirmExchange(int btn)
		{
		}

		// Token: 0x0600438E RID: 17294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E04")]
		[Address(RVA = "0x1015CE100", Offset = "0x15CE100", VA = "0x1015CE100")]
		private void ChangeWeapon()
		{
		}

		// Token: 0x0600438F RID: 17295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E05")]
		[Address(RVA = "0x1015CE55C", Offset = "0x15CE55C", VA = "0x1015CE55C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06004390 RID: 17296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E06")]
		[Address(RVA = "0x1015CE58C", Offset = "0x15CE58C", VA = "0x1015CE58C")]
		public WeaponListPlayer()
		{
		}

		// Token: 0x04005422 RID: 21538
		[Token(Token = "0x4003B40")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.WeaponListUI;

		// Token: 0x04005423 RID: 21539
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003B41")]
		private WeaponListPlayer.eStep m_Step;

		// Token: 0x04005424 RID: 21540
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B42")]
		private WeaponListUI m_UI;

		// Token: 0x04005425 RID: 21541
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B43")]
		private UserBattlePartyData m_PartyData;

		// Token: 0x04005426 RID: 21542
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003B44")]
		private UserSupportPartyData m_SupportPartyData;

		// Token: 0x04005427 RID: 21543
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003B45")]
		private int m_SlotIdx;

		// Token: 0x04005428 RID: 21544
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003B46")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04005429 RID: 21545
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003B47")]
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x02000E41 RID: 3649
		[Token(Token = "0x200114A")]
		private enum eStep
		{
			// Token: 0x0400542C RID: 21548
			[Token(Token = "0x4006C2C")]
			Hide,
			// Token: 0x0400542D RID: 21549
			[Token(Token = "0x4006C2D")]
			LoadUI,
			// Token: 0x0400542E RID: 21550
			[Token(Token = "0x4006C2E")]
			LoadWait,
			// Token: 0x0400542F RID: 21551
			[Token(Token = "0x4006C2F")]
			WaitBG,
			// Token: 0x04005430 RID: 21552
			[Token(Token = "0x4006C30")]
			SetupAndPlayIn,
			// Token: 0x04005431 RID: 21553
			[Token(Token = "0x4006C31")]
			Main,
			// Token: 0x04005432 RID: 21554
			[Token(Token = "0x4006C32")]
			UnloadWait
		}
	}
}
