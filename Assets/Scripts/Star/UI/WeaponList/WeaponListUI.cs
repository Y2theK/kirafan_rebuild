﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E42 RID: 3650
	[Token(Token = "0x20009BD")]
	[StructLayout(3)]
	public class WeaponListUI : MenuUIBase
	{
		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x06004391 RID: 17297 RVA: 0x00019890 File Offset: 0x00017A90
		[Token(Token = "0x17000494")]
		public WeaponListUI.eButton SelectButton
		{
			[Token(Token = "0x6003E07")]
			[Address(RVA = "0x1015CE0F0", Offset = "0x15CE0F0", VA = "0x1015CE0F0")]
			get
			{
				return WeaponListUI.eButton.Weapon;
			}
		}

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x06004392 RID: 17298 RVA: 0x000198A8 File Offset: 0x00017AA8
		[Token(Token = "0x17000495")]
		public long SelectedWeaponMngID
		{
			[Token(Token = "0x6003E08")]
			[Address(RVA = "0x1015CE0F8", Offset = "0x15CE0F8", VA = "0x1015CE0F8")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x06004393 RID: 17299 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000496")]
		public List<long> SelectedWeaponList
		{
			[Token(Token = "0x6003E09")]
			[Address(RVA = "0x1015CE5DC", Offset = "0x15CE5DC", VA = "0x1015CE5DC")]
			get
			{
				return null;
			}
		}

		// Token: 0x14000080 RID: 128
		// (add) Token: 0x06004394 RID: 17300 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004395 RID: 17301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000080")]
		public event Action OnClickButton
		{
			[Token(Token = "0x6003E0A")]
			[Address(RVA = "0x1015CD640", Offset = "0x15CD640", VA = "0x1015CD640")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003E0B")]
			[Address(RVA = "0x1015CE6F0", Offset = "0x15CE6F0", VA = "0x1015CE6F0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004396 RID: 17302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E0C")]
		[Address(RVA = "0x1015CD2E8", Offset = "0x15CD2E8", VA = "0x1015CD2E8")]
		public void SetupEquip(UserBattlePartyData partyData, int slotIdx)
		{
		}

		// Token: 0x06004397 RID: 17303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E0D")]
		[Address(RVA = "0x1015CD494", Offset = "0x15CD494", VA = "0x1015CD494")]
		public void SetupEquip(UserSupportPartyData supportPartyData, int slotIdx)
		{
		}

		// Token: 0x06004398 RID: 17304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E0E")]
		[Address(RVA = "0x1015CE7FC", Offset = "0x15CE7FC", VA = "0x1015CE7FC")]
		private void SetupEquip(List<long> equipped)
		{
		}

		// Token: 0x06004399 RID: 17305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E0F")]
		[Address(RVA = "0x1015CED30", Offset = "0x15CED30", VA = "0x1015CED30")]
		public void SetupUpgrade()
		{
		}

		// Token: 0x0600439A RID: 17306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E10")]
		[Address(RVA = "0x1015CF0AC", Offset = "0x15CF0AC", VA = "0x1015CF0AC")]
		public void SetupEvolution()
		{
		}

		// Token: 0x0600439B RID: 17307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E11")]
		[Address(RVA = "0x1015CF2AC", Offset = "0x15CF2AC", VA = "0x1015CF2AC")]
		public void SetupSale()
		{
		}

		// Token: 0x0600439C RID: 17308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E12")]
		[Address(RVA = "0x1015CFC78", Offset = "0x15CFC78", VA = "0x1015CFC78")]
		public void SetUpSkillLvUP()
		{
		}

		// Token: 0x0600439D RID: 17309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E13")]
		[Address(RVA = "0x1015CFE78", Offset = "0x15CFE78", VA = "0x1015CFE78")]
		public void Refresh()
		{
		}

		// Token: 0x0600439E RID: 17310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E14")]
		[Address(RVA = "0x1015D01A0", Offset = "0x15D01A0", VA = "0x1015D01A0")]
		private void Update()
		{
		}

		// Token: 0x0600439F RID: 17311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E15")]
		[Address(RVA = "0x1015D0354", Offset = "0x15D0354", VA = "0x1015D0354")]
		private void ChangeStep(WeaponListUI.eStep step)
		{
		}

		// Token: 0x060043A0 RID: 17312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E16")]
		[Address(RVA = "0x1015D049C", Offset = "0x15D049C", VA = "0x1015D049C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060043A1 RID: 17313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E17")]
		[Address(RVA = "0x1015D0558", Offset = "0x15D0558", VA = "0x1015D0558", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060043A2 RID: 17314 RVA: 0x000198C0 File Offset: 0x00017AC0
		[Token(Token = "0x6003E18")]
		[Address(RVA = "0x1015D0634", Offset = "0x15D0634", VA = "0x1015D0634", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060043A3 RID: 17315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E19")]
		[Address(RVA = "0x1015D0644", Offset = "0x15D0644", VA = "0x1015D0644")]
		public void OnClickWeaponEquipCallBack(WeaponScrollItemData data)
		{
		}

		// Token: 0x060043A4 RID: 17316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1A")]
		[Address(RVA = "0x1015D079C", Offset = "0x15D079C", VA = "0x1015D079C")]
		private void OnClickEquip(long mngID)
		{
		}

		// Token: 0x060043A5 RID: 17317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1B")]
		[Address(RVA = "0x1015D07B0", Offset = "0x15D07B0", VA = "0x1015D07B0")]
		public void OnClickRemoveCallBack()
		{
		}

		// Token: 0x060043A6 RID: 17318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1C")]
		[Address(RVA = "0x1015D07C8", Offset = "0x15D07C8", VA = "0x1015D07C8")]
		public void OnClickWeaponCallBack(WeaponScrollItemData data)
		{
		}

		// Token: 0x060043A7 RID: 17319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1D")]
		[Address(RVA = "0x1015D09E0", Offset = "0x15D09E0", VA = "0x1015D09E0")]
		public void OnClickSaleButtonCallBack()
		{
		}

		// Token: 0x060043A8 RID: 17320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1E")]
		[Address(RVA = "0x1015D09F4", Offset = "0x15D09F4", VA = "0x1015D09F4")]
		public void OnClickReleaseSelectButtonCallBack()
		{
		}

		// Token: 0x060043A9 RID: 17321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E1F")]
		[Address(RVA = "0x1015CF930", Offset = "0x15CF930", VA = "0x1015CF930")]
		private void UpdateDisp()
		{
		}

		// Token: 0x060043AA RID: 17322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E20")]
		[Address(RVA = "0x1015D0D78", Offset = "0x15D0D78", VA = "0x1015D0D78")]
		public void OnHoldWeaponCallBack(WeaponScrollItemData data)
		{
		}

		// Token: 0x060043AB RID: 17323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E21")]
		[Address(RVA = "0x1015D0E40", Offset = "0x15D0E40", VA = "0x1015D0E40")]
		public WeaponListUI()
		{
		}

		// Token: 0x04005433 RID: 21555
		[Token(Token = "0x4003B49")]
		private const float NORMALHEIGHT = 480f;

		// Token: 0x04005434 RID: 21556
		[Token(Token = "0x4003B4A")]
		private const float FOOTERHEIGHT = 415f;

		// Token: 0x04005435 RID: 21557
		[Token(Token = "0x4003B4B")]
		private const int SELECT_MAX = 20;

		// Token: 0x04005436 RID: 21558
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003B4C")]
		private WeaponListUI.eStep m_Step;

		// Token: 0x04005437 RID: 21559
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003B4D")]
		[SerializeField]
		private WeaponScroll m_Scroll;

		// Token: 0x04005438 RID: 21560
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003B4E")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005439 RID: 21561
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B4F")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x0400543A RID: 21562
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003B50")]
		[SerializeField]
		private WeaponDetailGroup m_DetailGroup;

		// Token: 0x0400543B RID: 21563
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003B51")]
		[SerializeField]
		private GameObject m_SaleObj;

		// Token: 0x0400543C RID: 21564
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003B52")]
		[SerializeField]
		private Text m_SelectNumText;

		// Token: 0x0400543D RID: 21565
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003B53")]
		[SerializeField]
		private Text m_SalePrice;

		// Token: 0x0400543E RID: 21566
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003B54")]
		[SerializeField]
		private CustomButton m_ResetButton;

		// Token: 0x0400543F RID: 21567
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B55")]
		[SerializeField]
		private CustomButton m_ExecSaleButton;

		// Token: 0x04005440 RID: 21568
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003B56")]
		private UserBattlePartyData m_PartyData;

		// Token: 0x04005441 RID: 21569
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003B57")]
		private UserSupportPartyData m_SupportPartyData;

		// Token: 0x04005442 RID: 21570
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003B58")]
		private int m_SlotIdx;

		// Token: 0x04005443 RID: 21571
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003B59")]
		private long m_CharaMngID;

		// Token: 0x04005444 RID: 21572
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003B5A")]
		private long m_SelectedWeaponMngID;

		// Token: 0x04005445 RID: 21573
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003B5B")]
		private List<WeaponScrollItemData> m_SelectedList;

		// Token: 0x04005446 RID: 21574
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003B5C")]
		private WeaponListUI.eButton m_SelectButton;

		// Token: 0x02000E43 RID: 3651
		[Token(Token = "0x200114B")]
		private enum eStep
		{
			// Token: 0x04005449 RID: 21577
			[Token(Token = "0x4006C34")]
			Hide,
			// Token: 0x0400544A RID: 21578
			[Token(Token = "0x4006C35")]
			In,
			// Token: 0x0400544B RID: 21579
			[Token(Token = "0x4006C36")]
			Idle,
			// Token: 0x0400544C RID: 21580
			[Token(Token = "0x4006C37")]
			Out,
			// Token: 0x0400544D RID: 21581
			[Token(Token = "0x4006C38")]
			End,
			// Token: 0x0400544E RID: 21582
			[Token(Token = "0x4006C39")]
			Window,
			// Token: 0x0400544F RID: 21583
			[Token(Token = "0x4006C3A")]
			Num
		}

		// Token: 0x02000E44 RID: 3652
		[Token(Token = "0x200114C")]
		public enum eButton
		{
			// Token: 0x04005451 RID: 21585
			[Token(Token = "0x4006C3C")]
			None = -1,
			// Token: 0x04005452 RID: 21586
			[Token(Token = "0x4006C3D")]
			Weapon,
			// Token: 0x04005453 RID: 21587
			[Token(Token = "0x4006C3E")]
			Sale
		}
	}
}
