﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E45 RID: 3653
	[Token(Token = "0x20009BE")]
	[StructLayout(3)]
	public class WeaponRemoveItem : ScrollItemIcon
	{
		// Token: 0x060043AC RID: 17324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E22")]
		[Address(RVA = "0x1015D0EC0", Offset = "0x15D0EC0", VA = "0x1015D0EC0")]
		public WeaponRemoveItem()
		{
		}

		// Token: 0x04005454 RID: 21588
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B5E")]
		[SerializeField]
		private CustomButton m_Button;
	}
}
