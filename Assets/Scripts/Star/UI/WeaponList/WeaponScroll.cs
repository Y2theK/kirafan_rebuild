﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E46 RID: 3654
	[Token(Token = "0x20009BF")]
	[StructLayout(3)]
	public class WeaponScroll : ScrollViewBase
	{
		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x060043AD RID: 17325 RVA: 0x000198D8 File Offset: 0x00017AD8
		[Token(Token = "0x17000497")]
		public int WeaponNum
		{
			[Token(Token = "0x6003E23")]
			[Address(RVA = "0x1015D0EC8", Offset = "0x15D0EC8", VA = "0x1015D0EC8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x060043AE RID: 17326 RVA: 0x000198F0 File Offset: 0x00017AF0
		[Token(Token = "0x17000498")]
		public int FilteredNum
		{
			[Token(Token = "0x6003E24")]
			[Address(RVA = "0x1015D0F28", Offset = "0x15D0F28", VA = "0x1015D0F28")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x060043AF RID: 17327 RVA: 0x00019908 File Offset: 0x00017B08
		[Token(Token = "0x17000499")]
		public WeaponScroll.eMode Mode
		{
			[Token(Token = "0x6003E25")]
			[Address(RVA = "0x1015D09D8", Offset = "0x15D09D8", VA = "0x1015D09D8")]
			get
			{
				return WeaponScroll.eMode.View;
			}
		}

		// Token: 0x14000081 RID: 129
		// (add) Token: 0x060043B0 RID: 17328 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060043B1 RID: 17329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000081")]
		public event Action<WeaponScrollItemData> OnClickWeapon
		{
			[Token(Token = "0x6003E26")]
			[Address(RVA = "0x1015CEB10", Offset = "0x15CEB10", VA = "0x1015CEB10")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003E27")]
			[Address(RVA = "0x1015D0F88", Offset = "0x15D0F88", VA = "0x1015D0F88")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000082 RID: 130
		// (add) Token: 0x060043B2 RID: 17330 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060043B3 RID: 17331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000082")]
		public event Action<WeaponScrollItemData> OnHoldWeapon
		{
			[Token(Token = "0x6003E28")]
			[Address(RVA = "0x1015CEF9C", Offset = "0x15CEF9C", VA = "0x1015CEF9C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003E29")]
			[Address(RVA = "0x1015D1098", Offset = "0x15D1098", VA = "0x1015D1098")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000083 RID: 131
		// (add) Token: 0x060043B4 RID: 17332 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060043B5 RID: 17333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000083")]
		public event Action OnClickRemove
		{
			[Token(Token = "0x6003E2A")]
			[Address(RVA = "0x1015CEC20", Offset = "0x15CEC20", VA = "0x1015CEC20")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003E2B")]
			[Address(RVA = "0x1015D11A8", Offset = "0x15D11A8", VA = "0x1015D11A8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060043B6 RID: 17334 RVA: 0x00019920 File Offset: 0x00017B20
		[Token(Token = "0x6003E2C")]
		[Address(RVA = "0x1015D12B8", Offset = "0x15D12B8", VA = "0x1015D12B8")]
		public bool IsEquipMode()
		{
			return default(bool);
		}

		// Token: 0x060043B7 RID: 17335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E2D")]
		[Address(RVA = "0x1015CEA34", Offset = "0x15CEA34", VA = "0x1015CEA34")]
		public void SetEquipData(long charaMngID, List<long> equippedWeapon)
		{
		}

		// Token: 0x060043B8 RID: 17336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E2E")]
		[Address(RVA = "0x1015CEA2C", Offset = "0x15CEA2C", VA = "0x1015CEA2C")]
		public void SetMode(WeaponScroll.eMode mode)
		{
		}

		// Token: 0x060043B9 RID: 17337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E2F")]
		[Address(RVA = "0x1015D0AF8", Offset = "0x15D0AF8", VA = "0x1015D0AF8")]
		public void SetEnableSaleSelect(bool flg)
		{
		}

		// Token: 0x060043BA RID: 17338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E30")]
		[Address(RVA = "0x1015D1B7C", Offset = "0x15D1B7C", VA = "0x1015D1B7C", Slot = "7")]
		public override void Setup()
		{
		}

		// Token: 0x060043BB RID: 17339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E31")]
		[Address(RVA = "0x1015CFF78", Offset = "0x15CFF78", VA = "0x1015CFF78")]
		public void OpenUserWeaponList()
		{
		}

		// Token: 0x060043BC RID: 17340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E32")]
		[Address(RVA = "0x1015D12C8", Offset = "0x15D12C8", VA = "0x1015D12C8")]
		private void ApplyMode()
		{
		}

		// Token: 0x060043BD RID: 17341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E33")]
		[Address(RVA = "0x1015D1BA4", Offset = "0x15D1BA4", VA = "0x1015D1BA4")]
		public void SetEnbaleRemoveIcon(bool flg)
		{
		}

		// Token: 0x060043BE RID: 17342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E34")]
		[Address(RVA = "0x1015D1BAC", Offset = "0x15D1BAC", VA = "0x1015D1BAC")]
		public void AddRemoveIcon()
		{
		}

		// Token: 0x060043BF RID: 17343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E35")]
		[Address(RVA = "0x1015D20CC", Offset = "0x15D20CC", VA = "0x1015D20CC")]
		private void SetupRawItemDataList()
		{
		}

		// Token: 0x060043C0 RID: 17344 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003E36")]
		[Address(RVA = "0x1015D2AE8", Offset = "0x15D2AE8", VA = "0x1015D2AE8")]
		public List<WeaponScrollItemData> GetRawDataList()
		{
			return null;
		}

		// Token: 0x060043C1 RID: 17345 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003E37")]
		[Address(RVA = "0x1015D2AF0", Offset = "0x15D2AF0", VA = "0x1015D2AF0")]
		public List<WeaponScrollItemData> GetFilteredDataList()
		{
			return null;
		}

		// Token: 0x060043C2 RID: 17346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E38")]
		[Address(RVA = "0x1015D2564", Offset = "0x15D2564", VA = "0x1015D2564")]
		private void SetupFilteredItemList()
		{
		}

		// Token: 0x060043C3 RID: 17347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E39")]
		[Address(RVA = "0x1015D2BC0", Offset = "0x15D2BC0", VA = "0x1015D2BC0", Slot = "14")]
		protected override void OnClickIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060043C4 RID: 17348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E3A")]
		[Address(RVA = "0x1015D2CE0", Offset = "0x15D2CE0", VA = "0x1015D2CE0", Slot = "15")]
		protected override void OnHoldIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060043C5 RID: 17349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E3B")]
		[Address(RVA = "0x1015D2E00", Offset = "0x15D2E00", VA = "0x1015D2E00")]
		protected void OnClickRemoveIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060043C6 RID: 17350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E3C")]
		[Address(RVA = "0x1015D2E0C", Offset = "0x15D2E0C", VA = "0x1015D2E0C")]
		public void DoSort()
		{
		}

		// Token: 0x060043C7 RID: 17351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E3D")]
		[Address(RVA = "0x1015D2448", Offset = "0x15D2448", VA = "0x1015D2448")]
		private void SortRawItemList()
		{
		}

		// Token: 0x060043C8 RID: 17352 RVA: 0x00019938 File Offset: 0x00017B38
		[Token(Token = "0x6003E3E")]
		[Address(RVA = "0x1015D2FC8", Offset = "0x15D2FC8", VA = "0x1015D2FC8")]
		private int SortCompare(WeaponScrollItemData r, WeaponScrollItemData l)
		{
			return 0;
		}

		// Token: 0x060043C9 RID: 17353 RVA: 0x00019950 File Offset: 0x00017B50
		[Token(Token = "0x6003E3F")]
		[Address(RVA = "0x1015D31E4", Offset = "0x15D31E4", VA = "0x1015D31E4")]
		private long GetSortKey(long mngID)
		{
			return 0L;
		}

		// Token: 0x060043CA RID: 17354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E40")]
		[Address(RVA = "0x1015D3678", Offset = "0x15D3678", VA = "0x1015D3678")]
		public void DoFilter()
		{
		}

		// Token: 0x060043CB RID: 17355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E41")]
		[Address(RVA = "0x1015D384C", Offset = "0x15D384C", VA = "0x1015D384C", Slot = "16")]
		protected override void UpdateItemPosition()
		{
		}

		// Token: 0x060043CC RID: 17356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E42")]
		[Address(RVA = "0x1015D3968", Offset = "0x15D3968", VA = "0x1015D3968")]
		public WeaponScroll()
		{
		}

		// Token: 0x04005455 RID: 21589
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003B5F")]
		[SerializeField]
		private Text m_NumText;

		// Token: 0x04005456 RID: 21590
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003B60")]
		[SerializeField]
		private WeaponRemoveItem m_RemoveIconPrefab;

		// Token: 0x04005457 RID: 21591
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003B61")]
		private eClassType m_ClassType;

		// Token: 0x04005458 RID: 21592
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003B62")]
		private long m_CharaMngID;

		// Token: 0x04005459 RID: 21593
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003B63")]
		private List<long> m_EquippedWeaponMngIDList;

		// Token: 0x0400545A RID: 21594
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003B64")]
		private WeaponScroll.eMode m_Mode;

		// Token: 0x0400545B RID: 21595
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003B65")]
		private ScrollViewBase.DispItem m_RemoveIconViewItem;

		// Token: 0x0400545C RID: 21596
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003B66")]
		private bool m_EnableRemoveIcon;

		// Token: 0x0400545D RID: 21597
		[Cpp2IlInjected.FieldOffset(Offset = "0xEC")]
		[Token(Token = "0x4003B67")]
		private int m_ExceptWeaponCount;

		// Token: 0x0400545E RID: 21598
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003B68")]
		private List<WeaponScrollItemData> m_RawItemDataList;

		// Token: 0x0400545F RID: 21599
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003B69")]
		private List<WeaponScrollItemData> m_FilteredItemDataList;

		// Token: 0x02000E47 RID: 3655
		[Token(Token = "0x200114D")]
		public enum eMode
		{
			// Token: 0x04005464 RID: 21604
			[Token(Token = "0x4006C40")]
			View,
			// Token: 0x04005465 RID: 21605
			[Token(Token = "0x4006C41")]
			Equip,
			// Token: 0x04005466 RID: 21606
			[Token(Token = "0x4006C42")]
			Upgrade,
			// Token: 0x04005467 RID: 21607
			[Token(Token = "0x4006C43")]
			Sale,
			// Token: 0x04005468 RID: 21608
			[Token(Token = "0x4006C44")]
			Evolution,
			// Token: 0x04005469 RID: 21609
			[Token(Token = "0x4006C45")]
			SkillLvUP
		}
	}
}
