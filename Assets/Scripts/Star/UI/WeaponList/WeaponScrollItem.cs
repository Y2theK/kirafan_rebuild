﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E48 RID: 3656
	[Token(Token = "0x20009C0")]
	[StructLayout(3)]
	public class WeaponScrollItem : ScrollItemIcon
	{
		// Token: 0x060043CD RID: 17357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E43")]
		[Address(RVA = "0x1015D3980", Offset = "0x15D3980", VA = "0x1015D3980", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060043CE RID: 17358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E44")]
		[Address(RVA = "0x1015D3EA0", Offset = "0x15D3EA0", VA = "0x1015D3EA0")]
		public WeaponScrollItem()
		{
		}

		// Token: 0x0400546A RID: 21610
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B6D")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x0400546B RID: 21611
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003B6E")]
		[SerializeField]
		private GameObject m_Equip;

		// Token: 0x0400546C RID: 21612
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003B6F")]
		[SerializeField]
		private GameObject m_Sale;

		// Token: 0x0400546D RID: 21613
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003B70")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x0400546E RID: 21614
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003B71")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x0400546F RID: 21615
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003B72")]
		protected WeaponScroll m_WeaponScroll;

		// Token: 0x04005470 RID: 21616
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003B73")]
		protected WeaponScrollItemData m_WeaponData;
	}
}
