﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.WeaponList
{
	// Token: 0x02000E49 RID: 3657
	[Token(Token = "0x20009C1")]
	[StructLayout(3)]
	public class WeaponScrollItemData : ScrollItemData
	{
		// Token: 0x060043CF RID: 17359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003E45")]
		[Address(RVA = "0x1015D2A2C", Offset = "0x15D2A2C", VA = "0x1015D2A2C")]
		public WeaponScrollItemData(long mngID, WeaponScrollItemData.eStatus status)
		{
		}

		// Token: 0x04005471 RID: 21617
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B74")]
		public long m_WeaponMngID;

		// Token: 0x04005472 RID: 21618
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B75")]
		public UserWeaponData m_WeaponData;

		// Token: 0x04005473 RID: 21619
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B76")]
		public int m_SortedIdx;

		// Token: 0x04005474 RID: 21620
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003B77")]
		public int m_SaleSelectIdx;

		// Token: 0x04005475 RID: 21621
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003B78")]
		public bool m_IsEquieped;

		// Token: 0x04005476 RID: 21622
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003B79")]
		public bool m_Enable;

		// Token: 0x04005477 RID: 21623
		[Cpp2IlInjected.FieldOffset(Offset = "0x32")]
		[Token(Token = "0x4003B7A")]
		public bool m_CantSelect;

		// Token: 0x04005478 RID: 21624
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003B7B")]
		public WeaponScrollItemData.eStatus m_Status;

		// Token: 0x02000E4A RID: 3658
		[Token(Token = "0x200114E")]
		public enum eStatus
		{
			// Token: 0x0400547A RID: 21626
			[Token(Token = "0x4006C47")]
			None,
			// Token: 0x0400547B RID: 21627
			[Token(Token = "0x4006C48")]
			EnableEquip,
			// Token: 0x0400547C RID: 21628
			[Token(Token = "0x4006C49")]
			DisableEquip,
			// Token: 0x0400547D RID: 21629
			[Token(Token = "0x4006C4A")]
			Upgrade,
			// Token: 0x0400547E RID: 21630
			[Token(Token = "0x4006C4B")]
			LevelMax,
			// Token: 0x0400547F RID: 21631
			[Token(Token = "0x4006C4C")]
			Sale,
			// Token: 0x04005480 RID: 21632
			[Token(Token = "0x4006C4D")]
			SaleSelected,
			// Token: 0x04005481 RID: 21633
			[Token(Token = "0x4006C4E")]
			Equiped
		}
	}
}
