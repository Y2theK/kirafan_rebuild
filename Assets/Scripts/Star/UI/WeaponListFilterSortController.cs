﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CC8 RID: 3272
	[Token(Token = "0x20008BD")]
	[StructLayout(3)]
	public class WeaponListFilterSortController : MonoBehaviour
	{
		// Token: 0x06003BE9 RID: 15337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A6")]
		[Address(RVA = "0x1015D3EA8", Offset = "0x15D3EA8", VA = "0x1015D3EA8")]
		private void LateUpdate()
		{
		}

		// Token: 0x06003BEA RID: 15338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A7")]
		[Address(RVA = "0x1015D3F7C", Offset = "0x15D3F7C", VA = "0x1015D3F7C")]
		public void OpenFilterWindow()
		{
		}

		// Token: 0x06003BEB RID: 15339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A8")]
		[Address(RVA = "0x1015D40E8", Offset = "0x15D40E8", VA = "0x1015D40E8")]
		public void OnExecuteFilter()
		{
		}

		// Token: 0x06003BEC RID: 15340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A9")]
		[Address(RVA = "0x1015D4134", Offset = "0x15D4134", VA = "0x1015D4134")]
		public void OpenSortWindow()
		{
		}

		// Token: 0x06003BED RID: 15341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036AA")]
		[Address(RVA = "0x1015D421C", Offset = "0x15D421C", VA = "0x1015D421C")]
		public void OnExecuteSort()
		{
		}

		// Token: 0x06003BEE RID: 15342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036AB")]
		[Address(RVA = "0x1015D4268", Offset = "0x15D4268", VA = "0x1015D4268")]
		public WeaponListFilterSortController()
		{
		}

		// Token: 0x04004B08 RID: 19208
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40034BF")]
		[SerializeField]
		private WeaponScroll m_WeaponScroll;

		// Token: 0x04004B09 RID: 19209
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40034C0")]
		[SerializeField]
		private Text m_HaveNumText;
	}
}
