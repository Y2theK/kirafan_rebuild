﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CAF RID: 3247
	[Token(Token = "0x20008B5")]
	[StructLayout(3)]
	public class WeaponNamePlate : MonoBehaviour
	{
		// Token: 0x06003B8C RID: 15244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364E")]
		[Address(RVA = "0x1015D4270", Offset = "0x15D4270", VA = "0x1015D4270")]
		public void Apply(WeaponListDB_Param weaponParam, [Optional] string replaceWeaponText)
		{
		}

		// Token: 0x06003B8D RID: 15245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600364F")]
		[Address(RVA = "0x1015D4A4C", Offset = "0x15D4A4C", VA = "0x1015D4A4C")]
		public void Destroy()
		{
		}

		// Token: 0x06003B8E RID: 15246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003650")]
		[Address(RVA = "0x1015D4CA4", Offset = "0x15D4CA4", VA = "0x1015D4CA4")]
		public WeaponNamePlate()
		{
		}

		// Token: 0x04004A39 RID: 19001
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003456")]
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04004A3A RID: 19002
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003457")]
		[SerializeField]
		private FaceIcon m_FaceIcon;

		// Token: 0x04004A3B RID: 19003
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003458")]
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x04004A3C RID: 19004
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003459")]
		[SerializeField]
		private Text m_ClassText;

		// Token: 0x04004A3D RID: 19005
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400345A")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004A3E RID: 19006
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400345B")]
		[SerializeField]
		private Text m_JobText;
	}
}
