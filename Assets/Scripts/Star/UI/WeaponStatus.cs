﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CB0 RID: 3248
	[Token(Token = "0x20008B6")]
	[StructLayout(3)]
	public class WeaponStatus : MonoBehaviour
	{
		// Token: 0x06003B8F RID: 15247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003651")]
		[Address(RVA = "0x1015D4CAC", Offset = "0x15D4CAC", VA = "0x1015D4CAC")]
		public void Apply(int[] values, [Optional] int[] addValues)
		{
		}

		// Token: 0x06003B90 RID: 15248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003652")]
		[Address(RVA = "0x1015D51E0", Offset = "0x15D51E0", VA = "0x1015D51E0")]
		public WeaponStatus()
		{
		}

		// Token: 0x04004A3F RID: 19007
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400345C")]
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x04004A40 RID: 19008
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400345D")]
		[SerializeField]
		private Text[] m_AddValueText;

		// Token: 0x02000CB1 RID: 3249
		[Token(Token = "0x20010C2")]
		public enum eStatus
		{
			// Token: 0x04004A42 RID: 19010
			[Token(Token = "0x4006936")]
			Atk,
			// Token: 0x04004A43 RID: 19011
			[Token(Token = "0x4006937")]
			Mgc,
			// Token: 0x04004A44 RID: 19012
			[Token(Token = "0x4006938")]
			Def,
			// Token: 0x04004A45 RID: 19013
			[Token(Token = "0x4006939")]
			MDef,
			// Token: 0x04004A46 RID: 19014
			[Token(Token = "0x400693A")]
			Num
		}
	}
}
