﻿using System;
using UnityEngine;

namespace Star.UI {
    public class WebViewGroup : UIGroup {
        [SerializeField] private WebViewController m_WVCtrl;
        [SerializeField] private RectTransform m_TargetRect;

        private string m_Url;

        private Action<string> m_Callback;

        public RectTransform targetRect {
            get => m_TargetRect;
            set => m_TargetRect = value;
        }

        public void Open(string url, Action<string> callback = null) {
            m_Url = url;
            m_Callback = callback;
            base.Open();
        }

        public override void Hide() {
            base.Hide();
            m_WVCtrl.DestroyWebViewObject();
        }

        public override void Close() {
            base.Close();
            m_WVCtrl.DestroyWebViewObject();
        }

        public override void Update() {
            base.Update();
            if (IsIdle() && !m_WVCtrl.IsActiveWebViewObj() && !GameSystem.Inst.CmnMsgWindow.IsActive && m_Url != null) {
                m_WVCtrl.LoadURL(m_Url, m_TargetRect, WVCallBack);
            }
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            m_WVCtrl.DestroyWebViewObject();
        }

        private void WVCallBack(string msg) {
            int start = msg.IndexOf("<");
            int end = msg.IndexOf(">");
            if (start != -1 && end != -1) {
                string text = msg.Substring(start + 1, end - start - 1);
                string url = msg.Substring(end + 1);
                if (text != null && text == "browser") {
                    Application.OpenURL(url);
                    return;
                }
            }
            m_Callback.Call(msg);
        }
    }
}
