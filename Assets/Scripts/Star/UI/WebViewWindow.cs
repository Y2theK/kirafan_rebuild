﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class WebViewWindow : MonoBehaviour {
        [SerializeField] private WebViewGroup m_WebviewGroup;
        [SerializeField] private Text m_TitleText;

        public void Open(string title, string url) {
            m_TitleText.text = title;
            gameObject.SetActive(true);
            m_WebviewGroup.Open(url, null);
        }

        public void Close() {
            m_WebviewGroup.Close();
        }

        public void Hide() {
            m_WebviewGroup.Hide();
        }
    }
}
