﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000E3C RID: 3644
	[Token(Token = "0x20009B9")]
	[Serializable]
	[StructLayout(3)]
	public class WebViewWindowTabInformation
	{
		// Token: 0x06004365 RID: 17253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DDB")]
		[Address(RVA = "0x1015D5660", Offset = "0x15D5660", VA = "0x1015D5660")]
		public WebViewWindowTabInformation(string title, string URL)
		{
		}

		// Token: 0x06004366 RID: 17254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DDC")]
		[Address(RVA = "0x1015D5698", Offset = "0x15D5698", VA = "0x1015D5698")]
		public WebViewWindowTabInformation()
		{
		}

		// Token: 0x04005409 RID: 21513
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003B2C")]
		public string m_Title;

		// Token: 0x0400540A RID: 21514
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B2D")]
		public string m_URL;
	}
}
