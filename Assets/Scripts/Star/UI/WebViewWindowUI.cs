﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E3D RID: 3645
	[Token(Token = "0x20009BA")]
	[StructLayout(3)]
	public class WebViewWindowUI : MonoBehaviour
	{
		// Token: 0x06004367 RID: 17255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DDD")]
		[Address(RVA = "0x1015D56A0", Offset = "0x15D56A0", VA = "0x1015D56A0", Slot = "4")]
		protected virtual void Start()
		{
		}

		// Token: 0x06004368 RID: 17256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DDE")]
		[Address(RVA = "0x1015D56A4", Offset = "0x15D56A4", VA = "0x1015D56A4")]
		public void Setup()
		{
		}

		// Token: 0x06004369 RID: 17257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DDF")]
		[Address(RVA = "0x1015D571C", Offset = "0x15D571C", VA = "0x1015D571C")]
		public void Destroy()
		{
		}

		// Token: 0x0600436A RID: 17258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE0")]
		[Address(RVA = "0x1015D5834", Offset = "0x15D5834", VA = "0x1015D5834")]
		public void OpenInfoWindow(string url)
		{
		}

		// Token: 0x0600436B RID: 17259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE1")]
		[Address(RVA = "0x1015D5D4C", Offset = "0x15D5D4C", VA = "0x1015D5D4C", Slot = "5")]
		public virtual void Open()
		{
		}

		// Token: 0x0600436C RID: 17260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE2")]
		[Address(RVA = "0x1015D6094", Offset = "0x15D6094", VA = "0x1015D6094", Slot = "6")]
		public virtual void Close()
		{
		}

		// Token: 0x0600436D RID: 17261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE3")]
		[Address(RVA = "0x1015D60F4", Offset = "0x15D60F4", VA = "0x1015D60F4")]
		private void Update()
		{
		}

		// Token: 0x0600436E RID: 17262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE4")]
		[Address(RVA = "0x1015D6134", Offset = "0x15D6134", VA = "0x1015D6134", Slot = "7")]
		protected virtual void UpdateIn()
		{
		}

		// Token: 0x0600436F RID: 17263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE5")]
		[Address(RVA = "0x1015D6230", Offset = "0x15D6230", VA = "0x1015D6230", Slot = "8")]
		protected virtual void UpdateIdle()
		{
		}

		// Token: 0x06004370 RID: 17264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE6")]
		[Address(RVA = "0x1015D6234", Offset = "0x15D6234", VA = "0x1015D6234", Slot = "9")]
		protected virtual void UpdateOut()
		{
		}

		// Token: 0x06004371 RID: 17265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE7")]
		[Address(RVA = "0x1015D6278", Offset = "0x15D6278", VA = "0x1015D6278")]
		private void OnGUI()
		{
		}

		// Token: 0x06004372 RID: 17266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE8")]
		[Address(RVA = "0x1015D5798", Offset = "0x15D5798", VA = "0x1015D5798")]
		private void DestroyWebView()
		{
		}

		// Token: 0x06004373 RID: 17267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DE9")]
		[Address(RVA = "0x1015D5B20", Offset = "0x15D5B20", VA = "0x1015D5B20")]
		public void AddTab(WebViewWindowTabInformation tabInformation)
		{
		}

		// Token: 0x06004374 RID: 17268 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003DEA")]
		[Address(RVA = "0x1015D5F44", Offset = "0x15D5F44", VA = "0x1015D5F44")]
		public string[] GetTabTitleArray(List<WebViewWindowTabInformation> tabInformations)
		{
			return null;
		}

		// Token: 0x06004375 RID: 17269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DEB")]
		[Address(RVA = "0x1015D5C48", Offset = "0x15D5C48", VA = "0x1015D5C48")]
		public void SetSelectedTab(int index)
		{
		}

		// Token: 0x06004376 RID: 17270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DEC")]
		[Address(RVA = "0x1015D6308", Offset = "0x15D6308", VA = "0x1015D6308")]
		public void SetViewMargin(int marginLeft, int marginTop, int marginRight, int marginBottom)
		{
		}

		// Token: 0x06004377 RID: 17271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DED")]
		[Address(RVA = "0x1015D5B90", Offset = "0x15D5B90", VA = "0x1015D5B90")]
		public void SetWindowTitle(string title)
		{
		}

		// Token: 0x06004378 RID: 17272 RVA: 0x00019860 File Offset: 0x00017A60
		[Token(Token = "0x6003DEE")]
		[Address(RVA = "0x1015D627C", Offset = "0x15D627C", VA = "0x1015D627C")]
		public bool IsOpenWindow()
		{
			return default(bool);
		}

		// Token: 0x06004379 RID: 17273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DEF")]
		[Address(RVA = "0x1015D64E8", Offset = "0x15D64E8", VA = "0x1015D64E8")]
		public void OnClickTabButton(int index)
		{
		}

		// Token: 0x0600437A RID: 17274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003DF0")]
		[Address(RVA = "0x1015D6528", Offset = "0x15D6528", VA = "0x1015D6528")]
		public WebViewWindowUI()
		{
		}

		// Token: 0x0400540B RID: 21515
		[Token(Token = "0x4003B2E")]
		private const string URL_UPDATE = "update";

		// Token: 0x0400540C RID: 21516
		[Token(Token = "0x4003B2F")]
		private const string URL_EVENT = "event";

		// Token: 0x0400540D RID: 21517
		[Token(Token = "0x4003B30")]
		private const string URL_BUG = "bug";

		// Token: 0x0400540E RID: 21518
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003B31")]
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x0400540F RID: 21519
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003B32")]
		[SerializeField]
		private Text m_WindowTitle;

		// Token: 0x04005410 RID: 21520
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003B33")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04005411 RID: 21521
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003B34")]
		[SerializeField]
		private WebViewController m_Controller;

		// Token: 0x04005412 RID: 21522
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003B35")]
		[SerializeField]
		private RectTransform m_RectSamples;

		// Token: 0x04005413 RID: 21523
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003B36")]
		private List<WebViewWindowTabInformation> m_TabInformations;

		// Token: 0x04005414 RID: 21524
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003B37")]
		private bool m_Setuped;

		// Token: 0x04005415 RID: 21525
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003B38")]
		private string m_OpenURL;

		// Token: 0x04005416 RID: 21526
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003B39")]
		private WebViewWindowUI.eStep m_Step;

		// Token: 0x02000E3E RID: 3646
		[Token(Token = "0x2001149")]
		public enum eStep
		{
			// Token: 0x04005418 RID: 21528
			[Token(Token = "0x4006C27")]
			None = -1,
			// Token: 0x04005419 RID: 21529
			[Token(Token = "0x4006C28")]
			In,
			// Token: 0x0400541A RID: 21530
			[Token(Token = "0x4006C29")]
			Idle,
			// Token: 0x0400541B RID: 21531
			[Token(Token = "0x4006C2A")]
			Out
		}
	}
}
