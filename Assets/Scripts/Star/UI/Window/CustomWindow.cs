﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Window {
    public class CustomWindow : MonoBehaviour {
        public const int FOOTER_YES = 0;
        public const int FOOTER_NO = 1;
        public const int FOOTER_OK = 0;
        public const int FOOTER_CLOSE = 0;
        private const float FOOTER_EXTEND_MARGIN = 32f;

        [SerializeField] protected AnimUIPlayer[] m_Anims;
        [SerializeField] private VerticalLayoutGroup m_VerticalLayoutGroup;
        [SerializeField] private RectTransform m_WindowRectTransform;
        [SerializeField] private GameObject m_BGDark;
        [SerializeField] protected WindowTitle m_Title;
        [SerializeField] protected WindowContent m_Content;
        [SerializeField] protected WindowFooter m_Footer;
        [SerializeField] protected bool m_IsAvailableAndroidBackKey = true;

        protected List<FooterButton> m_FooterButtonList = new List<FooterButton>();
        protected int m_LastPressedFooterButtonID = -1;
        protected RectTransform m_RectTransform;
        public CustomButton m_TargetBackButton;

        private eText_CommonDB[] TARGET_BACK_BUTTON_ENUMS_Common = new eText_CommonDB[] {
            eText_CommonDB.CommonOK,
            eText_CommonDB.CommonNo,
            eText_CommonDB.CommonCancel,
            eText_CommonDB.CommonClose,
            eText_CommonDB.CommonGoBack,
            eText_CommonDB.BattleResultNext,
            eText_CommonDB.ADVSkipCancelButton,
            eText_CommonDB.GachaPlayResultWindowCloseButton,
            eText_CommonDB.MissionButtonChancel,
            eText_CommonDB.TitleTitleMainAgreeNoAgreeButton,
            eText_CommonDB.StoreReviewNo
        };

        private eText_MessageDB[] TARGET_BACK_BUTTON_ENUMS_Message = new eText_MessageDB[] {
            eText_MessageDB.GachaPlayReplayCheck,
            eText_MessageDB.MovieSkipNo
        };

        public event Action OnClickFooterButton;

        public RectTransform WindowRectTransform => m_WindowRectTransform;
        public GameObject BGDark => m_BGDark;
        public int LastPressedFooterButtonID => m_LastPressedFooterButtonID;

        public RectTransform RectTransform {
            get {
                if (m_RectTransform == null) {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        protected virtual void Update() {
            UpdateAndroidBackKey();
        }

        public void Open() {
            for (int i = 0; i < m_Anims.Length; i++) {
                m_Anims[i].PlayIn();
            }
            if (m_BGDark != null) {
                FixSortOrder component = RectTransform.GetComponent<FixSortOrder>();
                m_BGDark.GetComponent<FixSortOrder>().SetSortOrder(component.GetSortOrderID(), component.GetOffset() - 1);
            }
        }

        public void Close() {
            for (int i = 0; i < m_Anims.Length; i++) {
                m_Anims[i].PlayOut();
            }
        }

        public bool IsPlayingAnim() {
            for (int i = 0; i < m_Anims.Length; i++) {
                if (!m_Anims[i].IsEnd) {
                    return true;
                }
            }
            return false;
        }

        public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrderType, int offset) {
            RectTransform.GetComponent<FixSortOrder>().SetSortOrder(sortOrderType, offset);
            if (m_BGDark != null) {
                m_BGDark.GetComponent<FixSortOrder>().SetSortOrder(sortOrderType, offset - 1);
            }
        }

        public void SetTitle(string titleText) {
            if (m_Title != null) {
                if (string.IsNullOrEmpty(titleText)) {
                    if (m_Title.m_GameObject.activeSelf) {
                        m_Title.m_GameObject.SetActive(false);
                    }
                } else {
                    m_Title.m_GameObject.SetActive(true);
                    m_Title.m_TextObj.text = titleText;
                }
            }
        }

        public void SetContent(RectTransform contentObj) {
            if (contentObj != null) {
                contentObj.SetParent(m_Content.m_ContentParent, false);
                contentObj.anchorMin = new Vector2(0.5f, 1f);
                contentObj.anchorMax = new Vector2(0.5f, 1f);
                contentObj.pivot = new Vector2(0.5f, 1f);
                contentObj.anchoredPosition = new Vector2(0f, 0f);
                contentObj.localScale = Vector3.one;
            }
        }

        public void ClearFooterButton() {
            for (int i = 0; i < m_FooterButtonList.Count; i++) {
                Destroy(m_FooterButtonList[i].gameObject);
            }
            m_FooterButtonList.Clear();
            m_TargetBackButton = null;
        }

        public FooterButton AddFooterButton(int id, string text) {
            FooterButton footerButton = Instantiate(m_Footer.m_FooterButtonPrefab, m_Footer.m_FooterButtonParent);
            footerButton.Create(id);
            Text footerText = footerButton.GetComponentsInChildren<Text>(true)[0];
            footerText.text = text;
            float prefWidth = footerText.preferredWidth + FOOTER_EXTEND_MARGIN;
            float width = footerButton.CustomButton.GetComponent<RectTransform>().rect.width;
            if (prefWidth > width) {
                float scale = width / prefWidth;
                footerText.rectTransform.localScale = new Vector3(scale, scale);
            }
            footerButton.OnClick += OnClickFooterButtonCallBack;
            m_FooterButtonList.Add(footerButton);
            return footerButton;
        }

        public void OnClickFooterButtonCallBack(int buttonID) {
            m_LastPressedFooterButtonID = buttonID;
            OnClickFooterButton.Call();
        }

        public void SetFooterOK() {
            ClearFooterButton();
            AddFooterButton(FOOTER_OK, GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonOK));
        }

        public void SetFooterClose() {
            ClearFooterButton();
            FooterButton footerButton = AddFooterButton(FOOTER_CLOSE, GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonClose));
            if (footerButton != null) {
                m_TargetBackButton = footerButton.CustomButton;
            }
        }

        public void SetFooterYesNo() {
            ClearFooterButton();
            FooterButton footerButton = AddFooterButton(FOOTER_NO, GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonNo));
            if (footerButton != null) {
                m_TargetBackButton = footerButton.CustomButton;
            }
            AddFooterButton(FOOTER_YES, GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonYES));
        }

        public void SetFooterCancel() {
            ClearFooterButton();
            FooterButton footerButton = AddFooterButton(FOOTER_NO, GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonCancel));
            if (footerButton != null) {
                m_TargetBackButton = footerButton.CustomButton;
            }
        }

        public FooterButton GetFooterButton(int id) {
            for (int i = 0; i < m_FooterButtonList.Count; i++) {
                if (m_FooterButtonList[i].GetID() == id) {
                    return m_FooterButtonList[i];
                }
            }
            return null;
        }

        private void UpdateAndroidBackKey() {
            if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape)) {
                if (InputTouch.Inst.GetAvailableTouchCnt() > 0) {
                    return;
                }
                if (!m_IsAvailableAndroidBackKey) {
                    return;
                }
                if (IsPlayingAnim()) {
                    return;
                }
                if (m_TargetBackButton == null) {
                    m_TargetBackButton = FindTargetBackButton();
                }
                if (m_TargetBackButton == null) {
                    return;
                }
                if (!m_TargetBackButton.IsEnableInput) {
                    return;
                }
                if (!m_TargetBackButton.Interactable) {
                    return;
                }
                if (!Utility.CheckTapUI(m_TargetBackButton.gameObject)) {
                    return;
                }
                m_TargetBackButton.ExecuteClick();
            }
        }

        private CustomButton FindTargetBackButton() {
            CustomButton[] customButtons = GetComponentsInChildren<CustomButton>();
            if (customButtons != null) {
                for (int i = 0; i < customButtons.Length; i++) {
                    Text[] texts = customButtons[i].GetComponentsInChildren<Text>();
                    if (texts != null) {
                        for (int j = 0; j < texts.Length; j++) {
                            if (CompareTargetBackButtonText(texts[j].text)) {
                                return customButtons[i];
                            }
                        }
                    }
                }
            }
            return null;
        }

        private bool CompareTargetBackButtonText(string text) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            for (int i = 0; i < TARGET_BACK_BUTTON_ENUMS_Common.Length; i++) {
                if (text == dbMng.GetTextCommon(TARGET_BACK_BUTTON_ENUMS_Common[i])) {
                    return true;
                }
            }
            for (int j = 0; j < TARGET_BACK_BUTTON_ENUMS_Message.Length; j++) {
                if (text == dbMng.GetTextMessage(TARGET_BACK_BUTTON_ENUMS_Message[j])) {
                    return true;
                }
            }
            return false;
        }

        [Serializable]
        public class WindowTitle {
            public GameObject m_GameObject;
            public Text m_TextObj;
        }

        [Serializable]
        public class WindowContent {
            public GameObject m_GameObject;
            public RectTransform m_ContentParent;
        }

        [Serializable]
        public class WindowFooter {
            public FooterButton m_FooterButtonPrefab;
            public RectTransform m_FooterButtonParent;
        }
    }
}
