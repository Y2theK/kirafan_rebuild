﻿using System;
using UnityEngine;

namespace Star.UI.Window {
    public class FooterButton : MonoBehaviour {
        [SerializeField] private CustomButton m_ButtonPrefab;

        private CustomButton m_Button;
        private int m_ID = -1;
        public Action<int> OnClick;

        public CustomButton CustomButton => m_Button;

        public void Create(int id) {
            m_ID = id;
            m_Button = Instantiate(m_ButtonPrefab, GetComponent<RectTransform>());
            RectTransform buttonTransform = m_Button.GetComponent<RectTransform>();
            buttonTransform.localPosition = Vector2.zero;
            buttonTransform.anchorMin = new Vector2(0f, 0f);
            buttonTransform.anchorMax = new Vector2(1f, 1f);
            buttonTransform.sizeDelta = new Vector2(0f, 0f);
            m_Button.m_OnClick.AddListener(OnClickCallBack);
        }

        public int GetID() {
            return m_ID;
        }

        public void OnClickCallBack() {
            OnClick.Call(m_ID);
        }
    }
}
