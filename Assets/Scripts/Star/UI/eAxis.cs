﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D51 RID: 3409
	[Token(Token = "0x2000924")]
	[StructLayout(3, Size = 4)]
	public enum eAxis
	{
		// Token: 0x04004DD9 RID: 19929
		[Token(Token = "0x40036CE")]
		Error,
		// Token: 0x04004DDA RID: 19930
		[Token(Token = "0x40036CF")]
		Vertical,
		// Token: 0x04004DDB RID: 19931
		[Token(Token = "0x40036D0")]
		Horizontal
	}
}
