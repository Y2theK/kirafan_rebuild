﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D21 RID: 3361
	[Token(Token = "0x20008FE")]
	[StructLayout(3)]
	public class iTweenController : MonoBehaviour
	{
		// Token: 0x06003DA2 RID: 15778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385E")]
		[Address(RVA = "0x1015D8168", Offset = "0x15D8168", VA = "0x1015D8168")]
		public void Play(iTweenWrapperPlayArgument argument)
		{
		}

		// Token: 0x06003DA3 RID: 15779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385F")]
		[Address(RVA = "0x1015D816C", Offset = "0x15D816C", VA = "0x1015D816C")]
		public iTweenController()
		{
		}

		// Token: 0x02000D22 RID: 3362
		[Token(Token = "0x20010EA")]
		public enum eiTweenActionType
		{
			// Token: 0x04004D09 RID: 19721
			[Token(Token = "0x4006A31")]
			None,
			// Token: 0x04004D0A RID: 19722
			[Token(Token = "0x4006A32")]
			MoveTo,
			// Token: 0x04004D0B RID: 19723
			[Token(Token = "0x4006A33")]
			RotateTo,
			// Token: 0x04004D0C RID: 19724
			[Token(Token = "0x4006A34")]
			ScaleTo,
			// Token: 0x04004D0D RID: 19725
			[Token(Token = "0x4006A35")]
			ColorTo,
			// Token: 0x04004D0E RID: 19726
			[Token(Token = "0x4006A36")]
			ValueTo
		}

		// Token: 0x02000D23 RID: 3363
		// (Invoke) Token: 0x06003DA5 RID: 15781
		[Token(Token = "0x20010EB")]
		public delegate void OnCompleteDelegate(object obj);
	}
}
