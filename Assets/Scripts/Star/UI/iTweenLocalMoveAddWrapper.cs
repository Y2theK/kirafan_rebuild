﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D24 RID: 3364
	[Token(Token = "0x20008FF")]
	[StructLayout(3)]
	public class iTweenLocalMoveAddWrapper : iTweenMoveToWrapper
	{
		// Token: 0x06003DA8 RID: 15784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003860")]
		[Address(RVA = "0x1015D86C8", Offset = "0x15D86C8", VA = "0x1015D86C8", Slot = "4")]
		public override void Play(iTweenWrapperPlayArgument argument)
		{
		}

		// Token: 0x06003DA9 RID: 15785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003861")]
		[Address(RVA = "0x1015D917C", Offset = "0x15D917C", VA = "0x1015D917C")]
		public iTweenLocalMoveAddWrapper()
		{
		}
	}
}
