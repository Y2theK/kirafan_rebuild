﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D25 RID: 3365
	[Token(Token = "0x2000900")]
	[StructLayout(3)]
	public class iTweenMoveAddWrapper : iTweenWrapper
	{
		// Token: 0x06003DAA RID: 15786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003862")]
		[Address(RVA = "0x1015D918C", Offset = "0x15D918C", VA = "0x1015D918C", Slot = "5")]
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06003DAB RID: 15787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003863")]
		[Address(RVA = "0x1015D9204", Offset = "0x15D9204", VA = "0x1015D9204", Slot = "6")]
		public override void Stop()
		{
		}

		// Token: 0x06003DAC RID: 15788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003864")]
		[Address(RVA = "0x1015D9308", Offset = "0x15D9308", VA = "0x1015D9308")]
		public iTweenMoveAddWrapper()
		{
		}
	}
}
