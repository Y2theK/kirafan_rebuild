﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D26 RID: 3366
	[Token(Token = "0x2000901")]
	[StructLayout(3)]
	public class iTweenMoveToWrapper : iTweenWrapper
	{
		// Token: 0x06003DAD RID: 15789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003865")]
		[Address(RVA = "0x1015D9318", Offset = "0x15D9318", VA = "0x1015D9318", Slot = "5")]
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06003DAE RID: 15790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003866")]
		[Address(RVA = "0x1015D9390", Offset = "0x15D9390", VA = "0x1015D9390", Slot = "6")]
		public override void Stop()
		{
		}

		// Token: 0x06003DAF RID: 15791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003867")]
		[Address(RVA = "0x1015D9184", Offset = "0x15D9184", VA = "0x1015D9184")]
		public iTweenMoveToWrapper()
		{
		}
	}
}
