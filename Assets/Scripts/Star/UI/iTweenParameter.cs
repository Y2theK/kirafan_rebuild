﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D2A RID: 3370
	[Token(Token = "0x2000905")]
	[StructLayout(3)]
	public class iTweenParameter
	{
		// Token: 0x06003DBA RID: 15802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003872")]
		[Address(RVA = "0x1015D941C", Offset = "0x15D941C", VA = "0x1015D941C")]
		public iTweenParameter(string in_paramName, float in_value)
		{
		}

		// Token: 0x04004D0F RID: 19727
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003627")]
		public string paramName;

		// Token: 0x04004D10 RID: 19728
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003628")]
		public float value;
	}
}
