﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D27 RID: 3367
	[Token(Token = "0x2000902")]
	[StructLayout(3)]
	public class iTweenRotateToWrapper : iTweenWrapper
	{
		// Token: 0x06003DB0 RID: 15792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003868")]
		[Address(RVA = "0x1015D9458", Offset = "0x15D9458", VA = "0x1015D9458", Slot = "5")]
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06003DB1 RID: 15793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003869")]
		[Address(RVA = "0x1015D94D0", Offset = "0x15D94D0", VA = "0x1015D94D0", Slot = "6")]
		public override void Stop()
		{
		}

		// Token: 0x06003DB2 RID: 15794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600386A")]
		[Address(RVA = "0x1015D955C", Offset = "0x15D955C", VA = "0x1015D955C")]
		public iTweenRotateToWrapper()
		{
		}
	}
}
