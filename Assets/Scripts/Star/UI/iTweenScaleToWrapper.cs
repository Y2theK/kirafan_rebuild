﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D28 RID: 3368
	[Token(Token = "0x2000903")]
	[StructLayout(3)]
	public class iTweenScaleToWrapper : iTweenWrapper
	{
		// Token: 0x06003DB3 RID: 15795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600386B")]
		[Address(RVA = "0x1015D9564", Offset = "0x15D9564", VA = "0x1015D9564", Slot = "5")]
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06003DB4 RID: 15796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600386C")]
		[Address(RVA = "0x1015D95DC", Offset = "0x15D95DC", VA = "0x1015D95DC", Slot = "6")]
		public override void Stop()
		{
		}

		// Token: 0x06003DB5 RID: 15797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600386D")]
		[Address(RVA = "0x1015D9668", Offset = "0x15D9668", VA = "0x1015D9668")]
		public iTweenScaleToWrapper()
		{
		}
	}
}
