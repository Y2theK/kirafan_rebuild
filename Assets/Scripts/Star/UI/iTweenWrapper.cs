﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D2C RID: 3372
	[Token(Token = "0x2000907")]
	[StructLayout(3)]
	public class iTweenWrapper : MonoBehaviour
	{
		// Token: 0x06003DC7 RID: 15815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600387F")]
		[Address(RVA = "0x1015D9670", Offset = "0x15D9670", VA = "0x1015D9670")]
		private void Start()
		{
		}

		// Token: 0x06003DC8 RID: 15816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003880")]
		[Address(RVA = "0x1015D8E08", Offset = "0x15D8E08", VA = "0x1015D8E08", Slot = "4")]
		public virtual void Play(iTweenWrapperPlayArgument argument)
		{
		}

		// Token: 0x06003DC9 RID: 15817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003881")]
		[Address(RVA = "0x1015D96DC", Offset = "0x15D96DC", VA = "0x1015D96DC", Slot = "5")]
		protected virtual void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06003DCA RID: 15818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003882")]
		[Address(RVA = "0x1015D9300", Offset = "0x15D9300", VA = "0x1015D9300", Slot = "6")]
		public virtual void Stop()
		{
		}

		// Token: 0x06003DCB RID: 15819 RVA: 0x00018870 File Offset: 0x00016A70
		[Token(Token = "0x6003883")]
		[Address(RVA = "0x1015D9290", Offset = "0x15D9290", VA = "0x1015D9290")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06003DCC RID: 15820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003884")]
		[Address(RVA = "0x1015D96E0", Offset = "0x15D96E0", VA = "0x1015D96E0", Slot = "7")]
		protected virtual void OnCompleteCallback()
		{
		}

		// Token: 0x06003DCD RID: 15821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003885")]
		[Address(RVA = "0x1015D9310", Offset = "0x15D9310", VA = "0x1015D9310")]
		public iTweenWrapper()
		{
		}

		// Token: 0x04004D1C RID: 19740
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003634")]
		protected GameObject m_Target;

		// Token: 0x04004D1D RID: 19741
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003635")]
		protected OniTweenCompleteDelegate m_OnCompleteCallback;
	}
}
