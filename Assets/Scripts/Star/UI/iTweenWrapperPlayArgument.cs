﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D2B RID: 3371
	[Token(Token = "0x2000906")]
	[StructLayout(3)]
	public class iTweenWrapperPlayArgument
	{
		// Token: 0x06003DBB RID: 15803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003873")]
		[Address(RVA = "0x1015D96FC", Offset = "0x15D96FC", VA = "0x1015D96FC")]
		public iTweenWrapperPlayArgument(GameObject in_target)
		{
		}

		// Token: 0x06003DBC RID: 15804 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003874")]
		[Address(RVA = "0x1015D8CE8", Offset = "0x15D8CE8", VA = "0x1015D8CE8")]
		public iTweenWrapperPlayArgument AddXParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DBD RID: 15805 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003875")]
		[Address(RVA = "0x1015D8D48", Offset = "0x15D8D48", VA = "0x1015D8D48")]
		public iTweenWrapperPlayArgument AddYParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DBE RID: 15806 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003876")]
		[Address(RVA = "0x1015D8DA8", Offset = "0x15D8DA8", VA = "0x1015D8DA8")]
		public iTweenWrapperPlayArgument AddZParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DBF RID: 15807 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003877")]
		[Address(RVA = "0x1015D992C", Offset = "0x15D992C", VA = "0x1015D992C")]
		public iTweenWrapperPlayArgument AddRParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DC0 RID: 15808 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003878")]
		[Address(RVA = "0x1015D998C", Offset = "0x15D998C", VA = "0x1015D998C")]
		public iTweenWrapperPlayArgument AddGParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DC1 RID: 15809 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003879")]
		[Address(RVA = "0x1015D99EC", Offset = "0x15D99EC", VA = "0x1015D99EC")]
		public iTweenWrapperPlayArgument AddBParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DC2 RID: 15810 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600387A")]
		[Address(RVA = "0x1015D9A4C", Offset = "0x15D9A4C", VA = "0x1015D9A4C")]
		public iTweenWrapperPlayArgument AddAParameter(float value)
		{
			return null;
		}

		// Token: 0x06003DC3 RID: 15811 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600387B")]
		[Address(RVA = "0x1015D9798", Offset = "0x15D9798", VA = "0x1015D9798")]
		public iTweenWrapperPlayArgument AddParameter(string paramName, float value)
		{
			return null;
		}

		// Token: 0x06003DC4 RID: 15812 RVA: 0x00018858 File Offset: 0x00016A58
		[Token(Token = "0x600387C")]
		[Address(RVA = "0x1015D9678", Offset = "0x15D9678", VA = "0x1015D9678")]
		public int HowManyParameters()
		{
			return 0;
		}

		// Token: 0x06003DC5 RID: 15813 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600387D")]
		[Address(RVA = "0x1015D8B2C", Offset = "0x15D8B2C", VA = "0x1015D8B2C")]
		public iTweenParameter GetParameter(int index)
		{
			return null;
		}

		// Token: 0x06003DC6 RID: 15814 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600387E")]
		[Address(RVA = "0x1015D8BD4", Offset = "0x15D8BD4", VA = "0x1015D8BD4")]
		public iTweenParameter GetParameter(string paramName)
		{
			return null;
		}

		// Token: 0x04004D11 RID: 19729
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003629")]
		public GameObject target;

		// Token: 0x04004D12 RID: 19730
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400362A")]
		public bool isLocal;

		// Token: 0x04004D13 RID: 19731
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400362B")]
		private List<iTweenParameter> parameters;

		// Token: 0x04004D14 RID: 19732
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400362C")]
		public float time;

		// Token: 0x04004D15 RID: 19733
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400362D")]
		public iTween.EaseType easeType;

		// Token: 0x04004D16 RID: 19734
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400362E")]
		public float delay;

		// Token: 0x04004D17 RID: 19735
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400362F")]
		public iTween.LoopType loopType;

		// Token: 0x04004D18 RID: 19736
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003630")]
		public string onupdate;

		// Token: 0x04004D19 RID: 19737
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003631")]
		public object onupdatetarget;

		// Token: 0x04004D1A RID: 19738
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003632")]
		public OniTweenCompleteDelegate oncompletecallback;

		// Token: 0x04004D1B RID: 19739
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003633")]
		public bool ignoretimescale;
	}
}
