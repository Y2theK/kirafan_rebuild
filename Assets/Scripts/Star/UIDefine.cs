﻿using UnityEngine;

namespace Star {
    public static class UIDefine {
        public static readonly int[] SORT_ORDER = new int[(int)eSortOrderTypeID.Num] {
            -100,
            0,
            100,
            200,
            700,
            -200,
            900,
            1000,
            1300,
            1100,
            1200,
            1400,
            1500,
            300,
            400,
            500,
            600,
            1200,
            1099,
            850,
            1000,
            -100,
            -100,
            0,
            1303,
            -100,
            900
        };

        public static readonly eCameraType[] CAMERA_TYPE = new eCameraType[(int)eSortOrderTypeID.Num] {
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.FullUI,
            eCameraType.FullUI,
            eCameraType.UI,
            eCameraType.System,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.System,
            eCameraType.System,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.UI,
            eCameraType.System,
            eCameraType.System,
            eCameraType.FullUI,
            eCameraType.UI,
            eCameraType.FullUI,
            eCameraType.FullUI,
            eCameraType.FullUI,
            eCameraType.FullUI,
            eCameraType.FullUI,
            eCameraType.FullUI
        };

        public const int STAMINA_DISPMAX = 9999;
        public const int GEM_DISPMAX = 9999999;
        public const int GOLD_DISPMAX = 99999999;
        public const int ITEM_HAVENUM_DISPMAX = 9999;
        public const int ITEM_HAVENUM_DISPMAX_TRADE_SRC = 9999;
        public const int NUMBERSELECT_DISPMAX = 999;
        public const int DRAWPOINT_DISPMAX = 999999;
        public const long EVENTPOINT_DISPMAX = 9999999999L;
        public const int CHARA_LV_DIGIT = 3;
        public const int CHARA_FRIENDSHIP_LV_DIGIT = 2;
        public const int NEXT_EXP_DIGIT = 8;
        public const int CHARA_COST_DIGIT = 3;
        public const int CHARA_STATUS_DIGIT = 6;
        public const int SKILL_LV_DIGIT = 3;
        public const int WEAPON_LV_DIGIT = 3;
        public const int ORB_LV_DIGIT = 2;

        public static readonly Color NONE_ELEMENT_COLOR = new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f);
        public static readonly Color FIRE_COLOR = new Color(1f, 0.34117648f, 0.019607844f, 1f);
        public static readonly Color WATER_COLOR = new Color(0.28235295f, 0.73333335f, 0.8745098f, 1f);
        public static readonly Color EARTH_COLOR = new Color(0.9607843f, 0.5411765f, 0.043137256f, 1f);
        public static readonly Color WIND_COLOR = new Color(0.36078432f, 0.78431374f, 0.33333334f, 1f);
        public static readonly Color MOON_COLOR = new Color(0.8784314f, 0.44313726f, 0.79607844f, 1f);
        public static readonly Color SUN_COLOR = new Color(0.89411765f, 0.80784315f, 0.17254902f, 1f);

        public static readonly eElementType[] ELEMENT_SORT = new eElementType[]
        {
            eElementType.Fire,
            eElementType.Wind,
            eElementType.Earth,
            eElementType.Water,
            eElementType.Moon,
            eElementType.Sun
        };

        public static readonly Color CANT_COLOR = new Color(0.39215687f, 0.5882353f, 1f, 1f);
        public static readonly string STAMINA_REDUCTION = "blue";
        public static readonly Color32 TEXT_COLOR = new Color32(0x6e, 0x41, 0x35, 0xff);

        public const int SCREEN_SIZE_X = 1334;
        public const int SCREEN_SIZE_Y = 750;
        public const int SCREEN_ASPECT_RATIO_X = 16;
        public const int SCREEN_ASPECT_RATIO_Y = 9;
        public const float IDEAL_ASPECT_RATIO = 0.5625f;

        public enum eSortOrderTypeID {
            Inherited = -1,
            BackGround_Low,
            UI,
            BackGround_OverlayUI0,
            OverlayUI0,
            GlobalUI,
            Fade,
            Loading,
            UniqueWindow,
            CmnWindow,
            TutorialMessage,
            TutorialMessageOverlay,
            Connecting,
            TouchEffect,
            BackGround_OverlayUI1,
            OverlayUI1,
            BackGround_OverlayUI2,
            OverlayUI2,
            WebViewUI,
            InfoUI,
            FirstLoad,
            UniqueWindowInUI,
            SystemBG,
            TitleBackGround,
            TitleUI,
            FullScreenFilter,
            MissionPopUp,
            TitleLogo,
            Num
        }

        public enum eCameraType {
            UI,
            FullUI,
            System,
            Max
        }
    }
}
