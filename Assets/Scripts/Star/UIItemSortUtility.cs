﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BF1 RID: 3057
	[Token(Token = "0x200083D")]
	[StructLayout(3)]
	public static class UIItemSortUtility
	{
		// Token: 0x06003599 RID: 13721 RVA: 0x000169E0 File Offset: 0x00014BE0
		[Token(Token = "0x60030BE")]
		[Address(RVA = "0x1015D9DB4", Offset = "0x15D9DB4", VA = "0x1015D9DB4")]
		public static int ItemSortComparision(ItemListDB_Param a, ItemListDB_Param b)
		{
			return 0;
		}

		// Token: 0x0600359A RID: 13722 RVA: 0x000169F8 File Offset: 0x00014BF8
		[Token(Token = "0x60030BF")]
		[Address(RVA = "0x1023F4C18", Offset = "0x23F4C18", VA = "0x1023F4C18")]
		public static int ItemSortComparision<T>(UIItemSortUtility.ItemSortData<T> a, UIItemSortUtility.ItemSortData<T> b)
		{
			return 0;
		}

		// Token: 0x0600359B RID: 13723 RVA: 0x00016A10 File Offset: 0x00014C10
		[Token(Token = "0x60030C0")]
		[Address(RVA = "0x1023F4D04", Offset = "0x23F4D04", VA = "0x1023F4D04")]
		public static int ItemSortComparision_Appeal<T>(UIItemSortUtility.ItemSortData<T> a, UIItemSortUtility.ItemSortData<T> b)
		{
			return 0;
		}

		// Token: 0x0600359C RID: 13724 RVA: 0x00016A28 File Offset: 0x00014C28
		[Token(Token = "0x60030C1")]
		[Address(RVA = "0x1023F4D90", Offset = "0x23F4D90", VA = "0x1023F4D90")]
		public static int ItemSortComparision_Rarity<T>(UIItemSortUtility.ItemSortData<T> a, UIItemSortUtility.ItemSortData<T> b)
		{
			return 0;
		}

		// Token: 0x0600359D RID: 13725 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60030C2")]
		[Address(RVA = "0x1015D9DC4", Offset = "0x15D9DC4", VA = "0x1015D9DC4")]
		public static List<BattleDropItem.DropData> Sort(List<BattleDropItem.DropData> list, [Optional] Comparison<UIItemSortUtility.ItemSortData<BattleDropItem.DropData>> comparison)
		{
			return null;
		}

		// Token: 0x0600359E RID: 13726 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60030C3")]
		[Address(RVA = "0x1015D9F14", Offset = "0x15D9F14", VA = "0x1015D9F14")]
		public static List<UserItemData> Sort(List<UserItemData> list, [Optional] Comparison<UIItemSortUtility.ItemSortData<UserItemData>> comparison)
		{
			return null;
		}

		// Token: 0x0600359F RID: 13727 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60030C4")]
		[Address(RVA = "0x1015DA064", Offset = "0x15DA064", VA = "0x1015DA064")]
		public static List<int> Sort(List<int> list, [Optional] Comparison<UIItemSortUtility.ItemSortData<int>> comparison)
		{
			return null;
		}

		// Token: 0x060035A0 RID: 13728 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60030C5")]
		[Address(RVA = "0x1023C4A48", Offset = "0x23C4A48", VA = "0x1023C4A48")]
		public static List<T> Sort<T>(List<UIItemSortUtility.ItemSortData<T>> list, [Optional] Comparison<UIItemSortUtility.ItemSortData<T>> comparison)
		{
			return null;
		}

		// Token: 0x060035A1 RID: 13729 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60030C6")]
		[Address(RVA = "0x1023C5098", Offset = "0x23C5098", VA = "0x1023C5098")]
		private static List<T> SortInternal<T>(List<T> originalList, IEnumerable<int> itemIDList, [Optional] Comparison<UIItemSortUtility.ItemSortData<T>> comparison)
		{
			return null;
		}

		// Token: 0x02000BF2 RID: 3058
		[Token(Token = "0x200107D")]
		public class ItemSortData<T>
		{
			// Token: 0x060035A2 RID: 13730 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006191")]
			[Address(RVA = "0x1016D53E4", Offset = "0x16D53E4", VA = "0x1016D53E4")]
			public ItemSortData()
			{
			}

			// Token: 0x0400460E RID: 17934
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006826")]
			public T m_Holder;

			// Token: 0x0400460F RID: 17935
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006827")]
			public ItemListDB_Param m_Param;
		}
	}
}
