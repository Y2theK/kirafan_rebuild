﻿using Star.UI;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star {
    public static class UIUtility {
        public static bool CheckPointOverUI(Vector2 pos, int ignoreLayer = -1) {
            if (EventSystem.current == null) {
                return false;
            }
            PointerEventData evtData = new PointerEventData(EventSystem.current);
            evtData.position = pos;
            if (pos.x > Mathf.Epsilon && pos.x < Screen.width - Mathf.Epsilon && pos.y > Mathf.Epsilon && pos.y < Screen.height - Mathf.Epsilon) {
                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(evtData, results);
                int hits = results.Count;
                if (ignoreLayer != -1) {
                    for (int i = 0; i < results.Count; i++) {
                        if (results[i].gameObject.layer == ignoreLayer) {
                            hits--;
                        }
                    }
                }
                return hits > 0;
            }
            return true;
        }

        public static bool CheckPointerOverTarget(Vector2 pos, GameObject go) {
            PointerEventData evtData = new PointerEventData(EventSystem.current);
            evtData.position = pos;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(evtData, results);
            for (int i = 0; i < results.Count; i++) {
                if (results[i].gameObject == go) {
                    return true;
                }
            }
            return false;
        }

        public static Vector2 GetScaledScreenSize(CanvasScaler scaler) {
            float ratio = (float)Screen.width / Screen.height;
            Vector2 size = Vector2.zero;
            if (scaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight) {
                Vector2 res = scaler.referenceResolution;
                Vector2 a = new Vector2(res.x, res.x / ratio);
                Vector2 b = new Vector2(res.y * ratio, res.y);
                size = Vector2.Lerp(a, b, scaler.matchWidthOrHeight);
                if (res.x >= res.y || Screen.width > Screen.height) { } //TODO: what?
            }
            return size;
        }

        public static Vector2 RawScreenPositionToScaledScreenPosition(CanvasScaler canvasScaler, Vector2 rawPos) {
            Vector2 scaledScreenSize = GetScaledScreenSize(canvasScaler);
            Vector2 zero = Vector2.zero;
            if (canvasScaler.uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize) {
                if (canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.Expand) {
                    zero.x = scaledScreenSize.x / Screen.width * rawPos.x;
                    zero.y = scaledScreenSize.y / Screen.height * rawPos.y;
                } else if (canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight) {
                    if (canvasScaler.referenceResolution.x >= canvasScaler.referenceResolution.y || Screen.width > Screen.height) { } //TODO: what?
                    zero.x = scaledScreenSize.x / Screen.width * rawPos.x;
                    zero.y = scaledScreenSize.y / Screen.height * rawPos.y;
                }
            }
            return zero;
        }

        public static Rect CalcurateScreenSpaceRect(RectTransform rectTrans, Camera camera) {
            Vector3[] corners = new Vector3[4];
            rectTrans.GetWorldCorners(corners);
            Vector3 topLeft = RectTransformUtility.WorldToScreenPoint(camera, corners[1]);
            Vector3 bottomRight = RectTransformUtility.WorldToScreenPoint(camera, corners[3]);
            return new Rect(topLeft.x, bottomRight.y, bottomRight.x - topLeft.x, topLeft.y - bottomRight.y);
        }

        public static bool ContainRect(Rect outer, Rect inner) {
            if (outer.xMin <= inner.xMin && outer.xMax >= inner.xMax) {
                if (outer.yMin <= inner.yMin && outer.yMax >= inner.yMax) {
                    return true;
                }
            }
            return false;
        }

        public static T GetMenuComponent<T>(string menuName) where T : MenuUIBase {
            GameObject menu = GameObject.Find(menuName);
            T component = null;
            if (menu != null) {
                menu.TryGetComponent(out component);
            }
            return component;
        }

        public static int GetFriendListMax() {
            return GameSystem.Inst.UserDataMng.UserData.FriendLimit;
        }

        public static int GetMasterMaxLv() {
            return GameSystem.Inst.DbMng.MasterRankDB.m_Params.Length;
        }

        public static string GetMasterMaxLvRemainString() {
            return "--".PadLeft(8);
        }

        public static string CharaCurrentLvValueToString(int lv, eLvColorChange lvCol) {
            return CharaCurrentLvValueToString(lv.ToString(), lvCol);
        }

        public static string CharaCurrentLvValueToString(string lvText, eLvColorChange lvCol) {
            StringBuilder sb = new StringBuilder();
            sb.Append(lvText);
            if (lvCol == eLvColorChange.Increase) {
                sb = GetIncreaseString(sb);
            } else if (lvCol == eLvColorChange.Decrease) {
                sb = GetDecreaseString(sb);
            } else if (lvCol == eLvColorChange.Bold) {
                sb = GetBoldString(sb);
            }
            return sb.ToString();
        }

        public static string CharaCurrentLvValueToString(int lv, int increase = 0) {
            eLvColorChange change = eLvColorChange.None;
            if (increase > 0) {
                change = eLvColorChange.Increase;
            } else if (increase < 0) {
                change = eLvColorChange.Decrease;
            }
            return CharaCurrentLvValueToString(lv, change);
        }

        public static string CharaLvValueToString(int lv, int maxLv, eLvColorChange lvCol, eLvColorChange maxLvCol) {
            StringBuilder lvSb = new StringBuilder();
            lvSb.Append(lv.ToString());
            if (lvCol == eLvColorChange.Increase) {
                lvSb = GetIncreaseString(lvSb);
            } else if (lvCol == eLvColorChange.Decrease) {
                lvSb = GetDecreaseString(lvSb);
            } else if (lvCol == eLvColorChange.Bold) {
                lvSb = GetBoldString(lvSb);
            } else if (lvCol == eLvColorChange.IncreaseBold) {
                lvSb = GetIncreaseString(lvSb);
                lvSb = GetBoldString(lvSb);
            }
            lvSb.Append(" / ");
            StringBuilder maxSb = new StringBuilder();
            maxSb.Append(maxLv.ToString());
            if (maxLvCol == eLvColorChange.Increase) {
                maxSb = GetIncreaseString(maxSb);
            } else if (maxLvCol == eLvColorChange.Decrease) {
                maxSb = GetDecreaseString(maxSb);
            } else if (maxLvCol == eLvColorChange.Arousal) {
                maxSb = GetArousalString(maxSb);
            } else if (maxLvCol == eLvColorChange.Bold) {
                maxSb = GetBoldString(maxSb);
            }
            lvSb.Append(maxSb.ToString());
            return lvSb.ToString();
        }

        public static string CharaLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0) {
            eLvColorChange lvCol = eLvColorChange.None;
            if (increase > 0) {
                lvCol = eLvColorChange.Increase;
            } else if (increase < 0) {
                lvCol = eLvColorChange.Decrease;
            }
            eLvColorChange maxLvCol = eLvColorChange.None;
            if (increaseMaxLv > 0) {
                maxLvCol = eLvColorChange.Increase;
            } else if (increaseMaxLv < 0) {
                maxLvCol = eLvColorChange.Decrease;
            }
            return CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
        }

        public static string WeaponCurrentLvValueToString(int lv, eLvColorChange lvCol) {
            return CharaCurrentLvValueToString(lv, lvCol);
        }

        public static string WeaponCurrentLvValueToString(int lv, int increase = 0) {
            return CharaCurrentLvValueToString(lv, increase);
        }

        public static string WeaponLvValueToString(int lv, int maxLv, eLvColorChange lvCol, eLvColorChange maxLvCol) {
            return CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
        }

        public static string WeaponLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0) {
            return CharaLvValueToString(lv, maxLv, increase, increaseMaxLv);
        }

        public static string SkillCurrentLvValueToString(int lv, eLvColorChange lvCol) {
            return CharaCurrentLvValueToString(lv, lvCol);
        }

        public static string SkillCurrentLvValueToString(int lv, int increase = 0) {
            return CharaCurrentLvValueToString(lv, increase);
        }

        public static string SkillLvValueToString(int lv, int maxLv, eLvColorChange lvCol, eLvColorChange maxLvCol) {
            return CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
        }

        public static string SkillLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0) {
            return CharaLvValueToString(lv, maxLv, increase, increaseMaxLv);
        }

        public static string CharaCostToString(int cost, int diff = 0) {
            return CharaCurrentLvValueToString(cost, diff);
        }

        public static string StatusValueToString(int value) {
            return value.ToString("#,0");
        }

        public static string ItemHaveNumToString(int value, bool countStop = true) {
            if (countStop && value > UIDefine.ITEM_HAVENUM_DISPMAX) {
                value = UIDefine.ITEM_HAVENUM_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string ItemHaveNumToStringTradeSrc(int value, bool countStop = true) {
            if (countStop && value > UIDefine.ITEM_HAVENUM_DISPMAX) {
                value = UIDefine.ITEM_HAVENUM_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string ExpValueToString(long value, bool countStop = true) {
            if (countStop && value > UIDefine.GOLD_DISPMAX) {
                value = UIDefine.GOLD_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string GoldValueToString(int value, bool countStop = true) {
            if (countStop && value > UIDefine.GOLD_DISPMAX) {
                value = UIDefine.GOLD_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string GoldValueToString(long value, bool countStop = true) {
            if (countStop && value > UIDefine.GOLD_DISPMAX) {
                value = UIDefine.GOLD_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string GemValueToString(int value, bool countStop = true) {
            if (countStop && value > UIDefine.GEM_DISPMAX) {
                value = UIDefine.GEM_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string GemValueToString(long value, bool countStop = true) {
            if (countStop && value > UIDefine.GEM_DISPMAX) {
                value = UIDefine.GEM_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string KPValueToString(int value, bool countStop = true) {
            if (countStop) {
                int limit = (int)GameSystem.Inst.UserDataMng.UserData.KRRPoint.GetLimit();
                if (value >= limit) {
                    value = limit;
                    return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
                }
            }
            return value.ToString("#,0");
        }

        public static string KPValueToString(long value, bool countStop = true) {
            if (countStop) {
                long limit = GameSystem.Inst.UserDataMng.UserData.KRRPoint.GetLimit();
                if (value >= limit) {
                    value = limit;
                    return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
                }
            }
            return value.ToString("#,0");
        }

        public static string DrawPointValueToString(int value, bool countStop = true) {
            if (countStop && value > UIDefine.DRAWPOINT_DISPMAX) {
                value = UIDefine.DRAWPOINT_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string StaminaValueToString(int value, bool countStop = true) {
            if (countStop && value > UIDefine.STAMINA_DISPMAX) {
                value = UIDefine.STAMINA_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static string StaminaValueToString(long value, bool countStop = true, bool checkMax = true) {
            if (countStop) {
                if (value > UIDefine.STAMINA_DISPMAX) {
                    value = UIDefine.STAMINA_DISPMAX;
                    return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
                }
                if (checkMax && value >= GameSystem.Inst.UserDataMng.UserData.Stamina.GetValueMax()) {
                    return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
                }
            }
            return value.ToString();
        }

        public static string MoneyValueToString(int value) {
            return value.ToString("#,0");
        }

        public static string MoneyValueToString(long value) {
            return value.ToString("#,0");
        }

        public static string EventPointValueToString(long value, bool countStop = true) {
            if (countStop && value > UIDefine.EVENTPOINT_DISPMAX) {
                value = UIDefine.EVENTPOINT_DISPMAX;
                return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
            }
            return value.ToString("#,0");
        }

        public static StringBuilder GetNotEnoughString(StringBuilder sb) {
            sb.Insert(0, "<color=red>");
            sb.Append("</color>");
            return sb;
        }

        public static string GetNotEnoughString(string text) {
            return $"<color=red>{text}</color>";
        }

        public static string GetNeedGoldString(int amount) {
            if (GameSystem.Inst.UserDataMng.UserData.IsShortOfGold(amount)) {
                StringBuilder sb = new StringBuilder();
                sb.Append(GoldValueToString(amount, true));
                return GetNotEnoughString(sb).ToString();
            }
            return GoldValueToString(amount, true);
        }

        public static string GetNeedKPString(int amount) {
            if (GameSystem.Inst.UserDataMng.UserData.IsShortOfKP(amount)) {
                StringBuilder sb = new StringBuilder();
                sb.Append(KPValueToString(amount, false));
                return GetNotEnoughString(sb).ToString();
            }
            return KPValueToString(amount, false);
        }

        public static string GetNeedGemString(int amount) {
            if (GameSystem.Inst.UserDataMng.UserData.IsShortOfGem(amount)) {
                StringBuilder sb = new StringBuilder();
                sb.Append(GemValueToString(amount, true));
                return GetNotEnoughString(sb).ToString();
            }
            return GemValueToString(amount, true);
        }

        public static string GetPresentTypeToName(ePresentType presentType, int id) {
            string name = string.Empty;
            switch (presentType) {
                case ePresentType.Chara:
                    name = GameSystem.Inst.DbMng.CharaListDB.GetParam(id).m_Name;
                    break;
                case ePresentType.Item:
                    name = GameSystem.Inst.DbMng.ItemListDB.GetParam(id).m_Name;
                    break;
                case ePresentType.Weapon:
                    name = GameSystem.Inst.DbMng.WeaponListDB.GetParam(id).m_WeaponName;
                    break;
                case ePresentType.TownFacility:
                    name = GameSystem.Inst.DbMng.TownObjListDB.GetParam(id).m_ObjName;
                    break;
                case ePresentType.RoomObject:
                    name = RoomObjectListUtil.GetAccessKeyToObjectParam(id).m_Name;
                    break;
                case ePresentType.MasterOrb:
                    name = GameSystem.Inst.DbMng.MasterOrbListDB.GetParam(id).m_Name;
                    break;
                case ePresentType.KRRPoint:
                    name = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.KiraraPoint);
                    break;
                case ePresentType.Gold:
                    name = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Gold);
                    break;
                case ePresentType.UnlimitedGem:
                case ePresentType.LimitedGem:
                    name = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem);
                    break;
            }
            return name;
        }

        public static string GetPresentValueToString(ePresentType presentType, int amount, bool countStop = true) {
            switch (presentType) {
                case ePresentType.Item:
                    return ItemHaveNumToString(amount, countStop);
                case ePresentType.KRRPoint:
                    return KPValueToString(amount, countStop);
                case ePresentType.Gold:
                    return GoldValueToString(amount, countStop);
                case ePresentType.UnlimitedGem:
                case ePresentType.LimitedGem:
                    return GemValueToString(amount, countStop);
            }
            return amount.ToString();
        }

        public static string GetRewardSetText(RewardSet rewardSet) {
            StringBuilder sb = new StringBuilder();
            switch (rewardSet.m_Type) {
                case ePresentType.Chara:
                    sb.Append(GameSystem.Inst.DbMng.CharaListDB.GetParam(rewardSet.m_ID).m_Name);
                    break;
                case ePresentType.Item:
                    sb.Append(GameSystem.Inst.DbMng.ItemListDB.GetParam(rewardSet.m_ID).m_Name);
                    sb.Append("x");
                    sb.Append(rewardSet.m_Amount);
                    break;
                case ePresentType.Weapon:
                    sb.Append(GameSystem.Inst.DbMng.WeaponListDB.GetParam(rewardSet.m_ID).m_WeaponName);
                    sb.Append("x");
                    sb.Append(rewardSet.m_Amount);
                    break;
                case ePresentType.TownFacility:
                    sb.Append(GameSystem.Inst.DbMng.TownObjListDB.GetParam(rewardSet.m_ID).m_ObjName);
                    sb.Append("x");
                    sb.Append(rewardSet.m_Amount);
                    break;
                case ePresentType.RoomObject:
                    sb.Append(RoomObjectListUtil.GetAccessKeyToObjectParam(rewardSet.m_ID).m_Name);
                    sb.Append("x");
                    sb.Append(rewardSet.m_Amount);
                    break;
                case ePresentType.MasterOrb:
                    sb.Append(GameSystem.Inst.DbMng.MasterOrbListDB.GetParam(rewardSet.m_ID).m_Name);
                    sb.Append("x");
                    sb.Append(rewardSet.m_Amount);
                    break;
                case ePresentType.KRRPoint:
                    sb.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.KiraraPoint));
                    sb.Append("x");
                    sb.Append(KPValueToString(rewardSet.m_Amount));
                    break;
                case ePresentType.Gold:
                    sb.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Gold));
                    sb.Append("x");
                    sb.Append(GoldValueToString(rewardSet.m_Amount));
                    break;
                case ePresentType.UnlimitedGem:
                case ePresentType.LimitedGem:
                    sb.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem));
                    sb.Append("x");
                    sb.Append(GemValueToString(rewardSet.m_Amount));
                    break;
            }
            return sb.ToString();
        }

        public static StringBuilder GetIncreaseString(StringBuilder sb) {
            sb.Insert(0, "<color=#00C882>");
            sb.Append("</color>");
            return sb;
        }

        public static string GetIncreaseString(string text) {
            return $"<color=#00C882>{text}</color>";
        }

        public static StringBuilder GetDecreaseString(StringBuilder sb) {
            sb.Insert(0, "<color=#FF4141>");
            sb.Append("</color>");
            return sb;
        }

        public static StringBuilder GetArousalString(StringBuilder sb) {
            sb.Insert(0, "<color=#00C882>");
            sb.Append("</color>");
            return sb;
        }

        public static StringBuilder GetBoldString(StringBuilder sb) {
            sb.Insert(0, "<size=30>");
            sb.Append("</size>");
            return sb;
        }

        public static StringBuilder GetCountStopString(StringBuilder sb) {
            sb.Insert(0, "<color=#FFBE41>");
            sb.Append("</color>");
            return sb;
        }

        public static string GetLastLoginString(DateTime lastTime) {
            StringBuilder stringBuilder = new StringBuilder();
            TimeSpan timeSpan = GameSystem.Inst.ServerTime - lastTime;
            if (timeSpan.Days > 999) {
                stringBuilder.Append(999);
                stringBuilder.Append("日前");
            } else if (timeSpan.Days > 0) {
                stringBuilder.Append(timeSpan.Days.ToString());
                stringBuilder.Append("日前");
            } else if (timeSpan.Hours > 0) {
                stringBuilder.Append(timeSpan.Hours.ToString());
                stringBuilder.Append("時間前");
            } else {
                stringBuilder.Append(timeSpan.Minutes.ToString());
                stringBuilder.Append("分前");
            }
            return stringBuilder.ToString();
        }

        public static string GetSpanString(TimeSpan span) {
            StringBuilder stringBuilder = new StringBuilder();
            if (span.Days <= 999) {
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
                if (span.Days > 0) {
                    stringBuilder.Append(span.Days.ToString().PadLeft(3));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
                } else if (span.Hours > 0) {
                    stringBuilder.Append(span.Hours.ToString().PadLeft(2));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
                } else if (span.Minutes > 0) {
                    stringBuilder.Append(span.Minutes.ToString().PadLeft(3));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
                } else {
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
                }
            }
            return stringBuilder.ToString();
        }

        public static string GetTradeLimitSpanString(TimeSpan? span) {
            StringBuilder stringBuilder = new StringBuilder();
            if (span.HasValue) {
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
                if (span.Value.Days > 0) {
                    stringBuilder.Append(span.Value.Days.ToString().PadLeft(3));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
                } else if (span.Value.Hours > 0) {
                    stringBuilder.Append(span.Value.Hours.ToString().PadLeft(2));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
                } else if (span.Value.Minutes > 0) {
                    stringBuilder.Append(span.Value.Minutes.ToString().PadLeft(3));
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
                } else {
                    stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
                }
            } else {
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentFilterNotExistDeadLine));
            }
            return stringBuilder.ToString();
        }

        public static string GetMissionLimitSpanString(TimeSpan span) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
            if (span.Days > 0) {
                stringBuilder.Append(span.Days.ToString().PadLeft(3));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
            } else if (span.Hours > 0) {
                stringBuilder.Append(span.Hours.ToString().PadLeft(2));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
            } else if (span.Minutes > 0) {
                stringBuilder.Append(span.Minutes.ToString().PadLeft(3));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
            } else {
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
            }
            return stringBuilder.ToString();
        }

        public static string GetPremiumLimitSpanString(TimeSpan span) {
            string format = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.GemShopPurchaseLimitFormatRich);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.GemShopPurchaseLimitFormat));
            stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
            if (span.Days > 0) {
                stringBuilder.Append(string.Format(format, span.Days.ToString().PadLeft(3)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
            } else if (span.Hours > 0) {
                stringBuilder.Append(string.Format(format, span.Hours.ToString().PadLeft(2)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
            } else {
                int minutes = span.Minutes > 0 ? span.Minutes : 1;
                stringBuilder.Append(string.Format(format, minutes.ToString().PadLeft(3)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
            }
            return stringBuilder.ToString();
        }

        public static string GetRegularPassSpanString(TimeSpan span) {
            string format = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.GemShopPurchaseLimitFormatRich);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.GemShopReloadDay));
            stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
            if (span.Days > 0) {
                stringBuilder.Append(string.Format(format, span.Days.ToString().PadLeft(3)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
            } else if (span.Hours > 0) {
                stringBuilder.Append(string.Format(format, span.Hours.ToString().PadLeft(2)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
            } else {
                int minutes = span.Minutes > 0 ? span.Minutes : 1;
                stringBuilder.Append(string.Format(format, minutes.ToString().PadLeft(3)));
                stringBuilder.Append(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
            }
            return stringBuilder.ToString();
        }

        public static string GetDayOfWeek(DayOfWeek dayofweek) {
            return GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonSunday + (int)dayofweek);
        }

        public static void GetRemainTimeToTimes(out int fday, out int fhour, out int fminute, out int fsec, int ftimes) {
            fday = ftimes / 86400;
            ftimes %= 86400;
            fhour = ftimes / 3600;
            ftimes %= 3600;
            fminute = ftimes / 60;
            fsec = ftimes % 60;
        }

        public static string GetBuildTimeString(int sec) {
            GetRemainTimeToTimes(out int days, out int hours, out int mins, out int secs, sec);
            string time;
            if (days > 0 || hours > 0) {
                time = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainHour, days * 24 + hours);
                if (mins > 0) {
                    time += GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, mins);
                }
            } else if (mins > 0) {
                time = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, mins);
            } else {
                time = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainSec, secs);
            }
            return time;
        }

        public static string GetCharaQuestUnlockString(ref CharacterQuestListDB_Param param) {
            return GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.CharaQuestOpenCondition_CharacterEvolve + param.m_OpenCondition, param.m_OpenConditionValue);
        }

        public static Color GetIconColor(eElementType elementType) {
            return elementType switch {
                eElementType.None => UIDefine.NONE_ELEMENT_COLOR,
                eElementType.Fire => UIDefine.FIRE_COLOR,
                eElementType.Water => UIDefine.WATER_COLOR,
                eElementType.Earth => UIDefine.EARTH_COLOR,
                eElementType.Wind => UIDefine.WIND_COLOR,
                eElementType.Moon => UIDefine.MOON_COLOR,
                eElementType.Sun => UIDefine.SUN_COLOR,
                _ => Color.white,
            };
        }

        public static int ElementEnumToSortIdx(eElementType elementType) {
            if (elementType == eElementType.None) {
                return -1;
            }
            for (int i = 0; i < UIDefine.ELEMENT_SORT.Length; i++) {
                if (UIDefine.ELEMENT_SORT[i] == elementType) {
                    return i;
                }
            }
            Debug.LogError("Unknown Element");
            return -1;
        }

        public static eElementType ElementSortIdxToEnum(int sortIdx) {
            if (sortIdx >= 0 && sortIdx < UIDefine.ELEMENT_SORT.Length) {
                return UIDefine.ELEMENT_SORT[sortIdx];
            }
            Debug.LogError("Invalid Idx");
            return eElementType.None;
        }

        public static object FindHrcObject(Transform ptarget, string findname, Type pfindkey) {
            Transform transform = FindHrcTransform(ptarget, findname);
            transform.gameObject.TryGetComponent(pfindkey, out Component component);
            return component;
        }

        public static Transform FindHrcTransform(Transform ptarget, string findname) {
            string[] parts = findname.Split('/');
            int idx = 0;
            return FindKeyTransform(ptarget, parts, parts.Length, ref idx);
        }

        public static Transform FindKeyTransform(Transform ptarget, string[] findname, int flength, ref int findex) {
            if (ptarget.name == findname[findex]) {
                findex++;
                if (findex >= flength) {
                    return ptarget;
                }
            }
            int childCount = ptarget.childCount;
            for (int i = 0; i < childCount; i++) {
                Transform transform = FindKeyTransform(ptarget.GetChild(i), findname, flength, ref findex);
                if (transform != null) {
                    return transform;
                }
            }
            return null;
        }

        public static void AttachCanvasForChangeRenderOrder(GameObject obj, int sortOrder, bool addGraphicsRaycaster) {
            if (!obj.TryGetComponent(out Canvas canvas)) {
                canvas = obj.AddComponent<Canvas>();
            }
            canvas.overrideSorting = true;
            canvas.sortingOrder = sortOrder;
            if (addGraphicsRaycaster && !obj.TryGetComponent<GraphicRaycaster>(out _)) {
                obj.AddComponent<GraphicRaycaster>();
            }
        }

        public static void DetachCanvasForChangeRenderOrder(GameObject obj, bool removeGraphicsRaycaster) {
            if (removeGraphicsRaycaster) {
                if (obj.TryGetComponent<GraphicRaycaster>(out _)) {
                    obj.RemoveComponent<GraphicRaycaster>();
                }
            }
            if (obj.TryGetComponent<Canvas>(out _)) {
                obj.RemoveComponent<Canvas>();
            }
        }

        public static void PlayVoiceFromFavoriteChara(eSoundVoiceListDB voice) {
            List<eCharaNamedType> chars = new List<eCharaNamedType>();
            int charNum = GameSystem.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID().Length;
            for (int i = 0; i < charNum; i++) {
                long id = GameSystem.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID()[i];
                if (id != -1L) {
                    chars.Add(GameSystem.Inst.UserDataMng.GetUserCharaData(id).Param.NamedType);
                }
            }
            if (chars.Count > 0) {
                eCharaNamedType namedType = chars[UnityEngine.Random.Range(0, chars.Count)];
                GameSystem.Inst.VoiceCtrl.Request(namedType, voice, null, null, true);
            }
        }

        public static void UpdateAndroidBackKey(CustomButton customButton, Func<bool> checkFunc = null) {
            if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape)) {
                if (InputTouch.Inst.GetAvailableTouchCnt() > 0) {
                    return;
                }
                if (checkFunc != null && !checkFunc()) {
                    return;
                }
                if (customButton == null || !customButton.gameObject.activeInHierarchy) {
                    return;
                }
                if (!customButton.IsEnableInput || !customButton.Interactable) {
                    return;
                }
                if (!Utility.CheckTapUI(customButton.gameObject)) {
                    return;
                }
                if (GameSystem.Inst.InputBlock.IsBlockInput()) {
                    return;
                }
                Debug.Log("Android BackKey input." + customButton.gameObject);
                customButton.ExecuteClick();
            }
        }

        public static bool GetInputAndroidBackKey(CustomButton customButton = null) {
            if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape) && InputTouch.Inst.GetAvailableTouchCnt() <= 0) {
                if (customButton != null) {
                    if (!customButton.gameObject.activeSelf) {
                        return false;
                    }
                    if (!customButton.IsEnableInput || !customButton.Interactable) {
                        return false;
                    }
                    if (!Utility.CheckTapUI(customButton.gameObject)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static UserCharacterData CreateUserCharacterDefaultData(int charaID) {
            UserCharacterData data = new UserCharacterData();
            CharacterParam param = data.Param;
            CharacterListDB_Param chara = GameSystem.Inst.DbMng.CharaListDB.GetParam(charaID);
            param.UniqueSkillLearnDatas = new List<SkillLearnData> {
                new SkillLearnData(chara.m_CharaSkillID, 1, chara.m_SkillLimitLv, 0, chara.m_CharaSkillExpTableID)
            };
            param.ClassSkillLearnDatas = new List<SkillLearnData>();
            for (int i = 0; i < chara.m_ClassSkillIDs.Length; i++) {
                param.ClassSkillLearnDatas.Add(new SkillLearnData(chara.m_ClassSkillIDs[i], 1, chara.m_SkillLimitLv, 0, chara.m_ClassSkillExpTableIDs[i]));
            }
            CharacterUtility.SetupCharacterParam(param, -1, charaID, charaID, chara.m_InitLv, chara.m_InitLimitLv, 0, 0, 0, 0);
            return data;
        }

        public static int GetViewCharaID(int rawCharaID) {
            UserCharacterData data = GameSystem.Inst.UserDataMng.GetUserCharaData(rawCharaID);
            if (data != null) {
                return data.Param.ViewCharaID;
            }
            return rawCharaID;
        }

        public enum eLvColorChange {
            None,
            Increase,
            Decrease,
            Arousal,
            Bold,
            IncreaseBold
        }
    }
}
