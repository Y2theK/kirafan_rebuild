﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000610 RID: 1552
	[Token(Token = "0x2000503")]
	[StructLayout(3)]
	public class UniqueSkillSoundDB : ScriptableObject
	{
		// Token: 0x06001615 RID: 5653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C5")]
		[Address(RVA = "0x1015E00F4", Offset = "0x15E00F4", VA = "0x1015E00F4")]
		public UniqueSkillSoundDB()
		{
		}

		// Token: 0x040025D7 RID: 9687
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F41")]
		public UniqueSkillSoundDB_Param[] m_Params;
	}
}
