﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200060E RID: 1550
	[Token(Token = "0x2000501")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct UniqueSkillSoundDB_Data
	{
		// Token: 0x040025D3 RID: 9683
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F3D")]
		public int m_PlayFrame;

		// Token: 0x040025D4 RID: 9684
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F3E")]
		public string m_CueID;
	}
}
