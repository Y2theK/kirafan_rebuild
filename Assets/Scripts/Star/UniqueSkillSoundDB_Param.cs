﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200060F RID: 1551
	[Token(Token = "0x2000502")]
	[Serializable]
	[StructLayout(0)]
	public struct UniqueSkillSoundDB_Param
	{
		// Token: 0x040025D5 RID: 9685
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F3F")]
		public string m_CueSheet;

		// Token: 0x040025D6 RID: 9686
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F40")]
		public UniqueSkillSoundDB_Data[] m_Datas;
	}
}
