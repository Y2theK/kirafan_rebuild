﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A38 RID: 2616
	[Token(Token = "0x200074B")]
	[StructLayout(3)]
	public class UnitRoomBuilder : RoomBuilderBase
	{
		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06002CEE RID: 11502 RVA: 0x00013260 File Offset: 0x00011460
		// (set) Token: 0x06002CED RID: 11501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002D4")]
		public bool isNight
		{
			[Token(Token = "0x600295C")]
			[Address(RVA = "0x1015E03B4", Offset = "0x15E03B4", VA = "0x1015E03B4")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600295B")]
			[Address(RVA = "0x1015E0398", Offset = "0x15E0398", VA = "0x1015E0398")]
			set
			{
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06002CEF RID: 11503 RVA: 0x00013278 File Offset: 0x00011478
		[Token(Token = "0x170002D5")]
		public bool isPreparing
		{
			[Token(Token = "0x600295D")]
			[Address(RVA = "0x1015E03BC", Offset = "0x15E03BC", VA = "0x1015E03BC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06002CF0 RID: 11504 RVA: 0x00013290 File Offset: 0x00011490
		[Token(Token = "0x170002D6")]
		public bool isAvailable
		{
			[Token(Token = "0x600295E")]
			[Address(RVA = "0x1015E03CC", Offset = "0x15E03CC", VA = "0x1015E03CC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002CF1 RID: 11505 RVA: 0x000132A8 File Offset: 0x000114A8
		[Token(Token = "0x600295F")]
		[Address(RVA = "0x1015E03DC", Offset = "0x15E03DC", VA = "0x1015E03DC", Slot = "4")]
		public override bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06002CF2 RID: 11506 RVA: 0x000132C0 File Offset: 0x000114C0
		[Token(Token = "0x170002D7")]
		public bool isStable
		{
			[Token(Token = "0x6002960")]
			[Address(RVA = "0x1015E03EC", Offset = "0x15E03EC", VA = "0x1015E03EC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002CF3 RID: 11507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002961")]
		[Address(RVA = "0x1015E043C", Offset = "0x15E043C", VA = "0x1015E043C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06002CF4 RID: 11508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002962")]
		[Address(RVA = "0x1015E0444", Offset = "0x15E0444", VA = "0x1015E0444", Slot = "13")]
		public override void Destroy()
		{
		}

		// Token: 0x06002CF5 RID: 11509 RVA: 0x000132D8 File Offset: 0x000114D8
		[Token(Token = "0x6002963")]
		[Address(RVA = "0x1015E049C", Offset = "0x15E049C", VA = "0x1015E049C")]
		public bool CheckWithProcDestory()
		{
			return default(bool);
		}

		// Token: 0x06002CF6 RID: 11510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002964")]
		[Address(RVA = "0x1015E04E8", Offset = "0x15E04E8", VA = "0x1015E04E8")]
		private void Awake()
		{
		}

		// Token: 0x06002CF7 RID: 11511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002965")]
		[Address(RVA = "0x1015E05F8", Offset = "0x15E05F8", VA = "0x1015E05F8")]
		private void Start()
		{
		}

		// Token: 0x06002CF8 RID: 11512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002966")]
		[Address(RVA = "0x1015E0A4C", Offset = "0x15E0A4C", VA = "0x1015E0A4C")]
		private void Update()
		{
		}

		// Token: 0x06002CF9 RID: 11513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002967")]
		[Address(RVA = "0x1015E0AB4", Offset = "0x15E0AB4", VA = "0x1015E0AB4")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06002CFA RID: 11514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002968")]
		[Address(RVA = "0x1015E0BFC", Offset = "0x15E0BFC", VA = "0x1015E0BFC")]
		private void Update_PrepareWait()
		{
		}

		// Token: 0x06002CFB RID: 11515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002969")]
		[Address(RVA = "0x1015E0C8C", Offset = "0x15E0C8C", VA = "0x1015E0C8C")]
		private void Update_Main()
		{
		}

		// Token: 0x06002CFC RID: 11516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600296A")]
		[Address(RVA = "0x1015E0C90", Offset = "0x15E0C90", VA = "0x1015E0C90")]
		private void UpdateEventQue(RoomEventQue eventQue)
		{
		}

		// Token: 0x06002CFD RID: 11517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600296B")]
		[Address(RVA = "0x1015E0DD8", Offset = "0x15E0DD8", VA = "0x1015E0DD8")]
		private void UpdateDayAndNightShift()
		{
		}

		// Token: 0x06002CFE RID: 11518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600296C")]
		[Address(RVA = "0x1015E179C", Offset = "0x15E179C", VA = "0x1015E179C")]
		public void ReStartEventCommand()
		{
		}

		// Token: 0x06002CFF RID: 11519 RVA: 0x000132F0 File Offset: 0x000114F0
		[Token(Token = "0x600296D")]
		[Address(RVA = "0x1015E1934", Offset = "0x15E1934", VA = "0x1015E1934")]
		public bool Setup(IUnitRoomController roomController, int charaSlotLimit = 5)
		{
			return default(bool);
		}

		// Token: 0x06002D00 RID: 11520 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600296E")]
		[Address(RVA = "0x1015E1954", Offset = "0x15E1954", VA = "0x1015E1954")]
		private UnitRoomBuilder.ManagedObject GetManagedObjInternal(int idx)
		{
			return null;
		}

		// Token: 0x06002D01 RID: 11521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600296F")]
		[Address(RVA = "0x1015E1A24", Offset = "0x15E1A24", VA = "0x1015E1A24")]
		private UnitRoomBuilder.ManagedObject GetManagedObjFromMngIdInternal(BuilderManagedId builderMngId)
		{
			return null;
		}

		// Token: 0x06002D02 RID: 11522 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002970")]
		[Address(RVA = "0x1015E1B3C", Offset = "0x15E1B3C", VA = "0x1015E1B3C")]
		private UnitRoomBuilder.ManagedObject GetManagedObjFromResIdInternal(int resId, int idx)
		{
			return null;
		}

		// Token: 0x06002D03 RID: 11523 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002971")]
		[Address(RVA = "0x1015E1C54", Offset = "0x15E1C54", VA = "0x1015E1C54")]
		public UnitRoomBuilder.ManagedObject GetManagedObj(int idx)
		{
			return null;
		}

		// Token: 0x06002D04 RID: 11524 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002972")]
		[Address(RVA = "0x1015E1D58", Offset = "0x15E1D58", VA = "0x1015E1D58")]
		public UnitRoomBuilder.ManagedObject GetManagedObjFromMngId(BuilderManagedId builderMngId)
		{
			return null;
		}

		// Token: 0x06002D05 RID: 11525 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002973")]
		[Address(RVA = "0x1015E1EA4", Offset = "0x15E1EA4", VA = "0x1015E1EA4")]
		public UnitRoomBuilder.ManagedObject GetManagedObjFromResId(int resId, int idx)
		{
			return null;
		}

		// Token: 0x06002D06 RID: 11526 RVA: 0x00013308 File Offset: 0x00011508
		[Token(Token = "0x6002974")]
		[Address(RVA = "0x1015E1FF0", Offset = "0x15E1FF0", VA = "0x1015E1FF0")]
		public int GetManagedObjNum()
		{
			return 0;
		}

		// Token: 0x06002D07 RID: 11527 RVA: 0x00013320 File Offset: 0x00011520
		[Token(Token = "0x6002975")]
		[Address(RVA = "0x1015E20BC", Offset = "0x15E20BC", VA = "0x1015E20BC")]
		public int GetManagedObjNumFromResId(int resId)
		{
			return 0;
		}

		// Token: 0x06002D08 RID: 11528 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002976")]
		[Address(RVA = "0x1015E21CC", Offset = "0x15E21CC", VA = "0x1015E21CC")]
		private UnitRoomBuilder.ManagedChara GetManagedCharaInternal(int idx)
		{
			return null;
		}

		// Token: 0x06002D09 RID: 11529 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002977")]
		[Address(RVA = "0x1015E229C", Offset = "0x15E229C", VA = "0x1015E229C")]
		private UnitRoomBuilder.ManagedChara GetManagedCharaFromMngIdInternal(BuilderManagedId builderMngId)
		{
			return null;
		}

		// Token: 0x06002D0A RID: 11530 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002978")]
		[Address(RVA = "0x1015E23B4", Offset = "0x15E23B4", VA = "0x1015E23B4")]
		private UnitRoomBuilder.ManagedChara GetManagedCharaFromCharaIdInternal(int charaId)
		{
			return null;
		}

		// Token: 0x06002D0B RID: 11531 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002979")]
		[Address(RVA = "0x1015E24B4", Offset = "0x15E24B4", VA = "0x1015E24B4")]
		private UnitRoomBuilder.ManagedChara GetManagedCharaFromSlotIdInternal(int slotId)
		{
			return null;
		}

		// Token: 0x06002D0C RID: 11532 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600297A")]
		[Address(RVA = "0x1015E25B4", Offset = "0x15E25B4", VA = "0x1015E25B4")]
		public UnitRoomBuilder.ManagedChara GetManagedChara(int idx)
		{
			return null;
		}

		// Token: 0x06002D0D RID: 11533 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600297B")]
		[Address(RVA = "0x1015E26B8", Offset = "0x15E26B8", VA = "0x1015E26B8")]
		public UnitRoomBuilder.ManagedChara GetManagedCharaFromMngId(BuilderManagedId builderMngId)
		{
			return null;
		}

		// Token: 0x06002D0E RID: 11534 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600297C")]
		[Address(RVA = "0x1015E2804", Offset = "0x15E2804", VA = "0x1015E2804")]
		public UnitRoomBuilder.ManagedChara GetManagedCharaFromCharaId(int charaId)
		{
			return null;
		}

		// Token: 0x06002D0F RID: 11535 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600297D")]
		[Address(RVA = "0x1015E2938", Offset = "0x15E2938", VA = "0x1015E2938")]
		public UnitRoomBuilder.ManagedChara GetManagedCharaFromSlotId(int slotId)
		{
			return null;
		}

		// Token: 0x06002D10 RID: 11536 RVA: 0x00013338 File Offset: 0x00011538
		[Token(Token = "0x600297E")]
		[Address(RVA = "0x1015E2A6C", Offset = "0x15E2A6C", VA = "0x1015E2A6C")]
		public int GetManagedCharaNum()
		{
			return 0;
		}

		// Token: 0x06002D11 RID: 11537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600297F")]
		[Address(RVA = "0x1015E2B38", Offset = "0x15E2B38", VA = "0x1015E2B38")]
		public void SetNotifyCallback(int id, Action<int> callback)
		{
		}

		// Token: 0x06002D12 RID: 11538 RVA: 0x00013350 File Offset: 0x00011550
		[Token(Token = "0x6002980")]
		[Address(RVA = "0x1015E2BC8", Offset = "0x15E2BC8", VA = "0x1015E2BC8")]
		public bool IsPutable(eRoomObjectCategory category, int objId, CharacterDefine.eDir dir, int floorId, int gridX, int gridY, out bool isNotExistInDB)
		{
			return default(bool);
		}

		// Token: 0x06002D13 RID: 11539 RVA: 0x00013368 File Offset: 0x00011568
		[Token(Token = "0x6002981")]
		[Address(RVA = "0x1015E3318", Offset = "0x15E3318", VA = "0x1015E3318")]
		public bool PreLoadRoomObject(int resId, CharacterDefine.eDir dir, [Optional] Action<bool> callback, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D14 RID: 11540 RVA: 0x00013380 File Offset: 0x00011580
		[Token(Token = "0x6002982")]
		[Address(RVA = "0x1015E33E0", Offset = "0x15E33E0", VA = "0x1015E33E0")]
		public BuilderManagedId AddRoomObject(int resId, CharacterDefine.eDir dir, int floorId, int gridX, int gridY, bool alphaIn, UnitRoomBuilder.CallbackEventEndTime callback, bool isBarrier = true)
		{
			return default(BuilderManagedId);
		}

		// Token: 0x06002D15 RID: 11541 RVA: 0x00013398 File Offset: 0x00011598
		[Token(Token = "0x6002983")]
		[Address(RVA = "0x1015E36A0", Offset = "0x15E36A0", VA = "0x1015E36A0")]
		public bool RefleshRoomObject(BuilderManagedId builderManagedId, int resId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D16 RID: 11542 RVA: 0x000133B0 File Offset: 0x000115B0
		[Token(Token = "0x6002984")]
		[Address(RVA = "0x1015E13C8", Offset = "0x15E13C8", VA = "0x1015E13C8")]
		public bool RefleshRoomObject(BuilderManagedId builderManagedId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D17 RID: 11543 RVA: 0x000133C8 File Offset: 0x000115C8
		[Token(Token = "0x6002985")]
		[Address(RVA = "0x1015E3974", Offset = "0x15E3974", VA = "0x1015E3974")]
		private bool DeleteManagedObject(UnitRoomBuilder.ManagedObject managedObj, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D18 RID: 11544 RVA: 0x000133E0 File Offset: 0x000115E0
		[Token(Token = "0x6002986")]
		[Address(RVA = "0x1015E3BC4", Offset = "0x15E3BC4", VA = "0x1015E3BC4")]
		public bool DeleteRoomObjectFromMngId(BuilderManagedId builderMngId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D19 RID: 11545 RVA: 0x000133F8 File Offset: 0x000115F8
		[Token(Token = "0x6002987")]
		[Address(RVA = "0x1015E3BF4", Offset = "0x15E3BF4", VA = "0x1015E3BF4")]
		public bool DeleteRoomObjectFromResId(int resId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D1A RID: 11546 RVA: 0x00013410 File Offset: 0x00011610
		[Token(Token = "0x6002988")]
		[Address(RVA = "0x1015E3C54", Offset = "0x15E3C54", VA = "0x1015E3C54")]
		public bool DeleteRoomObjectFromResId(int resId, int idx, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D1B RID: 11547 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002989")]
		[Address(RVA = "0x1015E3C84", Offset = "0x15E3C84", VA = "0x1015E3C84")]
		public BuilderManagedId[] ChangeRoomObject(UnitRoomBuilder.ChangeData[] dstDatas, BuilderManagedId[] srcDatas, bool alphaIn, RoomEventCmd_ChangeObj.ChangeEffect changeEffect, UnitRoomBuilder.CallbackEventEndTime callback, bool isBarrier = true)
		{
			return null;
		}

		// Token: 0x06002D1C RID: 11548 RVA: 0x00013428 File Offset: 0x00011628
		[Token(Token = "0x600298A")]
		[Address(RVA = "0x1015E141C", Offset = "0x15E141C", VA = "0x1015E141C")]
		public bool SetBG(int objId, bool crossFade, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D1D RID: 11549 RVA: 0x00013440 File Offset: 0x00011640
		[Token(Token = "0x600298B")]
		[Address(RVA = "0x1015E153C", Offset = "0x15E153C", VA = "0x1015E153C")]
		public bool SetWall(int objId, bool crossFade, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D1E RID: 11550 RVA: 0x00013458 File Offset: 0x00011658
		[Token(Token = "0x600298C")]
		[Address(RVA = "0x1015E166C", Offset = "0x15E166C", VA = "0x1015E166C")]
		public bool SetFloor(int objId, bool crossFade, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D1F RID: 11551 RVA: 0x00013470 File Offset: 0x00011670
		[Token(Token = "0x600298D")]
		[Address(RVA = "0x1015E4478", Offset = "0x15E4478", VA = "0x1015E4478")]
		public BuilderManagedId SetChara(int slotId, int charaId, bool isWakeup = true, [Optional] UnitRoomBuilder.CallbackEventEndTime callback, bool isBarrier = true)
		{
			return default(BuilderManagedId);
		}

		// Token: 0x06002D20 RID: 11552 RVA: 0x00013488 File Offset: 0x00011688
		[Token(Token = "0x600298E")]
		[Address(RVA = "0x1015E44A8", Offset = "0x15E44A8", VA = "0x1015E44A8")]
		public BuilderManagedId SetChara(int slotId, int charaId, int gridX, int gridY, bool isWakeup = true, [Optional] UnitRoomBuilder.CallbackEventEndTime callback, bool isBarrier = true)
		{
			return default(BuilderManagedId);
		}

		// Token: 0x06002D21 RID: 11553 RVA: 0x000134A0 File Offset: 0x000116A0
		[Token(Token = "0x600298F")]
		[Address(RVA = "0x1015E47B8", Offset = "0x15E47B8", VA = "0x1015E47B8")]
		public bool RemoveCharaFromCharaId(int charaId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D22 RID: 11554 RVA: 0x000134B8 File Offset: 0x000116B8
		[Token(Token = "0x6002990")]
		[Address(RVA = "0x1015E46A8", Offset = "0x15E46A8", VA = "0x1015E46A8")]
		public bool RemoveCharaFromSlotId(int slotId, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D23 RID: 11555 RVA: 0x000134D0 File Offset: 0x000116D0
		[Token(Token = "0x6002991")]
		[Address(RVA = "0x1015E4928", Offset = "0x15E4928", VA = "0x1015E4928")]
		public bool RoomCameraMove(float tgtPosX, float tgtPosY, float tgtZoomSize, float time, bool isBarrier = true)
		{
			return default(bool);
		}

		// Token: 0x06002D24 RID: 11556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002992")]
		[Address(RVA = "0x1015E4A30", Offset = "0x15E4A30", VA = "0x1015E4A30")]
		private void AddRoomObj_EventCallback(BuilderManagedId builderManagedId, IRoomObjectControll objHandle)
		{
		}

		// Token: 0x06002D25 RID: 11557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002993")]
		[Address(RVA = "0x1015E511C", Offset = "0x15E511C", VA = "0x1015E511C")]
		private void DelRoomObj_EventCallback(BuilderManagedId builderManagedId)
		{
		}

		// Token: 0x06002D26 RID: 11558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002994")]
		[Address(RVA = "0x1015E51DC", Offset = "0x15E51DC", VA = "0x1015E51DC")]
		private void ChangeRoomObj_EventCallback(RoomEventCmd_ChangeObj.DstObj[] dstObjs, IRoomObjectControll[] dstRoomObjs, RoomEventCmd_ChangeObj.SrcObj[] srcObjs)
		{
		}

		// Token: 0x06002D27 RID: 11559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002995")]
		[Address(RVA = "0x1015E56E0", Offset = "0x15E56E0", VA = "0x1015E56E0")]
		private void SetBG_EventCallback(IRoomObjectControll objHandle)
		{
		}

		// Token: 0x06002D28 RID: 11560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002996")]
		[Address(RVA = "0x1015E57FC", Offset = "0x15E57FC", VA = "0x1015E57FC")]
		private void SetFloorOrWall_EventCallback()
		{
		}

		// Token: 0x06002D29 RID: 11561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002997")]
		[Address(RVA = "0x1015E5800", Offset = "0x15E5800", VA = "0x1015E5800")]
		private void SetChara_EventCallback(BuilderManagedId builderManagedId, UnitRoomChara roomChara)
		{
		}

		// Token: 0x06002D2A RID: 11562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002998")]
		[Address(RVA = "0x1015E58CC", Offset = "0x15E58CC", VA = "0x1015E58CC")]
		private void DelChara_EventCallback(BuilderManagedId builderManagedId)
		{
		}

		// Token: 0x06002D2B RID: 11563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002999")]
		[Address(RVA = "0x1015E5978", Offset = "0x15E5978", VA = "0x1015E5978")]
		private void MoveCamera_EventCallback()
		{
		}

		// Token: 0x06002D2C RID: 11564 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600299A")]
		[Address(RVA = "0x1015E597C", Offset = "0x15E597C", VA = "0x1015E597C", Slot = "6")]
		public override IRoomCharaManager GetCharaManager()
		{
			return null;
		}

		// Token: 0x06002D2D RID: 11565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600299B")]
		[Address(RVA = "0x1015E5984", Offset = "0x15E5984", VA = "0x1015E5984", Slot = "7")]
		public override void RequestEvent(IRoomEventCommand prequest)
		{
		}

		// Token: 0x06002D2E RID: 11566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600299C")]
		[Address(RVA = "0x1015E5988", Offset = "0x15E5988", VA = "0x1015E5988")]
		public void SendActionToController(IMainComCommand cmd)
		{
		}

		// Token: 0x06002D2F RID: 11567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600299D")]
		[Address(RVA = "0x1015E5A74", Offset = "0x15E5A74", VA = "0x1015E5A74", Slot = "10")]
		public override void SendActionToOwner(IMainComCommand cmd)
		{
		}

		// Token: 0x06002D30 RID: 11568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600299E")]
		[Address(RVA = "0x1015E5A78", Offset = "0x15E5A78", VA = "0x1015E5A78", Slot = "8")]
		public override void DestroyObjectQue(IRoomObjectControll obj)
		{
		}

		// Token: 0x06002D31 RID: 11569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600299F")]
		[Address(RVA = "0x1015E5A7C", Offset = "0x15E5A7C", VA = "0x1015E5A7C", Slot = "9")]
		public override void RequestCharaMoveMode(bool modeOn)
		{
		}

		// Token: 0x06002D32 RID: 11570 RVA: 0x000134E8 File Offset: 0x000116E8
		[Token(Token = "0x60029A0")]
		[Address(RVA = "0x1015E5B38", Offset = "0x15E5B38", VA = "0x1015E5B38", Slot = "11")]
		public override int GetFloorNO()
		{
			return 0;
		}

		// Token: 0x06002D33 RID: 11571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029A1")]
		[Address(RVA = "0x1015E5B40", Offset = "0x15E5B40", VA = "0x1015E5B40", Slot = "12")]
		protected override void SortRoom()
		{
		}

		// Token: 0x06002D34 RID: 11572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029A2")]
		[Address(RVA = "0x1015E5E14", Offset = "0x15E5E14", VA = "0x1015E5E14")]
		public UnitRoomBuilder()
		{
		}

		// Token: 0x04003C82 RID: 15490
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002B9B")]
		[SerializeField]
		private RoomCacheObjectData m_RoomObjectCache;

		// Token: 0x04003C83 RID: 15491
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002B9C")]
		[SerializeField]
		private List<RoomFloorCateory> m_FloorCategory;

		// Token: 0x04003C84 RID: 15492
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002B9D")]
		[SerializeField]
		private RoomEnvEffectManager m_EnvEffectManager;

		// Token: 0x04003C85 RID: 15493
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002B9E")]
		private IUnitRoomController m_RoomController;

		// Token: 0x04003C86 RID: 15494
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002B9F")]
		private UnitRoomCharaManager m_CharaManager;

		// Token: 0x04003C87 RID: 15495
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002BA0")]
		private RoomEventQue m_EventQue;

		// Token: 0x04003C88 RID: 15496
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002BA1")]
		private RoomEventQue m_SetupEventQue;

		// Token: 0x04003C89 RID: 15497
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002BA2")]
		private List<UnitRoomBuilder.ManagedObject> m_ManagedObjectList;

		// Token: 0x04003C8A RID: 15498
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002BA3")]
		private List<UnitRoomBuilder.ManagedChara> m_ManagedCharaList;

		// Token: 0x04003C8B RID: 15499
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002BA4")]
		private bool m_isNight;

		// Token: 0x04003C8C RID: 15500
		[Cpp2IlInjected.FieldOffset(Offset = "0xA9")]
		[Token(Token = "0x4002BA5")]
		private bool m_isNightHistory;

		// Token: 0x04003C8D RID: 15501
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002BA6")]
		private long m_BuilderManagedIdWork;

		// Token: 0x04003C8E RID: 15502
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002BA7")]
		private int m_CharaSlotLimit;

		// Token: 0x04003C8F RID: 15503
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4002BA8")]
		private UnitRoomBuilder.eState m_State;

		// Token: 0x02000A39 RID: 2617
		[Token(Token = "0x2000FBB")]
		private enum eState
		{
			// Token: 0x04003C91 RID: 15505
			[Token(Token = "0x4006459")]
			Invalid = -1,
			// Token: 0x04003C92 RID: 15506
			[Token(Token = "0x400645A")]
			Prepare,
			// Token: 0x04003C93 RID: 15507
			[Token(Token = "0x400645B")]
			PrepareWait,
			// Token: 0x04003C94 RID: 15508
			[Token(Token = "0x400645C")]
			Main
		}

		// Token: 0x02000A3A RID: 2618
		[Token(Token = "0x2000FBC")]
		public class ChangeData
		{
			// Token: 0x06002D35 RID: 11573 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600604C")]
			[Address(RVA = "0x1015E5FC0", Offset = "0x15E5FC0", VA = "0x1015E5FC0")]
			public ChangeData()
			{
			}

			// Token: 0x04003C95 RID: 15509
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400645D")]
			public int resId;

			// Token: 0x04003C96 RID: 15510
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400645E")]
			public CharacterDefine.eDir dir;

			// Token: 0x04003C97 RID: 15511
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400645F")]
			public int gridX;

			// Token: 0x04003C98 RID: 15512
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006460")]
			public int gridY;

			// Token: 0x04003C99 RID: 15513
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006461")]
			public int floorId;
		}

		// Token: 0x02000A3B RID: 2619
		[Token(Token = "0x2000FBD")]
		public enum eCallbackType
		{
			// Token: 0x04003C9B RID: 15515
			[Token(Token = "0x4006463")]
			Add,
			// Token: 0x04003C9C RID: 15516
			[Token(Token = "0x4006464")]
			Delete,
			// Token: 0x04003C9D RID: 15517
			[Token(Token = "0x4006465")]
			Move
		}

		// Token: 0x02000A3C RID: 2620
		// (Invoke) Token: 0x06002D37 RID: 11575
		[Token(Token = "0x2000FBE")]
		public delegate void CallbackEventEndTime(BuilderManagedId builderManagedId, UnitRoomBuilder.eCallbackType type, int value);

		// Token: 0x02000A3D RID: 2621
		[Token(Token = "0x2000FBF")]
		public class ManagedObject
		{
			// Token: 0x06002D3A RID: 11578 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006051")]
			[Address(RVA = "0x1015E3640", Offset = "0x15E3640", VA = "0x1015E3640")]
			internal ManagedObject(UnitRoomBuilder builder, IRoomObjectControll roomObject, BuilderManagedId builderManagedId, int resId, bool isNight)
			{
			}

			// Token: 0x06002D3B RID: 11579 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006052")]
			[Address(RVA = "0x1015E60A4", Offset = "0x15E60A4", VA = "0x1015E60A4")]
			private UnitRoomBuilder OwnerBuilder()
			{
				return null;
			}

			// Token: 0x17000310 RID: 784
			// (get) Token: 0x06002D3C RID: 11580 RVA: 0x00013500 File Offset: 0x00011700
			[Token(Token = "0x170006AE")]
			public BuilderManagedId builderManagedId
			{
				[Token(Token = "0x6006053")]
				[Address(RVA = "0x1015E56D8", Offset = "0x15E56D8", VA = "0x1015E56D8")]
				get
				{
					return default(BuilderManagedId);
				}
			}

			// Token: 0x17000311 RID: 785
			// (get) Token: 0x06002D3D RID: 11581 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x170006AF")]
			public IRoomObjectControll roomObject
			{
				[Token(Token = "0x6006054")]
				[Address(RVA = "0x1015E4470", Offset = "0x15E4470", VA = "0x1015E4470")]
				get
				{
					return null;
				}
			}

			// Token: 0x17000312 RID: 786
			// (get) Token: 0x06002D3E RID: 11582 RVA: 0x00013518 File Offset: 0x00011718
			[Token(Token = "0x170006B0")]
			public int resourceId
			{
				[Token(Token = "0x6006055")]
				[Address(RVA = "0x1015E396C", Offset = "0x15E396C", VA = "0x1015E396C")]
				get
				{
					return 0;
				}
			}

			// Token: 0x04003C9E RID: 15518
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006466")]
			internal BuilderManagedId m_BuilderManagedId;

			// Token: 0x04003C9F RID: 15519
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006467")]
			internal IRoomObjectControll m_RoomObject;

			// Token: 0x04003CA0 RID: 15520
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006468")]
			internal int m_ResId;

			// Token: 0x04003CA1 RID: 15521
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006469")]
			internal bool m_isNight;

			// Token: 0x04003CA2 RID: 15522
			[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
			[Token(Token = "0x400646A")]
			internal bool m_isAvailable;

			// Token: 0x04003CA3 RID: 15523
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400646B")]
			internal UnitRoomBuilder.CallbackEventEndTime m_Callback;

			// Token: 0x04003CA4 RID: 15524
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400646C")]
			private UnitRoomBuilder m_Builder;
		}

		// Token: 0x02000A3E RID: 2622
		[Token(Token = "0x2000FC0")]
		public class ManagedChara
		{
			// Token: 0x06002D3F RID: 11583 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006056")]
			[Address(RVA = "0x1015E48C4", Offset = "0x15E48C4", VA = "0x1015E48C4")]
			internal ManagedChara(UnitRoomBuilder builder, UnitRoomChara roomChara, BuilderManagedId builderManagedId, int slotId, int charaId)
			{
			}

			// Token: 0x06002D40 RID: 11584 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006057")]
			[Address(RVA = "0x1015E5FC8", Offset = "0x15E5FC8", VA = "0x1015E5FC8")]
			public void ChangeSlot(int slotId)
			{
			}

			// Token: 0x06002D41 RID: 11585 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006058")]
			[Address(RVA = "0x1015E6084", Offset = "0x15E6084", VA = "0x1015E6084")]
			private UnitRoomBuilder OwnerBuilder()
			{
				return null;
			}

			// Token: 0x17000313 RID: 787
			// (get) Token: 0x06002D42 RID: 11586 RVA: 0x00013530 File Offset: 0x00011730
			[Token(Token = "0x170006B1")]
			public BuilderManagedId builderManagedId
			{
				[Token(Token = "0x6006059")]
				[Address(RVA = "0x1015E4920", Offset = "0x15E4920", VA = "0x1015E4920")]
				get
				{
					return default(BuilderManagedId);
				}
			}

			// Token: 0x17000314 RID: 788
			// (get) Token: 0x06002D43 RID: 11587 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x170006B2")]
			public UnitRoomChara roomChara
			{
				[Token(Token = "0x600605A")]
				[Address(RVA = "0x1015E608C", Offset = "0x15E608C", VA = "0x1015E608C")]
				get
				{
					return null;
				}
			}

			// Token: 0x17000315 RID: 789
			// (get) Token: 0x06002D44 RID: 11588 RVA: 0x00013548 File Offset: 0x00011748
			[Token(Token = "0x170006B3")]
			public int slotId
			{
				[Token(Token = "0x600605B")]
				[Address(RVA = "0x1015E6094", Offset = "0x15E6094", VA = "0x1015E6094")]
				get
				{
					return 0;
				}
			}

			// Token: 0x17000316 RID: 790
			// (get) Token: 0x06002D45 RID: 11589 RVA: 0x00013560 File Offset: 0x00011760
			[Token(Token = "0x170006B4")]
			public int charaId
			{
				[Token(Token = "0x600605C")]
				[Address(RVA = "0x1015E609C", Offset = "0x15E609C", VA = "0x1015E609C")]
				get
				{
					return 0;
				}
			}

			// Token: 0x04003CA5 RID: 15525
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400646D")]
			internal BuilderManagedId m_BuilderManagedId;

			// Token: 0x04003CA6 RID: 15526
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400646E")]
			internal UnitRoomChara m_RoomChara;

			// Token: 0x04003CA7 RID: 15527
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400646F")]
			internal int m_Slot;

			// Token: 0x04003CA8 RID: 15528
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006470")]
			internal int m_CharaId;

			// Token: 0x04003CA9 RID: 15529
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006471")]
			internal bool m_isAvailable;

			// Token: 0x04003CAA RID: 15530
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006472")]
			internal UnitRoomBuilder.CallbackEventEndTime m_Callback;

			// Token: 0x04003CAB RID: 15531
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006473")]
			private UnitRoomBuilder m_Builder;
		}
	}
}
