﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009B3 RID: 2483
	[Token(Token = "0x2000705")]
	[StructLayout(3)]
	public class UnitRoomChara
	{
		// Token: 0x170002CA RID: 714
		// (get) Token: 0x0600295A RID: 10586 RVA: 0x000117A8 File Offset: 0x0000F9A8
		// (set) Token: 0x06002959 RID: 10585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000294")]
		public UnitRoomChara.eCharaState charaState
		{
			[Token(Token = "0x600260B")]
			[Address(RVA = "0x1015E6368", Offset = "0x15E6368", VA = "0x1015E6368")]
			get
			{
				return UnitRoomChara.eCharaState.Active;
			}
			[Token(Token = "0x600260A")]
			[Address(RVA = "0x1015E60AC", Offset = "0x15E60AC", VA = "0x1015E60AC")]
			set
			{
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x0600295B RID: 10587 RVA: 0x000117C0 File Offset: 0x0000F9C0
		[Token(Token = "0x17000295")]
		public int charaId
		{
			[Token(Token = "0x600260C")]
			[Address(RVA = "0x1015E6370", Offset = "0x15E6370", VA = "0x1015E6370")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x0600295C RID: 10588 RVA: 0x000117D8 File Offset: 0x0000F9D8
		[Token(Token = "0x17000296")]
		public int roomID
		{
			[Token(Token = "0x600260D")]
			[Address(RVA = "0x1015E6378", Offset = "0x15E6378", VA = "0x1015E6378")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x0600295D RID: 10589 RVA: 0x000117F0 File Offset: 0x0000F9F0
		[Token(Token = "0x17000297")]
		public int accessKey
		{
			[Token(Token = "0x600260E")]
			[Address(RVA = "0x1015E6380", Offset = "0x15E6380", VA = "0x1015E6380")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x0600295E RID: 10590 RVA: 0x00011808 File Offset: 0x0000FA08
		[Token(Token = "0x17000298")]
		public eCharaNamedType named
		{
			[Token(Token = "0x600260F")]
			[Address(RVA = "0x1015E6388", Offset = "0x15E6388", VA = "0x1015E6388")]
			get
			{
				return eCharaNamedType.Named_0000;
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x0600295F RID: 10591 RVA: 0x00011820 File Offset: 0x0000FA20
		[Token(Token = "0x17000299")]
		public int floorId
		{
			[Token(Token = "0x6002610")]
			[Address(RVA = "0x1015E6390", Offset = "0x15E6390", VA = "0x1015E6390")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06002961 RID: 10593 RVA: 0x00011838 File Offset: 0x0000FA38
		// (set) Token: 0x06002960 RID: 10592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700029A")]
		public int slotId
		{
			[Token(Token = "0x6002612")]
			[Address(RVA = "0x1015E6478", Offset = "0x15E6478", VA = "0x1015E6478")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002611")]
			[Address(RVA = "0x1015E5FDC", Offset = "0x15E5FDC", VA = "0x1015E5FDC")]
			set
			{
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06002962 RID: 10594 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700029B")]
		public CharacterHandler handle
		{
			[Token(Token = "0x6002613")]
			[Address(RVA = "0x1015E6480", Offset = "0x15E6480", VA = "0x1015E6480")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06002963 RID: 10595 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700029C")]
		public RoomObjectCtrlChara roomObj
		{
			[Token(Token = "0x6002614")]
			[Address(RVA = "0x1015E6488", Offset = "0x15E6488", VA = "0x1015E6488")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002964 RID: 10596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002615")]
		[Address(RVA = "0x1015E6490", Offset = "0x15E6490", VA = "0x1015E6490")]
		public UnitRoomChara(int charaId, int roomId, UnitRoomCharaManager pmanager)
		{
		}

		// Token: 0x06002965 RID: 10597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002616")]
		[Address(RVA = "0x1015E659C", Offset = "0x15E659C", VA = "0x1015E659C")]
		public UnitRoomChara(int charaId, int roomId, int initGridX, int initGridY, UnitRoomCharaManager pmanager)
		{
		}

		// Token: 0x06002966 RID: 10598 RVA: 0x00011850 File Offset: 0x0000FA50
		[Token(Token = "0x6002617")]
		[Address(RVA = "0x1015E66BC", Offset = "0x15E66BC", VA = "0x1015E66BC")]
		public bool IsRoomIn()
		{
			return default(bool);
		}

		// Token: 0x06002967 RID: 10599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002618")]
		[Address(RVA = "0x1015E66CC", Offset = "0x15E66CC", VA = "0x1015E66CC")]
		public void AttachObjectHandle(CharacterHandler handle, RoomObjectCtrlChara roomObj)
		{
		}

		// Token: 0x06002968 RID: 10600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002619")]
		[Address(RVA = "0x1015E6790", Offset = "0x15E6790", VA = "0x1015E6790")]
		public void SetUpRoomCharaObject(Transform parent)
		{
		}

		// Token: 0x06002969 RID: 10601 RVA: 0x00011868 File Offset: 0x0000FA68
		[Token(Token = "0x600261A")]
		[Address(RVA = "0x1015E6398", Offset = "0x15E6398", VA = "0x1015E6398")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600296A RID: 10602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600261B")]
		[Address(RVA = "0x1015E6AE8", Offset = "0x15E6AE8", VA = "0x1015E6AE8")]
		public void UpdatePakage(UnitRoomBuilder builder)
		{
		}

		// Token: 0x0600296B RID: 10603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600261C")]
		[Address(RVA = "0x1015E705C", Offset = "0x15E705C", VA = "0x1015E705C")]
		private void CheckStateChange(UnitRoomChara.eState fstate, UnitRoomChara.StackActCommand pque)
		{
		}

		// Token: 0x0600296C RID: 10604 RVA: 0x00011880 File Offset: 0x0000FA80
		[Token(Token = "0x600261D")]
		[Address(RVA = "0x1015E6AD8", Offset = "0x15E6AD8", VA = "0x1015E6AD8")]
		public bool IsScheduleOfSleep()
		{
			return default(bool);
		}

		// Token: 0x0600296D RID: 10605 RVA: 0x00011898 File Offset: 0x0000FA98
		[Token(Token = "0x600261E")]
		[Address(RVA = "0x1015E72AC", Offset = "0x15E72AC", VA = "0x1015E72AC")]
		public bool IsScheduleOfRoomNotSleep()
		{
			return default(bool);
		}

		// Token: 0x0600296E RID: 10606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600261F")]
		[Address(RVA = "0x1015E72BC", Offset = "0x15E72BC", VA = "0x1015E72BC")]
		public void Delete()
		{
		}

		// Token: 0x0600296F RID: 10607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002620")]
		[Address(RVA = "0x1015E73E8", Offset = "0x15E73E8", VA = "0x1015E73E8")]
		public void Clear()
		{
		}

		// Token: 0x06002970 RID: 10608 RVA: 0x000118B0 File Offset: 0x0000FAB0
		[Token(Token = "0x6002621")]
		[Address(RVA = "0x1015E758C", Offset = "0x15E758C", VA = "0x1015E758C")]
		public bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06002971 RID: 10609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002622")]
		[Address(RVA = "0x1015E6DF4", Offset = "0x15E6DF4", VA = "0x1015E6DF4")]
		public void BuildLink(UnitRoomBuilder builder)
		{
		}

		// Token: 0x06002972 RID: 10610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002623")]
		[Address(RVA = "0x1015E768C", Offset = "0x15E768C", VA = "0x1015E768C")]
		private void SetFreePosition(RoomGridState grid, UnitRoomBuilder builder)
		{
		}

		// Token: 0x06002973 RID: 10611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002624")]
		[Address(RVA = "0x1015E77E0", Offset = "0x15E77E0", VA = "0x1015E77E0")]
		public void SetPosition(int x, int y)
		{
		}

		// Token: 0x06002974 RID: 10612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002625")]
		[Address(RVA = "0x1015E759C", Offset = "0x15E759C", VA = "0x1015E759C")]
		public void AttachCharaHud(GameObject phudobj)
		{
		}

		// Token: 0x06002975 RID: 10613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002626")]
		[Address(RVA = "0x1015E60D4", Offset = "0x15E60D4", VA = "0x1015E60D4")]
		public void FeedBackCharaCallBack(UnitRoomChara.eCharaState charaState)
		{
		}

		// Token: 0x06002976 RID: 10614 RVA: 0x000118C8 File Offset: 0x0000FAC8
		[Token(Token = "0x6002627")]
		[Address(RVA = "0x1015E7AE0", Offset = "0x15E7AE0", VA = "0x1015E7AE0")]
		public bool SetSleep()
		{
			return default(bool);
		}

		// Token: 0x06002977 RID: 10615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002628")]
		[Address(RVA = "0x1015E7B78", Offset = "0x15E7B78", VA = "0x1015E7B78")]
		public void SetWakeUp()
		{
		}

		// Token: 0x06002978 RID: 10616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002629")]
		[Address(RVA = "0x1015E7C18", Offset = "0x15E7C18", VA = "0x1015E7C18")]
		public void SetUpInRoom(Transform parent, bool faction)
		{
		}

		// Token: 0x06002979 RID: 10617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600262A")]
		[Address(RVA = "0x1015E7C1C", Offset = "0x15E7C1C", VA = "0x1015E7C1C")]
		public void DropOutRoom()
		{
		}

		// Token: 0x0600297A RID: 10618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600262B")]
		[Address(RVA = "0x1015E78F0", Offset = "0x15E78F0", VA = "0x1015E78F0")]
		private void StackActCmd(UnitRoomChara.eState fstate, RoomActionCommand.eActionCode fcmd, int fkey, bool flock = false)
		{
		}

		// Token: 0x0600297B RID: 10619 RVA: 0x000118E0 File Offset: 0x0000FAE0
		[Token(Token = "0x600262C")]
		[Address(RVA = "0x1015E6DA0", Offset = "0x15E6DA0", VA = "0x1015E6DA0")]
		private bool IsStackActCmd()
		{
			return default(bool);
		}

		// Token: 0x0600297C RID: 10620 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600262D")]
		[Address(RVA = "0x1015E6DB0", Offset = "0x15E6DB0", VA = "0x1015E6DB0")]
		private UnitRoomChara.StackActCommand GetStackQue()
		{
			return null;
		}

		// Token: 0x0600297D RID: 10621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600262E")]
		[Address(RVA = "0x1015E7158", Offset = "0x15E7158", VA = "0x1015E7158")]
		private void SetNextQue()
		{
		}

		// Token: 0x0600297E RID: 10622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600262F")]
		[Address(RVA = "0x1015E7C30", Offset = "0x15E7C30", VA = "0x1015E7C30")]
		public void DestroyProc()
		{
		}

		// Token: 0x0600297F RID: 10623 RVA: 0x000118F8 File Offset: 0x0000FAF8
		[Token(Token = "0x6002630")]
		[Address(RVA = "0x1015E7D64", Offset = "0x15E7D64", VA = "0x1015E7D64")]
		public bool IsDestroyProc()
		{
			return default(bool);
		}

		// Token: 0x0400399C RID: 14748
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40029B4")]
		private UnitRoomChara.eCharaState m_CharaState;

		// Token: 0x0400399D RID: 14749
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40029B5")]
		private int m_CharaId;

		// Token: 0x0400399E RID: 14750
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40029B6")]
		private int m_RoomId;

		// Token: 0x0400399F RID: 14751
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40029B7")]
		private int m_AccessKey;

		// Token: 0x040039A0 RID: 14752
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029B8")]
		private eCharaNamedType m_Named;

		// Token: 0x040039A1 RID: 14753
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40029B9")]
		private int m_FloorId;

		// Token: 0x040039A2 RID: 14754
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029BA")]
		private int m_SlotId;

		// Token: 0x040039A3 RID: 14755
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029BB")]
		private CharacterHandler m_Handle;

		// Token: 0x040039A4 RID: 14756
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40029BC")]
		private RoomObjectCtrlChara m_RoomObj;

		// Token: 0x040039A5 RID: 14757
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40029BD")]
		private int m_InitGridPosX;

		// Token: 0x040039A6 RID: 14758
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40029BE")]
		private int m_InitGridPosY;

		// Token: 0x040039A7 RID: 14759
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029BF")]
		private UnitRoomCharaManager m_Manager;

		// Token: 0x040039A8 RID: 14760
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029C0")]
		private UnitRoomChara.StackActCommand[] m_StackQue;

		// Token: 0x040039A9 RID: 14761
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40029C1")]
		private short m_StackQueNum;

		// Token: 0x040039AA RID: 14762
		[Cpp2IlInjected.FieldOffset(Offset = "0x5A")]
		[Token(Token = "0x40029C2")]
		private short STACK_QUE_MAX;

		// Token: 0x040039AB RID: 14763
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40029C3")]
		private UnitRoomChara.eState m_State;

		// Token: 0x040039AC RID: 14764
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40029C4")]
		private UnitRoomChara.eState m_SleepKick;

		// Token: 0x040039AD RID: 14765
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40029C5")]
		private UnitRoomChara.eDestroyState m_DestoryState;

		// Token: 0x020009B4 RID: 2484
		[Token(Token = "0x2000F7E")]
		public enum eCharaState
		{
			// Token: 0x040039AF RID: 14767
			[Token(Token = "0x400635C")]
			Active,
			// Token: 0x040039B0 RID: 14768
			[Token(Token = "0x400635D")]
			Sleep
		}

		// Token: 0x020009B5 RID: 2485
		[Token(Token = "0x2000F7F")]
		private enum eState
		{
			// Token: 0x040039B2 RID: 14770
			[Token(Token = "0x400635F")]
			Non,
			// Token: 0x040039B3 RID: 14771
			[Token(Token = "0x4006360")]
			SetUp,
			// Token: 0x040039B4 RID: 14772
			[Token(Token = "0x4006361")]
			SetUpWait,
			// Token: 0x040039B5 RID: 14773
			[Token(Token = "0x4006362")]
			Update,
			// Token: 0x040039B6 RID: 14774
			[Token(Token = "0x4006363")]
			Sleep,
			// Token: 0x040039B7 RID: 14775
			[Token(Token = "0x4006364")]
			WakeUp,
			// Token: 0x040039B8 RID: 14776
			[Token(Token = "0x4006365")]
			End
		}

		// Token: 0x020009B6 RID: 2486
		[Token(Token = "0x2000F80")]
		private class StackActCommand
		{
			// Token: 0x06002980 RID: 10624 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600600D")]
			[Address(RVA = "0x1016030E8", Offset = "0x16030E8", VA = "0x1016030E8")]
			public StackActCommand()
			{
			}

			// Token: 0x040039B9 RID: 14777
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006366")]
			public UnitRoomChara.eState m_StackState;

			// Token: 0x040039BA RID: 14778
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006367")]
			public RoomActionCommand m_SetUpCmd;
		}

		// Token: 0x020009B7 RID: 2487
		[Token(Token = "0x2000F81")]
		private enum eDestroyState
		{
			// Token: 0x040039BC RID: 14780
			[Token(Token = "0x4006369")]
			state_None,
			// Token: 0x040039BD RID: 14781
			[Token(Token = "0x400636A")]
			state_WaitDestroy,
			// Token: 0x040039BE RID: 14782
			[Token(Token = "0x400636B")]
			state_FinishedDestroy
		}
	}
}
