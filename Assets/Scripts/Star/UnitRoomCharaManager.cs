﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x020009B8 RID: 2488
	[Token(Token = "0x2000706")]
	[StructLayout(3)]
	public class UnitRoomCharaManager : IRoomCharaManager
	{
		// Token: 0x06002981 RID: 10625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002631")]
		[Address(RVA = "0x1016030F0", Offset = "0x16030F0", VA = "0x1016030F0", Slot = "4")]
		public override void SetLinkManager(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002982 RID: 10626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002632")]
		[Address(RVA = "0x1016031D8", Offset = "0x16031D8", VA = "0x1016031D8", Slot = "5")]
		public override void BackLinkManager()
		{
		}

		// Token: 0x06002983 RID: 10627 RVA: 0x00011910 File Offset: 0x0000FB10
		[Token(Token = "0x6002633")]
		[Address(RVA = "0x101603230", Offset = "0x1603230", VA = "0x101603230", Slot = "6")]
		public override bool IsBuild()
		{
			return default(bool);
		}

		// Token: 0x06002984 RID: 10628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002634")]
		[Address(RVA = "0x101603318", Offset = "0x1603318", VA = "0x101603318")]
		private void CheckHourEventAction(RoomGreet.eCategory fevent)
		{
		}

		// Token: 0x06002985 RID: 10629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002635")]
		[Address(RVA = "0x1016035EC", Offset = "0x16035EC", VA = "0x1016035EC")]
		private void AddRoomCharacter(int charaId, int roomId)
		{
		}

		// Token: 0x06002986 RID: 10630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002636")]
		[Address(RVA = "0x101603674", Offset = "0x1603674", VA = "0x101603674")]
		private void Update()
		{
		}

		// Token: 0x06002987 RID: 10631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002637")]
		[Address(RVA = "0x101603960", Offset = "0x1603960", VA = "0x101603960", Slot = "10")]
		public override void DeleteChara()
		{
		}

		// Token: 0x06002988 RID: 10632 RVA: 0x00011928 File Offset: 0x0000FB28
		[Token(Token = "0x6002638")]
		[Address(RVA = "0x101603A2C", Offset = "0x1603A2C", VA = "0x101603A2C", Slot = "11")]
		public override bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06002989 RID: 10633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002639")]
		[Address(RVA = "0x101603ABC", Offset = "0x1603ABC", VA = "0x101603ABC", Slot = "13")]
		public override void RefreshCharaCall()
		{
		}

		// Token: 0x0600298A RID: 10634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600263A")]
		[Address(RVA = "0x101603AC0", Offset = "0x1603AC0", VA = "0x101603AC0", Slot = "12")]
		public override void DeleteCharacter(long charaId, bool cutout = false)
		{
		}

		// Token: 0x0600298B RID: 10635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600263B")]
		[Address(RVA = "0x101603DD4", Offset = "0x1603DD4", VA = "0x101603DD4")]
		private void CheckRoomInManageChara(int charaId, int roomId, bool cutout)
		{
		}

		// Token: 0x0600298C RID: 10636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600263C")]
		[Address(RVA = "0x101603F74", Offset = "0x1603F74", VA = "0x101603F74", Slot = "7")]
		public override void ResetState()
		{
		}

		// Token: 0x0600298D RID: 10637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600263D")]
		[Address(RVA = "0x101604194", Offset = "0x1604194", VA = "0x101604194", Slot = "8")]
		public override void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
		}

		// Token: 0x0600298E RID: 10638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600263E")]
		[Address(RVA = "0x1016043D8", Offset = "0x16043D8", VA = "0x1016043D8", Slot = "9")]
		public override void SetUpRoomCharacter()
		{
		}

		// Token: 0x0600298F RID: 10639 RVA: 0x00011940 File Offset: 0x0000FB40
		[Token(Token = "0x600263F")]
		[Address(RVA = "0x10160455C", Offset = "0x160455C", VA = "0x10160455C")]
		private bool IsRoomMateNamed(eCharaNamedType fnametype)
		{
			return default(bool);
		}

		// Token: 0x06002990 RID: 10640 RVA: 0x00011958 File Offset: 0x0000FB58
		[Token(Token = "0x6002640")]
		[Address(RVA = "0x101604644", Offset = "0x1604644", VA = "0x101604644")]
		public bool SetSleepFloor(int charaId, bool init)
		{
			return default(bool);
		}

		// Token: 0x06002991 RID: 10641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002641")]
		[Address(RVA = "0x101604718", Offset = "0x1604718", VA = "0x101604718")]
		public void ClrSleepFloor(int charaId, bool fsleep)
		{
		}

		// Token: 0x06002992 RID: 10642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002642")]
		[Address(RVA = "0x101603F70", Offset = "0x1603F70", VA = "0x101603F70")]
		public void AddScheduleCharacter(int charaId)
		{
		}

		// Token: 0x06002993 RID: 10643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002643")]
		[Address(RVA = "0x101603DD0", Offset = "0x1603DD0", VA = "0x101603DD0")]
		private void CheckMemberLimitCheck()
		{
		}

		// Token: 0x06002994 RID: 10644 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002644")]
		[Address(RVA = "0x101603668", Offset = "0x1603668", VA = "0x101603668")]
		public UnitRoomChara CheckEntryCharaManager(int charaId, int roomId)
		{
			return null;
		}

		// Token: 0x06002995 RID: 10645 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002645")]
		[Address(RVA = "0x101604838", Offset = "0x1604838", VA = "0x101604838")]
		public UnitRoomChara CheckEntryCharaManager(int charaId, int roomId, int initGridX, int initGridY)
		{
			return null;
		}

		// Token: 0x06002996 RID: 10646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002646")]
		[Address(RVA = "0x101604BC0", Offset = "0x1604BC0", VA = "0x101604BC0")]
		public void AddRoomChara(UnitRoomChara pakage)
		{
		}

		// Token: 0x06002997 RID: 10647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002647")]
		[Address(RVA = "0x101604DF8", Offset = "0x1604DF8", VA = "0x101604DF8")]
		public void CallbackCharaState(eCharaBuildUp type, int charaId, int roomId)
		{
		}

		// Token: 0x06002998 RID: 10648 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002648")]
		[Address(RVA = "0x101604E4C", Offset = "0x1604E4C", VA = "0x101604E4C")]
		public UnitRoomChara GetCharaSetUp(int fhandle)
		{
			return null;
		}

		// Token: 0x06002999 RID: 10649 RVA: 0x00011970 File Offset: 0x0000FB70
		[Token(Token = "0x6002649")]
		[Address(RVA = "0x101604F74", Offset = "0x1604F74", VA = "0x101604F74", Slot = "14")]
		public override int GetCharaPakageMax()
		{
			return 0;
		}

		// Token: 0x0600299A RID: 10650 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600264A")]
		[Address(RVA = "0x101604FA0", Offset = "0x1604FA0", VA = "0x101604FA0")]
		public UnitRoomChara GetCharaPakage(int findex)
		{
			return null;
		}

		// Token: 0x0600299B RID: 10651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600264B")]
		[Address(RVA = "0x101604FF0", Offset = "0x1604FF0", VA = "0x101604FF0")]
		public UnitRoomCharaManager()
		{
		}

		// Token: 0x040039BF RID: 14783
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029C6")]
		[SerializeField]
		public UnitRoomChara[] m_CharaTable;

		// Token: 0x040039C0 RID: 14784
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029C7")]
		public int m_EntryNum;

		// Token: 0x040039C1 RID: 14785
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40029C8")]
		public int m_EntryMax;

		// Token: 0x040039C2 RID: 14786
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40029C9")]
		public int m_InSubMemberRothe;

		// Token: 0x040039C3 RID: 14787
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40029CA")]
		public int m_OutSubMemberRothe;

		// Token: 0x040039C4 RID: 14788
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40029CB")]
		private int m_MakeUID;

		// Token: 0x040039C5 RID: 14789
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40029CC")]
		private short m_WaitUpChara;

		// Token: 0x040039C6 RID: 14790
		[Cpp2IlInjected.FieldOffset(Offset = "0x46")]
		[Token(Token = "0x40029CD")]
		private short m_WaitDropChara;

		// Token: 0x040039C7 RID: 14791
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029CE")]
		private long m_StepTime;

		// Token: 0x040039C8 RID: 14792
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029CF")]
		private int[] m_SleepCharaID;

		// Token: 0x040039C9 RID: 14793
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40029D0")]
		private int m_SleepCharaNum;

		// Token: 0x040039CA RID: 14794
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40029D1")]
		private int m_SleepFloorBedNum;

		// Token: 0x040039CB RID: 14795
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40029D2")]
		private int m_PlayFloorID;

		// Token: 0x040039CC RID: 14796
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40029D3")]
		private bool m_BuildUp;
	}
}
