﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000819 RID: 2073
	[Token(Token = "0x200061C")]
	[StructLayout(3)]
	public class UnitRoomComHideUI : UnitRoomCom
	{
		// Token: 0x0600209B RID: 8347 RVA: 0x0000E538 File Offset: 0x0000C738
		[Token(Token = "0x6001E13")]
		[Address(RVA = "0x101605000", Offset = "0x1605000", VA = "0x101605000", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x0600209C RID: 8348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E14")]
		[Address(RVA = "0x101605008", Offset = "0x1605008", VA = "0x101605008")]
		public UnitRoomComHideUI()
		{
		}
	}
}
