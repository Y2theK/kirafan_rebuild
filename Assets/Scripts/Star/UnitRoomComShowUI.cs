﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000818 RID: 2072
	[Token(Token = "0x200061B")]
	[StructLayout(3)]
	public class UnitRoomComShowUI : UnitRoomCom
	{
		// Token: 0x06002099 RID: 8345 RVA: 0x0000E520 File Offset: 0x0000C720
		[Token(Token = "0x6001E11")]
		[Address(RVA = "0x101605010", Offset = "0x1605010", VA = "0x101605010", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x0600209A RID: 8346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E12")]
		[Address(RVA = "0x101605018", Offset = "0x1605018", VA = "0x101605018")]
		public UnitRoomComShowUI()
		{
		}
	}
}
