﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BFB RID: 3067
	[Token(Token = "0x2000840")]
	[StructLayout(3)]
	public class Unlock
	{
		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x0600360E RID: 13838 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170003AD")]
		public WeaponUnlock WeaponUnlock
		{
			[Token(Token = "0x6003127")]
			[Address(RVA = "0x101605020", Offset = "0x1605020", VA = "0x101605020")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x0600360F RID: 13839 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170003AE")]
		public CharaQuestUnlock CharaQuestUnlock
		{
			[Token(Token = "0x6003128")]
			[Address(RVA = "0x101605028", Offset = "0x1605028", VA = "0x101605028")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003610 RID: 13840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003129")]
		[Address(RVA = "0x101605030", Offset = "0x1605030", VA = "0x101605030")]
		public Unlock()
		{
		}

		// Token: 0x06003611 RID: 13841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600312A")]
		[Address(RVA = "0x1016050C4", Offset = "0x16050C4", VA = "0x1016050C4")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06003612 RID: 13842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600312B")]
		[Address(RVA = "0x101605234", Offset = "0x1605234", VA = "0x101605234")]
		public void Update()
		{
		}

		// Token: 0x06003613 RID: 13843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600312C")]
		[Address(RVA = "0x1016055A0", Offset = "0x16055A0", VA = "0x1016055A0")]
		public void Play(Unlock.eStartPoint startPoint = Unlock.eStartPoint.CharaADV, bool isCallOnHome = false, bool disableGotoCharaQuest = false)
		{
		}

		// Token: 0x06003614 RID: 13844 RVA: 0x00016CF8 File Offset: 0x00014EF8
		[Token(Token = "0x600312D")]
		[Address(RVA = "0x1016055F8", Offset = "0x16055F8", VA = "0x1016055F8")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06003615 RID: 13845 RVA: 0x00016D10 File Offset: 0x00014F10
		[Token(Token = "0x600312E")]
		[Address(RVA = "0x101605614", Offset = "0x1605614", VA = "0x101605614")]
		public Unlock.eResult GetResult()
		{
			return Unlock.eResult.Success;
		}

		// Token: 0x06003616 RID: 13846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600312F")]
		[Address(RVA = "0x1016050CC", Offset = "0x16050CC", VA = "0x1016050CC")]
		private void InputBlock(bool flg)
		{
		}

		// Token: 0x04004628 RID: 17960
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400313F")]
		private WeaponUnlock m_WeaponUnlock;

		// Token: 0x04004629 RID: 17961
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003140")]
		private CharaQuestUnlock m_CharaQuestUnlock;

		// Token: 0x0400462A RID: 17962
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003141")]
		private Unlock.eUnlockStep m_UnlockStep;

		// Token: 0x0400462B RID: 17963
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003142")]
		private Unlock.eResult m_Result;

		// Token: 0x0400462C RID: 17964
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003143")]
		private bool m_IsCallOnHome;

		// Token: 0x0400462D RID: 17965
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003144")]
		private bool m_DisableGotoCharaQuest;

		// Token: 0x0400462E RID: 17966
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003145")]
		private GameObject m_InputBlockObj;

		// Token: 0x02000BFC RID: 3068
		[Token(Token = "0x2001084")]
		public enum eStartPoint
		{
			// Token: 0x04004630 RID: 17968
			[Token(Token = "0x400683E")]
			CharaADV,
			// Token: 0x04004631 RID: 17969
			[Token(Token = "0x400683F")]
			WeaponUnlock,
			// Token: 0x04004632 RID: 17970
			[Token(Token = "0x4006840")]
			CharaQuestUnlock
		}

		// Token: 0x02000BFD RID: 3069
		[Token(Token = "0x2001085")]
		private enum eUnlockStep
		{
			// Token: 0x04004634 RID: 17972
			[Token(Token = "0x4006842")]
			None = -1,
			// Token: 0x04004635 RID: 17973
			[Token(Token = "0x4006843")]
			CharaADV_Start,
			// Token: 0x04004636 RID: 17974
			[Token(Token = "0x4006844")]
			CharaADV_Wait,
			// Token: 0x04004637 RID: 17975
			[Token(Token = "0x4006845")]
			WeaponUnlock_Start,
			// Token: 0x04004638 RID: 17976
			[Token(Token = "0x4006846")]
			WeaponUnlock_Wait,
			// Token: 0x04004639 RID: 17977
			[Token(Token = "0x4006847")]
			CharaQuestUnlock_Start,
			// Token: 0x0400463A RID: 17978
			[Token(Token = "0x4006848")]
			CharaQuestUnlock_Wait,
			// Token: 0x0400463B RID: 17979
			[Token(Token = "0x4006849")]
			End
		}

		// Token: 0x02000BFE RID: 3070
		[Token(Token = "0x2001086")]
		public enum eResult
		{
			// Token: 0x0400463D RID: 17981
			[Token(Token = "0x400684B")]
			None = -1,
			// Token: 0x0400463E RID: 17982
			[Token(Token = "0x400684C")]
			Success,
			// Token: 0x0400463F RID: 17983
			[Token(Token = "0x400684D")]
			Success_GotoADV,
			// Token: 0x04004640 RID: 17984
			[Token(Token = "0x400684E")]
			Success_GotoCharaQuest
		}
	}
}
