﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200066C RID: 1644
	[Token(Token = "0x2000544")]
	[StructLayout(3)]
	public class UpgradeEffectHandler : MonoBehaviour
	{
		// Token: 0x17000210 RID: 528
		// (get) Token: 0x0600180C RID: 6156 RVA: 0x0000B040 File Offset: 0x00009240
		[Token(Token = "0x170001F9")]
		public UpgradeEffectHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x60016AE")]
			[Address(RVA = "0x10160561C", Offset = "0x160561C", VA = "0x10160561C")]
			get
			{
				return UpgradeEffectHandler.eAnimType.Effect;
			}
		}

		// Token: 0x0600180D RID: 6157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016AF")]
		[Address(RVA = "0x101605624", Offset = "0x1605624", VA = "0x101605624")]
		private void Awake()
		{
		}

		// Token: 0x0600180E RID: 6158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B0")]
		[Address(RVA = "0x1016058E8", Offset = "0x16058E8", VA = "0x1016058E8")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600180F RID: 6159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B1")]
		[Address(RVA = "0x1016058EC", Offset = "0x16058EC", VA = "0x1016058EC")]
		public void Destroy()
		{
		}

		// Token: 0x06001810 RID: 6160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B2")]
		[Address(RVA = "0x101605938", Offset = "0x1605938", VA = "0x101605938")]
		private void Update()
		{
		}

		// Token: 0x06001811 RID: 6161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B3")]
		[Address(RVA = "0x101605C44", Offset = "0x1605C44", VA = "0x101605C44")]
		public void SetSuccessLevel(int successLevel)
		{
		}

		// Token: 0x06001812 RID: 6162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B4")]
		[Address(RVA = "0x101605AB0", Offset = "0x1605AB0", VA = "0x101605AB0")]
		public void Play(UpgradeEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x06001813 RID: 6163 RVA: 0x0000B058 File Offset: 0x00009258
		[Token(Token = "0x60016B5")]
		[Address(RVA = "0x1016059D4", Offset = "0x16059D4", VA = "0x1016059D4")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06001814 RID: 6164 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016B6")]
		[Address(RVA = "0x101605C4C", Offset = "0x1605C4C", VA = "0x101605C4C")]
		private string GetAnimPlayKey(UpgradeEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x06001815 RID: 6165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B7")]
		[Address(RVA = "0x101605C9C", Offset = "0x1605C9C", VA = "0x101605C9C")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x06001816 RID: 6166 RVA: 0x0000B070 File Offset: 0x00009270
		[Token(Token = "0x60016B8")]
		[Address(RVA = "0x101605CF4", Offset = "0x1605CF4", VA = "0x101605CF4")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06001817 RID: 6167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016B9")]
		[Address(RVA = "0x101605D24", Offset = "0x1605D24", VA = "0x101605D24")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x06001818 RID: 6168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016BA")]
		[Address(RVA = "0x101605D7C", Offset = "0x1605D7C", VA = "0x101605D7C")]
		public UpgradeEffectHandler()
		{
		}

		// Token: 0x04002741 RID: 10049
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002046")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x04002742 RID: 10050
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002047")]
		private readonly int EXP_GAUGE_START_FRAME;

		// Token: 0x04002743 RID: 10051
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002048")]
		public Transform m_Transform;

		// Token: 0x04002744 RID: 10052
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002049")]
		public Animation m_Anim;

		// Token: 0x04002745 RID: 10053
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400204A")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04002746 RID: 10054
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400204B")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x04002747 RID: 10055
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400204C")]
		public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x04002748 RID: 10056
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400204D")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04002749 RID: 10057
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400204E")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x0400274A RID: 10058
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400204F")]
		public GameObject[] m_SuccessLevelObject;

		// Token: 0x0400274B RID: 10059
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002050")]
		private UpgradeEffectHandler.eAnimType m_CurrentAnimType;

		// Token: 0x0400274C RID: 10060
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002051")]
		private int m_SuccessLevel;

		// Token: 0x0400274D RID: 10061
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002052")]
		private float m_Elapsed;

		// Token: 0x0400274E RID: 10062
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002053")]
		public Action PlayVoiceCallback;

		// Token: 0x0400274F RID: 10063
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002054")]
		public Action PlayExpGaugeCallback;

		// Token: 0x0200066D RID: 1645
		[Token(Token = "0x2000DFC")]
		public enum eAnimType
		{
			// Token: 0x04002751 RID: 10065
			[Token(Token = "0x4005A7D")]
			None = -1,
			// Token: 0x04002752 RID: 10066
			[Token(Token = "0x4005A7E")]
			Effect,
			// Token: 0x04002753 RID: 10067
			[Token(Token = "0x4005A7F")]
			Loop,
			// Token: 0x04002754 RID: 10068
			[Token(Token = "0x4005A80")]
			Num
		}

		// Token: 0x0200066E RID: 1646
		[Token(Token = "0x2000DFD")]
		public enum eAnimEvent
		{
			// Token: 0x04002756 RID: 10070
			[Token(Token = "0x4005A82")]
			Whiteout = 102
		}
	}
}
