﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200066F RID: 1647
	[Token(Token = "0x2000545")]
	[StructLayout(3)]
	public class UpgradeEffectScene
	{
		// Token: 0x06001819 RID: 6169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016BB")]
		[Address(RVA = "0x101605EC4", Offset = "0x1605EC4", VA = "0x101605EC4")]
		public UpgradeEffectScene()
		{
		}

		// Token: 0x0600181A RID: 6170 RVA: 0x0000B088 File Offset: 0x00009288
		[Token(Token = "0x60016BC")]
		[Address(RVA = "0x101605F7C", Offset = "0x1605F7C", VA = "0x101605F7C")]
		public bool Destroy()
		{
			return default(bool);
		}

		// Token: 0x0600181B RID: 6171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016BD")]
		[Address(RVA = "0x1016060A4", Offset = "0x16060A4", VA = "0x1016060A4")]
		public void Update()
		{
		}

		// Token: 0x0600181C RID: 6172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016BE")]
		[Address(RVA = "0x1016064E8", Offset = "0x16064E8", VA = "0x1016064E8")]
		public void SetPlayExpGaugeCallback(Action playExpGaugeCallback)
		{
		}

		// Token: 0x0600181D RID: 6173 RVA: 0x0000B0A0 File Offset: 0x000092A0
		[Token(Token = "0x60016BF")]
		[Address(RVA = "0x1016064F0", Offset = "0x16064F0", VA = "0x1016064F0")]
		public bool Prepare(Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x0600181E RID: 6174 RVA: 0x0000B0B8 File Offset: 0x000092B8
		[Token(Token = "0x60016C0")]
		[Address(RVA = "0x1016065A0", Offset = "0x16065A0", VA = "0x1016065A0")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x0600181F RID: 6175 RVA: 0x0000B0D0 File Offset: 0x000092D0
		[Token(Token = "0x60016C1")]
		[Address(RVA = "0x1016065B0", Offset = "0x16065B0", VA = "0x1016065B0")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06001820 RID: 6176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C2")]
		[Address(RVA = "0x1016060C0", Offset = "0x16060C0", VA = "0x1016060C0")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C3")]
		[Address(RVA = "0x1016065C0", Offset = "0x16065C0", VA = "0x1016065C0")]
		public void Play(UserCharacterData userCharacterData, int successLevel)
		{
		}

		// Token: 0x06001822 RID: 6178 RVA: 0x0000B0E8 File Offset: 0x000092E8
		[Token(Token = "0x60016C4")]
		[Address(RVA = "0x101606750", Offset = "0x1606750", VA = "0x101606750")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06001823 RID: 6179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C5")]
		[Address(RVA = "0x101606450", Offset = "0x1606450", VA = "0x101606450")]
		private void Update_Main()
		{
		}

		// Token: 0x06001824 RID: 6180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C6")]
		[Address(RVA = "0x101606760", Offset = "0x1606760", VA = "0x101606760")]
		private void Skip()
		{
		}

		// Token: 0x06001825 RID: 6181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C7")]
		[Address(RVA = "0x10160685C", Offset = "0x160685C", VA = "0x10160685C")]
		private void PlayExpGauge()
		{
		}

		// Token: 0x06001826 RID: 6182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016C8")]
		[Address(RVA = "0x10160687C", Offset = "0x160687C", VA = "0x10160687C")]
		private void PlayVoice()
		{
		}

		// Token: 0x06001827 RID: 6183 RVA: 0x0000B100 File Offset: 0x00009300
		[Token(Token = "0x60016C9")]
		[Address(RVA = "0x101606B1C", Offset = "0x1606B1C", VA = "0x101606B1C")]
		private eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_000;
		}

		// Token: 0x06001828 RID: 6184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016CA")]
		[Address(RVA = "0x101606DEC", Offset = "0x1606DEC", VA = "0x101606DEC")]
		public void DetectTouch()
		{
		}

		// Token: 0x04002757 RID: 10071
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002055")]
		private UpgradeEffectScene.eMode m_Mode;

		// Token: 0x04002758 RID: 10072
		[Token(Token = "0x4002056")]
		private const string RESOURCE_PATH = "mix/mix_upgrade.muast";

		// Token: 0x04002759 RID: 10073
		[Token(Token = "0x4002057")]
		private const string OBJECT_PATH = "Mix_Upgrade";

		// Token: 0x0400275A RID: 10074
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002058")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x0400275B RID: 10075
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002059")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400275C RID: 10076
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400205A")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x0400275D RID: 10077
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400205B")]
		private GameObject m_EffectGO;

		// Token: 0x0400275E RID: 10078
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400205C")]
		private UpgradeEffectHandler m_EffectHndl;

		// Token: 0x0400275F RID: 10079
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400205D")]
		private Transform m_ParentTransform;

		// Token: 0x04002760 RID: 10080
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400205E")]
		private Vector3 m_Position;

		// Token: 0x04002761 RID: 10081
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400205F")]
		private SoundSeRequest m_SeRequest;

		// Token: 0x04002762 RID: 10082
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002060")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04002763 RID: 10083
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002061")]
		private Action m_PlayExpGaugeCallback;

		// Token: 0x04002764 RID: 10084
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002062")]
		private bool m_IsDonePlayExpGauge;

		// Token: 0x04002765 RID: 10085
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002063")]
		private UpgradeEffectScene.ePrepareStep m_PrepareStep;

		// Token: 0x04002766 RID: 10086
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002064")]
		private UpgradeEffectScene.eMainStep m_MainStep;

		// Token: 0x04002767 RID: 10087
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002065")]
		private bool m_DetectTouch;

		// Token: 0x04002768 RID: 10088
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002066")]
		private int m_RequestVoiceID;

		// Token: 0x02000670 RID: 1648
		[Token(Token = "0x2000DFE")]
		public enum eMode
		{
			// Token: 0x0400276A RID: 10090
			[Token(Token = "0x4005A84")]
			None = -1,
			// Token: 0x0400276B RID: 10091
			[Token(Token = "0x4005A85")]
			Prepare,
			// Token: 0x0400276C RID: 10092
			[Token(Token = "0x4005A86")]
			UpdateMain,
			// Token: 0x0400276D RID: 10093
			[Token(Token = "0x4005A87")]
			Destroy
		}

		// Token: 0x02000671 RID: 1649
		[Token(Token = "0x2000DFF")]
		private enum ePrepareStep
		{
			// Token: 0x0400276F RID: 10095
			[Token(Token = "0x4005A89")]
			None = -1,
			// Token: 0x04002770 RID: 10096
			[Token(Token = "0x4005A8A")]
			Prepare,
			// Token: 0x04002771 RID: 10097
			[Token(Token = "0x4005A8B")]
			Prepare_Wait,
			// Token: 0x04002772 RID: 10098
			[Token(Token = "0x4005A8C")]
			End,
			// Token: 0x04002773 RID: 10099
			[Token(Token = "0x4005A8D")]
			Prepare_Error
		}

		// Token: 0x02000672 RID: 1650
		[Token(Token = "0x2000E00")]
		private enum eMainStep
		{
			// Token: 0x04002775 RID: 10101
			[Token(Token = "0x4005A8F")]
			None = -1,
			// Token: 0x04002776 RID: 10102
			[Token(Token = "0x4005A90")]
			Play,
			// Token: 0x04002777 RID: 10103
			[Token(Token = "0x4005A91")]
			Play_Wait,
			// Token: 0x04002778 RID: 10104
			[Token(Token = "0x4005A92")]
			End
		}
	}
}
