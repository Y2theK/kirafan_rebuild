﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C0A RID: 3082
	[Token(Token = "0x2000844")]
	[StructLayout(3)]
	public class UserAbilityData
	{
		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x0600363E RID: 13886 RVA: 0x00016E60 File Offset: 0x00015060
		// (set) Token: 0x0600363F RID: 13887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B2")]
		public long MngID
		{
			[Token(Token = "0x6003153")]
			[Address(RVA = "0x101606DF8", Offset = "0x1606DF8", VA = "0x101606DF8")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6003154")]
			[Address(RVA = "0x101606E00", Offset = "0x1606E00", VA = "0x101606E00")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06003640 RID: 13888 RVA: 0x00016E78 File Offset: 0x00015078
		// (set) Token: 0x06003641 RID: 13889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B3")]
		public long CharaMngID
		{
			[Token(Token = "0x6003155")]
			[Address(RVA = "0x101606E08", Offset = "0x1606E08", VA = "0x101606E08")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6003156")]
			[Address(RVA = "0x101606E10", Offset = "0x1606E10", VA = "0x101606E10")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06003642 RID: 13890 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06003643 RID: 13891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B4")]
		public AbilityParam Param
		{
			[Token(Token = "0x6003157")]
			[Address(RVA = "0x101606E18", Offset = "0x1606E18", VA = "0x101606E18")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6003158")]
			[Address(RVA = "0x101606E20", Offset = "0x1606E20", VA = "0x101606E20")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06003644 RID: 13892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003159")]
		[Address(RVA = "0x101606E28", Offset = "0x1606E28", VA = "0x101606E28")]
		public UserAbilityData()
		{
		}
	}
}
