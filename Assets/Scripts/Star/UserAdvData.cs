﻿using System.Collections.Generic;

namespace Star {
    public class UserAdvData {
        private List<int> m_OccurredAdvList;

        public UserAdvData(int[] advIDs) {
            m_OccurredAdvList = new List<int>(advIDs);
        }

        public UserAdvData() {
            m_OccurredAdvList = new List<int>();
        }

        public List<int> GetOccurredAdvIDs() {
            return m_OccurredAdvList;
        }

        public void SetAdvIDs(int[] advIDs) {
            m_OccurredAdvList = new List<int>(advIDs);
        }

        public void AddOccurredAdvID(int advID) {
            if (!m_OccurredAdvList.Contains(advID)) {
                m_OccurredAdvList.Add(advID);
            }
        }

        public bool IsOccurredAdv(int advID) {
            return advID == -1 || m_OccurredAdvList.Contains(advID);
        }
    }
}
