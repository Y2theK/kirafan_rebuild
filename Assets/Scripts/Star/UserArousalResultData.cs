﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C0C RID: 3084
	[Token(Token = "0x2000846")]
	[StructLayout(3)]
	public class UserArousalResultData
	{
		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x0600364B RID: 13899 RVA: 0x00016EA8 File Offset: 0x000150A8
		// (set) Token: 0x0600364C RID: 13900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B5")]
		public int CharaID
		{
			[Token(Token = "0x6003160")]
			[Address(RVA = "0x10160714C", Offset = "0x160714C", VA = "0x10160714C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003161")]
			[Address(RVA = "0x101607154", Offset = "0x1607154", VA = "0x101607154")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x0600364D RID: 13901 RVA: 0x00016EC0 File Offset: 0x000150C0
		// (set) Token: 0x0600364E RID: 13902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B6")]
		public int BeforeArousalLevel
		{
			[Token(Token = "0x6003162")]
			[Address(RVA = "0x10160715C", Offset = "0x160715C", VA = "0x10160715C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003163")]
			[Address(RVA = "0x101607164", Offset = "0x1607164", VA = "0x101607164")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x0600364F RID: 13903 RVA: 0x00016ED8 File Offset: 0x000150D8
		// (set) Token: 0x06003650 RID: 13904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B7")]
		public int BeforeDuplicatedCount
		{
			[Token(Token = "0x6003164")]
			[Address(RVA = "0x10160716C", Offset = "0x160716C", VA = "0x10160716C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003165")]
			[Address(RVA = "0x101607174", Offset = "0x1607174", VA = "0x101607174")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x06003651 RID: 13905 RVA: 0x00016EF0 File Offset: 0x000150F0
		// (set) Token: 0x06003652 RID: 13906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B8")]
		public int AfterArousalLevel
		{
			[Token(Token = "0x6003166")]
			[Address(RVA = "0x10160717C", Offset = "0x160717C", VA = "0x10160717C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003167")]
			[Address(RVA = "0x101607184", Offset = "0x1607184", VA = "0x101607184")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x06003653 RID: 13907 RVA: 0x00016F08 File Offset: 0x00015108
		// (set) Token: 0x06003654 RID: 13908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003B9")]
		public int AfterDuplicatedCount
		{
			[Token(Token = "0x6003168")]
			[Address(RVA = "0x10160718C", Offset = "0x160718C", VA = "0x10160718C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003169")]
			[Address(RVA = "0x101607194", Offset = "0x1607194", VA = "0x101607194")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06003655 RID: 13909 RVA: 0x00016F20 File Offset: 0x00015120
		// (set) Token: 0x06003656 RID: 13910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003BA")]
		public int ArousalItemID
		{
			[Token(Token = "0x600316A")]
			[Address(RVA = "0x10160719C", Offset = "0x160719C", VA = "0x10160719C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600316B")]
			[Address(RVA = "0x1016071A4", Offset = "0x16071A4", VA = "0x1016071A4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06003657 RID: 13911 RVA: 0x00016F38 File Offset: 0x00015138
		// (set) Token: 0x06003658 RID: 13912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003BB")]
		public int ArousalItemAmount
		{
			[Token(Token = "0x600316C")]
			[Address(RVA = "0x1016071AC", Offset = "0x16071AC", VA = "0x1016071AC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600316D")]
			[Address(RVA = "0x1016071B4", Offset = "0x16071B4", VA = "0x1016071B4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x06003659 RID: 13913 RVA: 0x00016F50 File Offset: 0x00015150
		[Token(Token = "0x170003BC")]
		public bool IsLvUp
		{
			[Token(Token = "0x600316E")]
			[Address(RVA = "0x1016071BC", Offset = "0x16071BC", VA = "0x1016071BC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x0600365A RID: 13914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600316F")]
		[Address(RVA = "0x1016071D0", Offset = "0x16071D0", VA = "0x1016071D0")]
		public UserArousalResultData()
		{
		}
	}
}
