﻿using System;
using System.Collections.Generic;

namespace Star {
    [Serializable]
    public class UserBattlePartyData {
        public long MngID { get; set; }
        public string PartyName { get; set; }
        public long[] CharaMngIDs { get; set; }
        public long[] WeaponMngIDs { get; set; }
        public int OrbID { get; set; }

        public UserBattlePartyData() {
            MngID = -1L;
            OrbID = -1;
            ClearSlotAll();
        }

        public int GetSlotNum() {
            return CharaMngIDs.Length;
        }

        public void ClearSlotAll() {
            int members = BattleDefine.PARTY_MEMBER_NUM;
            CharaMngIDs = new long[members];
            for (int i = 0; i < members; i++) {
                CharaMngIDs[i] = -1L;
            }
            WeaponMngIDs = new long[members];
            for (int j = 0; j < members; j++) {
                WeaponMngIDs[j] = -1L;
            }
        }

        public long GetMemberAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Length) {
                return CharaMngIDs[slotIndex];
            }
            return -1L;
        }

        public int GetMemberIdx(long charaMngID) {
            for (int i = 0; i < CharaMngIDs.Length; i++) {
                if (CharaMngIDs[i] == charaMngID) {
                    return i;
                }
            }
            return -1;
        }

        public void SetMemberAt(int slotIndex, long charaMngID) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Length) {
                CharaMngIDs[slotIndex] = charaMngID;
            }
        }

        public void EmptyMemberAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Length) {
                CharaMngIDs[slotIndex] = -1L;
            }
        }

        public void EmptyMember(long charaMngID) {
            for (int i = 0; i < CharaMngIDs.Length; i++) {
                if (CharaMngIDs[i] == charaMngID) {
                    CharaMngIDs[i] = -1L;
                    break;
                }
            }
        }

        public List<long> GetAvailableMemberMngIDs() {
            List<long> ids = new List<long>();
            for (int i = 0; i < CharaMngIDs.Length; i++) {
                if (CharaMngIDs[i] != -1L) {
                    ids.Add(CharaMngIDs[i]);
                }
            }
            return ids;
        }

        public long GetWeaponAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Length) {
                return WeaponMngIDs[slotIndex];
            }
            return -1L;
        }

        public void SetWeaponAt(int slotIndex, long weaponMngID) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Length) {
                WeaponMngIDs[slotIndex] = weaponMngID;
            }
        }

        public void EmptyWeaponAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Length) {
                WeaponMngIDs[slotIndex] = -1L;
            }
        }

        public void EmptyWeapon(long weaponMngID) {
            for (int i = 0; i < WeaponMngIDs.Length; i++) {
                if (WeaponMngIDs[i] == weaponMngID) {
                    WeaponMngIDs[i] = -1L;
                    break;
                }
            }
        }
    }
}
