﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200072C RID: 1836
	[Token(Token = "0x20005A3")]
	[StructLayout(3)]
	public class UserCharaUtil
	{
		// Token: 0x06001AD1 RID: 6865 RVA: 0x0000BF40 File Offset: 0x0000A140
		[Token(Token = "0x60018A1")]
		[Address(RVA = "0x101607A68", Offset = "0x1607A68", VA = "0x101607A68")]
		public static CharacterListDB_Param GetCharaParam(long fmanageid)
		{
			return default(CharacterListDB_Param);
		}

		// Token: 0x06001AD2 RID: 6866 RVA: 0x0000BF58 File Offset: 0x0000A158
		[Token(Token = "0x60018A2")]
		[Address(RVA = "0x101607C6C", Offset = "0x1607C6C", VA = "0x101607C6C")]
		public static CharacterListDB_Param GetIDToCharaParam(int fcharaid)
		{
			return default(CharacterListDB_Param);
		}

		// Token: 0x06001AD3 RID: 6867 RVA: 0x0000BF70 File Offset: 0x0000A170
		[Token(Token = "0x60018A3")]
		[Address(RVA = "0x101607D04", Offset = "0x1607D04", VA = "0x101607D04")]
		public static NamedListDB_Param GetCharaIDToNameParam(int fcharaid)
		{
			return default(NamedListDB_Param);
		}

		// Token: 0x06001AD4 RID: 6868 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018A4")]
		[Address(RVA = "0x101607DE8", Offset = "0x1607DE8", VA = "0x101607DE8")]
		public static UserCharacterData GetCharaData(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001AD5 RID: 6869 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018A5")]
		[Address(RVA = "0x101607E64", Offset = "0x1607E64", VA = "0x101607E64")]
		public static UserFieldCharaData.RoomInCharaData GetRoomChara(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001AD6 RID: 6870 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018A6")]
		[Address(RVA = "0x101608000", Offset = "0x1608000", VA = "0x101608000")]
		public static UserScheduleData.DropState[] GetScheduleDropState(int fcharaid)
		{
			return null;
		}

		// Token: 0x06001AD7 RID: 6871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018A7")]
		[Address(RVA = "0x101608420", Offset = "0x1608420", VA = "0x101608420")]
		public UserCharaUtil()
		{
		}
	}
}
