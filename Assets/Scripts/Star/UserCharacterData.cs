﻿using System;

namespace Star {
    [Serializable]
    public class UserCharacterData {
        public long MngID { get; set; }
        public bool IsNew { get; set; }
        public CharacterParam Param { get; set; }

        public UserCharacterData() {
            MngID = -1L;
            IsNew = false;
            Param = new CharacterParam();
        }
    }
}
