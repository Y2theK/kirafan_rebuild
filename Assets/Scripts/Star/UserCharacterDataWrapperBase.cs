﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000624 RID: 1572
	[Token(Token = "0x2000517")]
	[StructLayout(3)]
	public abstract class UserCharacterDataWrapperBase : CharacterDataWrapperBaseExGen<UserCharacterData>
	{
		// Token: 0x06001634 RID: 5684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014E4")]
		[Address(RVA = "0x1016084D4", Offset = "0x16084D4", VA = "0x1016084D4")]
		public UserCharacterDataWrapperBase(eCharacterDataType characterDataType, UserCharacterData in_data)
		{
		}

		// Token: 0x06001635 RID: 5685 RVA: 0x00009C60 File Offset: 0x00007E60
		[Token(Token = "0x60014E5")]
		[Address(RVA = "0x10160853C", Offset = "0x160853C", VA = "0x10160853C", Slot = "4")]
		public override long GetMngId()
		{
			return 0L;
		}

		// Token: 0x06001636 RID: 5686 RVA: 0x00009C78 File Offset: 0x00007E78
		[Token(Token = "0x60014E6")]
		[Address(RVA = "0x101608568", Offset = "0x1608568", VA = "0x101608568", Slot = "5")]
		public override int GetCharaId()
		{
			return 0;
		}

		// Token: 0x06001637 RID: 5687 RVA: 0x00009C90 File Offset: 0x00007E90
		[Token(Token = "0x60014E7")]
		[Address(RVA = "0x1016085A8", Offset = "0x16085A8", VA = "0x1016085A8", Slot = "7")]
		public override eCharaNamedType GetCharaNamedType()
		{
			return eCharaNamedType.Named_0000;
		}

		// Token: 0x06001638 RID: 5688 RVA: 0x00009CA8 File Offset: 0x00007EA8
		[Token(Token = "0x60014E8")]
		[Address(RVA = "0x1016085E8", Offset = "0x16085E8", VA = "0x1016085E8", Slot = "8")]
		public override eClassType GetClassType()
		{
			return eClassType.Fighter;
		}

		// Token: 0x06001639 RID: 5689 RVA: 0x00009CC0 File Offset: 0x00007EC0
		[Token(Token = "0x60014E9")]
		[Address(RVA = "0x101608628", Offset = "0x1608628", VA = "0x101608628", Slot = "9")]
		public override eElementType GetElementType()
		{
			return eElementType.Fire;
		}

		// Token: 0x0600163A RID: 5690 RVA: 0x00009CD8 File Offset: 0x00007ED8
		[Token(Token = "0x60014EA")]
		[Address(RVA = "0x101608668", Offset = "0x1608668", VA = "0x101608668", Slot = "10")]
		public override eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x0600163B RID: 5691 RVA: 0x00009CF0 File Offset: 0x00007EF0
		[Token(Token = "0x60014EB")]
		[Address(RVA = "0x1016086A8", Offset = "0x16086A8", VA = "0x1016086A8", Slot = "11")]
		public override int GetCost()
		{
			return 0;
		}

		// Token: 0x0600163C RID: 5692 RVA: 0x00009D08 File Offset: 0x00007F08
		[Token(Token = "0x60014EC")]
		[Address(RVA = "0x1016086E8", Offset = "0x16086E8", VA = "0x1016086E8", Slot = "12")]
		public override int GetLv()
		{
			return 0;
		}

		// Token: 0x0600163D RID: 5693 RVA: 0x00009D20 File Offset: 0x00007F20
		[Token(Token = "0x60014ED")]
		[Address(RVA = "0x101608728", Offset = "0x1608728", VA = "0x101608728", Slot = "13")]
		public override int GetMaxLv()
		{
			return 0;
		}

		// Token: 0x0600163E RID: 5694 RVA: 0x00009D38 File Offset: 0x00007F38
		[Token(Token = "0x60014EE")]
		[Address(RVA = "0x101608768", Offset = "0x1608768", VA = "0x101608768", Slot = "14")]
		public override long GetExp()
		{
			return 0L;
		}

		// Token: 0x0600163F RID: 5695 RVA: 0x00009D50 File Offset: 0x00007F50
		[Token(Token = "0x60014EF")]
		[Address(RVA = "0x1016087A8", Offset = "0x16087A8", VA = "0x1016087A8", Slot = "15")]
		public override int GetLimitBreak()
		{
			return 0;
		}

		// Token: 0x06001640 RID: 5696 RVA: 0x00009D68 File Offset: 0x00007F68
		[Token(Token = "0x60014F0")]
		[Address(RVA = "0x1016087E8", Offset = "0x16087E8", VA = "0x1016087E8", Slot = "16")]
		public override int GetArousal()
		{
			return 0;
		}

		// Token: 0x06001641 RID: 5697 RVA: 0x00009D80 File Offset: 0x00007F80
		[Token(Token = "0x60014F1")]
		[Address(RVA = "0x101608828", Offset = "0x1608828", VA = "0x101608828", Slot = "17")]
		public override int GetFriendship()
		{
			return 0;
		}

		// Token: 0x06001642 RID: 5698 RVA: 0x00009D98 File Offset: 0x00007F98
		[Token(Token = "0x60014F2")]
		[Address(RVA = "0x1016088E8", Offset = "0x16088E8", VA = "0x1016088E8", Slot = "18")]
		public override int GetAroualLevel()
		{
			return 0;
		}

		// Token: 0x06001643 RID: 5699 RVA: 0x00009DB0 File Offset: 0x00007FB0
		[Token(Token = "0x60014F3")]
		[Address(RVA = "0x101608928", Offset = "0x1608928", VA = "0x101608928", Slot = "19")]
		public override int GetDuplicatedCount()
		{
			return 0;
		}

		// Token: 0x06001644 RID: 5700 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60014F4")]
		[Address(RVA = "0x10160884C", Offset = "0x160884C", VA = "0x10160884C")]
		public UserNamedData GetUserNamedData()
		{
			return null;
		}
	}
}
