﻿using System;
using UnityEngine;

namespace Star {
    public class UserData {
        public const int USERNAME_MAX = 10;
        public const int USERCOMMENT_MAX = 31;
        public const int USERCOMMENT_RETURN = 15;

        public long ID { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public eAgeType Age { get; set; }
        public string MyCode { get; set; }
        public int AchievementID { get; set; }
        public int Lv { get; set; }
        public long LvExp { get; set; }
        public long Exp { get; set; }
        public long Gold { get; set; }
        public long UnlimitedGem { get; set; }
        public long LimitedGem { get; set; }
        public long Gem => LimitedGem + UnlimitedGem;
        public long LotteryTicket { get; set; }
        public int PartyCost { get; set; }
        public UserStamina Stamina { get; set; }
        public UserKRRPoint KRRPoint { get; set; }
        public int WeaponLimit { get; set; }
        public int WeaponLimitCount { get; set; }
        public int FacilityLimit { get; set; }
        public int FacilityLimitCount { get; set; }
        public int RoomObjectLimit { get; set; }
        public int RoomObjectLimitCount { get; set; }
        public int CharacterLimit { get; set; }
        public int ItemLimit { get; set; }
        public int FriendLimit { get; set; }
        public int SupportLimit { get; set; }
        public int LoginCount { get; set; }
        public DateTime LastLoginAt { get; set; }
        public int LoginDays { get; set; }
        public int ContinuousDays { get; set; }

        public UserData() {
            ID = -1L;
            Name = string.Empty;
            Comment = string.Empty;
            Stamina = new UserStamina();
            KRRPoint = new UserKRRPoint();
            AchievementID = 0;
        }

        public void Update() {
            if (Stamina != null) {
                Stamina.UpdateStamina(Time.unscaledDeltaTime);
            }
            if (GameSystem.Inst.GlobalParam.RefreshBadgeTimeSec > 0f) {
                GameSystem.Inst.GlobalParam.RefreshBadgeTimeSec -= Time.deltaTime;
            }
        }

        public bool IsShortOfGold(long amount) {
            return Gold < amount;
        }

        public bool IsShortOfGem(long amount) {
            return Gem < amount;
        }

        public bool IsShortOfUnlimitedGem(long amount) {
            return UnlimitedGem < amount;
        }

        public bool IsShortOfKP(long amount) {
            return KRRPoint.GetPoint() < amount;
        }
    }
}
