﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C12 RID: 3090
	[Token(Token = "0x200084C")]
	[StructLayout(3)]
	public sealed class UserDataManager
	{
		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x060036CD RID: 14029 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036CE RID: 14030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E4")]
		public UserData UserData
		{
			[Token(Token = "0x60031E2")]
			[Address(RVA = "0x101608ED4", Offset = "0x1608ED4", VA = "0x101608ED4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031E3")]
			[Address(RVA = "0x101608EDC", Offset = "0x1608EDC", VA = "0x101608EDC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x060036CF RID: 14031 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036D0 RID: 14032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E5")]
		public List<UserCharacterData> UserCharaDatas
		{
			[Token(Token = "0x60031E4")]
			[Address(RVA = "0x101608EE4", Offset = "0x1608EE4", VA = "0x101608EE4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031E5")]
			[Address(RVA = "0x101608EEC", Offset = "0x1608EEC", VA = "0x101608EEC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x060036D1 RID: 14033 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036D2 RID: 14034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E6")]
		public Dictionary<eCharaNamedType, UserNamedData> UserNamedDatas
		{
			[Token(Token = "0x60031E6")]
			[Address(RVA = "0x101608EF4", Offset = "0x1608EF4", VA = "0x101608EF4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031E7")]
			[Address(RVA = "0x101608EFC", Offset = "0x1608EFC", VA = "0x101608EFC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x060036D3 RID: 14035 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036D4 RID: 14036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E7")]
		public List<UserWeaponData> UserWeaponDatas
		{
			[Token(Token = "0x60031E8")]
			[Address(RVA = "0x101608F04", Offset = "0x1608F04", VA = "0x101608F04")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031E9")]
			[Address(RVA = "0x101608F0C", Offset = "0x1608F0C", VA = "0x101608F0C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x060036D5 RID: 14037 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036D6 RID: 14038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E8")]
		public List<UserMasterOrbData> UserMasterOrbDatas
		{
			[Token(Token = "0x60031EA")]
			[Address(RVA = "0x101608F14", Offset = "0x1608F14", VA = "0x101608F14")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031EB")]
			[Address(RVA = "0x101608F1C", Offset = "0x1608F1C", VA = "0x101608F1C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x060036D7 RID: 14039 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036D8 RID: 14040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003E9")]
		public List<UserAbilityData> UserAbilityDatas
		{
			[Token(Token = "0x60031EC")]
			[Address(RVA = "0x101608F24", Offset = "0x1608F24", VA = "0x101608F24")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031ED")]
			[Address(RVA = "0x101608F2C", Offset = "0x1608F2C", VA = "0x101608F2C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x060036D9 RID: 14041 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036DA RID: 14042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003EA")]
		public List<UserItemData> UserItemDatas
		{
			[Token(Token = "0x60031EE")]
			[Address(RVA = "0x101608F34", Offset = "0x1608F34", VA = "0x101608F34")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031EF")]
			[Address(RVA = "0x101608F3C", Offset = "0x1608F3C", VA = "0x101608F3C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x060036DB RID: 14043 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036DC RID: 14044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003EB")]
		public List<UserBattlePartyData> UserBattlePartyDatas
		{
			[Token(Token = "0x60031F0")]
			[Address(RVA = "0x101608F44", Offset = "0x1608F44", VA = "0x101608F44")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031F1")]
			[Address(RVA = "0x101608F4C", Offset = "0x1608F4C", VA = "0x101608F4C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x060036DD RID: 14045 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036DE RID: 14046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003EC")]
		public List<UserSupportPartyData> UserSupportPartyDatas
		{
			[Token(Token = "0x60031F2")]
			[Address(RVA = "0x101608F54", Offset = "0x1608F54", VA = "0x101608F54")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031F3")]
			[Address(RVA = "0x101608F5C", Offset = "0x1608F5C", VA = "0x101608F5C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x060036DF RID: 14047 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036E0 RID: 14048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003ED")]
		public UserTownData UserTownData
		{
			[Token(Token = "0x60031F4")]
			[Address(RVA = "0x101608F64", Offset = "0x1608F64", VA = "0x101608F64")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031F5")]
			[Address(RVA = "0x101608F6C", Offset = "0x1608F6C", VA = "0x101608F6C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x060036E1 RID: 14049 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036E2 RID: 14050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003EE")]
		public List<UserTownObjectData> UserTownObjDatas
		{
			[Token(Token = "0x60031F6")]
			[Address(RVA = "0x101608F74", Offset = "0x1608F74", VA = "0x101608F74")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031F7")]
			[Address(RVA = "0x101608F7C", Offset = "0x1608F7C", VA = "0x101608F7C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x060036E3 RID: 14051 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036E4 RID: 14052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003EF")]
		public UserRoomListData UserRoomListData
		{
			[Token(Token = "0x60031F8")]
			[Address(RVA = "0x101608F84", Offset = "0x1608F84", VA = "0x101608F84")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031F9")]
			[Address(RVA = "0x101608F8C", Offset = "0x1608F8C", VA = "0x101608F8C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x060036E5 RID: 14053 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036E6 RID: 14054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F0")]
		public List<UserRoomObjectData> UserRoomObjDatas
		{
			[Token(Token = "0x60031FA")]
			[Address(RVA = "0x101608F94", Offset = "0x1608F94", VA = "0x101608F94")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031FB")]
			[Address(RVA = "0x101608F9C", Offset = "0x1608F9C", VA = "0x101608F9C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x060036E7 RID: 14055 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036E8 RID: 14056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F1")]
		public UserFieldCharaData UserFieldChara
		{
			[Token(Token = "0x60031FC")]
			[Address(RVA = "0x101607EF0", Offset = "0x1607EF0", VA = "0x101607EF0")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031FD")]
			[Address(RVA = "0x101608FA4", Offset = "0x1608FA4", VA = "0x101608FA4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x060036E9 RID: 14057 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036EA RID: 14058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F2")]
		public UserAdvData UserAdvData
		{
			[Token(Token = "0x60031FE")]
			[Address(RVA = "0x101608FAC", Offset = "0x1608FAC", VA = "0x101608FAC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60031FF")]
			[Address(RVA = "0x101608FB4", Offset = "0x1608FB4", VA = "0x101608FB4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x060036EB RID: 14059 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036EC RID: 14060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F3")]
		public List<UserArousalResultData> UserArousalResultDatas
		{
			[Token(Token = "0x6003200")]
			[Address(RVA = "0x101608FBC", Offset = "0x1608FBC", VA = "0x101608FBC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6003201")]
			[Address(RVA = "0x101608FC4", Offset = "0x1608FC4", VA = "0x101608FC4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x060036ED RID: 14061 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060036EE RID: 14062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F4")]
		public UserFavoriteMemberData UserFavoriteData
		{
			[Token(Token = "0x6003202")]
			[Address(RVA = "0x101608FCC", Offset = "0x1608FCC", VA = "0x101608FCC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6003203")]
			[Address(RVA = "0x101608FD4", Offset = "0x1608FD4", VA = "0x101608FD4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x060036EF RID: 14063 RVA: 0x000173A0 File Offset: 0x000155A0
		// (set) Token: 0x060036F0 RID: 14064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F5")]
		public int NoticeFriendCount
		{
			[Token(Token = "0x6003204")]
			[Address(RVA = "0x101608FDC", Offset = "0x1608FDC", VA = "0x101608FDC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003205")]
			[Address(RVA = "0x101608FE4", Offset = "0x1608FE4", VA = "0x101608FE4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x060036F1 RID: 14065 RVA: 0x000173B8 File Offset: 0x000155B8
		// (set) Token: 0x060036F2 RID: 14066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F6")]
		public int NoticePresentCount
		{
			[Token(Token = "0x6003206")]
			[Address(RVA = "0x101608FEC", Offset = "0x1608FEC", VA = "0x101608FEC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003207")]
			[Address(RVA = "0x101608FF4", Offset = "0x1608FF4", VA = "0x101608FF4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x060036F3 RID: 14067 RVA: 0x000173D0 File Offset: 0x000155D0
		// (set) Token: 0x060036F4 RID: 14068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F7")]
		public int NoticeTrainingCount
		{
			[Token(Token = "0x6003208")]
			[Address(RVA = "0x101608FFC", Offset = "0x1608FFC", VA = "0x101608FFC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003209")]
			[Address(RVA = "0x101609004", Offset = "0x1609004", VA = "0x101609004")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x060036F5 RID: 14069 RVA: 0x000173E8 File Offset: 0x000155E8
		// (set) Token: 0x060036F6 RID: 14070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F8")]
		public int NoticeLibraryCount
		{
			[Token(Token = "0x600320A")]
			[Address(RVA = "0x10160900C", Offset = "0x160900C", VA = "0x10160900C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600320B")]
			[Address(RVA = "0x101609014", Offset = "0x1609014", VA = "0x101609014")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x060036F7 RID: 14071 RVA: 0x00017400 File Offset: 0x00015600
		// (set) Token: 0x060036F8 RID: 14072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003F9")]
		public int NoticeMainOfferCount
		{
			[Token(Token = "0x600320C")]
			[Address(RVA = "0x10160901C", Offset = "0x160901C", VA = "0x10160901C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600320D")]
			[Address(RVA = "0x101609024", Offset = "0x1609024", VA = "0x101609024")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x060036F9 RID: 14073 RVA: 0x00017418 File Offset: 0x00015618
		// (set) Token: 0x060036FA RID: 14074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FA")]
		public int NoticeSubOfferCount
		{
			[Token(Token = "0x600320E")]
			[Address(RVA = "0x10160902C", Offset = "0x160902C", VA = "0x10160902C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600320F")]
			[Address(RVA = "0x101609034", Offset = "0x1609034", VA = "0x101609034")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x060036FB RID: 14075 RVA: 0x00017430 File Offset: 0x00015630
		// (set) Token: 0x060036FC RID: 14076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FB")]
		public int NoticeAchievementCount
		{
			[Token(Token = "0x6003210")]
			[Address(RVA = "0x10160903C", Offset = "0x160903C", VA = "0x10160903C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003211")]
			[Address(RVA = "0x101609044", Offset = "0x1609044", VA = "0x101609044")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060036FD RID: 14077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003212")]
		[Address(RVA = "0x10160904C", Offset = "0x160904C", VA = "0x10160904C")]
		public UserDataManager()
		{
		}

		// Token: 0x060036FE RID: 14078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003213")]
		[Address(RVA = "0x1016094F4", Offset = "0x16094F4", VA = "0x1016094F4")]
		public void Update()
		{
		}

		// Token: 0x060036FF RID: 14079 RVA: 0x00017448 File Offset: 0x00015648
		[Token(Token = "0x6003214")]
		[Address(RVA = "0x101609504", Offset = "0x1609504", VA = "0x101609504")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06003700 RID: 14080 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003215")]
		[Address(RVA = "0x101607B6C", Offset = "0x1607B6C", VA = "0x101607B6C")]
		public UserCharacterData GetUserCharaData(long mngID)
		{
			return null;
		}

		// Token: 0x06003701 RID: 14081 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003216")]
		[Address(RVA = "0x101609594", Offset = "0x1609594", VA = "0x101609594")]
		public UserCharacterData GetUserCharaData(int characterID)
		{
			return null;
		}

		// Token: 0x06003702 RID: 14082 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003217")]
		[Address(RVA = "0x1016096AC", Offset = "0x16096AC", VA = "0x1016096AC")]
		public List<UserCharacterData> GetUserCharaDatas(List<long> mngIDs)
		{
			return null;
		}

		// Token: 0x06003703 RID: 14083 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003218")]
		[Address(RVA = "0x1016097D8", Offset = "0x16097D8", VA = "0x1016097D8")]
		public List<UserCharacterData> GetUserCharaDatas(List<int> characterIDs)
		{
			return null;
		}

		// Token: 0x06003704 RID: 14084 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003219")]
		[Address(RVA = "0x101608968", Offset = "0x1608968", VA = "0x101608968")]
		public UserNamedData GetUserNamedData(eCharaNamedType namedType)
		{
			return null;
		}

		// Token: 0x06003705 RID: 14085 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600321A")]
		[Address(RVA = "0x101609904", Offset = "0x1609904", VA = "0x101609904")]
		public UserWeaponData GetUserWpnData(long wpnMngID)
		{
			return null;
		}

		// Token: 0x06003706 RID: 14086 RVA: 0x00017460 File Offset: 0x00015660
		[Token(Token = "0x600321B")]
		[Address(RVA = "0x101609A0C", Offset = "0x1609A0C", VA = "0x101609A0C")]
		public int GetItemNum(int itemID)
		{
			return 0;
		}

		// Token: 0x06003707 RID: 14087 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600321C")]
		[Address(RVA = "0x101609B20", Offset = "0x1609B20", VA = "0x101609B20")]
		public List<ItemListDB_Param> GetItemListFromUserData(eItemType itemType)
		{
			return null;
		}

		// Token: 0x06003708 RID: 14088 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600321D")]
		[Address(RVA = "0x101609E14", Offset = "0x1609E14", VA = "0x101609E14")]
		public UserMasterOrbData GetUserMasterOrbData(int orbId)
		{
			return null;
		}

		// Token: 0x06003709 RID: 14089 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600321E")]
		[Address(RVA = "0x101609F1C", Offset = "0x1609F1C", VA = "0x101609F1C")]
		public UserMasterOrbData GetEquipUserMasterOrbData()
		{
			return null;
		}

		// Token: 0x0600370A RID: 14090 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600321F")]
		[Address(RVA = "0x10160A044", Offset = "0x160A044", VA = "0x10160A044")]
		public List<UserMasterOrbData> GetSortedUserMasterOrbDatas()
		{
			return null;
		}

		// Token: 0x0600370B RID: 14091 RVA: 0x00017478 File Offset: 0x00015678
		[Token(Token = "0x6003220")]
		[Address(RVA = "0x10160A1A4", Offset = "0x160A1A4", VA = "0x10160A1A4")]
		private int UserOrbDataSortComp(UserMasterOrbData l, UserMasterOrbData r)
		{
			return 0;
		}

		// Token: 0x0600370C RID: 14092 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003221")]
		[Address(RVA = "0x10160A39C", Offset = "0x160A39C", VA = "0x10160A39C")]
		public UserAbilityData GetUserAbilityDataFromAbilityMngID(long mngID)
		{
			return null;
		}

		// Token: 0x0600370D RID: 14093 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003222")]
		[Address(RVA = "0x10160A49C", Offset = "0x160A49C", VA = "0x10160A49C")]
		public UserAbilityData GetUserAbilityDataFromCharaMngID(long charaMngID)
		{
			return null;
		}

		// Token: 0x0600370E RID: 14094 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003223")]
		[Address(RVA = "0x10160A59C", Offset = "0x160A59C", VA = "0x10160A59C")]
		public UserBattlePartyData GetUserBattlePartyData(long partyMngID)
		{
			return null;
		}

		// Token: 0x0600370F RID: 14095 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003224")]
		[Address(RVA = "0x10160A69C", Offset = "0x160A69C", VA = "0x10160A69C")]
		public UserSupportPartyData GetUserSupportPartyData(long partyMngID)
		{
			return null;
		}

		// Token: 0x06003710 RID: 14096 RVA: 0x00017490 File Offset: 0x00015690
		[Token(Token = "0x6003225")]
		[Address(RVA = "0x10160A7A4", Offset = "0x160A7A4", VA = "0x10160A7A4")]
		public int GetActivateSupportPartyIndex()
		{
			return 0;
		}

		// Token: 0x06003711 RID: 14097 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003226")]
		[Address(RVA = "0x10160A89C", Offset = "0x160A89C", VA = "0x10160A89C")]
		public UserRoomObjectData GetUserRoomObjData(eRoomObjectCategory objCategory, int objID)
		{
			return null;
		}

		// Token: 0x06003712 RID: 14098 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003227")]
		[Address(RVA = "0x10160A9E4", Offset = "0x160A9E4", VA = "0x10160A9E4")]
		public UserRoomObjectData GetUserRoomObjData(int resID)
		{
			return null;
		}

		// Token: 0x06003713 RID: 14099 RVA: 0x000174A8 File Offset: 0x000156A8
		[Token(Token = "0x6003228")]
		[Address(RVA = "0x10160AAE4", Offset = "0x160AAE4", VA = "0x10160AAE4")]
		public int GetUserRoomObjHaveNum()
		{
			return 0;
		}

		// Token: 0x06003714 RID: 14100 RVA: 0x000174C0 File Offset: 0x000156C0
		[Token(Token = "0x6003229")]
		[Address(RVA = "0x10160AD90", Offset = "0x160AD90", VA = "0x10160AD90")]
		public int GetUserRoomObjPlacementNum()
		{
			return 0;
		}

		// Token: 0x06003715 RID: 14101 RVA: 0x000174D8 File Offset: 0x000156D8
		[Token(Token = "0x600322A")]
		[Address(RVA = "0x10160B2B4", Offset = "0x160B2B4", VA = "0x10160B2B4")]
		public int GetUserRoomObjPlacementNum(int floorId)
		{
			return 0;
		}

		// Token: 0x06003716 RID: 14102 RVA: 0x000174F0 File Offset: 0x000156F0
		[Token(Token = "0x600322B")]
		[Address(RVA = "0x10160B430", Offset = "0x160B430", VA = "0x10160B430")]
		public int GetUserRoomObjRemainNum()
		{
			return 0;
		}

		// Token: 0x06003717 RID: 14103 RVA: 0x00017508 File Offset: 0x00015708
		[Token(Token = "0x600322C")]
		[Address(RVA = "0x10160B56C", Offset = "0x160B56C", VA = "0x10160B56C")]
		public bool IsExistUserArousalResultData()
		{
			return default(bool);
		}

		// Token: 0x06003718 RID: 14104 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600322D")]
		[Address(RVA = "0x10160B5D8", Offset = "0x160B5D8", VA = "0x10160B5D8")]
		public UserArousalResultData GetUserArousalResultData(int characterID)
		{
			return null;
		}

		// Token: 0x06003719 RID: 14105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600322E")]
		[Address(RVA = "0x10160B6D8", Offset = "0x160B6D8", VA = "0x10160B6D8")]
		public void UpdateSubOfferCount(OfferManager.TitleData[] titleDatas)
		{
		}
	}
}
