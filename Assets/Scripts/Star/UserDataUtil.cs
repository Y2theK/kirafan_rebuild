﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200072D RID: 1837
	[Token(Token = "0x20005A4")]
	[StructLayout(3)]
	public class UserDataUtil
	{
		// Token: 0x06001AD8 RID: 6872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018A8")]
		[Address(RVA = "0x10160B7E4", Offset = "0x160B7E4", VA = "0x10160B7E4")]
		public static void LinkUpUtil()
		{
		}

		// Token: 0x06001AD9 RID: 6873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018A9")]
		[Address(RVA = "0x10160B8EC", Offset = "0x160B8EC", VA = "0x10160B8EC")]
		public static void ClearUtil()
		{
		}

		// Token: 0x06001ADA RID: 6874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018AA")]
		[Address(RVA = "0x10160B948", Offset = "0x160B948", VA = "0x10160B948")]
		public UserDataUtil()
		{
		}

		// Token: 0x04002ADF RID: 10975
		[Token(Token = "0x40022C9")]
		public static UserTownData TownData;

		// Token: 0x04002AE0 RID: 10976
		[Token(Token = "0x40022CA")]
		public static List<UserTownObjectData> TownObjData;

		// Token: 0x04002AE1 RID: 10977
		[Token(Token = "0x40022CB")]
		public static UserFieldCharaData FieldChara;
	}
}
