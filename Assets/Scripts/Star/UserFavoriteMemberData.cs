﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000C13 RID: 3091
	[Token(Token = "0x200084D")]
	[StructLayout(3)]
	public class UserFavoriteMemberData
	{
		// Token: 0x0600371A RID: 14106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600322F")]
		[Address(RVA = "0x10160B950", Offset = "0x160B950", VA = "0x10160B950")]
		public void DataClear()
		{
		}

		// Token: 0x0600371B RID: 14107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003230")]
		[Address(RVA = "0x10160B9B0", Offset = "0x160B9B0", VA = "0x10160B9B0")]
		public void SetData(PlayerFavoriteMember src)
		{
		}

		// Token: 0x0600371C RID: 14108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003231")]
		[Address(RVA = "0x10160BA80", Offset = "0x160BA80", VA = "0x10160BA80")]
		public void SetDataFromManagedID(int index, long managedID)
		{
		}

		// Token: 0x0600371D RID: 14109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003232")]
		[Address(RVA = "0x10160BC68", Offset = "0x160BC68", VA = "0x10160BC68")]
		public void SetDataFromManagedIDs(long[] managedIDs)
		{
		}

		// Token: 0x0600371E RID: 14110 RVA: 0x00017520 File Offset: 0x00015720
		[Token(Token = "0x6003233")]
		[Address(RVA = "0x10160BB7C", Offset = "0x160BB7C", VA = "0x10160BB7C")]
		private UserFavoriteMemberData.FavoriteMemberData ManagedID2FavoriteData(long managedID)
		{
			return default(UserFavoriteMemberData.FavoriteMemberData);
		}

		// Token: 0x0600371F RID: 14111 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003234")]
		[Address(RVA = "0x10160BD9C", Offset = "0x160BD9C", VA = "0x10160BD9C")]
		public long[] GetManageedCharacterIDs()
		{
			return null;
		}

		// Token: 0x06003720 RID: 14112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003235")]
		[Address(RVA = "0x101609484", Offset = "0x1609484", VA = "0x101609484")]
		public UserFavoriteMemberData()
		{
		}

		// Token: 0x040046DD RID: 18141
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40031BA")]
		private Dictionary<int, UserFavoriteMemberData.FavoriteMemberData> dicMemberData;

		// Token: 0x02000C14 RID: 3092
		[Token(Token = "0x200108F")]
		public struct FavoriteMemberData
		{
			// Token: 0x040046DE RID: 18142
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006877")]
			public long managedCharacterId;

			// Token: 0x040046DF RID: 18143
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006878")]
			public int characterId;

			// Token: 0x040046E0 RID: 18144
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4006879")]
			public int arousalLevel;
		}
	}
}
