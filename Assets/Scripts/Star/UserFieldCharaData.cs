﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x0200072E RID: 1838
	[Token(Token = "0x20005A5")]
	[StructLayout(3)]
	public class UserFieldCharaData
	{
		// Token: 0x06001ADB RID: 6875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018AB")]
		[Address(RVA = "0x10160BF2C", Offset = "0x160BF2C", VA = "0x10160BF2C")]
		public void AddRoomInChara(PlayerFieldPartyMember pwwwfldchara)
		{
		}

		// Token: 0x06001ADC RID: 6876 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018AC")]
		[Address(RVA = "0x10160C28C", Offset = "0x160C28C", VA = "0x10160C28C")]
		public PlayerFieldPartyMember CreateRoomInChara(long fchrmngid)
		{
			return null;
		}

		// Token: 0x06001ADD RID: 6877 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018AD")]
		[Address(RVA = "0x10160C4B0", Offset = "0x160C4B0", VA = "0x10160C4B0")]
		public ScheduleMemberParam CreateRoomInChara2(long fchrmngid)
		{
			return null;
		}

		// Token: 0x06001ADE RID: 6878 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018AE")]
		[Address(RVA = "0x10160C590", Offset = "0x160C590", VA = "0x10160C590")]
		public ChangeScheduleMember CreateRoomInCharaSchedule(long fchrmngid)
		{
			return null;
		}

		// Token: 0x06001ADF RID: 6879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018AF")]
		[Address(RVA = "0x10160CA7C", Offset = "0x160CA7C", VA = "0x10160CA7C")]
		public void CheckManageCharaID(UserFieldCharaData.UpFieldCharaData[] fcharamngid, [Optional] UserFieldCharaData.CharaInOutEvent pcallback)
		{
		}

		// Token: 0x06001AE0 RID: 6880 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018B0")]
		[Address(RVA = "0x10160D490", Offset = "0x160D490", VA = "0x10160D490")]
		public static UserFieldCharaData.UpFieldCharaData[] CreateSetUpData(long[] charaMngIDs)
		{
			return null;
		}

		// Token: 0x06001AE1 RID: 6881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018B1")]
		[Address(RVA = "0x10160D658", Offset = "0x160D658", VA = "0x10160D658")]
		public void UpManageID(long fmanageid)
		{
		}

		// Token: 0x06001AE2 RID: 6882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018B2")]
		[Address(RVA = "0x10160D660", Offset = "0x160D660", VA = "0x10160D660")]
		public void ClearAll(long fmanageid = -1L)
		{
		}

		// Token: 0x06001AE3 RID: 6883 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018B3")]
		[Address(RVA = "0x10160D3DC", Offset = "0x160D3DC", VA = "0x10160D3DC")]
		public UserFieldCharaData.RoomInCharaData AddRoomInChara(long fmanageid, int fcharaid, int froomno = 0, int liveIdx = 0)
		{
			return null;
		}

		// Token: 0x06001AE4 RID: 6884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018B4")]
		[Address(RVA = "0x10160D6E8", Offset = "0x160D6E8", VA = "0x10160D6E8")]
		public void RemoveRoomInChara(long fmanageid)
		{
		}

		// Token: 0x06001AE5 RID: 6885 RVA: 0x0000BF88 File Offset: 0x0000A188
		[Token(Token = "0x60018B5")]
		[Address(RVA = "0x10160448C", Offset = "0x160448C", VA = "0x10160448C")]
		public int GetRoomInCharaNum()
		{
			return 0;
		}

		// Token: 0x06001AE6 RID: 6886 RVA: 0x0000BFA0 File Offset: 0x0000A1A0
		[Token(Token = "0x60018B6")]
		[Address(RVA = "0x10160D7EC", Offset = "0x160D7EC", VA = "0x10160D7EC")]
		public int GetSpecifiedRoomInCharaNum(int roomNo)
		{
			return 0;
		}

		// Token: 0x06001AE7 RID: 6887 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018B7")]
		[Address(RVA = "0x1016044EC", Offset = "0x16044EC", VA = "0x1016044EC")]
		public UserFieldCharaData.RoomInCharaData GetRoomInCharaAt(int findex)
		{
			return null;
		}

		// Token: 0x06001AE8 RID: 6888 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018B8")]
		[Address(RVA = "0x101607EF8", Offset = "0x1607EF8", VA = "0x101607EF8")]
		public UserFieldCharaData.RoomInCharaData GetRoomInChara(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001AE9 RID: 6889 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018B9")]
		[Address(RVA = "0x10160D928", Offset = "0x160D928", VA = "0x10160D928")]
		public long[] GetRoomInCharaManageID()
		{
			return null;
		}

		// Token: 0x06001AEA RID: 6890 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018BA")]
		[Address(RVA = "0x10160DAEC", Offset = "0x160DAEC", VA = "0x10160DAEC")]
		public long[] GetMaskBuildMngIDInCharaManageID(long fbuildmanageid)
		{
			return null;
		}

		// Token: 0x06001AEB RID: 6891 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018BB")]
		[Address(RVA = "0x10160DCA8", Offset = "0x160DCA8", VA = "0x10160DCA8")]
		public long[] GetNoMaskBuildMngIDInCharaManageID(long fbuildmanageid)
		{
			return null;
		}

		// Token: 0x06001AEC RID: 6892 RVA: 0x0000BFB8 File Offset: 0x0000A1B8
		[Token(Token = "0x60018BC")]
		[Address(RVA = "0x10160DE64", Offset = "0x160DE64", VA = "0x10160DE64")]
		public bool IsEntryFldChara(long fmanageid)
		{
			return default(bool);
		}

		// Token: 0x06001AED RID: 6893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018BD")]
		[Address(RVA = "0x10160940C", Offset = "0x160940C", VA = "0x10160940C")]
		public UserFieldCharaData()
		{
		}

		// Token: 0x04002AE2 RID: 10978
		[Token(Token = "0x40022CC")]
		public const int TOUCH_ITEM_NEW = 1;

		// Token: 0x04002AE3 RID: 10979
		[Token(Token = "0x40022CD")]
		public const int TOUCH_ITEM_SP = 2;

		// Token: 0x04002AE4 RID: 10980
		[Token(Token = "0x40022CE")]
		public const int CHANGE_SCHEDULE = 4;

		// Token: 0x04002AE5 RID: 10981
		[Token(Token = "0x40022CF")]
		public const int NEW_SCHEDULE = 8;

		// Token: 0x04002AE6 RID: 10982
		[Token(Token = "0x40022D0")]
		public const int SCHEDULE_CLEAR = 16;

		// Token: 0x04002AE7 RID: 10983
		[Token(Token = "0x40022D1")]
		public const int SCHEDULE_MARK = 32;

		// Token: 0x04002AE8 RID: 10984
		[Token(Token = "0x40022D2")]
		public const int ROOM_CATEGORY_MAIN = 1;

		// Token: 0x04002AE9 RID: 10985
		[Token(Token = "0x40022D3")]
		public const int ROOM_CATEGORY_SUB = 2;

		// Token: 0x04002AEA RID: 10986
		[Token(Token = "0x40022D4")]
		public const int ROOM_LIVE_MAX = 5;

		// Token: 0x04002AEB RID: 10987
		[Token(Token = "0x40022D5")]
		public const int ROOM_NUM_MAX = 2;

		// Token: 0x04002AEC RID: 10988
		[Token(Token = "0x40022D6")]
		public const int Version = 0;

		// Token: 0x04002AED RID: 10989
		[Token(Token = "0x40022D7")]
		public const int ScheduleVersion = 1;

		// Token: 0x04002AEE RID: 10990
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022D8")]
		public long MngID;

		// Token: 0x04002AEF RID: 10991
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022D9")]
		public long ScheduleTime;

		// Token: 0x04002AF0 RID: 10992
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40022DA")]
		private List<UserFieldCharaData.RoomInCharaData> m_RoomInChara;

		// Token: 0x0200072F RID: 1839
		[Token(Token = "0x2000E5A")]
		public class RoomInCharaData
		{
			// Token: 0x06001AEE RID: 6894 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF1")]
			[Address(RVA = "0x10160DFD0", Offset = "0x160DFD0", VA = "0x10160DFD0")]
			public void CheckDefaultScheduleUp(long ftimes)
			{
			}

			// Token: 0x06001AEF RID: 6895 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF2")]
			[Address(RVA = "0x10160E02C", Offset = "0x160E02C", VA = "0x10160E02C")]
			public void SetFlags(int flags)
			{
			}

			// Token: 0x06001AF0 RID: 6896 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF3")]
			[Address(RVA = "0x10160E034", Offset = "0x160E034", VA = "0x10160E034")]
			public void CreateInitCharaSchedule(long fsystemtime)
			{
			}

			// Token: 0x06001AF1 RID: 6897 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF4")]
			[Address(RVA = "0x10160E354", Offset = "0x160E354", VA = "0x10160E354")]
			public void SetLinkObjectPack(UserScheduleData.ListPack pschedule, long fsec)
			{
			}

			// Token: 0x06001AF2 RID: 6898 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF5")]
			[Address(RVA = "0x10160E520", Offset = "0x160E520", VA = "0x10160E520")]
			public void ChkUpSchedule(bool frefresh, long fsystemtime)
			{
			}

			// Token: 0x06001AF3 RID: 6899 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF6")]
			[Address(RVA = "0x10160E688", Offset = "0x160E688", VA = "0x10160E688")]
			private void SetScheduleDropParam(int fscheduletag, int fcharaid, int fscheduletimes, eTownMoveState fupmove, bool isSimulateMode)
			{
			}

			// Token: 0x06001AF4 RID: 6900 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF7")]
			[Address(RVA = "0x10160E7AC", Offset = "0x160E7AC", VA = "0x10160E7AC")]
			public void UserStateFlagUp()
			{
			}

			// Token: 0x06001AF5 RID: 6901 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF8")]
			[Address(RVA = "0x10160E7FC", Offset = "0x160E7FC", VA = "0x10160E7FC")]
			public void ChangeBuildPoint(long fpointmngid, int fscheduletag, int fnewscheudletag, int fscheduletime, int fchrid, long fkeytime, eTownMoveState fupmove, bool isSimulateMode)
			{
			}

			// Token: 0x06001AF6 RID: 6902 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF9")]
			[Address(RVA = "0x10160ED30", Offset = "0x160ED30", VA = "0x10160ED30")]
			public void ChangeBuildPoint2(long fpointmngid, int fscheduletag, int fnewscheudletag, int fscheduletime, int fchrid, long fkeytime, eTownMoveState fupmove, bool isSimulateMode)
			{
			}

			// Token: 0x06001AF7 RID: 6903 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EFA")]
			[Address(RVA = "0x10160ED3C", Offset = "0x160ED3C", VA = "0x10160ED3C")]
			public void ClearTouchItemEvent()
			{
			}

			// Token: 0x06001AF8 RID: 6904 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EFB")]
			[Address(RVA = "0x10160ED44", Offset = "0x160ED44", VA = "0x10160ED44")]
			public void ClearTouchItemNew()
			{
			}

			// Token: 0x06001AF9 RID: 6905 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005EFC")]
			[Address(RVA = "0x10160C79C", Offset = "0x160C79C", VA = "0x10160C79C")]
			public DropPresent[] CreateDropItemList()
			{
				return null;
			}

			// Token: 0x06001AFA RID: 6906 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EFD")]
			[Address(RVA = "0x10160C1D0", Offset = "0x160C1D0", VA = "0x10160C1D0")]
			public RoomInCharaData(long fmanageid, int fcharaid, int froomno, int liveIdx)
			{
			}

			// Token: 0x06001AFB RID: 6907 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EFE")]
			[Address(RVA = "0x10160ED54", Offset = "0x160ED54", VA = "0x10160ED54")]
			public void ReserveReCreateSchedule()
			{
			}

			// Token: 0x06001AFC RID: 6908 RVA: 0x0000BFD0 File Offset: 0x0000A1D0
			[Token(Token = "0x6005EFF")]
			[Address(RVA = "0x10160ED60", Offset = "0x160ED60", VA = "0x10160ED60")]
			public bool IsReservedReCreateSchedule()
			{
				return default(bool);
			}

			// Token: 0x06001AFD RID: 6909 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F00")]
			[Address(RVA = "0x10160ED68", Offset = "0x160ED68", VA = "0x10160ED68")]
			public void ClearReservedReCreateSchedule()
			{
			}

			// Token: 0x06001AFE RID: 6910 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F01")]
			[Address(RVA = "0x10160E288", Offset = "0x160E288", VA = "0x10160E288")]
			public void SetSchedule(UserScheduleData.ListPack pschedule)
			{
			}

			// Token: 0x06001AFF RID: 6911 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005F02")]
			[Address(RVA = "0x10160EECC", Offset = "0x160EECC", VA = "0x10160EECC")]
			public UserScheduleData.ListPack GetScheduleList(long fsettingtime)
			{
				return null;
			}

			// Token: 0x06001B00 RID: 6912 RVA: 0x0000BFE8 File Offset: 0x0000A1E8
			[Token(Token = "0x6005F03")]
			[Address(RVA = "0x10160EF20", Offset = "0x160EF20", VA = "0x10160EF20")]
			public long GetSchedulePlayTime()
			{
				return 0L;
			}

			// Token: 0x06001B01 RID: 6913 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F04")]
			[Address(RVA = "0x10160E324", Offset = "0x160E324", VA = "0x10160E324")]
			public void SetSchedulePlayTime(long fplaytime)
			{
			}

			// Token: 0x06001B02 RID: 6914 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F05")]
			[Address(RVA = "0x10160EB90", Offset = "0x160EB90", VA = "0x10160EB90")]
			public void PushScheduleDropItem(long fdroptime, bool isSimulateMode)
			{
			}

			// Token: 0x06001B03 RID: 6915 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F06")]
			[Address(RVA = "0x10160E704", Offset = "0x160E704", VA = "0x10160E704")]
			public void SetScheduleDropKey(int fdropkey, float fmagkey)
			{
			}

			// Token: 0x06001B04 RID: 6916 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F07")]
			[Address(RVA = "0x10160CA1C", Offset = "0x160CA1C", VA = "0x10160CA1C")]
			public void ClearDropItemList()
			{
			}

			// Token: 0x04002AF1 RID: 10993
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B8C")]
			public long ServerManageID;

			// Token: 0x04002AF2 RID: 10994
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B8D")]
			public long ManageID;

			// Token: 0x04002AF3 RID: 10995
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B8E")]
			public int CharacterID;

			// Token: 0x04002AF4 RID: 10996
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4005B8F")]
			public int PlayScheduleTagID;

			// Token: 0x04002AF5 RID: 10997
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B90")]
			public int ScheduleTagID;

			// Token: 0x04002AF6 RID: 10998
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B91")]
			public long TownBuildManageID;

			// Token: 0x04002AF7 RID: 10999
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B92")]
			public int RoomID;

			// Token: 0x04002AF8 RID: 11000
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4005B93")]
			public int LiveIdx;

			// Token: 0x04002AF9 RID: 11001
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B94")]
			public int TouchItemResultNo;

			// Token: 0x04002AFA RID: 11002
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x4005B95")]
			public int Flags;

			// Token: 0x04002AFB RID: 11003
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4005B96")]
			public int ArousalLv;

			// Token: 0x04002AFC RID: 11004
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x4005B97")]
			public bool m_NewUpSchedule;

			// Token: 0x04002AFD RID: 11005
			[Cpp2IlInjected.FieldOffset(Offset = "0x4D")]
			[Token(Token = "0x4005B98")]
			public bool m_isNewCreateSchedule;

			// Token: 0x04002AFE RID: 11006
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4005B99")]
			public UserScheduleData.Play m_Schedule;

			// Token: 0x04002AFF RID: 11007
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4005B9A")]
			public int m_ScheduleDropItemIcon;

			// Token: 0x04002B00 RID: 11008
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x4005B9B")]
			public bool m_NewSchedule;

			// Token: 0x04002B01 RID: 11009
			[Cpp2IlInjected.FieldOffset(Offset = "0x5D")]
			[Token(Token = "0x4005B9C")]
			public bool m_ChangeScheduleTag;

			// Token: 0x04002B02 RID: 11010
			[Cpp2IlInjected.FieldOffset(Offset = "0x5E")]
			[Token(Token = "0x4005B9D")]
			public bool m_TouchItemNew;

			// Token: 0x04002B03 RID: 11011
			[Cpp2IlInjected.FieldOffset(Offset = "0x5F")]
			[Token(Token = "0x4005B9E")]
			public bool m_TouchItemSp;

			// Token: 0x04002B04 RID: 11012
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4005B9F")]
			public bool m_ScheduleBaseUp;

			// Token: 0x04002B05 RID: 11013
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4005BA0")]
			public List<UserFieldCharaData.ScheduleDropItem> m_DropList;
		}

		// Token: 0x02000730 RID: 1840
		[Token(Token = "0x2000E5B")]
		public enum eUpState
		{
			// Token: 0x04002B07 RID: 11015
			[Token(Token = "0x4005BA2")]
			In,
			// Token: 0x04002B08 RID: 11016
			[Token(Token = "0x4005BA3")]
			Out,
			// Token: 0x04002B09 RID: 11017
			[Token(Token = "0x4005BA4")]
			StateChg
		}

		// Token: 0x02000731 RID: 1841
		// (Invoke) Token: 0x06001B06 RID: 6918
		[Token(Token = "0x2000E5C")]
		public delegate void CharaInOutEvent(UserFieldCharaData.eUpState finout, UserFieldCharaData.RoomInCharaData pchara);

		// Token: 0x02000732 RID: 1842
		[Token(Token = "0x2000E5D")]
		public struct UpFieldCharaData
		{
			// Token: 0x04002B0A RID: 11018
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005BA5")]
			public long ManageID;

			// Token: 0x04002B0B RID: 11019
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005BA6")]
			public int RoomID;

			// Token: 0x04002B0C RID: 11020
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4005BA7")]
			public int LiveIdx;

			// Token: 0x04002B0D RID: 11021
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BA8")]
			public int CharaID;
		}

		// Token: 0x02000733 RID: 1843
		[Token(Token = "0x2000E5E")]
		public class ScheduleDropItem
		{
			// Token: 0x06001B09 RID: 6921 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F0C")]
			[Address(RVA = "0x10160EF38", Offset = "0x160EF38", VA = "0x10160EF38")]
			public ScheduleDropItem()
			{
			}

			// Token: 0x04002B0E RID: 11022
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BA9")]
			public int m_ItemNo;

			// Token: 0x04002B0F RID: 11023
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005BAA")]
			public long m_DropTime;

			// Token: 0x04002B10 RID: 11024
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005BAB")]
			public float m_Mag;
		}
	}
}
