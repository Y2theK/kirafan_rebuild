﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C1C RID: 3100
	[Token(Token = "0x200084F")]
	[StructLayout(3)]
	public class UserItemData
	{
		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x06003729 RID: 14121 RVA: 0x00017550 File Offset: 0x00015750
		// (set) Token: 0x0600372A RID: 14122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FC")]
		public int ItemID
		{
			[Token(Token = "0x6003237")]
			[Address(RVA = "0x101609B10", Offset = "0x1609B10", VA = "0x101609B10")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003238")]
			[Address(RVA = "0x10160EF40", Offset = "0x160EF40", VA = "0x10160EF40")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x0600372B RID: 14123 RVA: 0x00017568 File Offset: 0x00015768
		// (set) Token: 0x0600372C RID: 14124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FD")]
		public int ItemNum
		{
			[Token(Token = "0x6003239")]
			[Address(RVA = "0x101609B18", Offset = "0x1609B18", VA = "0x101609B18")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600323A")]
			[Address(RVA = "0x10160EF48", Offset = "0x160EF48", VA = "0x10160EF48")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x0600372D RID: 14125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600323B")]
		[Address(RVA = "0x10160EF50", Offset = "0x160EF50", VA = "0x10160EF50")]
		public UserItemData()
		{
		}
	}
}
