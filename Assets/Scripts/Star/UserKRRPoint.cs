﻿namespace Star {
    public class UserKRRPoint {
        private long m_Point;
        private long m_Limit = 1000000L;

        public long GetPoint() {
            return m_Point;
        }

        public void SetPoint(long value) {
            m_Point = value;
        }

        public void AddPoint(int value) {
            m_Point += value;
        }

        public long GetLimit() {
            return m_Limit;
        }

        public void SetLimit(long value) {
            m_Limit = value;
        }
    }
}
