﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C1D RID: 3101
	[Token(Token = "0x2000850")]
	[StructLayout(3)]
	public class UserMasterOrbData
	{
		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x0600372E RID: 14126 RVA: 0x00017580 File Offset: 0x00015780
		// (set) Token: 0x0600372F RID: 14127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FE")]
		public long MngID
		{
			[Token(Token = "0x600323C")]
			[Address(RVA = "0x10160EFA4", Offset = "0x160EFA4", VA = "0x10160EFA4")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x600323D")]
			[Address(RVA = "0x10160EFAC", Offset = "0x160EFAC", VA = "0x10160EFAC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x06003730 RID: 14128 RVA: 0x00017598 File Offset: 0x00015798
		// (set) Token: 0x06003731 RID: 14129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003FF")]
		public int OrbID
		{
			[Token(Token = "0x600323E")]
			[Address(RVA = "0x101609F14", Offset = "0x1609F14", VA = "0x101609F14")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600323F")]
			[Address(RVA = "0x10160EFB4", Offset = "0x160EFB4", VA = "0x10160EFB4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x06003732 RID: 14130 RVA: 0x000175B0 File Offset: 0x000157B0
		// (set) Token: 0x06003733 RID: 14131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000400")]
		public int Lv
		{
			[Token(Token = "0x6003240")]
			[Address(RVA = "0x10160EFBC", Offset = "0x160EFBC", VA = "0x10160EFBC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003241")]
			[Address(RVA = "0x10160EFC4", Offset = "0x160EFC4", VA = "0x10160EFC4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x06003734 RID: 14132 RVA: 0x000175C8 File Offset: 0x000157C8
		// (set) Token: 0x06003735 RID: 14133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000401")]
		public long Exp
		{
			[Token(Token = "0x6003242")]
			[Address(RVA = "0x10160EFCC", Offset = "0x160EFCC", VA = "0x10160EFCC")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6003243")]
			[Address(RVA = "0x10160EFD4", Offset = "0x160EFD4", VA = "0x10160EFD4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06003736 RID: 14134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003244")]
		[Address(RVA = "0x10160EFDC", Offset = "0x160EFDC", VA = "0x10160EFDC")]
		public UserMasterOrbData()
		{
		}
	}
}
