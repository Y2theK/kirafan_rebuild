﻿namespace Star {
    public class UserNamedData {
        public long MngID { get; set; }
        public eCharaNamedType NamedType { get; set; }
        public int FriendShip { get; set; }
        public int FriendShipMax { get; set; }
        public long FriendShipExp { get; set; }

        public UserNamedData(long mngID, eCharaNamedType namedType, int friendship, int friendShipMax, uint friendshipExp) {
            MngID = mngID;
            NamedType = namedType;
            FriendShip = friendship;
            FriendShipMax = friendShipMax;
            FriendShipExp = friendshipExp;
        }

        public UserNamedData() {
            MngID = -1;
            NamedType = eCharaNamedType.None;
            FriendShip = 0;
            FriendShipMax = 0;
            FriendShipExp = 0;
        }
    }
}
