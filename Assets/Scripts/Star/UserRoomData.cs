﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star {
    [Serializable]
    public class UserRoomData {
        public const int Version = 0;

        [SerializeField] private List<PlacementData> m_Placements = new List<PlacementData>();

        public long MngID;
        public int FloorID;
        public long PlayerID;

        public UserRoomData() {
            MngID = -1L;
            FloorID = -1;
        }

        public void AddPlacement(PlacementData placementData) {
            m_Placements.Add(placementData);
        }

        public void RemovePlacement(long fmanageid) {
            for (int i = 0; i < m_Placements.Count; i++) {
                if (m_Placements[i].m_ManageID == fmanageid) {
                    m_Placements.RemoveAt(i);
                    break;
                }
            }
        }

        public void ClearPlacements() {
            m_Placements.Clear();
        }

        public int GetPlacementNum() {
            return m_Placements.Count;
        }

        public PlacementData GetPlacementAt(int index) {
            return m_Placements[index];
        }

        public PlacementData GetPlacementManageID(long fmanageid) {
            for (int i = 0; i < m_Placements.Count; i++) {
                if (m_Placements[i].m_ManageID == fmanageid) {
                    return m_Placements[i];
                }
            }
            return null;
        }

        public PlacementData GetPlacement(int x, int y, int froomNo = 0) {
            for (int i = 0; i < m_Placements.Count; i++) {
                if (m_Placements[i].m_X == x && m_Placements[i].m_Y == y && m_Placements[i].m_RoomNo == froomNo) {
                    return m_Placements[i];
                }
            }
            return null;
        }

        [Serializable]
        public class PlacementData {
            public int m_RoomNo;
            public int m_X;
            public int m_Y;
            public CharacterDefine.eDir m_Dir;
            public int m_ObjectID;
            public long m_ManageID;

            public PlacementData(int x, int y, CharacterDefine.eDir dir, long fmanageid, int roomSubID, int resourceID) {
                m_X = x;
                m_Y = y;
                m_Dir = dir;
                m_ManageID = fmanageid;
                m_RoomNo = roomSubID;
                m_ObjectID = resourceID;
            }
        }
    }
}
