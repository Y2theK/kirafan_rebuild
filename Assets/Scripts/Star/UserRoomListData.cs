﻿using System;
using System.Collections.Generic;

namespace Star {
    [Serializable]
    public class UserRoomListData {
        private List<PlayerFloorState> m_PlayerFloorList = new List<PlayerFloorState>();

        public void CheckEntryFloor(long fplayerid, int fgroupno, UserRoomData proomdata) {
            int floorCount = m_PlayerFloorList.Count;
            for (int i = 0; i < floorCount; i++) {
                if (m_PlayerFloorList[i].m_PlayerID == fplayerid) {
                    m_PlayerFloorList[i].CheckFloorData(fgroupno, proomdata);
                    return;
                }
            }
            PlayerFloorState playerFloorState = new PlayerFloorState();
            playerFloorState.m_PlayerID = fplayerid;
            playerFloorState.m_ActiveRoomMngID = proomdata.MngID;
            playerFloorState.CheckFloorData(fgroupno, proomdata);
            m_PlayerFloorList.Add(playerFloorState);
        }

        public void ChangePlayerFloorActive(long fplayerid, long fmanageid) {
            int floorCount = m_PlayerFloorList.Count;
            for (int i = 0; i < floorCount; i++) {
                if (m_PlayerFloorList[i].m_PlayerID == fplayerid) {
                    m_PlayerFloorList[i].m_ActiveRoomMngID = fmanageid;
                    break;
                }
            }
        }

        public void ReleasePlayerFloorList(long fplayerid) {
            int floorCount = m_PlayerFloorList.Count;
            for (int i = 0; i < floorCount; i++) {
                if (m_PlayerFloorList[i].m_PlayerID == fplayerid) {
                    m_PlayerFloorList.RemoveAt(i);
                    break;
                }
            }
        }

        public PlayerFloorState GetPlayerRoomList(long fplayerid) {
            int floorCount = m_PlayerFloorList.Count;
            for (int i = 0; i < floorCount; i++) {
                if (m_PlayerFloorList[i].m_PlayerID == fplayerid) {
                    return m_PlayerFloorList[i];
                }
            }
            return null;
        }

        public UserRoomData GetRoomData(long fmanageid) {
            int floorCount = m_PlayerFloorList.Count;
            for (int i = 0; i < floorCount; i++) {
                PlayerFloorState floor = m_PlayerFloorList[i];
                int roomCount = floor.m_RoomList.Count;
                for (int j = 0; j < roomCount; j++) {
                    if (floor.m_RoomList[j].MngID == fmanageid) {
                        return floor.m_RoomList[j];
                    }
                }
            }
            return null;
        }

        public int GetPlayerRoomListNum() {
            return m_PlayerFloorList.Count;
        }

        public PlayerFloorState GetPlayerRoomListAt(int findex) {
            return m_PlayerFloorList[findex];
        }

        public void ClearList() {
            m_PlayerFloorList.Clear();
        }

        public bool IsSetUpActive() {
            return m_PlayerFloorList.Count == 0;
        }

        [Serializable]
        public class PlayerFloorState {
            public long m_PlayerID;
            public long m_ActiveRoomMngID;
            public List<FloorData> m_List;
            public List<UserRoomData> m_RoomList;

            public PlayerFloorState() {
                m_List = new List<FloorData>();
                m_RoomList = new List<UserRoomData>();
            }

            public void CheckFloorData(int fgroupno, UserRoomData proomdata) {
                int floorNum = m_List.Count;
                for (int i = 0; i < floorNum; i++) {
                    if (m_List[i].m_GroupID == fgroupno) {
                        if (!m_List[i].CheckFloorState(proomdata)) {
                            m_RoomList.Add(proomdata);
                        }
                        return;
                    }
                }
                FloorData floorData = new FloorData();
                floorData.m_GroupID = fgroupno;
                floorData.CheckFloorState(proomdata);
                m_List.Add(floorData);
                m_RoomList.Add(proomdata);
            }

            public FloorData GetFloorGroupData(long fmanageid) {
                int floorNum = m_List.Count;
                for (int i = 0; i < floorNum; i++) {
                    int roomNum = m_List[i].m_State.Count;
                    for (int j = 0; j < roomNum; j++) {
                        if (m_List[i].m_State[j].MngID == fmanageid) {
                            return m_List[i];
                        }
                    }
                }
                return null;
            }

            public bool SearchFloorData(int floorid) {
                int floorNum = m_List.Count;
                for (int i = 0; i < floorNum; i++) {
                    int roomNum = m_List[i].m_State.Count;
                    for (int j = 0; j < roomNum; j++) {
                        if (m_List[i].m_State[j].FloorID == floorid) {
                            return true;
                        }
                    }
                }
                return false;
            }

            public int GetRoomNum() {
                return m_RoomList.Count;
            }

            public UserRoomData GetRoomData(int findex) {
                return m_RoomList[findex];
            }

            [Serializable]
            public class FloorData {
                public int m_GroupID;
                public List<UserRoomData> m_State;

                public FloorData() {
                    m_State = new List<UserRoomData>();
                }

                public bool CheckFloorState(UserRoomData proomdata) {
                    int roomNum = m_State.Count;
                    for (int i = 0; i < roomNum; i++) {
                        if (m_State[i].FloorID == proomdata.FloorID && m_State[i].MngID == proomdata.MngID) {
                            m_State[i] = proomdata;
                            return true;
                        }
                    }
                    m_State.Add(proomdata);
                    return false;
                }
            }
        }
    }
}
