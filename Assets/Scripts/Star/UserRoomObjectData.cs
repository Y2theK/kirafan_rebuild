﻿using System.Collections.Generic;

namespace Star {
    public class UserRoomObjectData {
        public int ResourceID;
        public eRoomObjectCategory ObjCategory;
        public int ObjID;
        public int PlacementNum;
        public List<RoomObjMngIDState> m_MngIDList = new List<RoomObjMngIDState>();

        public int RawHaveNum => m_MngIDList.Count;
        public int RemainNum => m_MngIDList.Count - PlacementNum;

        public int HaveNum {
            get {
                int count = m_MngIDList.Count;
                long id = GameSystem.Inst.UserDataMng.UserData.ID;
                UserRoomListData.PlayerFloorState floor = GameSystem.Inst.UserDataMng.UserRoomListData.GetPlayerRoomList(id);
                for (int i = 0; i < floor.GetRoomNum(); i++) {
                    UserRoomData room = floor.GetRoomData(i);
                    for (int j = 0; j < room.GetPlacementNum(); j++) {
                        UserRoomData.PlacementData placement = room.GetPlacementAt(i);
                        if (placement.m_RoomNo != 0) {
                            count -= GetMngIDState(placement.m_ManageID) != null ? 1 : 0;
                        }
                    }
                }
                return count;
            }
        }

        public UserRoomObjectData() { }

        public UserRoomObjectData(eRoomObjectCategory objCategory, int objID) {
            ObjCategory = objCategory;
            ObjID = objID;
            PlacementNum = 0;
        }

        public void ClrToListUpUse() {
            for (int i = 0; i < m_MngIDList.Count; i++) {
                m_MngIDList[i].m_SearchUse = false;
            }
        }

        public void SetListUpToUse(long fmanageid) {
            for (int i = 0; i < m_MngIDList.Count; i++) {
                if (m_MngIDList[i].m_ManageID == fmanageid) {
                    m_MngIDList[i].m_SearchUse = true;
                    break;
                }
            }
        }

        public bool ReleaseListUpNoUse() {
            for (int i = m_MngIDList.Count - 1; i >= 0; i--) {
                if (!m_MngIDList[i].m_SearchUse) {
                    m_MngIDList.RemoveAt(i);
                }
            }
            return m_MngIDList.Count != 0;
        }

        public long GetFreeLinkMngID(bool flock = true) {
            long result = -1L;
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (!m_MngIDList[i].m_LinkUse) {
                    result = m_MngIDList[i].m_ManageID;
                    m_MngIDList[i].m_LinkUse = flock;
                    if (flock) {
                        PlacementNum++;
                    }
                    break;
                }
            }
            return result;
        }

        public void SetFreeLinkMngID(long fmanageid) {
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (m_MngIDList[i].m_ManageID == fmanageid) {
                    m_MngIDList[i].m_LinkUse = false;
                    PlacementNum--;
                    break;
                }
            }
        }

        public void LockLinkMngID(long flockmngid) {
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (m_MngIDList[i].m_ManageID == flockmngid) {
                    if (!m_MngIDList[i].m_LinkUse) {
                        m_MngIDList[i].m_LinkUse = true;
                        PlacementNum++;
                    }
                    break;
                }
            }
        }

        public bool ReleaseLinkMngID(long fmanageid) {
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (m_MngIDList[i].m_ManageID == fmanageid) {
                    m_MngIDList.RemoveAt(i);
                    break;
                }
            }
            return m_MngIDList.Count == 0;
        }

        public bool IsLinkMngID(long fchkmngid) {
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (m_MngIDList[i].m_ManageID == fchkmngid) {
                    return true;
                }
            }
            return false;
        }

        public RoomObjMngIDState GetMngIDState(long manageID) {
            int count = m_MngIDList.Count;
            for (int i = 0; i < count; i++) {
                if (m_MngIDList[i].m_ManageID == manageID) {
                    return m_MngIDList[i];
                }
            }
            return null;
        }

        public void SetServerData(long fmanageid, int resourceid) {
            RoomObjMngIDState roomObjMngIDState = new RoomObjMngIDState();
            roomObjMngIDState.m_ManageID = fmanageid;
            roomObjMngIDState.m_LinkUse = false;
            m_MngIDList.Add(roomObjMngIDState);
            ResourceID = resourceid;
        }

        public class RoomObjMngIDState {
            public long m_ManageID;
            public bool m_LinkUse;
            public bool m_SearchUse;
        }
    }
}
