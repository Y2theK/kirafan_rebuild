﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000734 RID: 1844
	[Token(Token = "0x20005A6")]
	[StructLayout(3)]
	public class UserRoomUtil
	{
		// Token: 0x06001B0A RID: 6922 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018BE")]
		[Address(RVA = "0x1016110D0", Offset = "0x16110D0", VA = "0x1016110D0")]
		public static UserRoomData.PlacementData GetCategoryToPlacementData(UserRoomData pbase, eRoomObjectCategory fcategory)
		{
			return null;
		}

		// Token: 0x06001B0B RID: 6923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018BF")]
		[Address(RVA = "0x101611190", Offset = "0x1611190", VA = "0x101611190")]
		public static void ClearRoomData()
		{
		}

		// Token: 0x06001B0C RID: 6924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018C0")]
		[Address(RVA = "0x101611214", Offset = "0x1611214", VA = "0x101611214")]
		public static void ClearRoomObjData()
		{
		}

		// Token: 0x06001B0D RID: 6925 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C1")]
		[Address(RVA = "0x1016112E8", Offset = "0x16112E8", VA = "0x1016112E8")]
		public static UserRoomData GetManageIDToUserRoomData(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001B0E RID: 6926 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C2")]
		[Address(RVA = "0x101611374", Offset = "0x1611374", VA = "0x101611374")]
		public static PlayerRoomArrangement[] CreateRoomDataString(UserRoomData proom)
		{
			return null;
		}

		// Token: 0x06001B0F RID: 6927 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C3")]
		[Address(RVA = "0x101611614", Offset = "0x1611614", VA = "0x101611614")]
		public static RoomServerForm.RoomList BuildRoomList(RoomServerForm.DataChunk pchunk)
		{
			return null;
		}

		// Token: 0x06001B10 RID: 6928 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C4")]
		[Address(RVA = "0x10160B0B8", Offset = "0x160B0B8", VA = "0x10160B0B8")]
		public static UserRoomObjectData GetManageIDToUserRoomObjData(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001B11 RID: 6929 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C5")]
		[Address(RVA = "0x1016116DC", Offset = "0x16116DC", VA = "0x1016116DC")]
		public static UserRoomObjectData GetAccessIDToUserRoomObjData(int faccesskey)
		{
			return null;
		}

		// Token: 0x06001B12 RID: 6930 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60018C6")]
		[Address(RVA = "0x101611814", Offset = "0x1611814", VA = "0x101611814")]
		public static UserRoomObjectData GetUserRoomObjData(eRoomObjectCategory objCategory, int objID)
		{
			return null;
		}

		// Token: 0x06001B13 RID: 6931 RVA: 0x0000C000 File Offset: 0x0000A200
		[Token(Token = "0x60018C7")]
		[Address(RVA = "0x1016118A0", Offset = "0x16118A0", VA = "0x1016118A0")]
		public static int GetUserRoomObjHaveNum()
		{
			return 0;
		}

		// Token: 0x06001B14 RID: 6932 RVA: 0x0000C018 File Offset: 0x0000A218
		[Token(Token = "0x60018C8")]
		[Address(RVA = "0x101611914", Offset = "0x1611914", VA = "0x101611914")]
		public static int GetUserRoomObjPlacementNum()
		{
			return 0;
		}

		// Token: 0x06001B15 RID: 6933 RVA: 0x0000C030 File Offset: 0x0000A230
		[Token(Token = "0x60018C9")]
		[Address(RVA = "0x101611BD4", Offset = "0x1611BD4", VA = "0x101611BD4")]
		public static int GetUserRoomObjPlacementNum(int floorId)
		{
			return 0;
		}

		// Token: 0x06001B16 RID: 6934 RVA: 0x0000C048 File Offset: 0x0000A248
		[Token(Token = "0x60018CA")]
		[Address(RVA = "0x101611C50", Offset = "0x1611C50", VA = "0x101611C50")]
		public static int GetUserRoomObjRemainNum()
		{
			return 0;
		}

		// Token: 0x06001B17 RID: 6935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018CB")]
		[Address(RVA = "0x101611CC4", Offset = "0x1611CC4", VA = "0x101611CC4")]
		public static void ReleaseCheckRoomData(long fkeycode)
		{
		}

		// Token: 0x06001B18 RID: 6936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018CC")]
		[Address(RVA = "0x101611DCC", Offset = "0x1611DCC", VA = "0x101611DCC")]
		public static void ReleaseUserRoomObjData(long fmanageid)
		{
		}

		// Token: 0x06001B19 RID: 6937 RVA: 0x0000C060 File Offset: 0x0000A260
		[Token(Token = "0x60018CD")]
		[Address(RVA = "0x101611F48", Offset = "0x1611F48", VA = "0x101611F48")]
		public static long GetActiveRoomMngID(long fkeycode)
		{
			return 0L;
		}

		// Token: 0x06001B1A RID: 6938 RVA: 0x0000C078 File Offset: 0x0000A278
		[Token(Token = "0x60018CE")]
		[Address(RVA = "0x101611998", Offset = "0x1611998", VA = "0x101611998")]
		public static int GetActiveRoomFloorId()
		{
			return 0;
		}

		// Token: 0x06001B1B RID: 6939 RVA: 0x0000C090 File Offset: 0x0000A290
		[Token(Token = "0x60018CF")]
		[Address(RVA = "0x101612040", Offset = "0x1612040", VA = "0x101612040")]
		public static bool IsHaveSubRoom(long fplayerid)
		{
			return default(bool);
		}

		// Token: 0x06001B1C RID: 6940 RVA: 0x0000C0A8 File Offset: 0x0000A2A8
		[Token(Token = "0x60018D0")]
		[Address(RVA = "0x101612170", Offset = "0x1612170", VA = "0x101612170")]
		public static bool SetRoomChange(long fplayerid)
		{
			return default(bool);
		}

		// Token: 0x06001B1D RID: 6941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018D1")]
		[Address(RVA = "0x101612348", Offset = "0x1612348", VA = "0x101612348")]
		public UserRoomUtil()
		{
		}
	}
}
