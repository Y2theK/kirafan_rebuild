﻿namespace Star {
    public class UserScheduleData {
        public enum eType {
            Non,
            Sleep,
            Room,
            Office,
            Town,
            System,
            End
        }

        public enum eDropCategory {
            Money,
            Item,
            KRRPoint,
            Stamina,
            FriendShip
        }

        public class DropState {
            public eDropCategory m_Category;
            public int m_CategoryNo;
            public int m_Num;
        }

        public class Seg {
            public eType m_Type;
            public int m_WakeTime;
            public int m_UseTime;
            public int m_TagNameID;
        }

        public class ListPack {
            public Seg[] m_Table;
            public long m_SettingTime;
            public long m_ChangeStateTime;

            public int GetListNum() {
                return m_Table.Length;
            }

            public Seg GetTable(int findex) {
                return m_Table[findex];
            }
        }

        public class Play {
            public ListPack[] m_DayListUp;
            public long m_PlayTime;
            public int m_Version;
            public int m_DropItemID;
            public float m_DropMagKey;

            public Play() {
                m_DropItemID = -1;
                m_DropMagKey = 0f;
            }

            public void StackList(ListPack plist) {
                if (m_DayListUp == null) {
                    m_DayListUp = new ListPack[2];
                }
                m_DayListUp[1] = m_DayListUp[0];
                m_DayListUp[0] = plist;
            }
        }
    }
}
