﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000737 RID: 1847
	[Token(Token = "0x20005A8")]
	[StructLayout(3)]
	public class UserScheduleUtil
	{
		// Token: 0x06001B2E RID: 6958 RVA: 0x0000C168 File Offset: 0x0000A368
		[Token(Token = "0x60018E1")]
		[Address(RVA = "0x101612368", Offset = "0x1612368", VA = "0x101612368")]
		public static int MoveToScheduleStateUp(UserScheduleData.ListPack pschedule, eTownMoveState fstate, int fpoint, long fmovemngid)
		{
			return 0;
		}

		// Token: 0x06001B2F RID: 6959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018E2")]
		[Address(RVA = "0x10160E13C", Offset = "0x160E13C", VA = "0x10160E13C")]
		public static void TimeToScheduleStateUp(UserScheduleData.ListPack pschedule, eTownMoveState fstate, long fsec)
		{
		}

		// Token: 0x06001B30 RID: 6960 RVA: 0x0000C180 File Offset: 0x0000A380
		[Token(Token = "0x60018E3")]
		[Address(RVA = "0x10161261C", Offset = "0x161261C", VA = "0x10161261C")]
		public static bool CheckTagToBuilding(UserScheduleData.ListPack pschedule, int fpoint)
		{
			return default(bool);
		}

		// Token: 0x06001B31 RID: 6961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018E4")]
		[Address(RVA = "0x101612A2C", Offset = "0x1612A2C", VA = "0x101612A2C")]
		public UserScheduleUtil()
		{
		}
	}
}
