﻿using System;

namespace Star {
    public class UserStamina {
        private long m_Value;
        private long m_ValueMax;
        private float m_RemainSec;
        private float m_RemainSecMax;

        public long GetValue() {
            return m_Value;
        }

        public void SetValue(long value, bool isMaxLimit = false) {
            m_Value = value;
            if (isMaxLimit && m_Value > m_ValueMax) {
                m_Value = m_ValueMax;
            }
        }

        public long GetValueMax() {
            return m_ValueMax;
        }

        public void SetValueMax(long valueMax) {
            m_ValueMax = valueMax;
        }

        public float GetRemainSec() {
            return m_RemainSec;
        }

        public void SetRemainSec(float remainSec) {
            m_RemainSec = remainSec;
        }

        public void SetRemainSecMax(float remainSecMax) {
            m_RemainSecMax = remainSecMax;
        }

        public void UpdateStamina(float deltaSec) {
            if (m_Value >= m_ValueMax) {
                m_RemainSec = 0f;
                return;
            }
            m_RemainSec -= deltaSec;
            if (m_RemainSec <= 0f) {
                m_RemainSec = m_RemainSecMax + m_RemainSec;
                m_Value += 1L;
            }
        }

        public bool IsFull() {
            return m_Value >= m_ValueMax;
        }

        public TimeSpan GetTimeToCompleteRecoveryForPushNotification() {
            long diff = m_ValueMax - m_Value;
            int seconds = 0;
            if (diff > 0L) {
                seconds = (int)((diff - 1) * m_RemainSecMax + m_RemainSec);
            }
            return new TimeSpan(0, 0, seconds);
        }
    }
}
