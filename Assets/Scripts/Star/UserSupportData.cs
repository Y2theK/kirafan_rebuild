﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000C24 RID: 3108
	[Token(Token = "0x2000854")]
	[StructLayout(3)]
	public class UserSupportData
	{
		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x0600375E RID: 14174 RVA: 0x000176E8 File Offset: 0x000158E8
		// (set) Token: 0x0600375F RID: 14175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000407")]
		public long MngID
		{
			[Token(Token = "0x6003263")]
			[Address(RVA = "0x101612AEC", Offset = "0x1612AEC", VA = "0x101612AEC")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6003264")]
			[Address(RVA = "0x101612AF4", Offset = "0x1612AF4", VA = "0x101612AF4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06003760 RID: 14176 RVA: 0x00017700 File Offset: 0x00015900
		// (set) Token: 0x06003761 RID: 14177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000408")]
		public int CharaID
		{
			[Token(Token = "0x6003265")]
			[Address(RVA = "0x101612AFC", Offset = "0x1612AFC", VA = "0x101612AFC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003266")]
			[Address(RVA = "0x101612B04", Offset = "0x1612B04", VA = "0x101612B04")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06003762 RID: 14178 RVA: 0x00017718 File Offset: 0x00015918
		// (set) Token: 0x06003763 RID: 14179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000409")]
		public int Lv
		{
			[Token(Token = "0x6003267")]
			[Address(RVA = "0x101612B0C", Offset = "0x1612B0C", VA = "0x101612B0C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003268")]
			[Address(RVA = "0x101612B14", Offset = "0x1612B14", VA = "0x101612B14")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06003764 RID: 14180 RVA: 0x00017730 File Offset: 0x00015930
		// (set) Token: 0x06003765 RID: 14181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040A")]
		public int MaxLv
		{
			[Token(Token = "0x6003269")]
			[Address(RVA = "0x101612B1C", Offset = "0x1612B1C", VA = "0x101612B1C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600326A")]
			[Address(RVA = "0x101612B24", Offset = "0x1612B24", VA = "0x101612B24")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06003766 RID: 14182 RVA: 0x00017748 File Offset: 0x00015948
		// (set) Token: 0x06003767 RID: 14183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040B")]
		public long Exp
		{
			[Token(Token = "0x600326B")]
			[Address(RVA = "0x101612B2C", Offset = "0x1612B2C", VA = "0x101612B2C")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x600326C")]
			[Address(RVA = "0x101612B34", Offset = "0x1612B34", VA = "0x101612B34")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06003768 RID: 14184 RVA: 0x00017760 File Offset: 0x00015960
		// (set) Token: 0x06003769 RID: 14185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040C")]
		public int LimitBreak
		{
			[Token(Token = "0x600326D")]
			[Address(RVA = "0x101612B3C", Offset = "0x1612B3C", VA = "0x101612B3C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600326E")]
			[Address(RVA = "0x101612B44", Offset = "0x1612B44", VA = "0x101612B44")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x0600376A RID: 14186 RVA: 0x00017778 File Offset: 0x00015978
		// (set) Token: 0x0600376B RID: 14187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040D")]
		public int ArousalLv
		{
			[Token(Token = "0x600326F")]
			[Address(RVA = "0x101612B4C", Offset = "0x1612B4C", VA = "0x101612B4C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003270")]
			[Address(RVA = "0x101612B54", Offset = "0x1612B54", VA = "0x101612B54")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x0600376C RID: 14188 RVA: 0x00017790 File Offset: 0x00015990
		// (set) Token: 0x0600376D RID: 14189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040E")]
		public int DuplicatedCount
		{
			[Token(Token = "0x6003271")]
			[Address(RVA = "0x101612B5C", Offset = "0x1612B5C", VA = "0x101612B5C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003272")]
			[Address(RVA = "0x101612B64", Offset = "0x1612B64", VA = "0x101612B64")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x0600376E RID: 14190 RVA: 0x000177A8 File Offset: 0x000159A8
		// (set) Token: 0x0600376F RID: 14191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700040F")]
		public int UniqueSkillLv
		{
			[Token(Token = "0x6003273")]
			[Address(RVA = "0x101612B6C", Offset = "0x1612B6C", VA = "0x101612B6C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003274")]
			[Address(RVA = "0x101612B74", Offset = "0x1612B74", VA = "0x101612B74")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x06003770 RID: 14192 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06003771 RID: 14193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000410")]
		public int[] ClassSkillLvs
		{
			[Token(Token = "0x6003275")]
			[Address(RVA = "0x101612B7C", Offset = "0x1612B7C", VA = "0x101612B7C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6003276")]
			[Address(RVA = "0x101612B84", Offset = "0x1612B84", VA = "0x101612B84")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06003772 RID: 14194 RVA: 0x000177C0 File Offset: 0x000159C0
		// (set) Token: 0x06003773 RID: 14195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000411")]
		public int Cost
		{
			[Token(Token = "0x6003277")]
			[Address(RVA = "0x101612B8C", Offset = "0x1612B8C", VA = "0x101612B8C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003278")]
			[Address(RVA = "0x101612B94", Offset = "0x1612B94", VA = "0x101612B94")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06003774 RID: 14196 RVA: 0x000177D8 File Offset: 0x000159D8
		// (set) Token: 0x06003775 RID: 14197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000412")]
		public int FriendShip
		{
			[Token(Token = "0x6003279")]
			[Address(RVA = "0x101612B9C", Offset = "0x1612B9C", VA = "0x101612B9C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600327A")]
			[Address(RVA = "0x101612BA4", Offset = "0x1612BA4", VA = "0x101612BA4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x06003776 RID: 14198 RVA: 0x000177F0 File Offset: 0x000159F0
		// (set) Token: 0x06003777 RID: 14199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000413")]
		public long FriendShipExp
		{
			[Token(Token = "0x600327B")]
			[Address(RVA = "0x101612BAC", Offset = "0x1612BAC", VA = "0x101612BAC")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x600327C")]
			[Address(RVA = "0x101612BB4", Offset = "0x1612BB4", VA = "0x101612BB4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x06003778 RID: 14200 RVA: 0x00017808 File Offset: 0x00015A08
		// (set) Token: 0x06003779 RID: 14201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000414")]
		public int WeaponID
		{
			[Token(Token = "0x600327D")]
			[Address(RVA = "0x101612BBC", Offset = "0x1612BBC", VA = "0x101612BBC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600327E")]
			[Address(RVA = "0x101612BC4", Offset = "0x1612BC4", VA = "0x101612BC4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x0600377A RID: 14202 RVA: 0x00017820 File Offset: 0x00015A20
		// (set) Token: 0x0600377B RID: 14203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000415")]
		public int WeaponLv
		{
			[Token(Token = "0x600327F")]
			[Address(RVA = "0x101612BCC", Offset = "0x1612BCC", VA = "0x101612BCC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003280")]
			[Address(RVA = "0x101612BD4", Offset = "0x1612BD4", VA = "0x101612BD4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x0600377C RID: 14204 RVA: 0x00017838 File Offset: 0x00015A38
		// (set) Token: 0x0600377D RID: 14205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000416")]
		public int WeaponSkillLv
		{
			[Token(Token = "0x6003281")]
			[Address(RVA = "0x101612BDC", Offset = "0x1612BDC", VA = "0x101612BDC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003282")]
			[Address(RVA = "0x101612BE4", Offset = "0x1612BE4", VA = "0x101612BE4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x0600377E RID: 14206 RVA: 0x00017850 File Offset: 0x00015A50
		// (set) Token: 0x0600377F RID: 14207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000417")]
		public long WeaponSkillExp
		{
			[Token(Token = "0x6003283")]
			[Address(RVA = "0x101612BEC", Offset = "0x1612BEC", VA = "0x101612BEC")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6003284")]
			[Address(RVA = "0x101612BF4", Offset = "0x1612BF4", VA = "0x101612BF4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06003780 RID: 14208 RVA: 0x00017868 File Offset: 0x00015A68
		// (set) Token: 0x06003781 RID: 14209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000418")]
		public int AbilityBoardID
		{
			[Token(Token = "0x6003285")]
			[Address(RVA = "0x101612BFC", Offset = "0x1612BFC", VA = "0x101612BFC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6003286")]
			[Address(RVA = "0x101612C04", Offset = "0x1612C04", VA = "0x101612C04")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06003782 RID: 14210 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06003783 RID: 14211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000419")]
		public int[] AbilityEquipItemIDs
		{
			[Token(Token = "0x6003287")]
			[Address(RVA = "0x101612C0C", Offset = "0x1612C0C", VA = "0x101612C0C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6003288")]
			[Address(RVA = "0x101612C14", Offset = "0x1612C14", VA = "0x101612C14")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06003784 RID: 14212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003289")]
		[Address(RVA = "0x101612C1C", Offset = "0x1612C1C", VA = "0x101612C1C")]
		public UserSupportData()
		{
		}
	}
}
