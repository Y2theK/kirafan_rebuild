﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000626 RID: 1574
	[Token(Token = "0x2000519")]
	[StructLayout(3)]
	public class UserSupportDataWrapper : CharacterDataWrapperBaseExGen<UserSupportData>
	{
		// Token: 0x06001646 RID: 5702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014F6")]
		[Address(RVA = "0x101612C54", Offset = "0x1612C54", VA = "0x101612C54")]
		public UserSupportDataWrapper(UserSupportData in_data)
		{
		}

		// Token: 0x06001647 RID: 5703 RVA: 0x00009DC8 File Offset: 0x00007FC8
		[Token(Token = "0x60014F7")]
		[Address(RVA = "0x101612EF8", Offset = "0x1612EF8", VA = "0x101612EF8", Slot = "4")]
		public override long GetMngId()
		{
			return 0L;
		}

		// Token: 0x06001648 RID: 5704 RVA: 0x00009DE0 File Offset: 0x00007FE0
		[Token(Token = "0x60014F8")]
		[Address(RVA = "0x101612F24", Offset = "0x1612F24", VA = "0x101612F24", Slot = "5")]
		public override int GetCharaId()
		{
			return 0;
		}

		// Token: 0x06001649 RID: 5705 RVA: 0x00009DF8 File Offset: 0x00007FF8
		[Token(Token = "0x60014F9")]
		[Address(RVA = "0x101612F50", Offset = "0x1612F50", VA = "0x101612F50", Slot = "6")]
		public override eEventQuestDropExtPlusType GetCharaEventQuestDropExtPlusType()
		{
			return eEventQuestDropExtPlusType.Self;
		}

		// Token: 0x0600164A RID: 5706 RVA: 0x00009E10 File Offset: 0x00008010
		[Token(Token = "0x60014FA")]
		[Address(RVA = "0x101612FC8", Offset = "0x1612FC8", VA = "0x101612FC8", Slot = "7")]
		public override eCharaNamedType GetCharaNamedType()
		{
			return eCharaNamedType.Named_0000;
		}

		// Token: 0x0600164B RID: 5707 RVA: 0x00009E28 File Offset: 0x00008028
		[Token(Token = "0x60014FB")]
		[Address(RVA = "0x101612FD0", Offset = "0x1612FD0", VA = "0x101612FD0", Slot = "8")]
		public override eClassType GetClassType()
		{
			return eClassType.Fighter;
		}

		// Token: 0x0600164C RID: 5708 RVA: 0x00009E40 File Offset: 0x00008040
		[Token(Token = "0x60014FC")]
		[Address(RVA = "0x101612FD8", Offset = "0x1612FD8", VA = "0x101612FD8", Slot = "9")]
		public override eElementType GetElementType()
		{
			return eElementType.Fire;
		}

		// Token: 0x0600164D RID: 5709 RVA: 0x00009E58 File Offset: 0x00008058
		[Token(Token = "0x60014FD")]
		[Address(RVA = "0x101612FE0", Offset = "0x1612FE0", VA = "0x101612FE0", Slot = "10")]
		public override eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x0600164E RID: 5710 RVA: 0x00009E70 File Offset: 0x00008070
		[Token(Token = "0x60014FE")]
		[Address(RVA = "0x101612FE8", Offset = "0x1612FE8", VA = "0x101612FE8", Slot = "11")]
		public override int GetCost()
		{
			return 0;
		}

		// Token: 0x0600164F RID: 5711 RVA: 0x00009E88 File Offset: 0x00008088
		[Token(Token = "0x60014FF")]
		[Address(RVA = "0x10161303C", Offset = "0x161303C", VA = "0x10161303C", Slot = "12")]
		public override int GetLv()
		{
			return 0;
		}

		// Token: 0x06001650 RID: 5712 RVA: 0x00009EA0 File Offset: 0x000080A0
		[Token(Token = "0x6001500")]
		[Address(RVA = "0x101613068", Offset = "0x1613068", VA = "0x101613068", Slot = "13")]
		public override int GetMaxLv()
		{
			return 0;
		}

		// Token: 0x06001651 RID: 5713 RVA: 0x00009EB8 File Offset: 0x000080B8
		[Token(Token = "0x6001501")]
		[Address(RVA = "0x101613094", Offset = "0x1613094", VA = "0x101613094", Slot = "14")]
		public override long GetExp()
		{
			return 0L;
		}

		// Token: 0x06001652 RID: 5714 RVA: 0x00009ED0 File Offset: 0x000080D0
		[Token(Token = "0x6001502")]
		[Address(RVA = "0x1016130C0", Offset = "0x16130C0", VA = "0x1016130C0", Slot = "15")]
		public override int GetLimitBreak()
		{
			return 0;
		}

		// Token: 0x06001653 RID: 5715 RVA: 0x00009EE8 File Offset: 0x000080E8
		[Token(Token = "0x6001503")]
		[Address(RVA = "0x1016130EC", Offset = "0x16130EC", VA = "0x1016130EC", Slot = "16")]
		public override int GetArousal()
		{
			return 0;
		}

		// Token: 0x06001654 RID: 5716 RVA: 0x00009F00 File Offset: 0x00008100
		[Token(Token = "0x6001504")]
		[Address(RVA = "0x101613118", Offset = "0x1613118", VA = "0x101613118")]
		public int GetWeaponId()
		{
			return 0;
		}

		// Token: 0x06001655 RID: 5717 RVA: 0x00009F18 File Offset: 0x00008118
		[Token(Token = "0x6001505")]
		[Address(RVA = "0x101613170", Offset = "0x1613170", VA = "0x101613170")]
		public int GetWeaponIdNaked()
		{
			return 0;
		}

		// Token: 0x06001656 RID: 5718 RVA: 0x00009F30 File Offset: 0x00008130
		[Token(Token = "0x6001506")]
		[Address(RVA = "0x10161319C", Offset = "0x161319C", VA = "0x10161319C")]
		public int GetWeaponLv()
		{
			return 0;
		}

		// Token: 0x06001657 RID: 5719 RVA: 0x00009F48 File Offset: 0x00008148
		[Token(Token = "0x6001507")]
		[Address(RVA = "0x1016131EC", Offset = "0x16131EC", VA = "0x1016131EC")]
		public int GetWeaponMaxLv()
		{
			return 0;
		}

		// Token: 0x06001658 RID: 5720 RVA: 0x00009F60 File Offset: 0x00008160
		[Token(Token = "0x6001508")]
		[Address(RVA = "0x1016132B8", Offset = "0x16132B8", VA = "0x1016132B8")]
		public long GetWeaponExp()
		{
			return 0L;
		}

		// Token: 0x06001659 RID: 5721 RVA: 0x00009F78 File Offset: 0x00008178
		[Token(Token = "0x6001509")]
		[Address(RVA = "0x101613210", Offset = "0x1613210", VA = "0x101613210")]
		public WeaponListDB_Param GetWeaponListDBParam()
		{
			return default(WeaponListDB_Param);
		}

		// Token: 0x0600165A RID: 5722 RVA: 0x00009F90 File Offset: 0x00008190
		[Token(Token = "0x600150A")]
		[Address(RVA = "0x1016132C0", Offset = "0x16132C0", VA = "0x1016132C0")]
		public int GetWeaponSkillID()
		{
			return 0;
		}

		// Token: 0x0600165B RID: 5723 RVA: 0x00009FA8 File Offset: 0x000081A8
		[Token(Token = "0x600150B")]
		[Address(RVA = "0x1016132E4", Offset = "0x16132E4", VA = "0x1016132E4")]
		public sbyte GetWeaponSkillExpTableID()
		{
			return 0;
		}

		// Token: 0x0600165C RID: 5724 RVA: 0x00009FC0 File Offset: 0x000081C0
		[Token(Token = "0x600150C")]
		[Address(RVA = "0x101613308", Offset = "0x1613308", VA = "0x101613308")]
		public int GetWeaponSkillLv()
		{
			return 0;
		}

		// Token: 0x0600165D RID: 5725 RVA: 0x00009FD8 File Offset: 0x000081D8
		[Token(Token = "0x600150D")]
		[Address(RVA = "0x101613334", Offset = "0x1613334", VA = "0x101613334")]
		public int GetWeaponSkillMaxLv()
		{
			return 0;
		}

		// Token: 0x0600165E RID: 5726 RVA: 0x00009FF0 File Offset: 0x000081F0
		[Token(Token = "0x600150E")]
		[Address(RVA = "0x101613358", Offset = "0x1613358", VA = "0x101613358")]
		public int GetWeaponPassiveSkillID()
		{
			return 0;
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x0000A008 File Offset: 0x00008208
		[Token(Token = "0x600150F")]
		[Address(RVA = "0x10161337C", Offset = "0x161337C", VA = "0x10161337C", Slot = "17")]
		public override int GetFriendship()
		{
			return 0;
		}

		// Token: 0x06001660 RID: 5728 RVA: 0x0000A020 File Offset: 0x00008220
		[Token(Token = "0x6001510")]
		[Address(RVA = "0x1016133A8", Offset = "0x16133A8", VA = "0x1016133A8", Slot = "18")]
		public override int GetAroualLevel()
		{
			return 0;
		}

		// Token: 0x06001661 RID: 5729 RVA: 0x0000A038 File Offset: 0x00008238
		[Token(Token = "0x6001511")]
		[Address(RVA = "0x1016133D4", Offset = "0x16133D4", VA = "0x1016133D4", Slot = "19")]
		public override int GetDuplicatedCount()
		{
			return 0;
		}

		// Token: 0x04002626 RID: 9766
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001F90")]
		protected CharacterListDB_Param cldbp;

		// Token: 0x04002627 RID: 9767
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4001F91")]
		protected SkillLearnData usld;

		// Token: 0x04002628 RID: 9768
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4001F92")]
		protected List<SkillLearnData> cslds;
	}
}
