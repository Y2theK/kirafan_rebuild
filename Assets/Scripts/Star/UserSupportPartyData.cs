﻿using System.Collections.Generic;

namespace Star {
    public class UserSupportPartyData {
        public long MngID { get; set; }
        public string PartyName { get; set; }
        public List<long> CharaMngIDs { get; set; }
        public List<long> WeaponMngIDs { get; set; }
        public bool IsActive { get; set; }

        public UserSupportPartyData() {
            MngID = -1L;
            CharaMngIDs = new List<long>();
            WeaponMngIDs = new List<long>();
        }

        public int GetSlotNum() {
            return CharaMngIDs.Count;
        }

        public long GetMemberAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Count) {
                return CharaMngIDs[slotIndex];
            }
            return -1L;
        }

        public void SetMemberAt(int slotIndex, long charaMngID) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Count) {
                CharaMngIDs[slotIndex] = charaMngID;
            }
        }

        public void SetMembers(long[] charaMngIDs) {
            CharaMngIDs = new List<long>(charaMngIDs);
        }

        public void EmptyMemberAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < CharaMngIDs.Count) {
                CharaMngIDs[slotIndex] = -1L;
            }
        }

        public void EmptyMember(long charaMngID) {
            for (int i = 0; i < CharaMngIDs.Count; i++) {
                if (CharaMngIDs[i] == charaMngID) {
                    CharaMngIDs[i] = -1L;
                    break;
                }
            }
        }

        public long GetWeaponAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Count) {
                return WeaponMngIDs[slotIndex];
            }
            return -1L;
        }

        public void SetWeaponAt(int slotIndex, long weaponMngID) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Count) {
                WeaponMngIDs[slotIndex] = weaponMngID;
            }
        }

        public void SetWeapons(long[] weaponMngIDs) {
            WeaponMngIDs = new List<long>(weaponMngIDs);
        }

        public void EmptyWeaponAt(int slotIndex) {
            if (slotIndex >= 0 && slotIndex < WeaponMngIDs.Count) {
                WeaponMngIDs[slotIndex] = -1L;
            }
        }

        public void EmptyWeapon(long weaponMngID) {
            for (int i = 0; i < WeaponMngIDs.Count; i++) {
                if (WeaponMngIDs[i] == weaponMngID) {
                    WeaponMngIDs[i] = -1L;
                    break;
                }
            }
        }
    }
}
