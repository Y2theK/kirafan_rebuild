﻿using System.Collections.Generic;

namespace Star {
    public class UserTownData {
        public const int Version = 1;

        public int Level;
        public long DebugMngID;

        private List<BuildObjectData> m_BuildObjectDatas = new List<BuildObjectData>();
        private Dictionary<int, int> m_StoreDatas = new Dictionary<int, int>();

        public long MngID { get; set; }
        public long DataUp { get; set; }
        public bool NewBuild { get; set; }

        public UserTownData() {
            MngID = -1L;
            DebugMngID = -1L;
        }

        public List<BuildObjectUp> CreateBuildTable() {
            List<BuildObjectUp> list = new List<BuildObjectUp>();
            int dataNum = m_BuildObjectDatas.Count;
            for (int i = 0; i < dataNum; i++) {
                BuildObjectUp buildObjectUp = new BuildObjectUp();
                buildObjectUp.CopyData(m_BuildObjectDatas[i]);
                list.Add(buildObjectUp);
            }
            return list;
        }

        public bool IsSetUpConnect() {
            return MngID != -1L;
        }

        public void UpManageID(long fmanageid) {
            MngID = fmanageid;
        }

        public UserTownData GetManageIDToData(long fmanageid) {
            return MngID == fmanageid ? this : null;
        }

        public void ClearData(long fmanageid, long ftimes) {
            if (DataUp != ftimes || MngID != fmanageid || fmanageid == -1L) {
                int dataNum = m_BuildObjectDatas.Count;
                for (int i = dataNum - 1; i >= 0; i--) {
                    m_BuildObjectDatas.RemoveAt(i);
                }
            }
            MngID = fmanageid;
            DataUp = ftimes;
        }

        public void AddBuildObjectData(BuildObjectData buildObject) {
            m_BuildObjectDatas.Add(buildObject);
            NewBuild = true;
        }

        public void RemoveBuildObjectDataAtBuildPointIndex(int buildPointIndex) {
            for (int i = 0; i < m_BuildObjectDatas.Count; i++) {
                if (m_BuildObjectDatas[i].m_BuildPointIndex == buildPointIndex) {
                    m_BuildObjectDatas[i].ChangeBuildPoint(-1, false);
                    m_BuildObjectDatas.RemoveAt(i);
                    break;
                }
            }
        }

        public void RemoveBuildObjectDataAtIndex(int index) {
            if (index < m_BuildObjectDatas.Count) {
                m_BuildObjectDatas[index].ChangeBuildPoint(-1, false);
                m_BuildObjectDatas.RemoveAt(index);
            }
        }

        public int GetBuildObjectDataNum() {
            return m_BuildObjectDatas.Count;
        }

        public BuildObjectData GetBuildObjectDataAt(int index) {
            return m_BuildObjectDatas[index];
        }

        public BuildObjectData GetBuildObjectDataAtBuildPointIndex(int buildPointIndex) {
            for (int i = 0; i < m_BuildObjectDatas.Count; i++) {
                if (m_BuildObjectDatas[i].m_BuildPointIndex == buildPointIndex) {
                    return m_BuildObjectDatas[i];
                }
            }
            return null;
        }

        public BuildObjectData GetBuildObjectDataAtBuildMngID(long buildmngid) {
            for (int i = 0; i < m_BuildObjectDatas.Count; i++) {
                if (m_BuildObjectDatas[i].m_ManageID == buildmngid) {
                    return m_BuildObjectDatas[i];
                }
            }
            return null;
        }

        public int GetBuildNumByCategory(eTownObjectCategory category) {
            int num = 0;
            for (int i = 0; i < GetBuildObjectDataNum(); i++) {
                BuildObjectData data = GetBuildObjectDataAt(i);
                int id = TownUtility.CalcAreaToContentID(data.m_ObjID);
                if (GameSystem.Inst.DbMng.TownObjListDB.GetParam(id).m_Category == (int)category) {
                    num++;
                }
            }
            return num;
        }

        public void AddStoreDataNum(int objID, int addNum) {
            if (m_StoreDatas.ContainsKey(objID)) {
                m_StoreDatas[objID] += addNum;
                if (m_StoreDatas[objID] <= 0) {
                    m_StoreDatas.Remove(objID);
                }
            } else if (addNum > 0) {
                m_StoreDatas.Add(objID, addNum);
            }
        }

        public int GetStoreDataNum(int objID) {
            m_StoreDatas.TryGetValue(objID, out int value);
            return value;
        }

        public Dictionary<int, int> GetStoreDatas() {
            return m_StoreDatas;
        }

        public class BuildObjectUp {
            public long m_ManageID;
            public int m_BuildPointIndex;
            public int m_ObjID;
            public int m_Lv;
            public bool m_IsOpen;
            public long m_BuildingMs;
            public long m_ActionTime;

            public void CopyData(BuildObjectData pbase) {
                m_ManageID = pbase.m_ManageID;
                m_BuildPointIndex = pbase.m_BuildPointIndex;
                m_ObjID = pbase.m_ObjID;
                m_Lv = pbase.m_Lv;
                m_IsOpen = pbase.m_IsOpen;
                m_ActionTime = pbase.m_ActionTime;
                m_BuildingMs = pbase.m_BuildingMs;
            }
        }

        public class BuildObjectData {
            public long m_ManageID;
            public int m_BuildPointIndex;
            public int m_ObjID;
            public int m_Lv;
            public bool m_IsOpen;
            public long m_BuildingMs;
            public long m_ActionTime;
            public UserTownObjectData m_Object;

            public BuildObjectData(int buildPointIndex, int objID, bool isOpen, long buildms, long MngID, long actms) {
                m_BuildPointIndex = buildPointIndex;
                m_ObjID = objID;
                m_IsOpen = isOpen;
                m_BuildingMs = buildms;
                m_ManageID = MngID;
                m_Lv = 0;
                m_ActionTime = buildms > actms ? buildms : actms;
            }

            public void ChangeStoreData(UserTownObjectData pstoreobj, long fmarktime) {
                m_Lv = pstoreobj.Lv;
                m_IsOpen = true;
                m_ManageID = pstoreobj.m_ManageID;
                m_ObjID = pstoreobj.ObjID;
                m_ActionTime = fmarktime;
            }

            public void SetStoreState() {
                if (m_Object != null) {
                    m_Object.OpenState = UserTownObjectData.eOpenState.Non;
                }
            }

            public bool IsNextLevelUpObj() {
                if (m_Object == null) {
                    return true;
                }
                return m_Object.OpenState == UserTownObjectData.eOpenState.OnWait || m_Object.OpenState == UserTownObjectData.eOpenState.Wait;
            }

            public void ChangeBuildPoint(int fnextpoint, bool fchecks = false) {
                m_BuildPointIndex = fnextpoint;
                if (m_Object != null) {
                    m_Object.BuildPoint = fnextpoint;
                    m_Object.m_ChangeState = fchecks;
                }
            }

            public bool IsCheckStateChenge() {
                return m_Object != null && m_Object.m_ChangeState;
            }

            public void ClearChangeState() {
                if (m_Object != null) {
                    m_Object.m_ChangeState = false;
                }
            }

            public void SetActionTime(long fsettime) {
                m_ActionTime = fsettime;
                if (m_Object != null) {
                    m_Object.ActionTime = fsettime;
                }
            }

            public bool isLimitLevelUp() {
                return m_Lv >= GameSystem.Inst.DbMng.TownObjListDB.GetParam(m_ObjID).m_MaxLevel;
            }

            public bool AddLevel(int addLevel) {
                int newLv = m_Lv + addLevel;
                if (newLv > GameSystem.Inst.DbMng.TownObjListDB.GetParam(m_ObjID).m_MaxLevel) {
                    return false;
                }
                m_Lv = newLv;
                if (m_Object != null) {
                    m_Object.Lv = newLv;
                }
                return true;
            }

            public int AddChkLevel(int addLevel) {
                if (m_Object != null) {
                    return m_Object.AddChkLevel(addLevel);
                }
                int newLv = m_Lv + addLevel;
                if (newLv > GameSystem.Inst.DbMng.TownObjListDB.GetParam(m_ObjID).m_MaxLevel) {
                    return m_Lv;
                }
                return newLv;
            }

            public void ChangeLevelUp(long fmarktime, bool fopen) {
                m_IsOpen = fopen;
                m_BuildingMs = fmarktime;
                if (m_ActionTime < fmarktime) {
                    m_ActionTime = fmarktime;
                }
            }
        }
    }
}
