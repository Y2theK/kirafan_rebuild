﻿using WWWTypes;

namespace Star {
    public class UserTownObjectData {
        private static bool ms_SetUp;

        public int ObjID;
        public int Lv;
        public int GroupID;
        public long m_ManageID;
        public long BuildTime;
        public long ActionTime;
        public int BuildPoint;
        public eOpenState OpenState;
        public bool m_ChangeState;

        public UserTownObjectData() {
            ObjID = -1;
            m_ManageID = -1L;
            Lv = 0;
            BuildTime = 0L;
            OpenState = eOpenState.Non;
        }

        public UserTownObjectData(int objID, long fmngid) {
            ObjID = objID;
            m_ManageID = fmngid;
            Lv = 0;
            BuildTime = 0L;
            OpenState = eOpenState.Non;
        }

        public UserTownObjectData(ref PlayerTownFacility pparam) {
            ObjID = pparam.facilityId;
            m_ManageID = pparam.managedTownFacilityId;
            Lv = pparam.level;
            BuildTime = pparam.buildTime;
            ActionTime = pparam.actionTime;
            OpenState = (eOpenState)pparam.openState;
            if (BuildTime == 0L && ActionTime == 0L) {
                OpenState = eOpenState.Open;
            }
            if ((OpenState == eOpenState.OnWait || OpenState == eOpenState.Wait) && Lv != 0) {
                Lv--;
            }
            BuildPoint = pparam.buildPointIndex;
        }

        public static void SetConnect(bool fsetup) {
            ms_SetUp = fsetup;
        }

        public static bool IsSetUpConnect() {
            return ms_SetUp;
        }

        public int AddChkLevel(int addLevel) {
            int newLv = Lv + addLevel;
            if (newLv > GameSystem.Inst.DbMng.TownObjListDB.GetParam(ObjID).m_MaxLevel) {
                return Lv;
            }
            return newLv;
        }

        public enum eOpenState {
            Non,
            Open,
            Wait,
            PreWait,
            OnWait
        }
    }
}
