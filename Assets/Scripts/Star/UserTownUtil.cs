﻿using CommonLogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Star {
    public class UserTownUtil {
        public static UserTownObjectData GetTownObjData(long fmanageid) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].m_ManageID == fmanageid) {
                    return townObjs[i];
                }
            }
            return null;
        }

        public static void LinkTownBuildToObjectData(UserTownData.BuildObjectData pbuild, bool fdebugup = false) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].m_ManageID == pbuild.m_ManageID) {
                    pbuild.m_Object = townObjs[i];
                    pbuild.m_Lv = townObjs[i].Lv;
                    townObjs[i].BuildTime = pbuild.m_BuildingMs;
                    townObjs[i].ActionTime = pbuild.m_ActionTime;
                    pbuild.m_IsOpen = townObjs[i].OpenState == UserTownObjectData.eOpenState.Open;
                    break;
                }
            }
        }

        public static UserTownData.BuildObjectData CreateUserTownBuildData(long fmanageid, int fbuildpoint, int fobjid, bool fopen, long ftime = 0L, long facttime = 0L) {
            if (ftime == 0L) {
                ftime = ScheduleTimeUtil.GetSystemTimeMs();
            }
            UserTownData.BuildObjectData build = new UserTownData.BuildObjectData(fbuildpoint, fobjid, fopen, ftime, fmanageid, facttime);
            LinkTownBuildToObjectData(build, false);
            return build;
        }

        public static UserTownData.BuildObjectData GetTownBuildData(long fmanageid) {
            return GameSystem.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(fmanageid);
        }

        public static bool CalcTownBuildToDropItemState(ref DropItemInfo pstate, long fmanageid) {
            UserTownData.BuildObjectData build = GameSystem.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(fmanageid);
            if (build != null) {
                DropItemTool drop = TownItemDropDatabase.CalcDropPeformsnce(ref pstate, GameSystem.Inst.DbMng.TownObjListDB.GetParam(build.m_ObjID).m_LevelUpListID, build.m_Lv);
                return drop != null;
            }
            return false;
        }

        public static bool FindTownBuildToDropItemState(ref DropItemInfo pstate, eTownObjectCategory fcategory) {
            int idx = -1;
            UserTownData town = GameSystem.Inst.UserDataMng.UserTownData;
            for (int i = 0; i < town.GetBuildObjectDataNum(); i++) {
                UserTownData.BuildObjectData build = town.GetBuildObjectDataAt(i);
                if (build.m_IsOpen && GameSystem.Inst.DbMng.TownObjListDB.GetParam(build.m_ObjID).m_Category == (int)fcategory && build.m_Lv > -1) {
                    idx = i;
                }
            }
            if (idx >= 0) {
                UserTownData.BuildObjectData build = town.GetBuildObjectDataAt(idx);
                DropItemTool drop = TownItemDropDatabase.CalcDropPeformsnce(ref pstate, GameSystem.Inst.DbMng.TownObjListDB.GetParam(build.m_ObjID).m_LevelUpListID, build.m_Lv);
                if (drop != null) {
                    return true;
                }
            }
            return false;
        }

        public static UserTownData.BuildObjectData GetTownPointToBuildData(int fpoint) {
            return GameSystem.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(fpoint);
        }

        public static string CreateTownBuildListString(UserTownData.BuildObjectUp padd = null) {
            TownServerForm.DataChunk dataChunk = new TownServerForm.DataChunk();
            TownServerForm.Build build = new TownServerForm.Build();
            build.m_Table = GameSystem.Inst.UserDataMng.UserTownData.CreateBuildTable();
            if (padd != null) {
                build.m_Table.Add(padd);
            }
            string buildStr = JsonConvert.SerializeObject(build);
            dataChunk.m_Ver = 1;
            dataChunk.m_DataType = (int)TownServerForm.eDataType.Build;
            dataChunk.m_Data = Encoding.ASCII.GetBytes(buildStr);
            string chunkStr = JsonConvert.SerializeObject(dataChunk);
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(chunkStr));
        }

        public static TownServerForm.DataChunk GetDataChunk(string fcode) {
            string chunkStr = Encoding.ASCII.GetString(Convert.FromBase64String(fcode));
            return JsonConvert.DeserializeObject<TownServerForm.DataChunk>(chunkStr);
        }

        public static TownServerForm.Build BuildTownBuildList(TownServerForm.DataChunk pchunk) {
            if (pchunk.m_DataType == (int)TownServerForm.eDataType.Build) {
                string buildStr = Encoding.ASCII.GetString(pchunk.m_Data);
                return JsonConvert.DeserializeObject<TownServerForm.Build>(buildStr);
            }
            return null;
        }

        public static string CreateTownDebugPlayString() {
            TownServerForm.DataChunk dataChunk = new TownServerForm.DataChunk();
            string debugStr = JsonConvert.SerializeObject(new TownServerForm.DebugPlay
            {
                m_DebugPlay = true,
                m_Level = GameSystem.Inst.UserDataMng.UserTownData.Level
            });
            dataChunk.m_Ver = 1;
            dataChunk.m_DataType = (int)TownServerForm.eDataType.DebugPlay;
            dataChunk.m_Data = Encoding.ASCII.GetBytes(debugStr);
            string chunkStr = JsonConvert.SerializeObject(dataChunk);
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(chunkStr));
        }

        public static TownServerForm.DebugPlay BuildTownDebugCode(TownServerForm.DataChunk pchunk) {
            if (pchunk.m_DataType == (int)TownServerForm.eDataType.DebugPlay) {
                string debugStr = Encoding.ASCII.GetString(pchunk.m_Data);
                return JsonConvert.DeserializeObject<TownServerForm.DebugPlay>(debugStr);
            }
            return null;
        }

        public static long GetBuildUpTime(int fobjid, int flevel) {
            long time = TownUtility.GetTownObjectLevelUpConfirm(fobjid, flevel + 1, TownDefine.eTownLevelUpConditionCategory.BuildTime);
            if (time == 0L) {
                time = (long)GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_BuildTime;
            }
            return time * 1000L;
        }

        public static bool IsBuildMarks(int fobjid) {
            return GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_BuildTime > 0f;
        }

        public static bool IsStoreTownObject(int fobjid) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            short max = GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_MaxNum;
            int stored = 0;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].ObjID == fobjid) {
                    stored++;
                }
            }
            return stored < max;
        }

        public static bool IsAddTownObjData(int fobjid) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            short max = GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_MaxNum;
            int stored = 0;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].ObjID == fobjid) {
                    stored++;
                }
            }
            return stored < max;
        }

        public static long GetStoreTownObj(int fobjid) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].ObjID == fobjid) {
                    return townObjs[i].m_ManageID;
                }
            }
            return -1L;
        }

        public static void ClearTownObjData() {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            int count = townObjs.Count;
            for (int i = count - 1; i >= 0; i--) {
                townObjs.RemoveAt(i);
            }
            GameSystem.Inst.UserDataMng.UserTownData.GetStoreDatas().Clear();
        }

        public static void AddTownObjData(UserTownObjectData padd) {
            GameSystem.Inst.UserDataMng.UserTownObjDatas.Add(padd);
            GameSystem.Inst.UserDataMng.UserTownData.AddStoreDataNum(padd.ObjID, 1);
        }

        public static void RemoveTownObjData(long fmanageid) {
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            for (int i = 0; i < townObjs.Count; i++) {
                if (townObjs[i].m_ManageID == fmanageid) {
                    GameSystem.Inst.UserDataMng.UserTownData.AddStoreDataNum(townObjs[i].ObjID, -1);
                    townObjs.RemoveAt(i);
                    break;
                }
            }
        }

        public static int GetTownObjBuildNum(int objID) {
            int objNum = GameSystem.Inst.UserDataMng.UserTownData.GetBuildObjectDataNum();
            int buildNum = 0;
            for (int i = 0; i < objNum; i++) {
                if (GameSystem.Inst.UserDataMng.UserTownData.GetBuildObjectDataAt(i).m_ObjID == objID) {
                    buildNum++;
                }
            }
            return buildNum;
        }

        public static int GetTownObjStockNum() {
            UserTownData town = GameSystem.Inst.UserDataMng.UserTownData;
            List<UserTownObjectData> townObjs = GameSystem.Inst.UserDataMng.UserTownObjDatas;
            int stock = 0;
            for (int i = 0; i < townObjs.Count; i++) {
                UserTownObjectData townObj = townObjs[i];
                TownObjectListDB_Param param = GameSystem.Inst.DbMng.TownObjListDB.GetParam(townObj.ObjID);
                if (param.m_Category != (int)eTownObjectCategory.Area && param.m_Category != (int)eTownObjectCategory.Contents) {
                    bool found = false;
                    for (int j = 0; j < town.GetBuildObjectDataNum(); j++) {
                        if (town.GetBuildObjectDataAt(j).m_ManageID == townObj.m_ManageID) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        stock++;
                    }
                }
            }
            return stock;
        }

        public static long MakeBuildObjectToFreeManageID() {
            UserTownData town = GameSystem.Inst.UserDataMng.UserTownData;
            long id = 0L;
            for (int i = 0; i < town.GetBuildObjectDataNum(); i++) {
                if (id < town.GetBuildObjectDataAt(i).m_ManageID) {
                    id = town.GetBuildObjectDataAt(i).m_ManageID;
                }
            }
            return id + 1L;
        }

        public static bool CheckPointToBuilding(int fbuildpoint, int fobjid) {
            TownObjectListDB_Param res = GetTownResouceData(fobjid);
            int type = TownUtility.GetPointToBuildType(fbuildpoint);
            if (type == TownDefine.AREA_CTXID) {
                if (res.m_Category == (int)eTownObjectCategory.Area || res.m_Category == (int)eTownObjectCategory.Contents) {
                    return true;
                }
            } else if (type == TownDefine.MENU_BUFID) {
                if (res.m_Category == (int)eTownObjectCategory.Menu) {
                    return true;
                }
            } else if (type == TownDefine.AREA_CONTENTID) {
                if (res.m_Category == (int)eTownObjectCategory.Contents) {
                    return true;
                }
            } else if (type == TownDefine.AREA_BUFID) {
                if (res.m_Category == (int)eTownObjectCategory.Item || res.m_Category == (int)eTownObjectCategory.Money || res.m_Category == (int)eTownObjectCategory.Buf) {
                    return true;
                }
            }
            return false;
        }

        public static int GetSettingOptionKey(int fobjid) {
            TownObjectListDB_Param objParam = GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid);
            TownObjectLevelUpDB_Param lvParam = GameSystem.Inst.DbMng.TownObjLvUpDB.GetParam(objParam.m_LevelUpListID, 1);
            int key = 0;
            if (lvParam.m_TownUseFunc == 1) {
                UserTownData town = GameSystem.Inst.UserDataMng.UserTownData;
                for (int i = 0; i < town.GetBuildObjectDataNum(); i++) {
                    UserTownData.BuildObjectData objData = town.GetBuildObjectDataAt(i);
                    if (objData.m_BuildPointIndex != 0 && TownUtility.GetPointToBuildType(objData.m_BuildPointIndex) == TownDefine.AREA_CTXID) {
                        key++;
                    }
                }
                key += lvParam.m_TownActionStepKey;
            }
            return key;
        }

        public static bool IsManageIDToPreComplete(long fmanageid) {
            UserTownData.BuildObjectData build = GetTownBuildData(fmanageid);
            if (!build.m_IsOpen) {
                long time = ScheduleTimeUtil.GetSystemTimeMs();
                if (time - build.m_BuildingMs >= GetBuildUpTime(build.m_ObjID, build.m_Lv)) {
                    return true;
                }
            }
            return false;
        }

        public static long GetBuildTimeMs(long fmanageid) {
            UserTownData.BuildObjectData build = GetTownBuildData(fmanageid);
            long time = ScheduleTimeUtil.GetSystemTimeMs();
            return time - build.m_ActionTime;
        }

        public static TownObjectListDB_Param GetTownResouceData(int fresid) {
            return GameSystem.Inst.DbMng.TownObjListDB.GetParam(fresid);
        }

        public static long GetRemainingTime(long fmanageid, int fprebuild = -1) {
            UserTownData.BuildObjectData build = GetTownBuildData(fmanageid);
            long time = 0L;
            if (!build.m_IsOpen || fprebuild != -1) {
                int lv = build.m_Lv;
                if (fprebuild >= 0) {
                    lv = fprebuild;
                }
                time = ScheduleTimeUtil.GetSystemTimeMs() - build.m_ActionTime;
                long buildUpTime = GetBuildUpTime(build.m_ObjID, lv);
                if (time > buildUpTime) {
                    time = 0;
                } else {
                    time = buildUpTime - time;
                }
            }
            return time;
        }

        public static long CalcRemainGemNum(long fremainTimes) {
            int period = (int)GameSystem.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.ReduceBuildTimeTownFacilityPeriod);
            int amount = (int)GameSystem.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.ReduceBuildTimeTownFacilityAmount);
            return amount * (long)Math.Ceiling(fremainTimes / (period * 60 * 1000.0));
        }

        public static long GetRemainingUseGem(long fmanageid) {
            return CalcRemainGemNum(GetRemainingTime(fmanageid));
        }

        public static float CalcScheduleDropPer(float fbase) {
            UserTownData town = GameSystem.Inst.UserDataMng.UserTownData;
            int num = 0;
            for (int i = 0; i < town.GetBuildObjectDataNum(); i++) {
                if (TownUtility.GetPointToBuildType(town.GetBuildObjectDataAt(i).m_BuildPointIndex) == TownDefine.AREA_CTXID) {
                    num++;
                }
            }
            return num switch {
                0 => fbase * 0.7f,
                1 => fbase * 0.8f,
                2 => fbase * 0.9f,
                3 => fbase * 1f,
                4 => fbase * 1.1f,
                5 => fbase * 1.2f,
                _ => fbase * 1.3f,
            };
        }

        public static UserScheduleData.DropState GetFieldDropItems(int fkeyid) {
            FieldItemDropListDB db = GameSystem.Inst.DbMng.FieldItemDropListDB;
            UserScheduleData.DropState drop = null;
            for (int i = 0; i < db.m_Params.Length; i++) {
                if (db.m_Params[i].m_ID == fkeyid) {
                    switch ((eMissionRewardCategory)db.m_Params[i].m_Category[0]) {
                        case eMissionRewardCategory.Money:
                            drop = new UserScheduleData.DropState();
                            drop.m_Category = UserScheduleData.eDropCategory.Money;
                            drop.m_Num = db.m_Params[i].m_Num[0];
                            break;
                        case eMissionRewardCategory.Item:
                            drop = new UserScheduleData.DropState();
                            drop.m_Category = UserScheduleData.eDropCategory.Item;
                            drop.m_CategoryNo = db.m_Params[i].m_ObjectID[0];
                            drop.m_Num = db.m_Params[i].m_Num[0];
                            break;
                        case eMissionRewardCategory.KRRPoint:
                            drop = new UserScheduleData.DropState();
                            drop.m_Category = UserScheduleData.eDropCategory.KRRPoint;
                            drop.m_Num = db.m_Params[i].m_Num[0];
                            break;
                        case eMissionRewardCategory.Stamina:
                            drop = new UserScheduleData.DropState();
                            drop.m_Category = UserScheduleData.eDropCategory.Stamina;
                            drop.m_Num = db.m_Params[i].m_Num[0];
                            break;
                        case eMissionRewardCategory.Friendship:
                            drop = new UserScheduleData.DropState();
                            drop.m_Category = UserScheduleData.eDropCategory.FriendShip;
                            drop.m_Num = db.m_Params[i].m_Num[0];
                            break;
                    }
                    break;
                }
            }
            return drop;
        }
    }
}
