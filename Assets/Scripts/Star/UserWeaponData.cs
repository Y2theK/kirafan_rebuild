﻿namespace Star {
    public class UserWeaponData {
        public long MngID { get; set; }
        public WeaponParam Param { get; set; }

        public UserWeaponData() {
            MngID = -1L;
            Param = new WeaponParam();
        }
    }
}
