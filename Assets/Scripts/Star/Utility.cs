﻿using Meige;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star {
    public static class Utility {
        public static string GetiOSVersion() {
#if UNITY_IOS
			return SystemInfo.operatingSystem.Replace("iPhone OS ", string.Empty);
#else
            return null;
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public static void ReassignShader(Material mat) { }

        [Conditional("UNITY_EDITOR")]
        public static void ReassignShader(GameObject go) {
            Renderer[] renderers = go.GetComponents<Renderer>();
            foreach (Renderer renderer in renderers) {
                Material[] materials = renderer.materials;
                foreach (Material material in materials) {
                    //editor code
                }
            }
            renderers = go.GetComponentsInChildren<Renderer>(true);
            foreach (Renderer renderer in renderers) {
                Material[] materials = renderer.materials;
                foreach (Material material in materials) {
                    //editor code
                }
            }
            MeigeParticleEmitter[] emitters = go.GetComponentsInChildren<MeigeParticleEmitter>(true);
            foreach (MeigeParticleEmitter emitter in emitters) {
                //editor code
            }
        }

        [Conditional("UNITY_EDITOR")]
        public static void ReassignImageShader(GameObject go) {
            Image[] images = go.GetComponents<Image>();
            foreach (Image image in images) {
                //editor code
            }
            images = go.GetComponentsInChildren<Image>(true);
            foreach (Image image in images) {
                //editor code
            }
        }

        public static void ReassignSharedShader(GameObject go) {
            Renderer[] renderers = go.GetComponents<Renderer>();
            foreach (Renderer renderer in renderers) {
                Material[] sharedMaterials = renderer.sharedMaterials;
                foreach (Material material in sharedMaterials) {
                    //editor code
                }
            }
            renderers = go.GetComponentsInChildren<Renderer>(true);
            foreach (Renderer renderer in renderers) {
                Material[] sharedMaterials = renderer.sharedMaterials;
                foreach (Material material in sharedMaterials) {
                    //editor code
                }
            }
            MeigeParticleEmitter[] emitters = go.GetComponentsInChildren<MeigeParticleEmitter>();
            foreach (MeigeParticleEmitter emitter in emitters) {
                //editor code
            }
        }

        public static void SetLayerRecursively(GameObject go, int layer) {
            go.layer = layer;
            foreach (Transform child in go.transform) {
                SetLayerRecursively(child.gameObject, layer);
            }
        }

        public static bool CheckTapUI(GameObject go) {
            Transform transform = go.transform;
            while (!transform.TryGetComponent<Canvas>(out _)) {
                transform = transform.parent;
                if (transform == null) {
                    return true;
                }
            }
            if (!transform.TryGetComponent(out Canvas canvas)) {
                return true;
            }
            Vector2 pos = RectTransformUtility.WorldToScreenPoint(canvas.worldCamera, go.GetComponent<RectTransform>().position);
            PointerEventData evtData = new PointerEventData(EventSystem.current);
            evtData.position = pos;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(evtData, results);
            if (results == null || results.Count == 0) {
                return true;
            }
            GameObject hitObj = results[0].gameObject;
            List<GameObject> allGO = go.GetAllGameObject();
            for (int i = 0; i < allGO.Count; i++) {
                if (allGO[i] == hitObj) {
                    return true;
                }
            }
            return go == hitObj;
        }

        public static List<GameObject> GetAllGameObject(this GameObject go) {
            List<GameObject> result = new List<GameObject>();
            GetChildrenGameObjects(go, ref result);
            return result;
        }

        public static void GetChildrenGameObjects(GameObject go, ref List<GameObject> allChildren) {
            Transform transform = go.GetComponentInChildren<Transform>();
            if (transform.childCount == 0) {
                return;
            }
            foreach (Transform child in transform) {
                allChildren.Add(child.gameObject);
                GetChildrenGameObjects(child.gameObject, ref allChildren);
            }
        }
    }
}
