﻿namespace Star {
    public class Version {
        public const string CurrentVersion = "3.6.0";
        public const int CurrentVersionCode = 36000;

        public static bool IsApplying { get; set; } = false;
        public static bool IsOutdated { get; set; } = false;
    }
}
