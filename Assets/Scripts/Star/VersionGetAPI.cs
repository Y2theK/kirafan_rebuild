﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000AF2 RID: 2802
	[Token(Token = "0x20007A9")]
	[StructLayout(3)]
	public class VersionGetAPI
	{
		// Token: 0x14000059 RID: 89
		// (add) Token: 0x06003184 RID: 12676 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003185 RID: 12677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000059")]
		private event Action m_OnResponse_VersionGet
		{
			[Token(Token = "0x6002D45")]
			[Address(RVA = "0x101618708", Offset = "0x1618708", VA = "0x101618708")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002D46")]
			[Address(RVA = "0x101618814", Offset = "0x1618814", VA = "0x101618814")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003186 RID: 12678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D47")]
		[Address(RVA = "0x101618920", Offset = "0x1618920", VA = "0x101618920")]
		public void Request_VersionGet(Action callback)
		{
		}

		// Token: 0x06003187 RID: 12679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D48")]
		[Address(RVA = "0x101618AB8", Offset = "0x1618AB8", VA = "0x101618AB8")]
		private void OnResponse_VersionGet(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003188 RID: 12680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D49")]
		[Address(RVA = "0x101618BDC", Offset = "0x1618BDC", VA = "0x101618BDC")]
		public VersionGetAPI()
		{
		}
	}
}
