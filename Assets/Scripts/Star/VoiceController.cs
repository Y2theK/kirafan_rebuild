﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AB4 RID: 2740
	[Token(Token = "0x200078C")]
	[StructLayout(3)]
	public class VoiceController
	{
		// Token: 0x06002F36 RID: 12086 RVA: 0x00014478 File Offset: 0x00012678
		[Token(Token = "0x6002B18")]
		[Address(RVA = "0x101618BE4", Offset = "0x1618BE4", VA = "0x101618BE4")]
		private int GetNewUniqueID()
		{
			return 0;
		}

		// Token: 0x06002F37 RID: 12087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B19")]
		[Address(RVA = "0x101618BF8", Offset = "0x1618BF8", VA = "0x101618BF8")]
		public void Setup(SoundManager soundManager)
		{
		}

		// Token: 0x06002F38 RID: 12088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B1A")]
		[Address(RVA = "0x101618C00", Offset = "0x1618C00", VA = "0x101618C00")]
		public void Release()
		{
		}

		// Token: 0x06002F39 RID: 12089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B1B")]
		[Address(RVA = "0x101618EA0", Offset = "0x1618EA0", VA = "0x101618EA0")]
		public void Update()
		{
		}

		// Token: 0x06002F3A RID: 12090 RVA: 0x00014490 File Offset: 0x00012690
		[Token(Token = "0x6002B1C")]
		[Address(RVA = "0x101606A04", Offset = "0x1606A04", VA = "0x101606A04")]
		public bool IsPlaying(int requestUniqueID)
		{
			return default(bool);
		}

		// Token: 0x06002F3B RID: 12091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B1D")]
		[Address(RVA = "0x101619718", Offset = "0x1619718", VA = "0x101619718")]
		public void StopRequest(int requestUniqueID)
		{
		}

		// Token: 0x06002F3C RID: 12092 RVA: 0x000144A8 File Offset: 0x000126A8
		[Token(Token = "0x6002B1E")]
		[Address(RVA = "0x101606B24", Offset = "0x1606B24", VA = "0x101606B24")]
		public int Request(eCharaNamedType namedType, eSoundVoiceListDB cueID, string selector, string label, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F3D RID: 12093 RVA: 0x000144C0 File Offset: 0x000126C0
		[Token(Token = "0x6002B1F")]
		[Address(RVA = "0x1016198B8", Offset = "0x16198B8", VA = "0x1016198B8")]
		public int Request(eCharaNamedType namedType, string cueName, string selector, string label, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F3E RID: 12094 RVA: 0x000144D8 File Offset: 0x000126D8
		[Token(Token = "0x6002B20")]
		[Address(RVA = "0x101619A5C", Offset = "0x1619A5C", VA = "0x101619A5C")]
		public int RequestByNamedAndCueName(eCharaNamedType namedType, string cueName, string selector, string label, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F3F RID: 12095 RVA: 0x000144F0 File Offset: 0x000126F0
		[Token(Token = "0x6002B21")]
		[Address(RVA = "0x101619D20", Offset = "0x1619D20", VA = "0x101619D20")]
		public int Request(eSoundCueSheetDB cueSheetID, string cueName, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F40 RID: 12096 RVA: 0x00014508 File Offset: 0x00012708
		[Token(Token = "0x6002B22")]
		[Address(RVA = "0x10161A130", Offset = "0x161A130", VA = "0x10161A130")]
		public int Request(eSoundCueSheetDB cueSheetID, string[] cueNames, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F41 RID: 12097 RVA: 0x00014520 File Offset: 0x00012720
		[Token(Token = "0x6002B23")]
		[Address(RVA = "0x101619E0C", Offset = "0x1619E0C", VA = "0x101619E0C")]
		public int Request(string cueSheet, string cueName, string selector, string label, [Optional] string acbFile, [Optional] string awbFile, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F42 RID: 12098 RVA: 0x00014538 File Offset: 0x00012738
		[Token(Token = "0x6002B24")]
		[Address(RVA = "0x10161A1C4", Offset = "0x161A1C4", VA = "0x10161A1C4")]
		public int RequestFlavorText(int charaID, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F43 RID: 12099 RVA: 0x00014550 File Offset: 0x00012750
		[Token(Token = "0x6002B25")]
		[Address(RVA = "0x10161A3BC", Offset = "0x161A3BC", VA = "0x10161A3BC")]
		public int RequestCharaCutIn(int charaID, bool isSaveLatestUniqueID = false)
		{
			return 0;
		}

		// Token: 0x06002F44 RID: 12100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B26")]
		[Address(RVA = "0x10161A270", Offset = "0x161A270", VA = "0x10161A270")]
		private void GetUniqueCueSheetData(int charaID, out string out_cueSheet, out string out_acbFile, out string out_awbFile)
		{
		}

		// Token: 0x06002F45 RID: 12101 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002B27")]
		[Address(RVA = "0x10161A468", Offset = "0x161A468", VA = "0x10161A468")]
		public VoiceController.SoundVoiceControllResult Request(eSoundVoiceControllListDB id, int forceRequestCueSheetIndex = -1, int forceRequestNamedIndex = -1, int forceRequestCharaIdForSelector = -1, bool isSaveLatestUniqueID = false)
		{
			return null;
		}

		// Token: 0x06002F46 RID: 12102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B28")]
		[Address(RVA = "0x101619860", Offset = "0x1619860", VA = "0x101619860")]
		private void StopRequestLatestUniqueID(bool isSaveLatestUniqueID)
		{
		}

		// Token: 0x06002F47 RID: 12103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B29")]
		[Address(RVA = "0x1016198AC", Offset = "0x16198AC", VA = "0x1016198AC")]
		private void SaveLatestUniqueID(bool isSaveLatestUniqueID, int uniqueID)
		{
		}

		// Token: 0x06002F48 RID: 12104 RVA: 0x00014568 File Offset: 0x00012768
		[Token(Token = "0x6002B2A")]
		[Address(RVA = "0x10161AA18", Offset = "0x161AA18", VA = "0x10161AA18")]
		private int GetHour()
		{
			return 0;
		}

		// Token: 0x06002F49 RID: 12105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B2B")]
		[Address(RVA = "0x10161AA98", Offset = "0x161AA98", VA = "0x10161AA98")]
		public VoiceController()
		{
		}

		// Token: 0x04003EDC RID: 16092
		[Token(Token = "0x4002D21")]
		private const int CONTROLL_ACQUIRE_NUM = 4;

		// Token: 0x04003EDD RID: 16093
		[Token(Token = "0x4002D22")]
		private const int REQUEST_CAPACITY = 32;

		// Token: 0x04003EDE RID: 16094
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002D23")]
		private SoundManager m_SoundManager;

		// Token: 0x04003EDF RID: 16095
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002D24")]
		private Dictionary<string, VoiceController.RequestParam> m_RequestDict;

		// Token: 0x04003EE0 RID: 16096
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002D25")]
		private Dictionary<int, SoundHandler> m_SoundHndls;

		// Token: 0x04003EE1 RID: 16097
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002D26")]
		private List<string> m_Paths;

		// Token: 0x04003EE2 RID: 16098
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002D27")]
		private List<string> m_RemoveKeys;

		// Token: 0x04003EE3 RID: 16099
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002D28")]
		private List<int> m_RemoveKeys_SoundHndl;

		// Token: 0x04003EE4 RID: 16100
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002D29")]
		private int m_LatestUniqueID;

		// Token: 0x04003EE5 RID: 16101
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002D2A")]
		private int m_UniqueIDGenerator;

		// Token: 0x02000AB5 RID: 2741
		[Token(Token = "0x2000FF5")]
		public class SoundVoiceControllResult
		{
			// Token: 0x06002F4A RID: 12106 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060D7")]
			[Address(RVA = "0x10161AA00", Offset = "0x161AA00", VA = "0x10161AA00")]
			public SoundVoiceControllResult()
			{
			}

			// Token: 0x04003EE6 RID: 16102
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006527")]
			public int m_UniqueID;

			// Token: 0x04003EE7 RID: 16103
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006528")]
			public int m_RequestCueSheetIndex;

			// Token: 0x04003EE8 RID: 16104
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006529")]
			public int m_RequestNamedIndex;
		}

		// Token: 0x02000AB6 RID: 2742
		[Token(Token = "0x2000FF6")]
		private class RequestParam
		{
			// Token: 0x06002F4B RID: 12107 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060D8")]
			[Address(RVA = "0x10161989C", Offset = "0x161989C", VA = "0x10161989C")]
			public RequestParam()
			{
			}

			// Token: 0x04003EE9 RID: 16105
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400652A")]
			public int m_UniqueID;

			// Token: 0x04003EEA RID: 16106
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400652B")]
			public eCharaNamedType m_NamedType;

			// Token: 0x04003EEB RID: 16107
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400652C")]
			public eSoundVoiceListDB m_NamedVoiceCueID;

			// Token: 0x04003EEC RID: 16108
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400652D")]
			public string m_CueSheet;

			// Token: 0x04003EED RID: 16109
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400652E")]
			public string m_CueName;

			// Token: 0x04003EEE RID: 16110
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400652F")]
			public string m_Selector;

			// Token: 0x04003EEF RID: 16111
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006530")]
			public string m_Label;
		}
	}
}
