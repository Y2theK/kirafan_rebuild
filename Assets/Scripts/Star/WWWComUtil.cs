﻿using GachaResponseTypes;
using WWWTypes;

namespace Star {
    public class WWWComUtil {
        public static void SetTownBuildListKey(PlayerTown[] plist, bool fdataclear) {
            UserTownData userTownData = GameSystem.Inst.UserDataMng.UserTownData;
            if (fdataclear) {
                userTownData.ClearData(-1, 0);
            }

            if (plist.Length == 0) { return; }

            for (int i = 0; i < plist.Length; i++) {
                TownServerForm.DataChunk dataChunk = UserTownUtil.GetDataChunk(plist[i].gridData);
                if (dataChunk.m_DataType == 0) {
                    TownServerForm.Build build = UserTownUtil.BuildTownBuildList(dataChunk);
                    userTownData.ClearData(plist[i].managedTownId, 0);
                    for (int j = 0; j < build.m_Table.Count; j++) {
                        UserTownData.BuildObjectUp buildObj = build.m_Table[j];
                        long actionTime = buildObj.m_ActionTime;
                        for (int k = 0; k < GameSystem.Inst.UserDataMng.UserTownObjDatas.Count; k++) {
                            if (buildObj.m_ManageID == GameSystem.Inst.UserDataMng.UserTownObjDatas[k].m_ManageID) {
                                actionTime = GameSystem.Inst.UserDataMng.UserTownObjDatas[k].ActionTime;
                                break;
                            }
                        }
                        UserTownData.BuildObjectData buildObject = UserTownUtil.CreateUserTownBuildData(buildObj.m_ManageID, buildObj.m_BuildPointIndex, buildObj.m_ObjID, buildObj.m_IsOpen, buildObj.m_BuildingMs, actionTime);
                        userTownData.AddBuildObjectData(buildObject);
                    }
                } else if (dataChunk.m_DataType == 1) {
                    TownServerForm.DebugPlay debugPlay = UserTownUtil.BuildTownDebugCode(dataChunk);
                    if (debugPlay.m_DebugPlay) {
                        userTownData.Level = debugPlay.m_Level;
                        userTownData.DebugMngID = plist[i].managedTownId;
                    }
                }
            }
        }

        public static void SetTownObjectListKey(PlayerTownFacility[] plist, bool fdataclear) {
            if (fdataclear) {
                UserTownUtil.ClearTownObjData();
            }
            if (plist.Length != 0) {
                UserTownObjectData.SetConnect(true);
            }
            for (int i = 0; i < plist.Length; i++) {
                UserTownObjectData padd = new UserTownObjectData(ref plist[i]);
                UserTownUtil.AddTownObjData(padd);
            }
        }

        public static int SetRoomPlacementKey(PlayerRoom[] plist, bool fdataclear) {
            if (fdataclear) {
                UserRoomUtil.ClearRoomData();
            }
            if (plist.Length == 0) {
                return 1;
            }

            for (int i = 0; i < plist.Length; i++) {
                UserRoomData userRoomData = UserRoomUtil.GetManageIDToUserRoomData(plist[i].managedRoomId);
                if (userRoomData == null) {
                    userRoomData = new UserRoomData();
                    userRoomData.MngID = plist[i].managedRoomId;
                }
                userRoomData.ClearPlacements();
                userRoomData.PlayerID = GameSystem.Inst.UserDataMng.UserData.ID;
                userRoomData.FloorID = plist[i].floorId;
                if (userRoomData.FloorID <= 0) {
                    userRoomData.FloorID = 1;
                }
                for (int j = 0; j < plist[i].arrangeData.Length; j++) {
                    PlayerRoomArrangement arrange = plist[i].arrangeData[j];
                    UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(arrange.x, arrange.y, (CharacterDefine.eDir)arrange.dir, arrange.managedRoomObjectId, arrange.roomNo, arrange.roomObjectId);
                    UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(arrange.managedRoomObjectId);
                    if (manageIDToUserRoomObjData != null) {
                        if (manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Background || manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Wall || manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Floor) {
                            placementData.m_RoomNo = -1;
                            placementData.m_ObjectID = manageIDToUserRoomObjData.ResourceID;
                        }
                        manageIDToUserRoomObjData.LockLinkMngID(arrange.managedRoomObjectId);
                    }
                    userRoomData.AddPlacement(placementData);
                }
                GameSystem.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(GameSystem.Inst.UserDataMng.UserData.ID, plist[i].groupId, userRoomData);
            }
            GameSystem.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(GameSystem.Inst.UserDataMng.UserData.ID, plist[0].managedRoomId);
            return 0;
        }

        public static void SetFriendRoomPlacementKey(long fplayerid, PlayerRoom[] plist) {
            if (plist.Length == 0) { return; }

            for (int i = 0; i < plist.Length; i++) {
                UserRoomData userRoomData = UserRoomUtil.GetManageIDToUserRoomData(plist[i].managedRoomId);
                if (userRoomData == null) {
                    userRoomData = new UserRoomData();
                    userRoomData.MngID = plist[i].managedRoomId;
                }
                userRoomData.ClearPlacements();
                userRoomData.PlayerID = fplayerid;
                userRoomData.FloorID = plist[i].floorId;
                if (userRoomData.FloorID <= 0) {
                    userRoomData.FloorID = 1;
                }
                for (int j = 0; j < plist[i].arrangeData.Length; j++) {
                    PlayerRoomArrangement arrange = plist[i].arrangeData[j];
                    UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(arrange.x, arrange.y, (CharacterDefine.eDir)arrange.dir, arrange.managedRoomObjectId, arrange.roomNo, arrange.roomObjectId);
                    if (RoomObjectListUtil.GetIDToCategory(arrange.roomObjectId) == eRoomObjectCategory.Background || RoomObjectListUtil.GetIDToCategory(arrange.roomObjectId) == eRoomObjectCategory.Wall || RoomObjectListUtil.GetIDToCategory(arrange.roomObjectId) == eRoomObjectCategory.Floor) {
                        placementData.m_RoomNo = -1;
                    }
                    userRoomData.AddPlacement(placementData);
                }
                GameSystem.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(fplayerid, plist[i].groupId, userRoomData);
            }
            GameSystem.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(fplayerid, plist[0].managedRoomId);
        }

        public static void SetRoomObjectList(PlayerRoomObject[] psetuplist, bool fdataclear, bool fsetupmarge = false) {
            if (fdataclear) {
                UserRoomUtil.ClearRoomObjData();
            }
            UserDataManager userDataMng = GameSystem.Inst.UserDataMng;
            if (fsetupmarge) {
                for (int i = 0; i < userDataMng.UserRoomObjDatas.Count; i++) {
                    userDataMng.UserRoomObjDatas[i].ClrToListUpUse();
                }
                for (int i = 0; i < psetuplist.Length; i++) {
                    UserRoomObjectData userRoomObjectData = UserRoomUtil.GetManageIDToUserRoomObjData(psetuplist[i].managedRoomObjectId);
                    if (userRoomObjectData == null) {
                        RoomObjectListDB_Param param = RoomObjectListUtil.GetAccessKeyToObjectParam(psetuplist[i].roomObjectId);
                        userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)param.m_Category, param.m_ID);
                        userDataMng.UserRoomObjDatas.Add(userRoomObjectData);
                        userRoomObjectData.SetServerData(psetuplist[i].managedRoomObjectId, psetuplist[i].roomObjectId);
                    }
                    userRoomObjectData.SetListUpToUse(psetuplist[i].managedRoomObjectId);
                }
                for (int i = userDataMng.UserRoomObjDatas.Count - 1; i >= 0; i--) {
                    if (!userDataMng.UserRoomObjDatas[i].ReleaseListUpNoUse()) {
                        userDataMng.UserRoomObjDatas.RemoveAt(i);
                    }
                }
            } else {
                for (int i = 0; i < userDataMng.UserRoomObjDatas.Count; i++) {
                    UserRoomObjectData userRoomObjectData = UserRoomUtil.GetAccessIDToUserRoomObjData(psetuplist[i].roomObjectId);
                    if (userRoomObjectData == null) {
                        RoomObjectListDB_Param param = RoomObjectListUtil.GetAccessKeyToObjectParam(psetuplist[i].roomObjectId);
                        userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)param.m_Category, param.m_ID);
                        userDataMng.UserRoomObjDatas.Add(userRoomObjectData);
                    }
                    userRoomObjectData.SetServerData(psetuplist[i].managedRoomObjectId, psetuplist[i].roomObjectId);
                }
            }
        }

        public static void SetFieldPartyListKey(PlayerFieldPartyMember[] plist, bool fdataclear) {
            UserFieldCharaData userFieldChara = GameSystem.Inst.UserDataMng.UserFieldChara;
            if (fdataclear) {
                userFieldChara.ClearAll(-1);
                userFieldChara.UpManageID(1);
            }
            if (plist.Length != 0) {
                for (int i = 0; i < plist.Length; i++) {
                    if (!userFieldChara.IsEntryFldChara(plist[i].managedCharacterId)) {
                        userFieldChara.AddRoomInChara(plist[i]);
                    }
                }
            }
        }

        public static void SetPlayedGachaAdvertiseList(GetAll param) {
            for (int i = 0; i < param.gachas.Length; i++) {
                var gacha = param.gachas[i].gacha;
                if (!string.IsNullOrEmpty(gacha.mvName)) {
                    if (LocalSaveData.Inst.playedGachaAdvertiseList != null) {
                        if (LocalSaveData.Inst.playedGachaAdvertiseList.Contains(gacha.id)) { continue; }
                    }
                    GameSystem.Inst.AddUnplayedGachaAdvertise(new GameSystem.UnplayedGachaAdvertise(gacha.id, gacha.mvName, gacha.startAt, gacha.endAt));
                }
            }
        }
    }
}
