﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000612 RID: 1554
	[Token(Token = "0x2000505")]
	[StructLayout(3)]
	public class WeaponEvolutionListDB : ScriptableObject
	{
		// Token: 0x06001616 RID: 5654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C6")]
		[Address(RVA = "0x10161CA18", Offset = "0x161CA18", VA = "0x10161CA18")]
		public WeaponEvolutionListDB()
		{
		}

		// Token: 0x040025DF RID: 9695
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F49")]
		public WeaponEvolutionListDB_Param[] m_Params;
	}
}
