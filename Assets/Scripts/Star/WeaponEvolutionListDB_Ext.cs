﻿namespace Star {
    public static class WeaponEvolutionListDB_Ext {
        public static WeaponEvolutionListDB_Param? GetParamBySrcWeaponID(this WeaponEvolutionListDB self, int srcWeaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcWeaponID == srcWeaponID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static WeaponEvolutionListDB_Param? GetParamByDestWeaponID(this WeaponEvolutionListDB self, int destWeaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_DestWeaponID == destWeaponID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static bool IsExistParam(this WeaponEvolutionListDB self, int srcWeaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcWeaponID == srcWeaponID) {
                    return true;
                }
            }
            return false;
        }
    }
}
