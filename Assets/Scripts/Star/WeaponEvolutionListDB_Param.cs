﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000611 RID: 1553
	[Token(Token = "0x2000504")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct WeaponEvolutionListDB_Param
	{
		// Token: 0x040025D8 RID: 9688
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F42")]
		public int m_RecipeID;

		// Token: 0x040025D9 RID: 9689
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001F43")]
		public int m_SrcWeaponID;

		// Token: 0x040025DA RID: 9690
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F44")]
		public int m_DestWeaponID;

		// Token: 0x040025DB RID: 9691
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001F45")]
		public int m_AmountGold;

		// Token: 0x040025DC RID: 9692
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F46")]
		public int[] m_ItemIDs;

		// Token: 0x040025DD RID: 9693
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F47")]
		public int[] m_ItemNums;

		// Token: 0x040025DE RID: 9694
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001F48")]
		public int m_isFinalEvo;
	}
}
