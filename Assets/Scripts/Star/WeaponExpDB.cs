﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000614 RID: 1556
	[Token(Token = "0x2000507")]
	[StructLayout(3)]
	public class WeaponExpDB : ScriptableObject
	{
		// Token: 0x06001617 RID: 5655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C7")]
		[Address(RVA = "0x10161CDA8", Offset = "0x161CDA8", VA = "0x10161CDA8")]
		public WeaponExpDB()
		{
		}

		// Token: 0x040025E4 RID: 9700
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F4E")]
		public WeaponExpDB_Param[] m_Params;
	}
}
