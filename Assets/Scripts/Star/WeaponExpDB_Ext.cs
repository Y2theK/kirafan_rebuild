﻿namespace Star {
    public static class WeaponExpDB_Ext {
        public static int GetNextExp(this WeaponExpDB self, int currentLv, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                int lvIdx = idx + currentLv - 1;
                if (lvIdx >= 0 && lvIdx < self.m_Params.Length) {
                    return self.m_Params[lvIdx].m_NextExp;
                }
            }
            return 0;
        }

        public static long[] GetNextMaxExps(this WeaponExpDB self, int lv_a, int lv_b, sbyte expTableID) {
            int lvs = lv_b - lv_a + 1;
            long[] array = null;
            if (lvs > 0) {
                array = new long[lvs];
                for (int i = 0; i < lvs; i++) {
                    array[i] = self.GetNextExp(lv_a + i, expTableID);
                }
            }
            return array;
        }

        public static int GetUpgradeAmount(this WeaponExpDB self, int currentLv, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                int lvIdx = idx + currentLv - 1;
                if (lvIdx >= 0 && lvIdx < self.m_Params.Length) {
                    return self.m_Params[lvIdx].m_UpgradeAmount;
                }
            }
            return 0;
        }
    }
}
