﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000613 RID: 1555
	[Token(Token = "0x2000506")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct WeaponExpDB_Param
	{
		// Token: 0x040025E0 RID: 9696
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F4A")]
		public int m_ID;

		// Token: 0x040025E1 RID: 9697
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001F4B")]
		public int m_Lv;

		// Token: 0x040025E2 RID: 9698
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F4C")]
		public int m_NextExp;

		// Token: 0x040025E3 RID: 9699
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001F4D")]
		public int m_UpgradeAmount;
	}
}
