﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000616 RID: 1558
	[Token(Token = "0x2000509")]
	[StructLayout(3)]
	public class WeaponListDB : ScriptableObject
	{
		// Token: 0x06001618 RID: 5656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C8")]
		[Address(RVA = "0x10161D0D8", Offset = "0x161D0D8", VA = "0x10161D0D8")]
		public WeaponListDB()
		{
		}

		// Token: 0x04002602 RID: 9730
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F6C")]
		public WeaponListDB_Param[] m_Params;
	}
}
