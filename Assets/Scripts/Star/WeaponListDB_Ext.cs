﻿using System.Collections.Generic;

namespace Star {
    public static class WeaponListDB_Ext {
        public static bool IsCharacterWeapon(this WeaponListDB_Param self) {
            return self.m_EquipableCharaID != -1;
        }

        public static bool IsCharacterWeapon(this WeaponListDB_Param self, int characterID) {
            return self.m_EquipableCharaID == characterID;
        }

        public static WeaponListDB_Param GetParam(this WeaponListDB self, int weaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == weaponID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static WeaponListDB_Param? GetParamNullable(this WeaponListDB self, int weaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == weaponID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static List<WeaponListDB_Param> GetParamsByEquipableCharaID(this WeaponListDB self, int characterID) {
            List<WeaponListDB_Param> dbParams = new List<WeaponListDB_Param>();
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_EquipableCharaID == characterID) {
                    dbParams.Add(self.m_Params[i]);
                }
            }
            return dbParams;
        }

        public static bool IsExistParam(this WeaponListDB self, int weaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == weaponID) {
                    return true;
                }
            }
            return false;
        }

        public static bool IsCharacterWeapon(this WeaponListDB self, int weaponID) {
            WeaponListDB_Param? param = GetParamNullable(self, weaponID);
            return param.HasValue && param.Value.IsCharacterWeapon();
        }

        public static bool IsBonusItem(this WeaponListDB self, int weaponID, int itemID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == weaponID) {
                    int[] itemIds = self.m_Params[i].m_UpgradeBonusMaterialItemIDs;
                    for (int j = 0; j < itemIds.Length; j++) {
                        if (itemIds[i] == itemID) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
