﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000615 RID: 1557
	[Token(Token = "0x2000508")]
	[Serializable]
	[StructLayout(0, Size = 128)]
	public struct WeaponListDB_Param
	{
		// Token: 0x040025E5 RID: 9701
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F4F")]
		public int m_ID;

		// Token: 0x040025E6 RID: 9702
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F50")]
		public string m_WeaponName;

		// Token: 0x040025E7 RID: 9703
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F51")]
		public int m_Rare;

		// Token: 0x040025E8 RID: 9704
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001F52")]
		public int m_ClassType;

		// Token: 0x040025E9 RID: 9705
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F53")]
		public int m_Cost;

		// Token: 0x040025EA RID: 9706
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001F54")]
		public int m_ControllType;

		// Token: 0x040025EB RID: 9707
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001F55")]
		public int m_ResourceID_L;

		// Token: 0x040025EC RID: 9708
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001F56")]
		public int m_ResourceID_R;

		// Token: 0x040025ED RID: 9709
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001F57")]
		public int m_ClassAnimType;

		// Token: 0x040025EE RID: 9710
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001F58")]
		public sbyte m_ExpTableID;

		// Token: 0x040025EF RID: 9711
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001F59")]
		public int m_InitLv;

		// Token: 0x040025F0 RID: 9712
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001F5A")]
		public int m_LimitLv;

		// Token: 0x040025F1 RID: 9713
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001F5B")]
		public int m_EvolvedCount;

		// Token: 0x040025F2 RID: 9714
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001F5C")]
		public int m_InitAtk;

		// Token: 0x040025F3 RID: 9715
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001F5D")]
		public int m_InitMgc;

		// Token: 0x040025F4 RID: 9716
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001F5E")]
		public int m_InitDef;

		// Token: 0x040025F5 RID: 9717
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001F5F")]
		public int m_InitMDef;

		// Token: 0x040025F6 RID: 9718
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4001F60")]
		public int m_MaxAtk;

		// Token: 0x040025F7 RID: 9719
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4001F61")]
		public int m_MaxMgc;

		// Token: 0x040025F8 RID: 9720
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4001F62")]
		public int m_MaxDef;

		// Token: 0x040025F9 RID: 9721
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4001F63")]
		public int m_MaxMDef;

		// Token: 0x040025FA RID: 9722
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4001F64")]
		public int m_SkillID;

		// Token: 0x040025FB RID: 9723
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4001F65")]
		public int m_SkillLimitLv;

		// Token: 0x040025FC RID: 9724
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4001F66")]
		public sbyte m_SkillExpTableID;

		// Token: 0x040025FD RID: 9725
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4001F67")]
		public int m_SaleAmount;

		// Token: 0x040025FE RID: 9726
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4001F68")]
		public byte m_CanSell;

		// Token: 0x040025FF RID: 9727
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4001F69")]
		public int[] m_UpgradeBonusMaterialItemIDs;

		// Token: 0x04002600 RID: 9728
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4001F6A")]
		public int m_PassiveSkillID;

		// Token: 0x04002601 RID: 9729
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4001F6B")]
		public int m_EquipableCharaID;
	}
}
