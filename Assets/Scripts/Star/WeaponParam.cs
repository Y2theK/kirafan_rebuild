﻿using System;

namespace Star {
    [Serializable]
    public class WeaponParam {
        public long MngID { get; set; }
        public int WeaponID { get; set; }
        public int ResourceID_L { get; set; }
        public int ResourceID_R { get; set; }
        public eWeaponControllType ControllType { get; set; }
        public int ClassAnimType { get; set; }
        public int Lv { get; set; }
        public int MaxLv { get; set; }
        public long Exp { get; set; }
        public int Atk { get; set; }
        public int Mgc { get; set; }
        public int Def { get; set; }
        public int MDef { get; set; }
        public int Cost { get; set; }
        public SkillLearnData SkillLearnData { get; set; }
        public int PassiveSkillID { get; set; }

        public WeaponParam() {
            MngID = -1;
            WeaponID = -1;
            ResourceID_L = -1;
            ResourceID_R = -1;
            ControllType = eWeaponControllType.RightON;
            ClassAnimType = 0;
            Lv = 1;
            MaxLv = 1;
            PassiveSkillID = -1;
        }

        public int GetParam(BattleDefine.eStatus st) {
            return st switch {
                BattleDefine.eStatus.Atk => Atk,
                BattleDefine.eStatus.Mgc => Mgc,
                BattleDefine.eStatus.Def => Def,
                BattleDefine.eStatus.MDef => MDef,
                _ => 0,
            };
        }
    }
}
