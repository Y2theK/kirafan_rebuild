﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000619 RID: 1561
	[Token(Token = "0x200050C")]
	[StructLayout(3)]
	public class WeaponRecipeListDB : ScriptableObject
	{
		// Token: 0x06001619 RID: 5657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C9")]
		[Address(RVA = "0x10161DA04", Offset = "0x161DA04", VA = "0x10161DA04")]
		public WeaponRecipeListDB()
		{
		}

		// Token: 0x0400260C RID: 9740
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F76")]
		public WeaponRecipeListDB_Param[] m_Params;
	}
}
