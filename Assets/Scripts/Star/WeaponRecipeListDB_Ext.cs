﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000548 RID: 1352
	[Token(Token = "0x200043B")]
	[StructLayout(3)]
	public static class WeaponRecipeListDB_Ext
	{
		// Token: 0x060015D0 RID: 5584 RVA: 0x00009B70 File Offset: 0x00007D70
		[Token(Token = "0x6001480")]
		[Address(RVA = "0x10161DA0C", Offset = "0x161DA0C", VA = "0x10161DA0C")]
		public static WeaponRecipeListDB_Param GetParamWithRecipeID(this WeaponRecipeListDB self, int recipeID)
		{
			return default(WeaponRecipeListDB_Param);
		}

		// Token: 0x060015D1 RID: 5585 RVA: 0x00009B88 File Offset: 0x00007D88
		[Token(Token = "0x6001481")]
		[Address(RVA = "0x10161DB00", Offset = "0x161DB00", VA = "0x10161DB00")]
		public static WeaponRecipeListDB_Param GetParam(this WeaponRecipeListDB self, int weaponID)
		{
			return default(WeaponRecipeListDB_Param);
		}
	}
}
