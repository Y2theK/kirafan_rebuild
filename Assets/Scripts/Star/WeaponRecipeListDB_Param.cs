﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000618 RID: 1560
	[Token(Token = "0x200050B")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct WeaponRecipeListDB_Param
	{
		// Token: 0x04002607 RID: 9735
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F71")]
		public int m_ID;

		// Token: 0x04002608 RID: 9736
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001F72")]
		public int m_WeaponID;

		// Token: 0x04002609 RID: 9737
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F73")]
		public int m_Amount;

		// Token: 0x0400260A RID: 9738
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F74")]
		public int[] m_ItemIDs;

		// Token: 0x0400260B RID: 9739
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F75")]
		public int[] m_ItemNums;
	}
}
