﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061B RID: 1563
	[Token(Token = "0x200050E")]
	[StructLayout(3)]
	public class WeaponSkillUpItemsDB : ScriptableObject
	{
		// Token: 0x0600161A RID: 5658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014CA")]
		[Address(RVA = "0x10161DBF4", Offset = "0x161DBF4", VA = "0x10161DBF4")]
		public WeaponSkillUpItemsDB()
		{
		}

		// Token: 0x04002612 RID: 9746
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F7C")]
		public WeaponSkillUpItemsDB_Param[] m_Params;
	}
}
