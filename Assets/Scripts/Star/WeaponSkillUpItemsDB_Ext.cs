﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000549 RID: 1353
	[Token(Token = "0x200043C")]
	[StructLayout(3)]
	public static class WeaponSkillUpItemsDB_Ext
	{
		// Token: 0x060015D2 RID: 5586 RVA: 0x00009BA0 File Offset: 0x00007DA0
		[Token(Token = "0x6001482")]
		[Address(RVA = "0x10161DBFC", Offset = "0x161DBFC", VA = "0x10161DBFC")]
		public static WeaponSkillUpItemsDB_Param GetParamFromItemID(this WeaponSkillUpItemsDB self, int itemID)
		{
			return default(WeaponSkillUpItemsDB_Param);
		}
	}
}
