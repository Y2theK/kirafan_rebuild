﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200061A RID: 1562
	[Token(Token = "0x200050D")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct WeaponSkillUpItemsDB_Param
	{
		// Token: 0x0400260D RID: 9741
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F77")]
		public int m_ID;

		// Token: 0x0400260E RID: 9742
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001F78")]
		public int m_ItemID;

		// Token: 0x0400260F RID: 9743
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F79")]
		public int m_AddExp;

		// Token: 0x04002610 RID: 9744
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001F7A")]
		public sbyte m_flgForUniqueWpn;

		// Token: 0x04002611 RID: 9745
		[Cpp2IlInjected.FieldOffset(Offset = "0xD")]
		[Token(Token = "0x4001F7B")]
		public sbyte m_flgForCommonWpn;
	}
}
