﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000BFF RID: 3071
	[Token(Token = "0x2000841")]
	[StructLayout(3)]
	public class WeaponUnlock
	{
		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06003617 RID: 13847 RVA: 0x00016D28 File Offset: 0x00014F28
		[Token(Token = "0x170003AF")]
		public bool IsReceived
		{
			[Token(Token = "0x6003130")]
			[Address(RVA = "0x10161DC90", Offset = "0x161DC90", VA = "0x10161DC90")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06003618 RID: 13848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003131")]
		[Address(RVA = "0x10161DCFC", Offset = "0x161DCFC", VA = "0x10161DCFC")]
		public WeaponUnlock()
		{
		}

		// Token: 0x06003619 RID: 13849 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003132")]
		[Address(RVA = "0x10161DD74", Offset = "0x161DD74", VA = "0x10161DD74")]
		public List<WeaponUnlock.ReceivedData> GetReceivedDatas()
		{
			return null;
		}

		// Token: 0x0600361A RID: 13850 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003133")]
		[Address(RVA = "0x10161DD7C", Offset = "0x161DD7C", VA = "0x10161DD7C")]
		public WeaponUnlock.ReceivedData GetReceivedData(int weaponID)
		{
			return null;
		}

		// Token: 0x0600361B RID: 13851 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003134")]
		[Address(RVA = "0x10161DE7C", Offset = "0x161DE7C", VA = "0x10161DE7C")]
		public List<UserCharacterData> GetReceivedUserCharaDatas()
		{
			return null;
		}

		// Token: 0x0600361C RID: 13852 RVA: 0x00016D40 File Offset: 0x00014F40
		[Token(Token = "0x6003135")]
		[Address(RVA = "0x10161DFE8", Offset = "0x161DFE8", VA = "0x10161DFE8")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x0600361D RID: 13853 RVA: 0x00016D58 File Offset: 0x00014F58
		[Token(Token = "0x6003136")]
		[Address(RVA = "0x10161E2E0", Offset = "0x161E2E0", VA = "0x10161E2E0")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600361E RID: 13854 RVA: 0x00016D70 File Offset: 0x00014F70
		[Token(Token = "0x6003137")]
		[Address(RVA = "0x10161E2FC", Offset = "0x161E2FC", VA = "0x10161E2FC")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x0600361F RID: 13855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003138")]
		[Address(RVA = "0x10161EB80", Offset = "0x161EB80", VA = "0x10161EB80")]
		public void PlayBGM()
		{
		}

		// Token: 0x06003620 RID: 13856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003139")]
		[Address(RVA = "0x10161EF60", Offset = "0x161EF60", VA = "0x10161EF60")]
		public void StopBGM()
		{
		}

		// Token: 0x06003621 RID: 13857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600313A")]
		[Address(RVA = "0x10161E59C", Offset = "0x161E59C", VA = "0x10161E59C")]
		private void Request_WeaponReceive()
		{
		}

		// Token: 0x06003622 RID: 13858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600313B")]
		[Address(RVA = "0x10161F01C", Offset = "0x161F01C", VA = "0x10161F01C")]
		private void OnResponse_WeaponReceive(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x04004641 RID: 17985
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003146")]
		private WeaponUnlock.eStep m_Step;

		// Token: 0x04004642 RID: 17986
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003147")]
		private List<WeaponUnlock.ReceivedData> m_ReceivedDatas;

		// Token: 0x04004643 RID: 17987
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003148")]
		private WeaponUnlockEffectScene m_EffectScene;

		// Token: 0x04004644 RID: 17988
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003149")]
		private int m_EffectPlayIndex;

		// Token: 0x04004645 RID: 17989
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400314A")]
		private string m_SavedCueSheet;

		// Token: 0x04004646 RID: 17990
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400314B")]
		private string m_SavedCueName;

		// Token: 0x02000C00 RID: 3072
		[Token(Token = "0x2001087")]
		private enum eStep
		{
			// Token: 0x04004648 RID: 17992
			[Token(Token = "0x4006850")]
			None = -1,
			// Token: 0x04004649 RID: 17993
			[Token(Token = "0x4006851")]
			API,
			// Token: 0x0400464A RID: 17994
			[Token(Token = "0x4006852")]
			API_Wait,
			// Token: 0x0400464B RID: 17995
			[Token(Token = "0x4006853")]
			EffectPrepare,
			// Token: 0x0400464C RID: 17996
			[Token(Token = "0x4006854")]
			EffectPrepare_Wait,
			// Token: 0x0400464D RID: 17997
			[Token(Token = "0x4006855")]
			EffectPlay_Wait,
			// Token: 0x0400464E RID: 17998
			[Token(Token = "0x4006856")]
			PreEnd,
			// Token: 0x0400464F RID: 17999
			[Token(Token = "0x4006857")]
			End
		}

		// Token: 0x02000C01 RID: 3073
		[Token(Token = "0x2001088")]
		public class ReceivedData
		{
			// Token: 0x06003623 RID: 13859 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600619D")]
			[Address(RVA = "0x10161E2C0", Offset = "0x161E2C0", VA = "0x10161E2C0")]
			public ReceivedData()
			{
			}

			// Token: 0x04004650 RID: 18000
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006858")]
			public int m_WeaponID;

			// Token: 0x04004651 RID: 18001
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006859")]
			public long m_WeaponMngID;

			// Token: 0x04004652 RID: 18002
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400685A")]
			public int m_CharaID;

			// Token: 0x04004653 RID: 18003
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400685B")]
			public long m_CharaMngID;
		}
	}
}
