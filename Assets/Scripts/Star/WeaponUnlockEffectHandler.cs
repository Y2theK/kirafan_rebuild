﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000C03 RID: 3075
	[Token(Token = "0x2000842")]
	[StructLayout(3)]
	public class WeaponUnlockEffectHandler : MonoBehaviour
	{
		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x06003627 RID: 13863 RVA: 0x00016DA0 File Offset: 0x00014FA0
		[Token(Token = "0x170003B0")]
		public WeaponUnlockEffectHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x600313C")]
			[Address(RVA = "0x10161F2F8", Offset = "0x161F2F8", VA = "0x10161F2F8")]
			get
			{
				return WeaponUnlockEffectHandler.eAnimType.Effect;
			}
		}

		// Token: 0x06003628 RID: 13864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600313D")]
		[Address(RVA = "0x10161F300", Offset = "0x161F300", VA = "0x10161F300")]
		private void Awake()
		{
		}

		// Token: 0x06003629 RID: 13865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600313E")]
		[Address(RVA = "0x10161F4F4", Offset = "0x161F4F4", VA = "0x10161F4F4")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600362A RID: 13866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600313F")]
		[Address(RVA = "0x10161F4F8", Offset = "0x161F4F8", VA = "0x10161F4F8")]
		private void Update()
		{
		}

		// Token: 0x0600362B RID: 13867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003140")]
		[Address(RVA = "0x10161F714", Offset = "0x161F714", VA = "0x10161F714")]
		public void ApplyTextures(Texture tex_charaIllust, Texture tex_weaponIcon)
		{
		}

		// Token: 0x0600362C RID: 13868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003141")]
		[Address(RVA = "0x10161F628", Offset = "0x161F628", VA = "0x10161F628")]
		public void Play(WeaponUnlockEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x0600362D RID: 13869 RVA: 0x00016DB8 File Offset: 0x00014FB8
		[Token(Token = "0x6003142")]
		[Address(RVA = "0x10161F54C", Offset = "0x161F54C", VA = "0x10161F54C")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x0600362E RID: 13870 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003143")]
		[Address(RVA = "0x10161F7B8", Offset = "0x161F7B8", VA = "0x10161F7B8")]
		private string GetAnimPlayKey(WeaponUnlockEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x0600362F RID: 13871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003144")]
		[Address(RVA = "0x10161F808", Offset = "0x161F808", VA = "0x10161F808")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x06003630 RID: 13872 RVA: 0x00016DD0 File Offset: 0x00014FD0
		[Token(Token = "0x6003145")]
		[Address(RVA = "0x10161F860", Offset = "0x161F860", VA = "0x10161F860")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06003631 RID: 13873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003146")]
		[Address(RVA = "0x10161F890", Offset = "0x161F890", VA = "0x10161F890")]
		public WeaponUnlockEffectHandler()
		{
		}

		// Token: 0x04004656 RID: 18006
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400314C")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x04004657 RID: 18007
		[Token(Token = "0x400314D")]
		private const string OBJ_CHARA_ILLUST = "CharaIllust";

		// Token: 0x04004658 RID: 18008
		[Token(Token = "0x400314E")]
		private const string OBJ_WPN_ICON = "WPN_Icon";

		// Token: 0x04004659 RID: 18009
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400314F")]
		public Transform m_Transform;

		// Token: 0x0400465A RID: 18010
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003150")]
		public Animation m_Anim;

		// Token: 0x0400465B RID: 18011
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003151")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400465C RID: 18012
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003152")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x0400465D RID: 18013
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003153")]
		public MsbHandler m_MsbHndl;

		// Token: 0x0400465E RID: 18014
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003154")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x0400465F RID: 18015
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003155")]
		private WeaponUnlockEffectHandler.eAnimType m_CurrentAnimType;

		// Token: 0x02000C04 RID: 3076
		[Token(Token = "0x200108A")]
		public enum eAnimType
		{
			// Token: 0x04004661 RID: 18017
			[Token(Token = "0x400685F")]
			None = -1,
			// Token: 0x04004662 RID: 18018
			[Token(Token = "0x4006860")]
			Effect,
			// Token: 0x04004663 RID: 18019
			[Token(Token = "0x4006861")]
			Loop,
			// Token: 0x04004664 RID: 18020
			[Token(Token = "0x4006862")]
			Num
		}
	}
}
