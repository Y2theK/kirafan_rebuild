﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000C05 RID: 3077
	[Token(Token = "0x2000843")]
	[StructLayout(3)]
	public class WeaponUnlockEffectScene
	{
		// Token: 0x06003632 RID: 13874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003147")]
		[Address(RVA = "0x10161F9D0", Offset = "0x161F9D0", VA = "0x10161F9D0")]
		private void RequestAppearSe(bool isSkip)
		{
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x06003633 RID: 13875 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170003B1")]
		public WeaponUnlockEffectHandler EffectHndl
		{
			[Token(Token = "0x6003148")]
			[Address(RVA = "0x10161FBB0", Offset = "0x161FBB0", VA = "0x10161FBB0")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003634 RID: 13876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003149")]
		[Address(RVA = "0x10161E77C", Offset = "0x161E77C", VA = "0x10161E77C")]
		public WeaponUnlockEffectScene()
		{
		}

		// Token: 0x06003635 RID: 13877 RVA: 0x00016DE8 File Offset: 0x00014FE8
		[Token(Token = "0x600314A")]
		[Address(RVA = "0x10161ED3C", Offset = "0x161ED3C", VA = "0x10161ED3C")]
		public bool Destroy()
		{
			return default(bool);
		}

		// Token: 0x06003636 RID: 13878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600314B")]
		[Address(RVA = "0x10161E580", Offset = "0x161E580", VA = "0x10161E580")]
		public void Update()
		{
		}

		// Token: 0x06003637 RID: 13879 RVA: 0x00016E00 File Offset: 0x00015000
		[Token(Token = "0x600314C")]
		[Address(RVA = "0x10161E948", Offset = "0x161E948", VA = "0x10161E948")]
		public bool Prepare(int charaID, int weaponID, Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x06003638 RID: 13880 RVA: 0x00016E18 File Offset: 0x00015018
		[Token(Token = "0x600314D")]
		[Address(RVA = "0x10161EB70", Offset = "0x161EB70", VA = "0x10161EB70")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06003639 RID: 13881 RVA: 0x00016E30 File Offset: 0x00015030
		[Token(Token = "0x600314E")]
		[Address(RVA = "0x10161EB60", Offset = "0x161EB60", VA = "0x10161EB60")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x0600363A RID: 13882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600314F")]
		[Address(RVA = "0x10161FBB8", Offset = "0x161FBB8", VA = "0x10161FBB8")]
		private void Update_Prepare()
		{
		}

		// Token: 0x0600363B RID: 13883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003150")]
		[Address(RVA = "0x10161EC80", Offset = "0x161EC80", VA = "0x10161EC80")]
		public void Play()
		{
		}

		// Token: 0x0600363C RID: 13884 RVA: 0x00016E48 File Offset: 0x00015048
		[Token(Token = "0x6003151")]
		[Address(RVA = "0x10161ED2C", Offset = "0x161ED2C", VA = "0x10161ED2C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600363D RID: 13885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003152")]
		[Address(RVA = "0x101620080", Offset = "0x1620080", VA = "0x101620080")]
		private void Update_Main()
		{
		}

		// Token: 0x04004665 RID: 18021
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003156")]
		private WeaponUnlockEffectScene.eMode m_Mode;

		// Token: 0x04004666 RID: 18022
		[Token(Token = "0x4003157")]
		private const string RESOURCE_PATH = "weaponunlock.muast";

		// Token: 0x04004667 RID: 18023
		[Token(Token = "0x4003158")]
		private const string OBJECT_PATH = "weapon_unlock";

		// Token: 0x04004668 RID: 18024
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4003159")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x04004669 RID: 18025
		[Cpp2IlInjected.FieldOffset(Offset = "0x15")]
		[Token(Token = "0x400315A")]
		private bool m_LoadedGachaPlayCueSheet;

		// Token: 0x0400466A RID: 18026
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400315B")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400466B RID: 18027
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400315C")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x0400466C RID: 18028
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400315D")]
		private SpriteHandler m_SpriteHndl_CharaIllust;

		// Token: 0x0400466D RID: 18029
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400315E")]
		private SpriteHandler m_SpriteHndl_WeaponIcon;

		// Token: 0x0400466E RID: 18030
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400315F")]
		private GameObject m_EffectGO;

		// Token: 0x0400466F RID: 18031
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003160")]
		private WeaponUnlockEffectHandler m_EffectHndl;

		// Token: 0x04004670 RID: 18032
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003161")]
		private Transform m_ParentTransform;

		// Token: 0x04004671 RID: 18033
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003162")]
		private Vector3 m_Position;

		// Token: 0x04004672 RID: 18034
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003163")]
		private int m_CharaID;

		// Token: 0x04004673 RID: 18035
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003164")]
		private int m_WeaponID;

		// Token: 0x04004674 RID: 18036
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003165")]
		private SoundSeRequest[] m_SeRequests;

		// Token: 0x04004675 RID: 18037
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003166")]
		private WeaponUnlockEffectScene.ePrepareStep m_PrepareStep;

		// Token: 0x04004676 RID: 18038
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003167")]
		private WeaponUnlockEffectScene.eMainStep m_MainStep;

		// Token: 0x02000C06 RID: 3078
		[Token(Token = "0x200108B")]
		public enum eMode
		{
			// Token: 0x04004678 RID: 18040
			[Token(Token = "0x4006864")]
			None = -1,
			// Token: 0x04004679 RID: 18041
			[Token(Token = "0x4006865")]
			Prepare,
			// Token: 0x0400467A RID: 18042
			[Token(Token = "0x4006866")]
			UpdateMain,
			// Token: 0x0400467B RID: 18043
			[Token(Token = "0x4006867")]
			Destroy
		}

		// Token: 0x02000C07 RID: 3079
		[Token(Token = "0x200108C")]
		private enum eSE
		{
			// Token: 0x0400467D RID: 18045
			[Token(Token = "0x4006869")]
			APPEAR,
			// Token: 0x0400467E RID: 18046
			[Token(Token = "0x400686A")]
			APPEAR_LOOP,
			// Token: 0x0400467F RID: 18047
			[Token(Token = "0x400686B")]
			Num
		}

		// Token: 0x02000C08 RID: 3080
		[Token(Token = "0x200108D")]
		private enum ePrepareStep
		{
			// Token: 0x04004681 RID: 18049
			[Token(Token = "0x400686D")]
			None = -1,
			// Token: 0x04004682 RID: 18050
			[Token(Token = "0x400686E")]
			Prepare,
			// Token: 0x04004683 RID: 18051
			[Token(Token = "0x400686F")]
			Prepare_Wait,
			// Token: 0x04004684 RID: 18052
			[Token(Token = "0x4006870")]
			End,
			// Token: 0x04004685 RID: 18053
			[Token(Token = "0x4006871")]
			Prepare_Error
		}

		// Token: 0x02000C09 RID: 3081
		[Token(Token = "0x200108E")]
		private enum eMainStep
		{
			// Token: 0x04004687 RID: 18055
			[Token(Token = "0x4006873")]
			None = -1,
			// Token: 0x04004688 RID: 18056
			[Token(Token = "0x4006874")]
			Play,
			// Token: 0x04004689 RID: 18057
			[Token(Token = "0x4006875")]
			Play_Wait,
			// Token: 0x0400468A RID: 18058
			[Token(Token = "0x4006876")]
			End
		}
	}
}
