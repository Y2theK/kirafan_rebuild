﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061D RID: 1565
	[Token(Token = "0x2000510")]
	[StructLayout(3)]
	public class WebDataListDB : ScriptableObject
	{
		// Token: 0x0600161B RID: 5659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014CB")]
		[Address(RVA = "0x10162028C", Offset = "0x162028C", VA = "0x10162028C")]
		public WebDataListDB()
		{
		}

		// Token: 0x04002615 RID: 9749
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F7F")]
		public WebDataListDB_Param[] m_Params;
	}
}
