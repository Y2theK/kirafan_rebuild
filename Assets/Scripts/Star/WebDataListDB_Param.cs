﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200061C RID: 1564
	[Token(Token = "0x200050F")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct WebDataListDB_Param
	{
		// Token: 0x04002613 RID: 9747
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F7D")]
		public int m_ID;

		// Token: 0x04002614 RID: 9748
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F7E")]
		public string m_WebAdress;
	}
}
