﻿namespace Star {
    public class WebDataListUtil {
        public const string KRRUID = "?auser_id={0}";

        public static string GetIDToAdress(int accessId) {
            WebDataListDB webDataListDB = GameSystem.Inst.DbMng.WebDataListDB;
            if (webDataListDB != null) {
                int num = webDataListDB.m_Params.Length;
                for (int i = 0; i < num; i++) {
                    if (webDataListDB.m_Params[i].m_ID == accessId) {
                        return webDataListDB.m_Params[i].m_WebAdress;
                    }
                }
            }
            return null;
        }

        public static string GetIDToAdressKRRUID(int accessId) {
            string address = GetIDToAdress(accessId);
            if (string.IsNullOrEmpty(address)) {
                return address;
            }

            address += string.Format(KRRUID, GameSystem.Inst.UserDataMng.UserData.MyCode);
            return address;
        }
    }
}
