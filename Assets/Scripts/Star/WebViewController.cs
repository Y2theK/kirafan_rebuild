﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Star {
    public class WebViewController : MonoBehaviour {
        private WebViewObject m_WebViewObject;
        private bool m_Visibility;
        private bool m_SystemVisible = true;
        private string m_URL;
        private Rect m_Margin;

        private Action<string> m_CallBack;

        public bool IsActiveWebViewObj() {
            return m_WebViewObject != null;
        }

        private Rect TranslateCMRectToMargin(RectTransform target, Camera uiCamera) {
            Vector2 pos = uiCamera.WorldToScreenPoint(target.position);
            pos.y = uiCamera.pixelHeight - pos.y;

            float res = 1334f;
            CanvasScaler scaler = GetComponentInParent<CanvasScaler>();
            if (scaler != null) {
                res = scaler.referenceResolution.x;
            }
            res = Screen.width / res;

            Vector2 sizeDelta = target.sizeDelta;
            sizeDelta.x *= res * target.localScale.x;
            sizeDelta.y *= res * target.localScale.y;

            Rect rect = new Rect();
            rect.xMin = pos.x - sizeDelta.x / 2f;
            rect.xMax = Screen.width - (rect.xMin + sizeDelta.x);
            rect.yMin = pos.y - sizeDelta.y / 2f;
            rect.yMax = Screen.height - (rect.yMin + sizeDelta.y);
            return rect;
        }

        public void LoadURL(string url, RectTransform rect, Action<string> callback = null) {
            Rect rect2 = TranslateCMRectToMargin(rect, GameSystem.Inst.UICamera);
            LoadURL(url, (int)rect2.xMin, (int)rect2.yMin, (int)rect2.xMax, (int)rect2.yMax, callback);
        }

        public void LoadURL(string url, int marginLeft, int marginTop, int marginRight, int marginBottom, Action<string> callback = null) {
            if (!CreateAndOldDestroyWebViewObject()) {
                return;
            }
            m_URL = url;
            m_Margin = Rect.MinMaxRect(marginLeft, marginTop, marginRight, marginBottom);
            m_CallBack = callback;
            m_WebViewObject.Init(cb: (msg) => {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
                if (callback != null) {
                    callback(msg);
                }
            }, err: (msg) => {
                Debug.Log(string.Format("CallOnError[{0}]", msg));
            }, ld: (msg) => {
                Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
                m_WebViewObject.EvaluateJS(@"
                    if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                        window.Unity = {
                            call: function(msg) {
                                window.webkit.messageHandlers.unityControl.postMessage(msg);
                            }
                        }
                    } else {
                        window.Unity = {
                            call: function(msg) {
                                window.location = 'unity:' + msg;
                            }
                        }
                    }

                    window.addEventListener('click', function() {}, false);
                ");
            });
            SetMargins(marginLeft, marginTop, marginRight, marginBottom);
            m_WebViewObject.SetTextZoom(100);
            SetVisibility(true);
            Load(url);
        }

        private void Load(string Url) {
            if (m_WebViewObject == null) {
                return;
            }

            if (Url.StartsWith("http")) {
                m_WebViewObject.LoadURL(Url.Replace(" ", "%20"));
            }

            string[] extensions = new string[] { ".jpg", ".js", ".html" };
            for (int i = 0; i < extensions.Length; i++) {
                string ext = extensions[i];
                string url = Url.Replace(".html", ext);
                string streaming = Path.Combine(Application.streamingAssetsPath, url);
                string persistent = Path.Combine(Application.persistentDataPath, url);
                byte[] bytes;
                if (streaming.Contains("://")) { //TODO: may need a try/catch
                    bytes = UnityWebRequest.Get(streaming).downloadHandler.data;
                } else {
                    bytes = File.ReadAllBytes(streaming);
                }
                File.WriteAllBytes(persistent, bytes);
                if (ext == ".html") {
                    m_WebViewObject.LoadURL($"file://{persistent.Replace(" ", "%20")}");
                }
            }
        }

        private void LateUpdate() {
            if (m_WebViewObject != null && GameSystem.Inst != null && GameSystem.Inst.IsAvailable()) {
                if (GameSystem.Inst.CmnMsgWindow.IsActive) {
                    if (m_SystemVisible) {
                        SetSystemVisibility(false);
                    }
                } else if (!m_SystemVisible) {
                    SetSystemVisibility(true);
                }
            }
        }

        private void SetSystemVisibility(bool flg) {
            if (m_WebViewObject != null) {
                if (flg && !m_SystemVisible) {
                    m_SystemVisible = flg;
                    CreateAndOldDestroyWebViewObject();
                    LoadURL(m_URL, (int)m_Margin.xMin, (int)m_Margin.yMin, (int)m_Margin.xMax, (int)m_Margin.yMax, m_CallBack);
                } else if (!flg && m_SystemVisible) {
                    m_SystemVisible = flg;
                    if (m_WebViewObject != null) {
                        m_WebViewObject.SetVisibility(m_SystemVisible && m_Visibility);
                    }
                }
            }
        }

        public void SetVisibility(bool flg) {
            if (m_WebViewObject != null) {
                m_Visibility = flg;
                m_WebViewObject.SetVisibility(m_SystemVisible && m_Visibility);
            }
        }

        public void SetMargins(WebViewMargins margins) {
            SetMargins(margins.m_Left, margins.m_Top, margins.m_Right, margins.m_Bottom);
        }

        public void SetMargins(int left, int top, int right, int bottom) {
            if (m_WebViewObject != null) {
                m_WebViewObject.SetMargins(left, top, right, bottom);
            }
        }

        private bool CreateAndOldDestroyWebViewObject() {
            DestroyWebViewObject();
            return AcquireWebViewObject();
        }

        private WebViewObject CreateWebViewObject() {
            GameObject gameObject = new GameObject("WebViewObject");
            if (gameObject != null) {
                return gameObject.AddComponent<WebViewObject>();
            }
            return null;
        }

        public bool DestroyWebViewObject() {
            if (m_WebViewObject == null) {
                return true;
            }
            if (m_WebViewObject.gameObject != null) {
                Destroy(m_WebViewObject.gameObject);
            } else {
                Destroy(m_WebViewObject);
            }
            m_WebViewObject = null;
            return true;
        }

        public Rect MarginToRect(WebViewMargins margins) {
            return Rect.MinMaxRect(margins.m_Left, margins.m_Bottom - Screen.height, Screen.width - margins.m_Right, -margins.m_Top);
        }

        private bool AcquireWebViewObject() {
            if (m_WebViewObject != null) {
                return true;
            }
            m_WebViewObject = CreateWebViewObject();
            return m_WebViewObject != null;
        }

        private string UserAgent() {
#if UNITY_IOS
            return $"iPhone/{Device.generation}";
#elif UNITY_ANDROID
            return $"Android/{SystemInfo.deviceModel}";
#else
            return string.Empty;
#endif
        }
    }
}
