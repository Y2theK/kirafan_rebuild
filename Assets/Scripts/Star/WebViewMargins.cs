﻿namespace Star {
    public class WebViewMargins {
        public int m_Left;
        public int m_Top;
        public int m_Right;
        public int m_Bottom;

        public WebViewMargins(int left, int top, int right, int bottom) {
            m_Left = left;
            m_Top = top;
            m_Right = right;
            m_Bottom = bottom;
        }

        public WebViewMargins() { }
    }
}
