﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000620 RID: 1568
	[Token(Token = "0x2000513")]
	[StructLayout(3)]
	public class WordLibraryListDB : ScriptableObject
	{
		// Token: 0x0600161F RID: 5663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014CF")]
		[Address(RVA = "0x101621C10", Offset = "0x1621C10", VA = "0x101621C10")]
		public WordLibraryListDB()
		{
		}

		// Token: 0x04002619 RID: 9753
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F83")]
		public WordLibraryListDB_Param[] m_Params;
	}
}
