﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200061F RID: 1567
	[Token(Token = "0x2000512")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct WordLibraryListDB_Param
	{
		// Token: 0x04002617 RID: 9751
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F81")]
		public string m_TitleName;

		// Token: 0x04002618 RID: 9752
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F82")]
		public string m_Descript;
	}
}
