﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BDC RID: 3036
	[Token(Token = "0x2000833")]
	[StructLayout(3)]
	public class Wrapper<MemberType>
	{
		// Token: 0x06003531 RID: 13617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003073")]
		[Address(RVA = "0x1016D5410", Offset = "0x16D5410", VA = "0x1016D5410")]
		public Wrapper()
		{
		}

		// Token: 0x0400456B RID: 17771
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40030EC")]
		public MemberType value;
	}
}
