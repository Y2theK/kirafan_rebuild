﻿namespace Star {
    public enum eADVCategory {
        None,
        Story,
        Event,
        Chara,
        Cross,
        Weapon,
        Other,
        Writer,
        Num
    }
}
