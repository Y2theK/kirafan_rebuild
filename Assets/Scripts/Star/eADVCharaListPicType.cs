﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000356 RID: 854
	[Token(Token = "0x20002D3")]
	[StructLayout(3, Size = 4)]
	public enum eADVCharaListPicType
	{
		// Token: 0x04000CAA RID: 3242
		[Token(Token = "0x4000A0A")]
		StandPic,
		// Token: 0x04000CAB RID: 3243
		[Token(Token = "0x4000A0B")]
		Face
	}
}
