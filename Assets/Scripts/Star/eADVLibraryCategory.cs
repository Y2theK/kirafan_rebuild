﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004A4 RID: 1188
	[Token(Token = "0x200039C")]
	[StructLayout(3, Size = 4)]
	public enum eADVLibraryCategory
	{
		// Token: 0x04001620 RID: 5664
		[Token(Token = "0x4001006")]
		None,
		// Token: 0x04001621 RID: 5665
		[Token(Token = "0x4001007")]
		Story,
		// Token: 0x04001622 RID: 5666
		[Token(Token = "0x4001008")]
		Event,
		// Token: 0x04001623 RID: 5667
		[Token(Token = "0x4001009")]
		Writer,
		// Token: 0x04001624 RID: 5668
		[Token(Token = "0x400100A")]
		Num
	}
}
