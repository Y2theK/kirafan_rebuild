﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200035D RID: 861
	[Token(Token = "0x20002DA")]
	[StructLayout(3, Size = 4)]
	public enum eADVMotAction
	{
		// Token: 0x04000CCC RID: 3276
		[Token(Token = "0x4000A2C")]
		None,
		// Token: 0x04000CCD RID: 3277
		[Token(Token = "0x4000A2D")]
		Move,
		// Token: 0x04000CCE RID: 3278
		[Token(Token = "0x4000A2E")]
		Scale
	}
}
