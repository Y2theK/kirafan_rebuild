﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000348 RID: 840
	[Token(Token = "0x20002C6")]
	[StructLayout(3, Size = 4)]
	public enum eActionAggregationState
	{
		// Token: 0x04000C7C RID: 3196
		[Token(Token = "0x40009E6")]
		Wait,
		// Token: 0x04000C7D RID: 3197
		[Token(Token = "0x40009E7")]
		Play,
		// Token: 0x04000C7E RID: 3198
		[Token(Token = "0x40009E8")]
		Finish
	}
}
