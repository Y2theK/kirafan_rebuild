﻿namespace Star {
    public enum eAgeType {
        None,
        Age15,
        Age16_19,
        Age20
    }
}
