﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000371 RID: 881
	[Token(Token = "0x20002E7")]
	[StructLayout(3, Size = 4)]
	public enum eBattleAICommandConditionType
	{
		// Token: 0x04000D36 RID: 3382
		[Token(Token = "0x4000A72")]
		Self_HpRange,
		// Token: 0x04000D37 RID: 3383
		[Token(Token = "0x4000A73")]
		Self_StateAbnormal,
		// Token: 0x04000D38 RID: 3384
		[Token(Token = "0x4000A74")]
		Self_StatusChange,
		// Token: 0x04000D39 RID: 3385
		[Token(Token = "0x4000A75")]
		Self_ChargeCount,
		// Token: 0x04000D3A RID: 3386
		[Token(Token = "0x4000A76")]
		Self_Reserve_4,
		// Token: 0x04000D3B RID: 3387
		[Token(Token = "0x4000A77")]
		Self_Reserve_5,
		// Token: 0x04000D3C RID: 3388
		[Token(Token = "0x4000A78")]
		Self_Reserve_6,
		// Token: 0x04000D3D RID: 3389
		[Token(Token = "0x4000A79")]
		Self_Reserve_7,
		// Token: 0x04000D3E RID: 3390
		[Token(Token = "0x4000A7A")]
		Self_Reserve_8,
		// Token: 0x04000D3F RID: 3391
		[Token(Token = "0x4000A7B")]
		Self_Reserve_9,
		// Token: 0x04000D40 RID: 3392
		[Token(Token = "0x4000A7C")]
		Self_Reserve_10,
		// Token: 0x04000D41 RID: 3393
		[Token(Token = "0x4000A7D")]
		Self_Reserve_11,
		// Token: 0x04000D42 RID: 3394
		[Token(Token = "0x4000A7E")]
		Self_Reserve_12,
		// Token: 0x04000D43 RID: 3395
		[Token(Token = "0x4000A7F")]
		Self_Reserve_13,
		// Token: 0x04000D44 RID: 3396
		[Token(Token = "0x4000A80")]
		Self_Reserve_14,
		// Token: 0x04000D45 RID: 3397
		[Token(Token = "0x4000A81")]
		Self_Reserve_15,
		// Token: 0x04000D46 RID: 3398
		[Token(Token = "0x4000A82")]
		Self_Reserve_16,
		// Token: 0x04000D47 RID: 3399
		[Token(Token = "0x4000A83")]
		Self_Reserve_17,
		// Token: 0x04000D48 RID: 3400
		[Token(Token = "0x4000A84")]
		Self_Reserve_18,
		// Token: 0x04000D49 RID: 3401
		[Token(Token = "0x4000A85")]
		Self_Reserve_19,
		// Token: 0x04000D4A RID: 3402
		[Token(Token = "0x4000A86")]
		Self_Reserve_20,
		// Token: 0x04000D4B RID: 3403
		[Token(Token = "0x4000A87")]
		MyParty_AliveNum,
		// Token: 0x04000D4C RID: 3404
		[Token(Token = "0x4000A88")]
		MyParty_Reserve_1,
		// Token: 0x04000D4D RID: 3405
		[Token(Token = "0x4000A89")]
		MyParty_Reserve_2,
		// Token: 0x04000D4E RID: 3406
		[Token(Token = "0x4000A8A")]
		MyParty_Reserve_3,
		// Token: 0x04000D4F RID: 3407
		[Token(Token = "0x4000A8B")]
		MyParty_Reserve_4,
		// Token: 0x04000D50 RID: 3408
		[Token(Token = "0x4000A8C")]
		MyParty_Reserve_5,
		// Token: 0x04000D51 RID: 3409
		[Token(Token = "0x4000A8D")]
		MyParty_Reserve_6,
		// Token: 0x04000D52 RID: 3410
		[Token(Token = "0x4000A8E")]
		MyParty_Reserve_7,
		// Token: 0x04000D53 RID: 3411
		[Token(Token = "0x4000A8F")]
		MyParty_Reserve_8,
		// Token: 0x04000D54 RID: 3412
		[Token(Token = "0x4000A90")]
		MyParty_Reserve_9,
		// Token: 0x04000D55 RID: 3413
		[Token(Token = "0x4000A91")]
		MyParty_Reserve_10,
		// Token: 0x04000D56 RID: 3414
		[Token(Token = "0x4000A92")]
		MyParty_Reserve_11,
		// Token: 0x04000D57 RID: 3415
		[Token(Token = "0x4000A93")]
		MyParty_Reserve_12,
		// Token: 0x04000D58 RID: 3416
		[Token(Token = "0x4000A94")]
		MyParty_Reserve_13,
		// Token: 0x04000D59 RID: 3417
		[Token(Token = "0x4000A95")]
		MyParty_Reserve_14,
		// Token: 0x04000D5A RID: 3418
		[Token(Token = "0x4000A96")]
		MyParty_Reserve_15,
		// Token: 0x04000D5B RID: 3419
		[Token(Token = "0x4000A97")]
		MyParty_Reserve_16,
		// Token: 0x04000D5C RID: 3420
		[Token(Token = "0x4000A98")]
		MyParty_Reserve_17,
		// Token: 0x04000D5D RID: 3421
		[Token(Token = "0x4000A99")]
		MyParty_Reserve_18,
		// Token: 0x04000D5E RID: 3422
		[Token(Token = "0x4000A9A")]
		MyParty_Reserve_19,
		// Token: 0x04000D5F RID: 3423
		[Token(Token = "0x4000A9B")]
		MyParty_Reserve_20,
		// Token: 0x04000D60 RID: 3424
		[Token(Token = "0x4000A9C")]
		TgtParty_AliveNum,
		// Token: 0x04000D61 RID: 3425
		[Token(Token = "0x4000A9D")]
		TgtParty_StateAbnormalCount,
		// Token: 0x04000D62 RID: 3426
		[Token(Token = "0x4000A9E")]
		TgtParty_NormalAttackUseCount,
		// Token: 0x04000D63 RID: 3427
		[Token(Token = "0x4000A9F")]
		TgtParty_SkillUseCount,
		// Token: 0x04000D64 RID: 3428
		[Token(Token = "0x4000AA0")]
		TgtParty_TogetherAttackUseCount,
		// Token: 0x04000D65 RID: 3429
		[Token(Token = "0x4000AA1")]
		TgtParty_MemberChangeCount,
		// Token: 0x04000D66 RID: 3430
		[Token(Token = "0x4000AA2")]
		TgtParty_ClassCount,
		// Token: 0x04000D67 RID: 3431
		[Token(Token = "0x4000AA3")]
		TgtParty_StatusChange,
		// Token: 0x04000D68 RID: 3432
		[Token(Token = "0x4000AA4")]
		TgtParty_DeadCount,
		// Token: 0x04000D69 RID: 3433
		[Token(Token = "0x4000AA5")]
		TgtParty_Reserve_4,
		// Token: 0x04000D6A RID: 3434
		[Token(Token = "0x4000AA6")]
		TgtParty_Reserve_5,
		// Token: 0x04000D6B RID: 3435
		[Token(Token = "0x4000AA7")]
		TgtParty_Reserve_6,
		// Token: 0x04000D6C RID: 3436
		[Token(Token = "0x4000AA8")]
		TgtParty_Reserve_7,
		// Token: 0x04000D6D RID: 3437
		[Token(Token = "0x4000AA9")]
		TgtParty_Reserve_8,
		// Token: 0x04000D6E RID: 3438
		[Token(Token = "0x4000AAA")]
		TgtParty_Reserve_9,
		// Token: 0x04000D6F RID: 3439
		[Token(Token = "0x4000AAB")]
		TgtParty_Reserve_10,
		// Token: 0x04000D70 RID: 3440
		[Token(Token = "0x4000AAC")]
		TgtParty_Reserve_11,
		// Token: 0x04000D71 RID: 3441
		[Token(Token = "0x4000AAD")]
		TgtParty_Reserve_12,
		// Token: 0x04000D72 RID: 3442
		[Token(Token = "0x4000AAE")]
		TgtParty_Reserve_13,
		// Token: 0x04000D73 RID: 3443
		[Token(Token = "0x4000AAF")]
		TgtParty_Reserve_14,
		// Token: 0x04000D74 RID: 3444
		[Token(Token = "0x4000AB0")]
		TgtParty_Reserve_15,
		// Token: 0x04000D75 RID: 3445
		[Token(Token = "0x4000AB1")]
		TgtParty_Reserve_16,
		// Token: 0x04000D76 RID: 3446
		[Token(Token = "0x4000AB2")]
		TgtParty_Reserve_17,
		// Token: 0x04000D77 RID: 3447
		[Token(Token = "0x4000AB3")]
		TgtParty_Reserve_18,
		// Token: 0x04000D78 RID: 3448
		[Token(Token = "0x4000AB4")]
		TgtParty_Reserve_19,
		// Token: 0x04000D79 RID: 3449
		[Token(Token = "0x4000AB5")]
		TgtParty_Reserve_20,
		// Token: 0x04000D7A RID: 3450
		[Token(Token = "0x4000AB6")]
		Flag,
		// Token: 0x04000D7B RID: 3451
		[Token(Token = "0x4000AB7")]
		Num
	}
}
