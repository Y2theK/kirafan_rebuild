﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000372 RID: 882
	[Token(Token = "0x20002E8")]
	[StructLayout(3, Size = 4)]
	public enum eBattleAICommandTargetSingleConditionType
	{
		// Token: 0x04000D7D RID: 3453
		[Token(Token = "0x4000AB9")]
		Dying,
		// Token: 0x04000D7E RID: 3454
		[Token(Token = "0x4000ABA")]
		Element,
		// Token: 0x04000D7F RID: 3455
		[Token(Token = "0x4000ABB")]
		Class,
		// Token: 0x04000D80 RID: 3456
		[Token(Token = "0x4000ABC")]
		StateAbnormal,
		// Token: 0x04000D81 RID: 3457
		[Token(Token = "0x4000ABD")]
		WeakElement,
		// Token: 0x04000D82 RID: 3458
		[Token(Token = "0x4000ABE")]
		StatusChange,
		// Token: 0x04000D83 RID: 3459
		[Token(Token = "0x4000ABF")]
		Self,
		// Token: 0x04000D84 RID: 3460
		[Token(Token = "0x4000AC0")]
		Num
	}
}
