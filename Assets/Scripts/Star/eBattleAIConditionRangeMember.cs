﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000375 RID: 885
	[Token(Token = "0x20002EB")]
	[StructLayout(3, Size = 4)]
	public enum eBattleAIConditionRangeMember
	{
		// Token: 0x04000D8D RID: 3469
		[Token(Token = "0x4000AC9")]
		MyParty,
		// Token: 0x04000D8E RID: 3470
		[Token(Token = "0x4000ACA")]
		NotSelf,
		// Token: 0x04000D8F RID: 3471
		[Token(Token = "0x4000ACB")]
		Self,
		// Token: 0x04000D90 RID: 3472
		[Token(Token = "0x4000ACC")]
		Num
	}
}
