﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000370 RID: 880
	[Token(Token = "0x20002E6")]
	[StructLayout(3, Size = 4)]
	public enum eBattleAIPatternChangeConditionType
	{
		// Token: 0x04000D33 RID: 3379
		[Token(Token = "0x4000A6F")]
		DeadCount,
		// Token: 0x04000D34 RID: 3380
		[Token(Token = "0x4000A70")]
		Num
	}
}
