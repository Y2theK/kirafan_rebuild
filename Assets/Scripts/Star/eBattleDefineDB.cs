﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004AE RID: 1198
	[Token(Token = "0x20003A6")]
	[StructLayout(3, Size = 4)]
	public enum eBattleDefineDB
	{
		// Token: 0x04001656 RID: 5718
		[Token(Token = "0x400103C")]
		OrderValueBaseMin,
		// Token: 0x04001657 RID: 5719
		[Token(Token = "0x400103D")]
		OrderValueBaseMax,
		// Token: 0x04001658 RID: 5720
		[Token(Token = "0x400103E")]
		OrderValueDecreaseStart,
		// Token: 0x04001659 RID: 5721
		[Token(Token = "0x400103F")]
		OrderValueDecreaseInterval,
		// Token: 0x0400165A RID: 5722
		[Token(Token = "0x4001040")]
		OrderValueMin,
		// Token: 0x0400165B RID: 5723
		[Token(Token = "0x4001041")]
		OrderValueMax,
		// Token: 0x0400165C RID: 5724
		[Token(Token = "0x4001042")]
		QuickMoveOrderValueMin,
		// Token: 0x0400165D RID: 5725
		[Token(Token = "0x4001043")]
		QuickMoveOrderValueMax,
		// Token: 0x0400165E RID: 5726
		[Token(Token = "0x4001044")]
		MemberChangeRecast,
		// Token: 0x0400165F RID: 5727
		[Token(Token = "0x4001045")]
		DamageRandMin,
		// Token: 0x04001660 RID: 5728
		[Token(Token = "0x4001046")]
		DamageRandMax,
		// Token: 0x04001661 RID: 5729
		[Token(Token = "0x4001047")]
		RecoverBonusMin,
		// Token: 0x04001662 RID: 5730
		[Token(Token = "0x4001048")]
		RecoverBonusMax,
		// Token: 0x04001663 RID: 5731
		[Token(Token = "0x4001049")]
		RecoverBonusInterval,
		// Token: 0x04001664 RID: 5732
		[Token(Token = "0x400104A")]
		RecoverBonusPlus,
		// Token: 0x04001665 RID: 5733
		[Token(Token = "0x400104B")]
		RecoverBonusSameElement,
		// Token: 0x04001666 RID: 5734
		[Token(Token = "0x400104C")]
		GuardCoef,
		// Token: 0x04001667 RID: 5735
		[Token(Token = "0x400104D")]
		ElementCoefRegist,
		// Token: 0x04001668 RID: 5736
		[Token(Token = "0x400104E")]
		ElementCoefWeak,
		// Token: 0x04001669 RID: 5737
		[Token(Token = "0x400104F")]
		ElementCoefRegistMin,
		// Token: 0x0400166A RID: 5738
		[Token(Token = "0x4001050")]
		ElementCoefRegistMax,
		// Token: 0x0400166B RID: 5739
		[Token(Token = "0x4001051")]
		ElementCoefDefaultMin,
		// Token: 0x0400166C RID: 5740
		[Token(Token = "0x4001052")]
		ElementCoefDefaultMax,
		// Token: 0x0400166D RID: 5741
		[Token(Token = "0x4001053")]
		ElementCoefWeakMin,
		// Token: 0x0400166E RID: 5742
		[Token(Token = "0x4001054")]
		ElementCoefWeakMax,
		// Token: 0x0400166F RID: 5743
		[Token(Token = "0x4001055")]
		BuffCoefPlayerAtkMin,
		// Token: 0x04001670 RID: 5744
		[Token(Token = "0x4001056")]
		BuffCoefPlayerAtkMax,
		// Token: 0x04001671 RID: 5745
		[Token(Token = "0x4001057")]
		BuffCoefPlayerDefMin,
		// Token: 0x04001672 RID: 5746
		[Token(Token = "0x4001058")]
		BuffCoefPlayerDefMax,
		// Token: 0x04001673 RID: 5747
		[Token(Token = "0x4001059")]
		BuffCoefPlayerLuckMin,
		// Token: 0x04001674 RID: 5748
		[Token(Token = "0x400105A")]
		BuffCoefPlayerLuckMax,
		// Token: 0x04001675 RID: 5749
		[Token(Token = "0x400105B")]
		BuffCoefEnemyAtkMin,
		// Token: 0x04001676 RID: 5750
		[Token(Token = "0x400105C")]
		BuffCoefEnemyAtkMax,
		// Token: 0x04001677 RID: 5751
		[Token(Token = "0x400105D")]
		BuffCoefEnemyDefMin,
		// Token: 0x04001678 RID: 5752
		[Token(Token = "0x400105E")]
		BuffCoefEnemyDefMax,
		// Token: 0x04001679 RID: 5753
		[Token(Token = "0x400105F")]
		BuffCoefEnemyLuckMin,
		// Token: 0x0400167A RID: 5754
		[Token(Token = "0x4001060")]
		BuffCoefEnemyLuckMax,
		// Token: 0x0400167B RID: 5755
		[Token(Token = "0x4001061")]
		CriticalProbablityMax,
		// Token: 0x0400167C RID: 5756
		[Token(Token = "0x4001062")]
		CriticalStrongElementCoef,
		// Token: 0x0400167D RID: 5757
		[Token(Token = "0x4001063")]
		CriticalCoef,
		// Token: 0x0400167E RID: 5758
		[Token(Token = "0x4001064")]
		CriticalCoefMin,
		// Token: 0x0400167F RID: 5759
		[Token(Token = "0x4001065")]
		CriticalCoefMax,
		// Token: 0x04001680 RID: 5760
		[Token(Token = "0x4001066")]
		CriticalAdjustmentCoef,
		// Token: 0x04001681 RID: 5761
		[Token(Token = "0x4001067")]
		TogetherCharge_OnAttack,
		// Token: 0x04001682 RID: 5762
		[Token(Token = "0x4001068")]
		TogetherCharge_OnAttack_weak,
		// Token: 0x04001683 RID: 5763
		[Token(Token = "0x4001069")]
		TogetherCharge_OnAttack_regist,
		// Token: 0x04001684 RID: 5764
		[Token(Token = "0x400106A")]
		TogetherCharge_OnAttackCriticalCoef,
		// Token: 0x04001685 RID: 5765
		[Token(Token = "0x400106B")]
		TogetherCharge_OnDamage,
		// Token: 0x04001686 RID: 5766
		[Token(Token = "0x400106C")]
		TogetherCharge_OnDamage_weak,
		// Token: 0x04001687 RID: 5767
		[Token(Token = "0x400106D")]
		TogetherCharge_OnDamage_regist,
		// Token: 0x04001688 RID: 5768
		[Token(Token = "0x400106E")]
		TogetherCharge_OnStun,
		// Token: 0x04001689 RID: 5769
		[Token(Token = "0x400106F")]
		TogetherChargeDefineMax,
		// Token: 0x0400168A RID: 5770
		[Token(Token = "0x4001070")]
		TogetherAttackChainCoef_0,
		// Token: 0x0400168B RID: 5771
		[Token(Token = "0x4001071")]
		TogetherAttackChainCoef_1,
		// Token: 0x0400168C RID: 5772
		[Token(Token = "0x4001072")]
		TogetherAttackChainCoef_2,
		// Token: 0x0400168D RID: 5773
		[Token(Token = "0x4001073")]
		TogetherAttackSkipLv,
		// Token: 0x0400168E RID: 5774
		[Token(Token = "0x4001074")]
		StateAbnormalConfusion_Turn,
		// Token: 0x0400168F RID: 5775
		[Token(Token = "0x4001075")]
		StateAbnormalParalysis_Turn,
		// Token: 0x04001690 RID: 5776
		[Token(Token = "0x4001076")]
		StateAbnormalParalysis_CancelProbability,
		// Token: 0x04001691 RID: 5777
		[Token(Token = "0x4001077")]
		StateAbnormalPoison_Turn,
		// Token: 0x04001692 RID: 5778
		[Token(Token = "0x4001078")]
		StateAbnormalPoison_DamageRatio,
		// Token: 0x04001693 RID: 5779
		[Token(Token = "0x4001079")]
		StateAbnormalBearish_Turn,
		// Token: 0x04001694 RID: 5780
		[Token(Token = "0x400107A")]
		StateAbnormalSleep_Turn,
		// Token: 0x04001695 RID: 5781
		[Token(Token = "0x400107B")]
		StateAbnormalSleep_LoadFactor,
		// Token: 0x04001696 RID: 5782
		[Token(Token = "0x400107C")]
		StateAbnormalUnhappy_Turn,
		// Token: 0x04001697 RID: 5783
		[Token(Token = "0x400107D")]
		StateAbnormalSilence_Turn,
		// Token: 0x04001698 RID: 5784
		[Token(Token = "0x400107E")]
		StateAbnormalIsolation_Turn,
		// Token: 0x04001699 RID: 5785
		[Token(Token = "0x400107F")]
		HateValueMin,
		// Token: 0x0400169A RID: 5786
		[Token(Token = "0x4001080")]
		HateValueMax,
		// Token: 0x0400169B RID: 5787
		[Token(Token = "0x4001081")]
		HateAIConditionCoef,
		// Token: 0x0400169C RID: 5788
		[Token(Token = "0x4001082")]
		HateValueJoin1,
		// Token: 0x0400169D RID: 5789
		[Token(Token = "0x4001083")]
		DyingHpRatio,
		// Token: 0x0400169E RID: 5790
		[Token(Token = "0x4001084")]
		AutoAIDyingHpRatio,
		// Token: 0x0400169F RID: 5791
		[Token(Token = "0x4001085")]
		RegistedFriendTurn,
		// Token: 0x040016A0 RID: 5792
		[Token(Token = "0x4001086")]
		UnregistedFriendTurn,
		// Token: 0x040016A1 RID: 5793
		[Token(Token = "0x4001087")]
		StunValueMax,
		// Token: 0x040016A2 RID: 5794
		[Token(Token = "0x4001088")]
		StunAdditiveOrderValueRatioWhenStun,
		// Token: 0x040016A3 RID: 5795
		[Token(Token = "0x4001089")]
		StunAdditiveOrderValueRatioWhenAttacked,
		// Token: 0x040016A4 RID: 5796
		[Token(Token = "0x400108A")]
		StunDecreaseValueOnTurnStart,
		// Token: 0x040016A5 RID: 5797
		[Token(Token = "0x400108B")]
		BorderFromSkillLv_0,
		// Token: 0x040016A6 RID: 5798
		[Token(Token = "0x400108C")]
		BorderFromSkillLv_1,
		// Token: 0x040016A7 RID: 5799
		[Token(Token = "0x400108D")]
		BorderFromSkillLv_2,
		// Token: 0x040016A8 RID: 5800
		[Token(Token = "0x400108E")]
		DamageDiv,
		// Token: 0x040016A9 RID: 5801
		[Token(Token = "0x400108F")]
		PassiveStatusChangeAtkMin,
		// Token: 0x040016AA RID: 5802
		[Token(Token = "0x4001090")]
		PassiveStatusChangeAtkMax,
		// Token: 0x040016AB RID: 5803
		[Token(Token = "0x4001091")]
		PassiveStatusChangeDefMin,
		// Token: 0x040016AC RID: 5804
		[Token(Token = "0x4001092")]
		PassiveStatusChangeDefMax,
		// Token: 0x040016AD RID: 5805
		[Token(Token = "0x4001093")]
		PassiveStatusChangeLuckMin,
		// Token: 0x040016AE RID: 5806
		[Token(Token = "0x4001094")]
		PassiveStatusChangeLuckMax,
		// Token: 0x040016AF RID: 5807
		[Token(Token = "0x4001095")]
		Passive_StatusChangeByHpAtkMin,
		// Token: 0x040016B0 RID: 5808
		[Token(Token = "0x4001096")]
		Passive_StatusChangeByHpAtkMax,
		// Token: 0x040016B1 RID: 5809
		[Token(Token = "0x4001097")]
		Passive_StatusChangeByHpDefMin,
		// Token: 0x040016B2 RID: 5810
		[Token(Token = "0x4001098")]
		Passive_StatusChangeByHpDefMax,
		// Token: 0x040016B3 RID: 5811
		[Token(Token = "0x4001099")]
		Passive_StatusChangeByHpLuckMin,
		// Token: 0x040016B4 RID: 5812
		[Token(Token = "0x400109A")]
		Passive_StatusChangeByHpLuckMax,
		// Token: 0x040016B5 RID: 5813
		[Token(Token = "0x400109B")]
		Passive_reserve_13,
		// Token: 0x040016B6 RID: 5814
		[Token(Token = "0x400109C")]
		Passive_reserve_14,
		// Token: 0x040016B7 RID: 5815
		[Token(Token = "0x400109D")]
		Passive_reserve_15,
		// Token: 0x040016B8 RID: 5816
		[Token(Token = "0x400109E")]
		Passive_reserve_16,
		// Token: 0x040016B9 RID: 5817
		[Token(Token = "0x400109F")]
		Passive_reserve_17,
		// Token: 0x040016BA RID: 5818
		[Token(Token = "0x40010A0")]
		Passive_reserve_18,
		// Token: 0x040016BB RID: 5819
		[Token(Token = "0x40010A1")]
		Passive_reserve_19,
		// Token: 0x040016BC RID: 5820
		[Token(Token = "0x40010A2")]
		Passive_reserve_20,
		// Token: 0x040016BD RID: 5821
		[Token(Token = "0x40010A3")]
		Passive_reserve_21,
		// Token: 0x040016BE RID: 5822
		[Token(Token = "0x40010A4")]
		Passive_reserve_22,
		// Token: 0x040016BF RID: 5823
		[Token(Token = "0x40010A5")]
		Passive_reserve_23,
		// Token: 0x040016C0 RID: 5824
		[Token(Token = "0x40010A6")]
		Passive_reserve_24,
		// Token: 0x040016C1 RID: 5825
		[Token(Token = "0x40010A7")]
		Passive_reserve_25,
		// Token: 0x040016C2 RID: 5826
		[Token(Token = "0x40010A8")]
		Passive_reserve_26,
		// Token: 0x040016C3 RID: 5827
		[Token(Token = "0x40010A9")]
		Passive_reserve_27,
		// Token: 0x040016C4 RID: 5828
		[Token(Token = "0x40010AA")]
		Passive_reserve_28,
		// Token: 0x040016C5 RID: 5829
		[Token(Token = "0x40010AB")]
		Passive_reserve_29,
		// Token: 0x040016C6 RID: 5830
		[Token(Token = "0x40010AC")]
		Passive_reserve_30,
		// Token: 0x040016C7 RID: 5831
		[Token(Token = "0x40010AD")]
		Passive_reserve_31,
		// Token: 0x040016C8 RID: 5832
		[Token(Token = "0x40010AE")]
		Passive_reserve_32,
		// Token: 0x040016C9 RID: 5833
		[Token(Token = "0x40010AF")]
		Passive_reserve_33,
		// Token: 0x040016CA RID: 5834
		[Token(Token = "0x40010B0")]
		Passive_reserve_34,
		// Token: 0x040016CB RID: 5835
		[Token(Token = "0x40010B1")]
		Passive_reserve_35,
		// Token: 0x040016CC RID: 5836
		[Token(Token = "0x40010B2")]
		Passive_reserve_36,
		// Token: 0x040016CD RID: 5837
		[Token(Token = "0x40010B3")]
		Passive_reserve_37,
		// Token: 0x040016CE RID: 5838
		[Token(Token = "0x40010B4")]
		Passive_reserve_38,
		// Token: 0x040016CF RID: 5839
		[Token(Token = "0x40010B5")]
		Passive_reserve_39,
		// Token: 0x040016D0 RID: 5840
		[Token(Token = "0x40010B6")]
		Passive_reserve_40,
		// Token: 0x040016D1 RID: 5841
		[Token(Token = "0x40010B7")]
		Passive_reserve_41,
		// Token: 0x040016D2 RID: 5842
		[Token(Token = "0x40010B8")]
		Passive_reserve_42,
		// Token: 0x040016D3 RID: 5843
		[Token(Token = "0x40010B9")]
		Passive_reserve_43,
		// Token: 0x040016D4 RID: 5844
		[Token(Token = "0x40010BA")]
		Passive_reserve_44,
		// Token: 0x040016D5 RID: 5845
		[Token(Token = "0x40010BB")]
		Passive_reserve_45,
		// Token: 0x040016D6 RID: 5846
		[Token(Token = "0x40010BC")]
		Passive_reserve_46,
		// Token: 0x040016D7 RID: 5847
		[Token(Token = "0x40010BD")]
		Passive_reserve_47,
		// Token: 0x040016D8 RID: 5848
		[Token(Token = "0x40010BE")]
		Passive_reserve_48,
		// Token: 0x040016D9 RID: 5849
		[Token(Token = "0x40010BF")]
		Passive_reserve_49,
		// Token: 0x040016DA RID: 5850
		[Token(Token = "0x40010C0")]
		Passive_reserve_50,
		// Token: 0x040016DB RID: 5851
		[Token(Token = "0x40010C1")]
		Max
	}
}
