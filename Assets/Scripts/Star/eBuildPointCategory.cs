﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B88 RID: 2952
	[Token(Token = "0x2000806")]
	[StructLayout(3, Size = 4)]
	public enum eBuildPointCategory
	{
		// Token: 0x040043DF RID: 17375
		[Token(Token = "0x400300C")]
		Area,
		// Token: 0x040043E0 RID: 17376
		[Token(Token = "0x400300D")]
		Contents,
		// Token: 0x040043E1 RID: 17377
		[Token(Token = "0x400300E")]
		Buf,
		// Token: 0x040043E2 RID: 17378
		[Token(Token = "0x400300F")]
		Menu
	}
}
