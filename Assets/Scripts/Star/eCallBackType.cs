﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006AA RID: 1706
	[Token(Token = "0x200056E")]
	[StructLayout(3, Size = 4)]
	public enum eCallBackType
	{
		// Token: 0x0400295C RID: 10588
		[Token(Token = "0x4002221")]
		MoveBuild,
		// Token: 0x0400295D RID: 10589
		[Token(Token = "0x4002222")]
		SwapBuild,
		// Token: 0x0400295E RID: 10590
		[Token(Token = "0x4002223")]
		RemoveBuild,
		// Token: 0x0400295F RID: 10591
		[Token(Token = "0x4002224")]
		ScheduleChange,
		// Token: 0x04002960 RID: 10592
		[Token(Token = "0x4002225")]
		Event,
		// Token: 0x04002961 RID: 10593
		[Token(Token = "0x4002226")]
		StayChange,
		// Token: 0x04002962 RID: 10594
		[Token(Token = "0x4002227")]
		CompBuild,
		// Token: 0x04002963 RID: 10595
		[Token(Token = "0x4002228")]
		LevelUp
	}
}
