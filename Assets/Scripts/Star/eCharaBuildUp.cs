﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006A1 RID: 1697
	[Token(Token = "0x2000569")]
	[StructLayout(3, Size = 4)]
	public enum eCharaBuildUp
	{
		// Token: 0x04002941 RID: 10561
		[Token(Token = "0x400220D")]
		Add,
		// Token: 0x04002942 RID: 10562
		[Token(Token = "0x400220E")]
		Del,
		// Token: 0x04002943 RID: 10563
		[Token(Token = "0x400220F")]
		ChangeRoom
	}
}
