﻿using System;

namespace Star {
    [Flags]
    public enum eCharaParamCalc {
        Lv = 1,
        Weapon = 2,
        Arousal = 4,
        Ability = 8,
        Friendship = 16,
        Town = 32,
        Orb = 64,
        All = 127
    }
}
