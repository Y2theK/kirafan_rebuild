﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004D1 RID: 1233
	[Token(Token = "0x20003C9")]
	[StructLayout(3, Size = 4)]
	public enum eCharaQuestOpenCondition
	{
		// Token: 0x0400175B RID: 5979
		[Token(Token = "0x4001141")]
		CharacterEvolve,
		// Token: 0x0400175C RID: 5980
		[Token(Token = "0x4001142")]
		CharacterLvUp,
		// Token: 0x0400175D RID: 5981
		[Token(Token = "0x4001143")]
		WeaponEvolve
	}
}
