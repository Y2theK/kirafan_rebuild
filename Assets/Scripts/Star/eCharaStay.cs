﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006A9 RID: 1705
	[Token(Token = "0x200056D")]
	[StructLayout(3, Size = 4)]
	public enum eCharaStay
	{
		// Token: 0x04002956 RID: 10582
		[Token(Token = "0x400221B")]
		Room,
		// Token: 0x04002957 RID: 10583
		[Token(Token = "0x400221C")]
		Sleep,
		// Token: 0x04002958 RID: 10584
		[Token(Token = "0x400221D")]
		Content,
		// Token: 0x04002959 RID: 10585
		[Token(Token = "0x400221E")]
		Buf,
		// Token: 0x0400295A RID: 10586
		[Token(Token = "0x400221F")]
		Menu
	}
}
