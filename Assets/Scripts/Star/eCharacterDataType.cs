﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000621 RID: 1569
	[Token(Token = "0x2000514")]
	[StructLayout(3, Size = 4)]
	public enum eCharacterDataType
	{
		// Token: 0x0400261B RID: 9755
		[Token(Token = "0x4001F85")]
		Error,
		// Token: 0x0400261C RID: 9756
		[Token(Token = "0x4001F86")]
		UserCharacterData,
		// Token: 0x0400261D RID: 9757
		[Token(Token = "0x4001F87")]
		UserSupportData,
		// Token: 0x0400261E RID: 9758
		[Token(Token = "0x4001F88")]
		OutputCharaParam,
		// Token: 0x0400261F RID: 9759
		[Token(Token = "0x4001F89")]
		AfterUpgradeParam,
		// Token: 0x04002620 RID: 9760
		[Token(Token = "0x4001F8A")]
		AfterLimitBreakParam,
		// Token: 0x04002621 RID: 9761
		[Token(Token = "0x4001F8B")]
		AfterEvolutionParam,
		// Token: 0x04002622 RID: 9762
		[Token(Token = "0x4001F8C")]
		Empty,
		// Token: 0x04002623 RID: 9763
		[Token(Token = "0x4001F8D")]
		Lock
	}
}
