﻿namespace Star {
    public enum eClassType {
        None = -1,
        Fighter,
        Magician,
        Priest,
        Knight,
        Alchemist,
        Num
    }
}
