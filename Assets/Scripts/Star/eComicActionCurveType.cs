﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200048F RID: 1167
	[Token(Token = "0x2000387")]
	[StructLayout(3, Size = 4)]
	public enum eComicActionCurveType
	{
		// Token: 0x040015E3 RID: 5603
		[Token(Token = "0x4000FC9")]
		None,
		// Token: 0x040015E4 RID: 5604
		[Token(Token = "0x4000FCA")]
		Linear,
		// Token: 0x040015E5 RID: 5605
		[Token(Token = "0x4000FCB")]
		EaseIn,
		// Token: 0x040015E6 RID: 5606
		[Token(Token = "0x4000FCC")]
		EaseOut,
		// Token: 0x040015E7 RID: 5607
		[Token(Token = "0x4000FCD")]
		EaseInOut
	}
}
