﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000490 RID: 1168
	[Token(Token = "0x2000388")]
	[StructLayout(3, Size = 4)]
	public enum eComicActionElementType
	{
		// Token: 0x040015E9 RID: 5609
		[Token(Token = "0x4000FCF")]
		Position,
		// Token: 0x040015EA RID: 5610
		[Token(Token = "0x4000FD0")]
		Rotate,
		// Token: 0x040015EB RID: 5611
		[Token(Token = "0x4000FD1")]
		Scale,
		// Token: 0x040015EC RID: 5612
		[Token(Token = "0x4000FD2")]
		Color,
		// Token: 0x040015ED RID: 5613
		[Token(Token = "0x4000FD3")]
		ShakeRandom,
		// Token: 0x040015EE RID: 5614
		[Token(Token = "0x4000FD4")]
		ShakeX,
		// Token: 0x040015EF RID: 5615
		[Token(Token = "0x4000FD5")]
		ShakeY
	}
}
