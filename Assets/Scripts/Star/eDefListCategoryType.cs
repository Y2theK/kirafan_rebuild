﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200067F RID: 1663
	[Token(Token = "0x200054E")]
	[StructLayout(3, Size = 4)]
	public enum eDefListCategoryType
	{
		// Token: 0x040027AC RID: 10156
		[Token(Token = "0x4002089")]
		Town,
		// Token: 0x040027AD RID: 10157
		[Token(Token = "0x400208A")]
		Room,
		// Token: 0x040027AE RID: 10158
		[Token(Token = "0x400208B")]
		FldParty
	}
}
