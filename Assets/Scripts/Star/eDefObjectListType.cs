﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200067E RID: 1662
	[Token(Token = "0x200054D")]
	[StructLayout(3, Size = 4)]
	public enum eDefObjectListType
	{
		// Token: 0x040027A3 RID: 10147
		[Token(Token = "0x4002080")]
		Build,
		// Token: 0x040027A4 RID: 10148
		[Token(Token = "0x4002081")]
		Room,
		// Token: 0x040027A5 RID: 10149
		[Token(Token = "0x4002082")]
		BG,
		// Token: 0x040027A6 RID: 10150
		[Token(Token = "0x4002083")]
		Wall,
		// Token: 0x040027A7 RID: 10151
		[Token(Token = "0x4002084")]
		Floor,
		// Token: 0x040027A8 RID: 10152
		[Token(Token = "0x4002085")]
		Bedding,
		// Token: 0x040027A9 RID: 10153
		[Token(Token = "0x4002086")]
		System,
		// Token: 0x040027AA RID: 10154
		[Token(Token = "0x4002087")]
		FldChara
	}
}
