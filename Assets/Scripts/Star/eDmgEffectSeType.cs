﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000429 RID: 1065
	[Token(Token = "0x200034C")]
	[StructLayout(3, Size = 4)]
	public enum eDmgEffectSeType
	{
		// Token: 0x040012D5 RID: 4821
		[Token(Token = "0x4000D9E")]
		None = -1,
		// Token: 0x040012D6 RID: 4822
		[Token(Token = "0x4000D9F")]
		Slash,
		// Token: 0x040012D7 RID: 4823
		[Token(Token = "0x4000DA0")]
		Blow,
		// Token: 0x040012D8 RID: 4824
		[Token(Token = "0x4000DA1")]
		Magic,
		// Token: 0x040012D9 RID: 4825
		[Token(Token = "0x4000DA2")]
		Bite,
		// Token: 0x040012DA RID: 4826
		[Token(Token = "0x4000DA3")]
		Claw
	}
}
