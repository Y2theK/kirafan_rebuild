﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000428 RID: 1064
	[Token(Token = "0x200034B")]
	[StructLayout(3, Size = 4)]
	public enum eDmgEffectType
	{
		// Token: 0x040012CD RID: 4813
		[Token(Token = "0x4000D96")]
		None = -1,
		// Token: 0x040012CE RID: 4814
		[Token(Token = "0x4000D97")]
		PL_Single,
		// Token: 0x040012CF RID: 4815
		[Token(Token = "0x4000D98")]
		PL_All,
		// Token: 0x040012D0 RID: 4816
		[Token(Token = "0x4000D99")]
		EN_Slash,
		// Token: 0x040012D1 RID: 4817
		[Token(Token = "0x4000D9A")]
		EN_Blow,
		// Token: 0x040012D2 RID: 4818
		[Token(Token = "0x4000D9B")]
		EN_Bite,
		// Token: 0x040012D3 RID: 4819
		[Token(Token = "0x4000D9C")]
		EN_Claw
	}
}
