﻿namespace Star {
    public enum eElementType {
        None = -1,
        Fire,
        Water,
        Earth,
        Wind,
        Moon,
        Sun,
        Num
    }
}
