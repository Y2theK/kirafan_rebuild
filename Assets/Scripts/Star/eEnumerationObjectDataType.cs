﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000673 RID: 1651
	[Token(Token = "0x2000546")]
	[StructLayout(3, Size = 4)]
	public enum eEnumerationObjectDataType
	{
		// Token: 0x0400277A RID: 10106
		[Token(Token = "0x4002068")]
		Error,
		// Token: 0x0400277B RID: 10107
		[Token(Token = "0x4002069")]
		Chara,
		// Token: 0x0400277C RID: 10108
		[Token(Token = "0x400206A")]
		GachaDetail
	}
}
