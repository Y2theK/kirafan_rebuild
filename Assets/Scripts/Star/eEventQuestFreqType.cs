﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005D7 RID: 1495
	[Token(Token = "0x20004CA")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestFreqType
	{
		// Token: 0x04001E5D RID: 7773
		[Token(Token = "0x40017C7")]
		None,
		// Token: 0x04001E5E RID: 7774
		[Token(Token = "0x40017C8")]
		EveryYear,
		// Token: 0x04001E5F RID: 7775
		[Token(Token = "0x40017C9")]
		Monthly,
		// Token: 0x04001E60 RID: 7776
		[Token(Token = "0x40017CA")]
		Weekly,
		// Token: 0x04001E61 RID: 7777
		[Token(Token = "0x40017CB")]
		EveryDay
	}
}
