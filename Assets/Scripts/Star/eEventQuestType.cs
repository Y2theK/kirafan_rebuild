﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005D6 RID: 1494
	[Token(Token = "0x20004C9")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestType
	{
		// Token: 0x04001E58 RID: 7768
		[Token(Token = "0x40017C2")]
		DayOfTheWeek,
		// Token: 0x04001E59 RID: 7769
		[Token(Token = "0x40017C3")]
		Challenge,
		// Token: 0x04001E5A RID: 7770
		[Token(Token = "0x40017C4")]
		Blank1,
		// Token: 0x04001E5B RID: 7771
		[Token(Token = "0x40017C5")]
		Blank2
	}
}
