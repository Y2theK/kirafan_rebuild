﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004EA RID: 1258
	[Token(Token = "0x20003E0")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestUISettingCharaAnchor
	{
		// Token: 0x04001896 RID: 6294
		[Token(Token = "0x4001205")]
		LeftTop,
		// Token: 0x04001897 RID: 6295
		[Token(Token = "0x4001206")]
		LeftMiddle,
		// Token: 0x04001898 RID: 6296
		[Token(Token = "0x4001207")]
		LeftBottom
	}
}
