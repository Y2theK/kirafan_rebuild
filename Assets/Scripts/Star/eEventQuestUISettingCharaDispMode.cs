﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004EC RID: 1260
	[Token(Token = "0x20003E2")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestUISettingCharaDispMode
	{
		// Token: 0x0400189E RID: 6302
		[Token(Token = "0x400120D")]
		Chara,
		// Token: 0x0400189F RID: 6303
		[Token(Token = "0x400120E")]
		Hidden,
		// Token: 0x040018A0 RID: 6304
		[Token(Token = "0x400120F")]
		Writer
	}
}
