﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004E9 RID: 1257
	[Token(Token = "0x20003DF")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestUISettingCharaIllustType
	{
		// Token: 0x04001893 RID: 6291
		[Token(Token = "0x4001202")]
		CharaIllustChara,
		// Token: 0x04001894 RID: 6292
		[Token(Token = "0x4001203")]
		CharaIllustBust
	}
}
