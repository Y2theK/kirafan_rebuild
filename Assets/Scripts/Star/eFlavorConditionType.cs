﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200054E RID: 1358
	[Token(Token = "0x2000441")]
	[StructLayout(3, Size = 4)]
	public enum eFlavorConditionType
	{
		// Token: 0x040018C7 RID: 6343
		[Token(Token = "0x4001231")]
		Cond_None,
		// Token: 0x040018C8 RID: 6344
		[Token(Token = "0x4001232")]
		Cond_OfferStart,
		// Token: 0x040018C9 RID: 6345
		[Token(Token = "0x4001233")]
		Cond_OfferFinished
	}
}
