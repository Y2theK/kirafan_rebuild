﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005DA RID: 1498
	[Token(Token = "0x20004CD")]
	[StructLayout(3, Size = 4)]
	public enum eGachaDrawingType
	{
		// Token: 0x04001E7A RID: 7802
		[Token(Token = "0x40017E4")]
		UGem1 = 1,
		// Token: 0x04001E7B RID: 7803
		[Token(Token = "0x40017E5")]
		Gem1,
		// Token: 0x04001E7C RID: 7804
		[Token(Token = "0x40017E6")]
		Gem10,
		// Token: 0x04001E7D RID: 7805
		[Token(Token = "0x40017E7")]
		Item
	}
}
