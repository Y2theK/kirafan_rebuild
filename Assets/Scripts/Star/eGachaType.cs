﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005D9 RID: 1497
	[Token(Token = "0x20004CC")]
	[StructLayout(3, Size = 4)]
	public enum eGachaType
	{
		// Token: 0x04001E73 RID: 7795
		[Token(Token = "0x40017DD")]
		Summon = 1,
		// Token: 0x04001E74 RID: 7796
		[Token(Token = "0x40017DE")]
		Pickup,
		// Token: 0x04001E75 RID: 7797
		[Token(Token = "0x40017DF")]
		Won,
		// Token: 0x04001E76 RID: 7798
		[Token(Token = "0x40017E0")]
		UGem,
		// Token: 0x04001E77 RID: 7799
		[Token(Token = "0x40017E1")]
		Selection,
		// Token: 0x04001E78 RID: 7800
		[Token(Token = "0x40017E2")]
		Stepup
	}
}
