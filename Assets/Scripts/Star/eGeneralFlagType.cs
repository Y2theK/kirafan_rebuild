﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AC5 RID: 2757
	[Token(Token = "0x2000795")]
	[StructLayout(3, Size = 4)]
	public enum eGeneralFlagType
	{
		// Token: 0x04003F38 RID: 16184
		[Token(Token = "0x4002D5B")]
		PlayedOpenChapterIdPart1,
		// Token: 0x04003F39 RID: 16185
		[Token(Token = "0x4002D5C")]
		PlayedOpenChapterIdPart2,
		// Token: 0x04003F3A RID: 16186
		[Token(Token = "0x4002D5D")]
		LastPlayedChapterIdPart1 = 11,
		// Token: 0x04003F3B RID: 16187
		[Token(Token = "0x4002D5E")]
		LastPlayedChapterIdPart2
	}
}
