﻿namespace Star {
    public enum eItemType {
        Upgrade,
        Evolution,
        LimitBreak,
        Weapon,
        QuestKey,
        GachaTicket,
        TradeItem,
        Stamina,
        WeaponEvolution,
        GachaUp,
        SkillUp,
        Misc,
        AbilityBoard,
        AbilityFragment,
        AbilitySphere
    }
}
