﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000555 RID: 1365
	[Token(Token = "0x2000448")]
	[StructLayout(3, Size = 4)]
	public enum eLoginBonusType
	{
		// Token: 0x040018DE RID: 6366
		[Token(Token = "0x4001248")]
		Normal,
		// Token: 0x040018DF RID: 6367
		[Token(Token = "0x4001249")]
		Event,
		// Token: 0x040018E0 RID: 6368
		[Token(Token = "0x400124A")]
		Total
	}
}
