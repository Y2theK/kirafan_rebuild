﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000868 RID: 2152
	[Token(Token = "0x2000642")]
	[StructLayout(3, Size = 4)]
	public enum eMainComClassID
	{
		// Token: 0x040032D1 RID: 13009
		[Token(Token = "0x40025C6")]
		AddResultData,
		// Token: 0x040032D2 RID: 13010
		[Token(Token = "0x40025C7")]
		AddResultPlayerData,
		// Token: 0x040032D3 RID: 13011
		[Token(Token = "0x40025C8")]
		Max
	}
}
