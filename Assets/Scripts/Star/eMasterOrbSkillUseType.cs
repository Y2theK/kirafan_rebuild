﻿namespace Star {
    public enum eMasterOrbSkillUseType {
        OnceInTotal,
        OnceForEach
    }
}
