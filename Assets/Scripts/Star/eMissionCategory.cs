﻿namespace Star {
    public enum eMissionCategory {
        None = -1,
        Day,
        Week,
        Unlock,
        Event,
        Usually = 3,
        Beginner,
        Max
    }
}
