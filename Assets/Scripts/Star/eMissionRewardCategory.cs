﻿namespace Star {
    public enum eMissionRewardCategory {
        Non,
        Money,
        Item,
        KRRPoint,
        Stamina,
        Friendship,
        Gem
    }
}
