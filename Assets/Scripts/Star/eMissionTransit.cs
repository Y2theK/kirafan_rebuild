﻿namespace Star {
    public enum eMissionTransit {
        None,
        Quest,
        Training,
        Trade,
        Town,
        Room,
        Upgrade
    }
}
