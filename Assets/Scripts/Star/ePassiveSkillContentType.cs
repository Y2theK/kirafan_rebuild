﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200056E RID: 1390
	[Token(Token = "0x2000461")]
	[StructLayout(3, Size = 4)]
	public enum ePassiveSkillContentType
	{
		// Token: 0x04001942 RID: 6466
		[Token(Token = "0x40012AC")]
		StatusChange,
		// Token: 0x04001943 RID: 6467
		[Token(Token = "0x40012AD")]
		HateChange,
		// Token: 0x04001944 RID: 6468
		[Token(Token = "0x40012AE")]
		AbnormalDisable,
		// Token: 0x04001945 RID: 6469
		[Token(Token = "0x40012AF")]
		StunDisable,
		// Token: 0x04001946 RID: 6470
		[Token(Token = "0x40012B0")]
		StunCoef,
		// Token: 0x04001947 RID: 6471
		[Token(Token = "0x40012B1")]
		KiraraJumpGaugeChange,
		// Token: 0x04001948 RID: 6472
		[Token(Token = "0x40012B2")]
		KiraraJumpGaugeCoef,
		// Token: 0x04001949 RID: 6473
		[Token(Token = "0x40012B3")]
		CriticalDamageChange,
		// Token: 0x0400194A RID: 6474
		[Token(Token = "0x40012B4")]
		SkillChange,
		// Token: 0x0400194B RID: 6475
		[Token(Token = "0x40012B5")]
		OverRecover,
		// Token: 0x0400194C RID: 6476
		[Token(Token = "0x40012B6")]
		AbsorbAttack,
		// Token: 0x0400194D RID: 6477
		[Token(Token = "0x40012B7")]
		Revive,
		// Token: 0x0400194E RID: 6478
		[Token(Token = "0x40012B8")]
		CardTurnChange,
		// Token: 0x0400194F RID: 6479
		[Token(Token = "0x40012B9")]
		StatusRatioByHP,
		// Token: 0x04001950 RID: 6480
		[Token(Token = "0x40012BA")]
		CounterStatusChange,
		// Token: 0x04001951 RID: 6481
		[Token(Token = "0x40012BB")]
		AbnormalAdditionalProbability,
		// Token: 0x04001952 RID: 6482
		[Token(Token = "0x40012BC")]
		ElementResist,
		// Token: 0x04001953 RID: 6483
		[Token(Token = "0x40012BD")]
		TurnIncrease,
		// Token: 0x04001954 RID: 6484
		[Token(Token = "0x40012BE")]
		Num
	}
}
