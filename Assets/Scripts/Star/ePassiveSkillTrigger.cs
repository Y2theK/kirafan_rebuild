﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200056D RID: 1389
	[Token(Token = "0x2000460")]
	[StructLayout(3, Size = 4)]
	public enum ePassiveSkillTrigger
	{
		// Token: 0x0400193D RID: 6461
		[Token(Token = "0x40012A7")]
		OnStart,
		// Token: 0x0400193E RID: 6462
		[Token(Token = "0x40012A8")]
		OnDamage,
		// Token: 0x0400193F RID: 6463
		[Token(Token = "0x40012A9")]
		OnKilledEnemy,
		// Token: 0x04001940 RID: 6464
		[Token(Token = "0x40012AA")]
		Num
	}
}
