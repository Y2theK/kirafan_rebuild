﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005D4 RID: 1492
	[Token(Token = "0x20004C7")]
	[StructLayout(3, Size = 4)]
	public enum ePersonalityType
	{
		// Token: 0x04001E4F RID: 7759
		[Token(Token = "0x40017B9")]
		Normal,
		// Token: 0x04001E50 RID: 7760
		[Token(Token = "0x40017BA")]
		Boke,
		// Token: 0x04001E51 RID: 7761
		[Token(Token = "0x40017BB")]
		Tsukkomi,
		// Token: 0x04001E52 RID: 7762
		[Token(Token = "0x40017BC")]
		Tennen
	}
}
