﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005BE RID: 1470
	[Token(Token = "0x20004B1")]
	[StructLayout(3, Size = 4)]
	public enum ePlayBGMCategory
	{
		// Token: 0x04001BB1 RID: 7089
		[Token(Token = "0x400151B")]
		Default,
		// Token: 0x04001BB2 RID: 7090
		[Token(Token = "0x400151C")]
		Limited
	}
}
