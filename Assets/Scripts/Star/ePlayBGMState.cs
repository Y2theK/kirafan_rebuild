﻿namespace Star {
    public enum ePlayBGMState {
        PlayState_None,
        PlayState_HomeTown,
        PlayState_Training,
        PlayState_Edit,
        PlayState_Shop,
        PlayState_Room,
        PlayState_Present,
        PlayState_MainMenu,
        PlayState_Signup,
        PlayState_Quest,
        PlayState_Quest_Event
    }
}
