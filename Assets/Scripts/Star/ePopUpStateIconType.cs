﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200056F RID: 1391
	[Token(Token = "0x2000462")]
	[StructLayout(3, Size = 4)]
	public enum ePopUpStateIconType
	{
		// Token: 0x04001956 RID: 6486
		[Token(Token = "0x40012C0")]
		None,
		// Token: 0x04001957 RID: 6487
		[Token(Token = "0x40012C1")]
		Confusion,
		// Token: 0x04001958 RID: 6488
		[Token(Token = "0x40012C2")]
		Paralysis,
		// Token: 0x04001959 RID: 6489
		[Token(Token = "0x40012C3")]
		Poison,
		// Token: 0x0400195A RID: 6490
		[Token(Token = "0x40012C4")]
		Bearish,
		// Token: 0x0400195B RID: 6491
		[Token(Token = "0x40012C5")]
		Sleep,
		// Token: 0x0400195C RID: 6492
		[Token(Token = "0x40012C6")]
		Unhappy,
		// Token: 0x0400195D RID: 6493
		[Token(Token = "0x40012C7")]
		Silence,
		// Token: 0x0400195E RID: 6494
		[Token(Token = "0x40012C8")]
		Isolation,
		// Token: 0x0400195F RID: 6495
		[Token(Token = "0x40012C9")]
		StatusAtkUp,
		// Token: 0x04001960 RID: 6496
		[Token(Token = "0x40012CA")]
		StatusMgcUp,
		// Token: 0x04001961 RID: 6497
		[Token(Token = "0x40012CB")]
		StatusDefUp,
		// Token: 0x04001962 RID: 6498
		[Token(Token = "0x40012CC")]
		StatusMDefUp,
		// Token: 0x04001963 RID: 6499
		[Token(Token = "0x40012CD")]
		StatusSpdUp,
		// Token: 0x04001964 RID: 6500
		[Token(Token = "0x40012CE")]
		StatusLuckUp,
		// Token: 0x04001965 RID: 6501
		[Token(Token = "0x40012CF")]
		CriticalDamageUp,
		// Token: 0x04001966 RID: 6502
		[Token(Token = "0x40012D0")]
		HateUp,
		// Token: 0x04001967 RID: 6503
		[Token(Token = "0x40012D1")]
		ElementFireUp,
		// Token: 0x04001968 RID: 6504
		[Token(Token = "0x40012D2")]
		ElementWaterUp,
		// Token: 0x04001969 RID: 6505
		[Token(Token = "0x40012D3")]
		ElementEarthUp,
		// Token: 0x0400196A RID: 6506
		[Token(Token = "0x40012D4")]
		ElementWindUp,
		// Token: 0x0400196B RID: 6507
		[Token(Token = "0x40012D5")]
		ElementMoonUp,
		// Token: 0x0400196C RID: 6508
		[Token(Token = "0x40012D6")]
		ElementSunUp,
		// Token: 0x0400196D RID: 6509
		[Token(Token = "0x40012D7")]
		AbnormalAdditionalProbabilityDown,
		// Token: 0x0400196E RID: 6510
		[Token(Token = "0x40012D8")]
		StatusAtkDown,
		// Token: 0x0400196F RID: 6511
		[Token(Token = "0x40012D9")]
		StatusMgcDown,
		// Token: 0x04001970 RID: 6512
		[Token(Token = "0x40012DA")]
		StatusDefDown,
		// Token: 0x04001971 RID: 6513
		[Token(Token = "0x40012DB")]
		StatusMDefDown,
		// Token: 0x04001972 RID: 6514
		[Token(Token = "0x40012DC")]
		StatusSpdDown,
		// Token: 0x04001973 RID: 6515
		[Token(Token = "0x40012DD")]
		StatusLuckDown,
		// Token: 0x04001974 RID: 6516
		[Token(Token = "0x40012DE")]
		CriticalDamageDown,
		// Token: 0x04001975 RID: 6517
		[Token(Token = "0x40012DF")]
		HateDown,
		// Token: 0x04001976 RID: 6518
		[Token(Token = "0x40012E0")]
		ElementFireDown,
		// Token: 0x04001977 RID: 6519
		[Token(Token = "0x40012E1")]
		ElementWaterDown,
		// Token: 0x04001978 RID: 6520
		[Token(Token = "0x40012E2")]
		ElementEarthDown,
		// Token: 0x04001979 RID: 6521
		[Token(Token = "0x40012E3")]
		ElementWindDown,
		// Token: 0x0400197A RID: 6522
		[Token(Token = "0x40012E4")]
		ElementMoonDown,
		// Token: 0x0400197B RID: 6523
		[Token(Token = "0x40012E5")]
		ElementSunDown,
		// Token: 0x0400197C RID: 6524
		[Token(Token = "0x40012E6")]
		AbnormalAdditionalProbabilityUp,
		// Token: 0x0400197D RID: 6525
		[Token(Token = "0x40012E7")]
		Barrier,
		// Token: 0x0400197E RID: 6526
		[Token(Token = "0x40012E8")]
		DamageCut,
		// Token: 0x0400197F RID: 6527
		[Token(Token = "0x40012E9")]
		Regene,
		// Token: 0x04001980 RID: 6528
		[Token(Token = "0x40012EA")]
		AbsorbAttack,
		// Token: 0x04001981 RID: 6529
		[Token(Token = "0x40012EB")]
		StateAbnormalDisable,
		// Token: 0x04001982 RID: 6530
		[Token(Token = "0x40012EC")]
		LoadFactorReduce,
		// Token: 0x04001983 RID: 6531
		[Token(Token = "0x40012ED")]
		WeakElementBonus,
		// Token: 0x04001984 RID: 6532
		[Token(Token = "0x40012EE")]
		NextAtkUp,
		// Token: 0x04001985 RID: 6533
		[Token(Token = "0x40012EF")]
		NextMgcUp,
		// Token: 0x04001986 RID: 6534
		[Token(Token = "0x40012F0")]
		NextCritical,
		// Token: 0x04001987 RID: 6535
		[Token(Token = "0x40012F1")]
		TogetherAttackChainCoefChange,
		// Token: 0x04001988 RID: 6536
		[Token(Token = "0x40012F2")]
		TogetherAttackGaugeUp,
		// Token: 0x04001989 RID: 6537
		[Token(Token = "0x40012F3")]
		TogetherAttackGaugeDown,
		// Token: 0x0400198A RID: 6538
		[Token(Token = "0x40012F4")]
		TogetherAttackGaugeUpOnDamage,
		// Token: 0x0400198B RID: 6539
		[Token(Token = "0x40012F5")]
		RecastUp,
		// Token: 0x0400198C RID: 6540
		[Token(Token = "0x40012F6")]
		RecastDown,
		// Token: 0x0400198D RID: 6541
		[Token(Token = "0x40012F7")]
		ChargeCountUp,
		// Token: 0x0400198E RID: 6542
		[Token(Token = "0x40012F8")]
		ChargeCountDown,
		// Token: 0x0400198F RID: 6543
		[Token(Token = "0x40012F9")]
		ElementChangeFire,
		// Token: 0x04001990 RID: 6544
		[Token(Token = "0x40012FA")]
		ElementChangeWater,
		// Token: 0x04001991 RID: 6545
		[Token(Token = "0x40012FB")]
		ElementChangeEarth,
		// Token: 0x04001992 RID: 6546
		[Token(Token = "0x40012FC")]
		ElementChangeWind,
		// Token: 0x04001993 RID: 6547
		[Token(Token = "0x40012FD")]
		ElementChangeMoon,
		// Token: 0x04001994 RID: 6548
		[Token(Token = "0x40012FE")]
		ElementChangeSun,
		// Token: 0x04001995 RID: 6549
		[Token(Token = "0x40012FF")]
		StatusReset_Up,
		// Token: 0x04001996 RID: 6550
		[Token(Token = "0x4001300")]
		StatusReset_Down,
		// Token: 0x04001997 RID: 6551
		[Token(Token = "0x4001301")]
		StatusReset_Both,
		// Token: 0x04001998 RID: 6552
		[Token(Token = "0x4001302")]
		StatusChangeDisableUp,
		// Token: 0x04001999 RID: 6553
		[Token(Token = "0x4001303")]
		StatusChangeDisableDown,
		// Token: 0x0400199A RID: 6554
		[Token(Token = "0x4001304")]
		Num
	}
}
