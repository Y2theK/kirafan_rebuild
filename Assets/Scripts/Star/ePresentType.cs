﻿namespace Star {
    public enum ePresentType {
        None,
        Chara,
        Item,
        Weapon,
        TownFacility,
        RoomObject,
        MasterOrb,
        KRRPoint,
        Gold,
        UnlimitedGem,
        LimitedGem,
        PackageItem,
        AllowRoomObject,
        MasterOrbLvUp,
        Achievement
    }
}
