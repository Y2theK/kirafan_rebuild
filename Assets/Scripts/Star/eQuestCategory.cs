﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200057C RID: 1404
	[Token(Token = "0x200046F")]
	[StructLayout(3, Size = 4)]
	public enum eQuestCategory
	{
		// Token: 0x040019EA RID: 6634
		[Token(Token = "0x4001354")]
		None = -1,
		// Token: 0x040019EB RID: 6635
		[Token(Token = "0x4001355")]
		Chapter1,
		// Token: 0x040019EC RID: 6636
		[Token(Token = "0x4001356")]
		Event,
		// Token: 0x040019ED RID: 6637
		[Token(Token = "0x4001357")]
		Chara,
		// Token: 0x040019EE RID: 6638
		[Token(Token = "0x4001358")]
		Offer,
		// Token: 0x040019EF RID: 6639
		[Token(Token = "0x4001359")]
		Chapter2
	}
}
