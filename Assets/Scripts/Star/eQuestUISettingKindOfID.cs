﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004EB RID: 1259
	[Token(Token = "0x20003E1")]
	[StructLayout(3, Size = 4)]
	public enum eQuestUISettingKindOfID
	{
		// Token: 0x0400189A RID: 6298
		[Token(Token = "0x4001209")]
		Event,
		// Token: 0x0400189B RID: 6299
		[Token(Token = "0x400120A")]
		Group,
		// Token: 0x0400189C RID: 6300
		[Token(Token = "0x400120B")]
		Character
	}
}
