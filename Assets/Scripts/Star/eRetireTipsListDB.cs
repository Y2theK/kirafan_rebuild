﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000589 RID: 1417
	[Token(Token = "0x200047C")]
	[StructLayout(3, Size = 4)]
	public enum eRetireTipsListDB
	{
		// Token: 0x04001A1D RID: 6685
		[Token(Token = "0x4001387")]
		Retire_000,
		// Token: 0x04001A1E RID: 6686
		[Token(Token = "0x4001388")]
		Retire_001,
		// Token: 0x04001A1F RID: 6687
		[Token(Token = "0x4001389")]
		Retire_002,
		// Token: 0x04001A20 RID: 6688
		[Token(Token = "0x400138A")]
		Retire_003,
		// Token: 0x04001A21 RID: 6689
		[Token(Token = "0x400138B")]
		Retire_004,
		// Token: 0x04001A22 RID: 6690
		[Token(Token = "0x400138C")]
		Retire_005,
		// Token: 0x04001A23 RID: 6691
		[Token(Token = "0x400138D")]
		Retire_006,
		// Token: 0x04001A24 RID: 6692
		[Token(Token = "0x400138E")]
		Max
	}
}
