﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200059A RID: 1434
	[Token(Token = "0x200048D")]
	[StructLayout(3, Size = 4)]
	public enum eRoomActionCondCate
	{
		// Token: 0x04001A82 RID: 6786
		[Token(Token = "0x40013EC")]
		None,
		// Token: 0x04001A83 RID: 6787
		[Token(Token = "0x40013ED")]
		CardID,
		// Token: 0x04001A84 RID: 6788
		[Token(Token = "0x40013EE")]
		Character,
		// Token: 0x04001A85 RID: 6789
		[Token(Token = "0x40013EF")]
		Title
	}
}
