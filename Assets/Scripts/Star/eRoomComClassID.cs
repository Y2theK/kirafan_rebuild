﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000810 RID: 2064
	[Token(Token = "0x2000613")]
	[StructLayout(3, Size = 4)]
	public enum eRoomComClassID
	{
		// Token: 0x04003098 RID: 12440
		[Token(Token = "0x40024EC")]
		OpenUI = 2,
		// Token: 0x04003099 RID: 12441
		[Token(Token = "0x40024ED")]
		ActionPopup,
		// Token: 0x0400309A RID: 12442
		[Token(Token = "0x40024EE")]
		ModelChange,
		// Token: 0x0400309B RID: 12443
		[Token(Token = "0x40024EF")]
		MoveEditMode
	}
}
