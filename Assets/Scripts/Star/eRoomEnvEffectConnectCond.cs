﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200058F RID: 1423
	[Token(Token = "0x2000482")]
	[StructLayout(3, Size = 4)]
	public enum eRoomEnvEffectConnectCond
	{
		// Token: 0x04001A34 RID: 6708
		[Token(Token = "0x400139E")]
		None,
		// Token: 0x04001A35 RID: 6709
		[Token(Token = "0x400139F")]
		Chara,
		// Token: 0x04001A36 RID: 6710
		[Token(Token = "0x40013A0")]
		Object
	}
}
