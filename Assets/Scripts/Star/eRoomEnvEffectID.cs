﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200058D RID: 1421
	[Token(Token = "0x2000480")]
	[StructLayout(3, Size = 4)]
	public enum eRoomEnvEffectID
	{
		// Token: 0x04001A2C RID: 6700
		[Token(Token = "0x4001396")]
		Ripple00,
		// Token: 0x04001A2D RID: 6701
		[Token(Token = "0x4001397")]
		Ripple01
	}
}
