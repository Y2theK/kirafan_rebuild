﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200058E RID: 1422
	[Token(Token = "0x2000481")]
	[StructLayout(3, Size = 4)]
	public enum eRoomEnvEffectLayer
	{
		// Token: 0x04001A2F RID: 6703
		[Token(Token = "0x4001399")]
		Floor,
		// Token: 0x04001A30 RID: 6704
		[Token(Token = "0x400139A")]
		UnderChara,
		// Token: 0x04001A31 RID: 6705
		[Token(Token = "0x400139B")]
		OverChara,
		// Token: 0x04001A32 RID: 6706
		[Token(Token = "0x400139C")]
		Always
	}
}
