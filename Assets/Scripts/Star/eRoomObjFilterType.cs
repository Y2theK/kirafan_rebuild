﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000596 RID: 1430
	[Token(Token = "0x2000489")]
	[StructLayout(3, Size = 4)]
	public enum eRoomObjFilterType
	{
		// Token: 0x04001A51 RID: 6737
		[Token(Token = "0x40013BB")]
		Season,
		// Token: 0x04001A52 RID: 6738
		[Token(Token = "0x40013BC")]
		Motif,
		// Token: 0x04001A53 RID: 6739
		[Token(Token = "0x40013BD")]
		CharaAction,
		// Token: 0x04001A54 RID: 6740
		[Token(Token = "0x40013BE")]
		Category
	}
}
