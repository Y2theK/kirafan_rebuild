﻿namespace Star {
    public enum eRoomObjectCategory {
        Desk,
        Chair,
        Storage,
        Bedding,
        Appliances,
        Goods,
        Hobby,
        WallDecoration,
        Carpet,
        Screen,
        Floor,
        Wall,
        Background,
        Msgboard,
        Num
    }
}
