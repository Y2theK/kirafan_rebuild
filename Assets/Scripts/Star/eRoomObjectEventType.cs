﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000599 RID: 1433
	[Token(Token = "0x200048C")]
	[StructLayout(3, Size = 4)]
	public enum eRoomObjectEventType
	{
		// Token: 0x04001A7B RID: 6779
		[Token(Token = "0x40013E5")]
		None = -1,
		// Token: 0x04001A7C RID: 6780
		[Token(Token = "0x40013E6")]
		Sit,
		// Token: 0x04001A7D RID: 6781
		[Token(Token = "0x40013E7")]
		Sleep,
		// Token: 0x04001A7E RID: 6782
		[Token(Token = "0x40013E8")]
		PlayAnime,
		// Token: 0x04001A7F RID: 6783
		[Token(Token = "0x40013E9")]
		SitBook,
		// Token: 0x04001A80 RID: 6784
		[Token(Token = "0x40013EA")]
		PlayAnime2
	}
}
