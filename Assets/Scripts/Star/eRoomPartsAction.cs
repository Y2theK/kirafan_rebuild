﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A49 RID: 2633
	[Token(Token = "0x2000756")]
	[StructLayout(3, Size = 4)]
	public enum eRoomPartsAction
	{
		// Token: 0x04003CF1 RID: 15601
		[Token(Token = "0x4002BEE")]
		Move,
		// Token: 0x04003CF2 RID: 15602
		[Token(Token = "0x4002BEF")]
		Bind
	}
}
