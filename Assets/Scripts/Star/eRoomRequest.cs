﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A47 RID: 2631
	[Token(Token = "0x2000754")]
	[StructLayout(3, Size = 4)]
	public enum eRoomRequest
	{
		// Token: 0x04003CE9 RID: 15593
		[Token(Token = "0x4002BE6")]
		ChangeModel,
		// Token: 0x04003CEA RID: 15594
		[Token(Token = "0x4002BE7")]
		ChangeWall,
		// Token: 0x04003CEB RID: 15595
		[Token(Token = "0x4002BE8")]
		ChangeFloor,
		// Token: 0x04003CEC RID: 15596
		[Token(Token = "0x4002BE9")]
		ChangeBG,
		// Token: 0x04003CED RID: 15597
		[Token(Token = "0x4002BEA")]
		CharaRelease
	}
}
