﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200094B RID: 2379
	[Token(Token = "0x20006B8")]
	[StructLayout(3, Size = 4)]
	public enum eRoomScriptCommand
	{
		// Token: 0x040037FE RID: 14334
		[Token(Token = "0x400287B")]
		AnimePlay,
		// Token: 0x040037FF RID: 14335
		[Token(Token = "0x400287C")]
		Wait,
		// Token: 0x04003800 RID: 14336
		[Token(Token = "0x400287D")]
		AnimeWait,
		// Token: 0x04003801 RID: 14337
		[Token(Token = "0x400287E")]
		Bind,
		// Token: 0x04003802 RID: 14338
		[Token(Token = "0x400287F")]
		NonBind,
		// Token: 0x04003803 RID: 14339
		[Token(Token = "0x4002880")]
		ViewMode,
		// Token: 0x04003804 RID: 14340
		[Token(Token = "0x4002881")]
		Move,
		// Token: 0x04003805 RID: 14341
		[Token(Token = "0x4002882")]
		PopupMsg,
		// Token: 0x04003806 RID: 14342
		[Token(Token = "0x4002883")]
		SePlay,
		// Token: 0x04003807 RID: 14343
		[Token(Token = "0x4002884")]
		Jump,
		// Token: 0x04003808 RID: 14344
		[Token(Token = "0x4002885")]
		FaceAnime,
		// Token: 0x04003809 RID: 14345
		[Token(Token = "0x4002886")]
		ProgramEvent,
		// Token: 0x0400380A RID: 14346
		[Token(Token = "0x4002887")]
		ObjectDir,
		// Token: 0x0400380B RID: 14347
		[Token(Token = "0x4002888")]
		Effect,
		// Token: 0x0400380C RID: 14348
		[Token(Token = "0x4002889")]
		BaseTrs,
		// Token: 0x0400380D RID: 14349
		[Token(Token = "0x400288A")]
		PosAnime,
		// Token: 0x0400380E RID: 14350
		[Token(Token = "0x400288B")]
		RotAnime,
		// Token: 0x0400380F RID: 14351
		[Token(Token = "0x400288C")]
		BindRot,
		// Token: 0x04003810 RID: 14352
		[Token(Token = "0x400288D")]
		FrameChange,
		// Token: 0x04003811 RID: 14353
		[Token(Token = "0x400288E")]
		CharaViewMode,
		// Token: 0x04003812 RID: 14354
		[Token(Token = "0x400288F")]
		ObjAnime,
		// Token: 0x04003813 RID: 14355
		[Token(Token = "0x4002890")]
		CharaCfg,
		// Token: 0x04003814 RID: 14356
		[Token(Token = "0x4002891")]
		CueSe,
		// Token: 0x04003815 RID: 14357
		[Token(Token = "0x4002892")]
		ObjCfg,
		// Token: 0x04003816 RID: 14358
		[Token(Token = "0x4002893")]
		ObjLinkActive,
		// Token: 0x04003817 RID: 14359
		[Token(Token = "0x4002894")]
		ReflectMoveFlg,
		// Token: 0x04003818 RID: 14360
		[Token(Token = "0x4002895")]
		ForceViewMode,
		// Token: 0x04003819 RID: 14361
		[Token(Token = "0x4002896")]
		ResetBaseRot,
		// Token: 0x0400381A RID: 14362
		[Token(Token = "0x4002897")]
		BaseTrsNonRev,
		// Token: 0x0400381B RID: 14363
		[Token(Token = "0x4002898")]
		ResetAnime,
		// Token: 0x0400381C RID: 14364
		[Token(Token = "0x4002899")]
		ResetObjAnime
	}
}
