﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000940 RID: 2368
	[Token(Token = "0x20006B2")]
	[StructLayout(3, Size = 4)]
	public enum eRoomSleepType
	{
		// Token: 0x040037C1 RID: 14273
		[Token(Token = "0x400285E")]
		Loop,
		// Token: 0x040037C2 RID: 14274
		[Token(Token = "0x400285F")]
		OneShot,
		// Token: 0x040037C3 RID: 14275
		[Token(Token = "0x4002860")]
		SetUpSleep
	}
}
