﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000814 RID: 2068
	[Token(Token = "0x2000617")]
	[StructLayout(3, Size = 4)]
	public enum eRoomUICategory
	{
		// Token: 0x040030A2 RID: 12450
		[Token(Token = "0x40024F6")]
		CharaTouch,
		// Token: 0x040030A3 RID: 12451
		[Token(Token = "0x40024F7")]
		CharaView
	}
}
