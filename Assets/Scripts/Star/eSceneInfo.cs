﻿namespace Star {
    public enum eSceneInfo {
        None = -1,
        Home,
        Present,
        PresentHistory,
        Quest,
        QuestMain,
        QuestWeek,
        QuestChallenge,
        Edit,
        PartyEdit,
        SupportEdit,
        WeaponEquip,
        Upgrade,
        LimitBreak,
        Evolution,
        MasterEquip,
        Member,
        CharaList,
        CharaDetail,
        Shop,
        ShopTrade,
        NormalTrade,
        LimitTrade,
        TradeConfirm,
        ItemSale,
        SaleConfirm,
        ShopWeapon,
        WeaponCreate,
        WeaponCreateConfirm,
        WeaponUpgrade,
        WeaponUpgradeConfirm,
        WeaponSale,
        ShopBuild,
        ShopBuildPointSelect,
        ShopBuildObjSelect,
        Town,
        Schedule,
        Room,
        RoomMain,
        RoomSub,
        Gacha,
        Menu,
        UserData,
        Friend,
        Library,
        LibraryTitle,
        LibraryWord,
        LibraryADV,
        LibraryCharaADV,
        Option,
        Notification,
        Support,
        Inherit,
        PurchaseHistory,
        Training,
        CharacterDictionary,
        Chest,
        WeaponEvolution,
        BeginnersMission,
        WeaponSkillLvUP,
        ViewChange,
        ContentRoom,
        Achievement,
        Market,
        LibraryOPMovie,
        Ability,
        AbilitySphereList,
        AbilitySphereUpgrade
    }
}
