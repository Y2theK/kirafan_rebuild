﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005D5 RID: 1493
	[Token(Token = "0x20004C8")]
	[StructLayout(3, Size = 4)]
	public enum eScheduleConditionDayType
	{
		// Token: 0x04001E54 RID: 7764
		[Token(Token = "0x40017BE")]
		None = -1,
		// Token: 0x04001E55 RID: 7765
		[Token(Token = "0x40017BF")]
		Weekday,
		// Token: 0x04001E56 RID: 7766
		[Token(Token = "0x40017C0")]
		Holiday
	}
}
