﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000703 RID: 1795
	[Token(Token = "0x2000592")]
	[StructLayout(3, Size = 4)]
	public enum eScheduleSegName
	{
		// Token: 0x04002A79 RID: 10873
		[Token(Token = "0x40022A6")]
		Sleep,
		// Token: 0x04002A7A RID: 10874
		[Token(Token = "0x40022A7")]
		Room,
		// Token: 0x04002A7B RID: 10875
		[Token(Token = "0x40022A8")]
		Office,
		// Token: 0x04002A7C RID: 10876
		[Token(Token = "0x40022A9")]
		Buf,
		// Token: 0x04002A7D RID: 10877
		[Token(Token = "0x40022AA")]
		System
	}
}
