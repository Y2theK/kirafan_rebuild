﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000702 RID: 1794
	[Token(Token = "0x2000591")]
	[StructLayout(3, Size = 4)]
	public enum eScheduleTypeUIType
	{
		// Token: 0x04002A72 RID: 10866
		[Token(Token = "0x400229F")]
		Non,
		// Token: 0x04002A73 RID: 10867
		[Token(Token = "0x40022A0")]
		Sleep,
		// Token: 0x04002A74 RID: 10868
		[Token(Token = "0x40022A1")]
		Room,
		// Token: 0x04002A75 RID: 10869
		[Token(Token = "0x40022A2")]
		Office,
		// Token: 0x04002A76 RID: 10870
		[Token(Token = "0x40022A3")]
		Town,
		// Token: 0x04002A77 RID: 10871
		[Token(Token = "0x40022A4")]
		System
	}
}
