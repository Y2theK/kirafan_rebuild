﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000373 RID: 883
	[Token(Token = "0x20002E9")]
	[StructLayout(3, Size = 4)]
	public enum eSingleTargetPriorityWhenHateSame
	{
		// Token: 0x04000D86 RID: 3462
		[Token(Token = "0x4000AC2")]
		ABC,
		// Token: 0x04000D87 RID: 3463
		[Token(Token = "0x4000AC3")]
		CBA
	}
}
