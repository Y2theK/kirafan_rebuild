﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000425 RID: 1061
	[Token(Token = "0x2000348")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionEffectAttachTo
	{
		// Token: 0x040012C3 RID: 4803
		[Token(Token = "0x4000D8C")]
		WeaponLeft,
		// Token: 0x040012C4 RID: 4804
		[Token(Token = "0x4000D8D")]
		WeaponRight
	}
}
