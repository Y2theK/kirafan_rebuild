﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000422 RID: 1058
	[Token(Token = "0x2000345")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionEffectPosType
	{
		// Token: 0x040012B3 RID: 4787
		[Token(Token = "0x4000D7C")]
		Self,
		// Token: 0x040012B4 RID: 4788
		[Token(Token = "0x4000D7D")]
		SkillTarget_Tgt,
		// Token: 0x040012B5 RID: 4789
		[Token(Token = "0x4000D7E")]
		SkillTarget_My,
		// Token: 0x040012B6 RID: 4790
		[Token(Token = "0x4000D7F")]
		PartyCenter_Tgt,
		// Token: 0x040012B7 RID: 4791
		[Token(Token = "0x4000D80")]
		PartyCenter_My
	}
}
