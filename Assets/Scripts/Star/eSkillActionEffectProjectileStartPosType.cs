﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000424 RID: 1060
	[Token(Token = "0x2000347")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionEffectProjectileStartPosType
	{
		// Token: 0x040012BF RID: 4799
		[Token(Token = "0x4000D88")]
		Self,
		// Token: 0x040012C0 RID: 4800
		[Token(Token = "0x4000D89")]
		WeaponLeft,
		// Token: 0x040012C1 RID: 4801
		[Token(Token = "0x4000D8A")]
		WeaponRight
	}
}
