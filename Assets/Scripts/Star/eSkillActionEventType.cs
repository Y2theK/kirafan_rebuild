﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000421 RID: 1057
	[Token(Token = "0x2000344")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionEventType
	{
		// Token: 0x040012A1 RID: 4769
		[Token(Token = "0x4000D6A")]
		Solve,
		// Token: 0x040012A2 RID: 4770
		[Token(Token = "0x4000D6B")]
		DamageAnim,
		// Token: 0x040012A3 RID: 4771
		[Token(Token = "0x4000D6C")]
		DamageEffect,
		// Token: 0x040012A4 RID: 4772
		[Token(Token = "0x4000D6D")]
		EffectPlay,
		// Token: 0x040012A5 RID: 4773
		[Token(Token = "0x4000D6E")]
		EffectProjectile_Straight,
		// Token: 0x040012A6 RID: 4774
		[Token(Token = "0x4000D6F")]
		EffectProjectile_Parabola,
		// Token: 0x040012A7 RID: 4775
		[Token(Token = "0x4000D70")]
		EffectProjectile_Penetrate,
		// Token: 0x040012A8 RID: 4776
		[Token(Token = "0x4000D71")]
		EffectAttach,
		// Token: 0x040012A9 RID: 4777
		[Token(Token = "0x4000D72")]
		EffectDetach,
		// Token: 0x040012AA RID: 4778
		[Token(Token = "0x4000D73")]
		TrailAttach,
		// Token: 0x040012AB RID: 4779
		[Token(Token = "0x4000D74")]
		TrailDetach,
		// Token: 0x040012AC RID: 4780
		[Token(Token = "0x4000D75")]
		WeaponThrow_Parabola,
		// Token: 0x040012AD RID: 4781
		[Token(Token = "0x4000D76")]
		WeaponVisible,
		// Token: 0x040012AE RID: 4782
		[Token(Token = "0x4000D77")]
		WeaponReset,
		// Token: 0x040012AF RID: 4783
		[Token(Token = "0x4000D78")]
		PlaySE,
		// Token: 0x040012B0 RID: 4784
		[Token(Token = "0x4000D79")]
		PlayVoice,
		// Token: 0x040012B1 RID: 4785
		[Token(Token = "0x4000D7A")]
		Num
	}
}
