﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000423 RID: 1059
	[Token(Token = "0x2000346")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionLocatorType
	{
		// Token: 0x040012B9 RID: 4793
		[Token(Token = "0x4000D82")]
		Origin = -1,
		// Token: 0x040012BA RID: 4794
		[Token(Token = "0x4000D83")]
		Body,
		// Token: 0x040012BB RID: 4795
		[Token(Token = "0x4000D84")]
		WpnTop = 0,
		// Token: 0x040012BC RID: 4796
		[Token(Token = "0x4000D85")]
		WpnCenter,
		// Token: 0x040012BD RID: 4797
		[Token(Token = "0x4000D86")]
		WpnBottom
	}
}
