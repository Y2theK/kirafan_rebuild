﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000426 RID: 1062
	[Token(Token = "0x2000349")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionTrailAttachTo
	{
		// Token: 0x040012C6 RID: 4806
		[Token(Token = "0x4000D8F")]
		WeaponLeft,
		// Token: 0x040012C7 RID: 4807
		[Token(Token = "0x4000D90")]
		WeaponRight
	}
}
