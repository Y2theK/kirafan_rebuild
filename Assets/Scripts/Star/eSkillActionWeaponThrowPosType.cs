﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000427 RID: 1063
	[Token(Token = "0x200034A")]
	[StructLayout(3, Size = 4)]
	public enum eSkillActionWeaponThrowPosType
	{
		// Token: 0x040012C9 RID: 4809
		[Token(Token = "0x4000D92")]
		Self,
		// Token: 0x040012CA RID: 4810
		[Token(Token = "0x4000D93")]
		SkillTarget_Tgt,
		// Token: 0x040012CB RID: 4811
		[Token(Token = "0x4000D94")]
		PartyCenter_Tgt
	}
}
