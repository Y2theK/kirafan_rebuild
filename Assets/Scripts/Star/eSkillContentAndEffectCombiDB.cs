﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005A3 RID: 1443
	[Token(Token = "0x2000496")]
	[StructLayout(3, Size = 4)]
	public enum eSkillContentAndEffectCombiDB
	{
		// Token: 0x04001ADB RID: 6875
		[Token(Token = "0x4001445")]
		Recover,
		// Token: 0x04001ADC RID: 6876
		[Token(Token = "0x4001446")]
		StatusChange_Up,
		// Token: 0x04001ADD RID: 6877
		[Token(Token = "0x4001447")]
		StatusChange_Down,
		// Token: 0x04001ADE RID: 6878
		[Token(Token = "0x4001448")]
		Abnormal,
		// Token: 0x04001ADF RID: 6879
		[Token(Token = "0x4001449")]
		AbnormalRecover,
		// Token: 0x04001AE0 RID: 6880
		[Token(Token = "0x400144A")]
		AbnormalAdditionalProbability,
		// Token: 0x04001AE1 RID: 6881
		[Token(Token = "0x400144B")]
		ElementResist_FireUp,
		// Token: 0x04001AE2 RID: 6882
		[Token(Token = "0x400144C")]
		ElementResist_WaterUp,
		// Token: 0x04001AE3 RID: 6883
		[Token(Token = "0x400144D")]
		ElementResist_EarthUp,
		// Token: 0x04001AE4 RID: 6884
		[Token(Token = "0x400144E")]
		ElementResist_WindUp,
		// Token: 0x04001AE5 RID: 6885
		[Token(Token = "0x400144F")]
		ElementResist_MoonUp,
		// Token: 0x04001AE6 RID: 6886
		[Token(Token = "0x4001450")]
		ElementResist_SunUp,
		// Token: 0x04001AE7 RID: 6887
		[Token(Token = "0x4001451")]
		ElementResist_FireDown,
		// Token: 0x04001AE8 RID: 6888
		[Token(Token = "0x4001452")]
		ElementResist_WaterDown,
		// Token: 0x04001AE9 RID: 6889
		[Token(Token = "0x4001453")]
		ElementResist_EarthDown,
		// Token: 0x04001AEA RID: 6890
		[Token(Token = "0x4001454")]
		ElementResist_WindDown,
		// Token: 0x04001AEB RID: 6891
		[Token(Token = "0x4001455")]
		ElementResist_MoonDown,
		// Token: 0x04001AEC RID: 6892
		[Token(Token = "0x4001456")]
		ElementResist_SunDown,
		// Token: 0x04001AED RID: 6893
		[Token(Token = "0x4001457")]
		ElementChange,
		// Token: 0x04001AEE RID: 6894
		[Token(Token = "0x4001458")]
		WeakElementBonus,
		// Token: 0x04001AEF RID: 6895
		[Token(Token = "0x4001459")]
		NextAttackUp,
		// Token: 0x04001AF0 RID: 6896
		[Token(Token = "0x400145A")]
		NextAttackCritical,
		// Token: 0x04001AF1 RID: 6897
		[Token(Token = "0x400145B")]
		Barrier,
		// Token: 0x04001AF2 RID: 6898
		[Token(Token = "0x400145C")]
		RecastChange,
		// Token: 0x04001AF3 RID: 6899
		[Token(Token = "0x400145D")]
		HateChange,
		// Token: 0x04001AF4 RID: 6900
		[Token(Token = "0x400145E")]
		ChargeChange,
		// Token: 0x04001AF5 RID: 6901
		[Token(Token = "0x400145F")]
		ChainCoefChange,
		// Token: 0x04001AF6 RID: 6902
		[Token(Token = "0x4001460")]
		CriticalDamageUp,
		// Token: 0x04001AF7 RID: 6903
		[Token(Token = "0x4001461")]
		CriticalDamageDown,
		// Token: 0x04001AF8 RID: 6904
		[Token(Token = "0x4001462")]
		TogetherAttackGaugeUpOnDamage,
		// Token: 0x04001AF9 RID: 6905
		[Token(Token = "0x4001463")]
		Max
	}
}
