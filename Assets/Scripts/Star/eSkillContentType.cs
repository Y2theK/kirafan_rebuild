﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005AB RID: 1451
	[Token(Token = "0x200049E")]
	[StructLayout(3, Size = 4)]
	public enum eSkillContentType
	{
		// Token: 0x04001B0F RID: 6927
		[Token(Token = "0x4001479")]
		Attack,
		// Token: 0x04001B10 RID: 6928
		[Token(Token = "0x400147A")]
		Recover,
		// Token: 0x04001B11 RID: 6929
		[Token(Token = "0x400147B")]
		StatusChange,
		// Token: 0x04001B12 RID: 6930
		[Token(Token = "0x400147C")]
		StatusChangeReset,
		// Token: 0x04001B13 RID: 6931
		[Token(Token = "0x400147D")]
		Abnormal,
		// Token: 0x04001B14 RID: 6932
		[Token(Token = "0x400147E")]
		AbnormalRecover,
		// Token: 0x04001B15 RID: 6933
		[Token(Token = "0x400147F")]
		AbnormalDisable,
		// Token: 0x04001B16 RID: 6934
		[Token(Token = "0x4001480")]
		AbnormalAdditionalProbability,
		// Token: 0x04001B17 RID: 6935
		[Token(Token = "0x4001481")]
		ElementResist,
		// Token: 0x04001B18 RID: 6936
		[Token(Token = "0x4001482")]
		ElementChange,
		// Token: 0x04001B19 RID: 6937
		[Token(Token = "0x4001483")]
		WeakElementBonus,
		// Token: 0x04001B1A RID: 6938
		[Token(Token = "0x4001484")]
		NextAttackUp,
		// Token: 0x04001B1B RID: 6939
		[Token(Token = "0x4001485")]
		NextAttackCritical,
		// Token: 0x04001B1C RID: 6940
		[Token(Token = "0x4001486")]
		Barrier,
		// Token: 0x04001B1D RID: 6941
		[Token(Token = "0x4001487")]
		RecastChange,
		// Token: 0x04001B1E RID: 6942
		[Token(Token = "0x4001488")]
		KiraraJumpGaugeChange,
		// Token: 0x04001B1F RID: 6943
		[Token(Token = "0x4001489")]
		KiraraJumpGaugeCoef,
		// Token: 0x04001B20 RID: 6944
		[Token(Token = "0x400148A")]
		OrderChange,
		// Token: 0x04001B21 RID: 6945
		[Token(Token = "0x400148B")]
		HateChange,
		// Token: 0x04001B22 RID: 6946
		[Token(Token = "0x400148C")]
		ChargeChange,
		// Token: 0x04001B23 RID: 6947
		[Token(Token = "0x400148D")]
		ChainCoefChange,
		// Token: 0x04001B24 RID: 6948
		[Token(Token = "0x400148E")]
		Card,
		// Token: 0x04001B25 RID: 6949
		[Token(Token = "0x400148F")]
		StunRecover,
		// Token: 0x04001B26 RID: 6950
		[Token(Token = "0x4001490")]
		Regene,
		// Token: 0x04001B27 RID: 6951
		[Token(Token = "0x4001491")]
		NoOperation,
		// Token: 0x04001B28 RID: 6952
		[Token(Token = "0x4001492")]
		LoadFactorReduce,
		// Token: 0x04001B29 RID: 6953
		[Token(Token = "0x4001493")]
		CriticalDamageChange,
		// Token: 0x04001B2A RID: 6954
		[Token(Token = "0x4001494")]
		AttackSelf,
		// Token: 0x04001B2B RID: 6955
		[Token(Token = "0x4001495")]
		RandomStatusChange,
		// Token: 0x04001B2C RID: 6956
		[Token(Token = "0x4001496")]
		KiraraJumpGaugeUpOnDamage,
		// Token: 0x04001B2D RID: 6957
		[Token(Token = "0x4001497")]
		StatusChangeDisable,
		// Token: 0x04001B2E RID: 6958
		[Token(Token = "0x4001498")]
		Num
	}
}
