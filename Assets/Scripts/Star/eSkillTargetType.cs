﻿namespace Star {
    public enum eSkillTargetType {
        Self,
        TgtSingle,
        TgtAll,
        MySingle,
        MyAll,
        Num
    }
}
