﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005B0 RID: 1456
	[Token(Token = "0x20004A3")]
	[StructLayout(3, Size = 4)]
	public enum eSkillType
	{
		// Token: 0x04001B42 RID: 6978
		[Token(Token = "0x40014AC")]
		NormalAttack_atk,
		// Token: 0x04001B43 RID: 6979
		[Token(Token = "0x40014AD")]
		NormalAttack_mgc,
		// Token: 0x04001B44 RID: 6980
		[Token(Token = "0x40014AE")]
		Guard,
		// Token: 0x04001B45 RID: 6981
		[Token(Token = "0x40014AF")]
		Attack_atk,
		// Token: 0x04001B46 RID: 6982
		[Token(Token = "0x40014B0")]
		Attack_mgc,
		// Token: 0x04001B47 RID: 6983
		[Token(Token = "0x40014B1")]
		Recovery,
		// Token: 0x04001B48 RID: 6984
		[Token(Token = "0x40014B2")]
		AssistBuff,
		// Token: 0x04001B49 RID: 6985
		[Token(Token = "0x40014B3")]
		AssistDeBuff,
		// Token: 0x04001B4A RID: 6986
		[Token(Token = "0x40014B4")]
		Other,
		// Token: 0x04001B4B RID: 6987
		[Token(Token = "0x40014B5")]
		Num
	}
}
