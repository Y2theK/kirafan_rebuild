﻿namespace Star {
    public enum eSoundVoiceControllListDB {
        TitleCall,
        TitleStart,
        Logo_A,
        Logo_B,
        Logo_C,
        Logo_D,
        Kanna_in,
        Raine_in,
        Poruka_in,
        Claire_in,
        Cork_in,
        Max
    }
}
