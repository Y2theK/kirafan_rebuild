﻿namespace Star {
    public enum eStateAbnormal {
        Confusion,
        Paralysis,
        Poison,
        Bearish,
        Sleep,
        Unhappy,
        Silence,
        Isolation,
        Num
    }
}
