﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005E1 RID: 1505
	[Token(Token = "0x20004D4")]
	[StructLayout(3, Size = 4)]
	public enum eStateIconCategory
	{
		// Token: 0x04001EF2 RID: 7922
		[Token(Token = "0x400185C")]
		None,
		// Token: 0x04001EF3 RID: 7923
		[Token(Token = "0x400185D")]
		Abnormal,
		// Token: 0x04001EF4 RID: 7924
		[Token(Token = "0x400185E")]
		BuffDebuff,
		// Token: 0x04001EF5 RID: 7925
		[Token(Token = "0x400185F")]
		Other
	}
}
