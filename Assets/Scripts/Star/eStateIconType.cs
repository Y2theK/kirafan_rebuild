﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005E0 RID: 1504
	[Token(Token = "0x20004D3")]
	[StructLayout(3, Size = 4)]
	public enum eStateIconType
	{
		// Token: 0x04001EBC RID: 7868
		[Token(Token = "0x4001826")]
		None,
		// Token: 0x04001EBD RID: 7869
		[Token(Token = "0x4001827")]
		Confusion,
		// Token: 0x04001EBE RID: 7870
		[Token(Token = "0x4001828")]
		Paralysis,
		// Token: 0x04001EBF RID: 7871
		[Token(Token = "0x4001829")]
		Poison,
		// Token: 0x04001EC0 RID: 7872
		[Token(Token = "0x400182A")]
		Bearish,
		// Token: 0x04001EC1 RID: 7873
		[Token(Token = "0x400182B")]
		Sleep,
		// Token: 0x04001EC2 RID: 7874
		[Token(Token = "0x400182C")]
		Unhappy,
		// Token: 0x04001EC3 RID: 7875
		[Token(Token = "0x400182D")]
		Silence,
		// Token: 0x04001EC4 RID: 7876
		[Token(Token = "0x400182E")]
		Isolation,
		// Token: 0x04001EC5 RID: 7877
		[Token(Token = "0x400182F")]
		StatusAtkUp,
		// Token: 0x04001EC6 RID: 7878
		[Token(Token = "0x4001830")]
		StatusMgcUp,
		// Token: 0x04001EC7 RID: 7879
		[Token(Token = "0x4001831")]
		StatusDefUp,
		// Token: 0x04001EC8 RID: 7880
		[Token(Token = "0x4001832")]
		StatusMDefUp,
		// Token: 0x04001EC9 RID: 7881
		[Token(Token = "0x4001833")]
		StatusSpdUp,
		// Token: 0x04001ECA RID: 7882
		[Token(Token = "0x4001834")]
		StatusLuckUp,
		// Token: 0x04001ECB RID: 7883
		[Token(Token = "0x4001835")]
		CriticalDamageUp,
		// Token: 0x04001ECC RID: 7884
		[Token(Token = "0x4001836")]
		HateUp,
		// Token: 0x04001ECD RID: 7885
		[Token(Token = "0x4001837")]
		ElementFireUp,
		// Token: 0x04001ECE RID: 7886
		[Token(Token = "0x4001838")]
		ElementWindUp,
		// Token: 0x04001ECF RID: 7887
		[Token(Token = "0x4001839")]
		ElementEarthUp,
		// Token: 0x04001ED0 RID: 7888
		[Token(Token = "0x400183A")]
		ElementWaterUp,
		// Token: 0x04001ED1 RID: 7889
		[Token(Token = "0x400183B")]
		ElementMoonUp,
		// Token: 0x04001ED2 RID: 7890
		[Token(Token = "0x400183C")]
		ElementSunUp,
		// Token: 0x04001ED3 RID: 7891
		[Token(Token = "0x400183D")]
		AbnormalAdditionalProbabilityDown,
		// Token: 0x04001ED4 RID: 7892
		[Token(Token = "0x400183E")]
		StatusAtkDown,
		// Token: 0x04001ED5 RID: 7893
		[Token(Token = "0x400183F")]
		StatusMgcDown,
		// Token: 0x04001ED6 RID: 7894
		[Token(Token = "0x4001840")]
		StatusDefDown,
		// Token: 0x04001ED7 RID: 7895
		[Token(Token = "0x4001841")]
		StatusMDefDown,
		// Token: 0x04001ED8 RID: 7896
		[Token(Token = "0x4001842")]
		StatusSpdDown,
		// Token: 0x04001ED9 RID: 7897
		[Token(Token = "0x4001843")]
		StatusLuckDown,
		// Token: 0x04001EDA RID: 7898
		[Token(Token = "0x4001844")]
		CriticalDamageDown,
		// Token: 0x04001EDB RID: 7899
		[Token(Token = "0x4001845")]
		HateDown,
		// Token: 0x04001EDC RID: 7900
		[Token(Token = "0x4001846")]
		ElementFireDown,
		// Token: 0x04001EDD RID: 7901
		[Token(Token = "0x4001847")]
		ElementWindDown,
		// Token: 0x04001EDE RID: 7902
		[Token(Token = "0x4001848")]
		ElementEarthDown,
		// Token: 0x04001EDF RID: 7903
		[Token(Token = "0x4001849")]
		ElementWaterDown,
		// Token: 0x04001EE0 RID: 7904
		[Token(Token = "0x400184A")]
		ElementMoonDown,
		// Token: 0x04001EE1 RID: 7905
		[Token(Token = "0x400184B")]
		ElementSunDown,
		// Token: 0x04001EE2 RID: 7906
		[Token(Token = "0x400184C")]
		AbnormalAdditionalProbabilityUp,
		// Token: 0x04001EE3 RID: 7907
		[Token(Token = "0x400184D")]
		Barrier,
		// Token: 0x04001EE4 RID: 7908
		[Token(Token = "0x400184E")]
		Regene,
		// Token: 0x04001EE5 RID: 7909
		[Token(Token = "0x400184F")]
		AbsorbAttack,
		// Token: 0x04001EE6 RID: 7910
		[Token(Token = "0x4001850")]
		StateAbnormalDisable,
		// Token: 0x04001EE7 RID: 7911
		[Token(Token = "0x4001851")]
		LoadFactorReduce,
		// Token: 0x04001EE8 RID: 7912
		[Token(Token = "0x4001852")]
		WeakElementBonus,
		// Token: 0x04001EE9 RID: 7913
		[Token(Token = "0x4001853")]
		NextAtkUp,
		// Token: 0x04001EEA RID: 7914
		[Token(Token = "0x4001854")]
		NextMgcUp,
		// Token: 0x04001EEB RID: 7915
		[Token(Token = "0x4001855")]
		NextCritical,
		// Token: 0x04001EEC RID: 7916
		[Token(Token = "0x4001856")]
		TogetherAttackChainCoefChange,
		// Token: 0x04001EED RID: 7917
		[Token(Token = "0x4001857")]
		TogetherAttackGaugeUpOnDamage,
		// Token: 0x04001EEE RID: 7918
		[Token(Token = "0x4001858")]
		StatusChangeDisableUp,
		// Token: 0x04001EEF RID: 7919
		[Token(Token = "0x4001859")]
		StatusChangeDisableDown,
		// Token: 0x04001EF0 RID: 7920
		[Token(Token = "0x400185A")]
		Num
	}
}
