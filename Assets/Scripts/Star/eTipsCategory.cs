﻿namespace Star {
    public enum eTipsCategory {
        None = -1,
        Common,
        Battle,
        Town,
        Room,
        Num
    }
}
