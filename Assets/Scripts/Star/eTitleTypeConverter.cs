﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008BF RID: 2239
	[Token(Token = "0x2000669")]
	[StructLayout(3)]
	public class eTitleTypeConverter
	{
		// Token: 0x060024C0 RID: 9408 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021E4")]
		[Address(RVA = "0x101621C18", Offset = "0x1621C18", VA = "0x101621C18")]
		public eTitleTypeConverter Setup()
		{
			return null;
		}

		// Token: 0x060024C1 RID: 9409 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021E5")]
		[Address(RVA = "0x101621CAC", Offset = "0x1621CAC", VA = "0x101621CAC")]
		public eTitleTypeConverter Setup(TitleListDB titleDB)
		{
			return null;
		}

		// Token: 0x060024C2 RID: 9410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021E6")]
		[Address(RVA = "0x101621CE0", Offset = "0x1621CE0", VA = "0x101621CE0")]
		public void SealNonplayableTitle()
		{
		}

		// Token: 0x060024C3 RID: 9411 RVA: 0x0000FCD8 File Offset: 0x0000DED8
		[Token(Token = "0x60021E7")]
		[Address(RVA = "0x101621E54", Offset = "0x1621E54", VA = "0x101621E54")]
		public int HowManyTitles()
		{
			return 0;
		}

		// Token: 0x060024C4 RID: 9412 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021E8")]
		[Address(RVA = "0x101621E80", Offset = "0x1621E80", VA = "0x101621E80")]
		public eTitleType[] GetOrderTitleTypes()
		{
			return null;
		}

		// Token: 0x060024C5 RID: 9413 RVA: 0x0000FCF0 File Offset: 0x0000DEF0
		[Token(Token = "0x60021E9")]
		[Address(RVA = "0x101621E88", Offset = "0x1621E88", VA = "0x101621E88")]
		public eTitleType ConvertFromOrderToTitleType(int order)
		{
			return eTitleType.Title_0000;
		}

		// Token: 0x060024C6 RID: 9414 RVA: 0x0000FD08 File Offset: 0x0000DF08
		[Token(Token = "0x60021EA")]
		[Address(RVA = "0x101621ED8", Offset = "0x1621ED8", VA = "0x101621ED8")]
		public int ConvertFromTitleTypeToOrder(eTitleType title)
		{
			return 0;
		}

		// Token: 0x060024C7 RID: 9415 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021EB")]
		[Address(RVA = "0x101621F70", Offset = "0x1621F70", VA = "0x101621F70")]
		public string ConvertFromOrderToTitleName(int order)
		{
			return null;
		}

		// Token: 0x060024C8 RID: 9416 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021EC")]
		[Address(RVA = "0x101621FB0", Offset = "0x1621FB0", VA = "0x101621FB0")]
		public string ConvertFromTitleTypeToTitleName(eTitleType title)
		{
			return null;
		}

		// Token: 0x060024C9 RID: 9417 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021ED")]
		[Address(RVA = "0x101621FDC", Offset = "0x1621FDC", VA = "0x101621FDC")]
		public string[] GetOrderTitles()
		{
			return null;
		}

		// Token: 0x060024CA RID: 9418 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60021EE")]
		[Address(RVA = "0x1016220F8", Offset = "0x16220F8", VA = "0x1016220F8")]
		public static eTitleType[] Sort(eTitleType[] source, bool isSeal)
		{
			return null;
		}

		// Token: 0x060024CB RID: 9419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021EF")]
		[Address(RVA = "0x10162236C", Offset = "0x162236C", VA = "0x10162236C")]
		public eTitleTypeConverter()
		{
		}

		// Token: 0x040034C7 RID: 13511
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002698")]
		private TitleListDB m_DB;

		// Token: 0x040034C8 RID: 13512
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002699")]
		private eTitleType[] m_Table;
	}
}
