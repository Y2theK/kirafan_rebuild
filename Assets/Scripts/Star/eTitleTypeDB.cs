﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000690 RID: 1680
	[Token(Token = "0x200055F")]
	[StructLayout(3, Size = 4)]
	public enum eTitleTypeDB
	{
		// Token: 0x040028F9 RID: 10489
		[Token(Token = "0x40021D6")]
		None = -1,
		// Token: 0x040028FA RID: 10490
		[Token(Token = "0x40021D7")]
		def1,
		// Token: 0x040028FB RID: 10491
		[Token(Token = "0x40021D8")]
		def2,
		// Token: 0x040028FC RID: 10492
		[Token(Token = "0x40021D9")]
		def3,
		// Token: 0x040028FD RID: 10493
		[Token(Token = "0x40021DA")]
		def4,
		// Token: 0x040028FE RID: 10494
		[Token(Token = "0x40021DB")]
		def5,
		// Token: 0x040028FF RID: 10495
		[Token(Token = "0x40021DC")]
		def6,
		// Token: 0x04002900 RID: 10496
		[Token(Token = "0x40021DD")]
		def7,
		// Token: 0x04002901 RID: 10497
		[Token(Token = "0x40021DE")]
		def8,
		// Token: 0x04002902 RID: 10498
		[Token(Token = "0x40021DF")]
		def9,
		// Token: 0x04002903 RID: 10499
		[Token(Token = "0x40021E0")]
		def10,
		// Token: 0x04002904 RID: 10500
		[Token(Token = "0x40021E1")]
		def11,
		// Token: 0x04002905 RID: 10501
		[Token(Token = "0x40021E2")]
		def12,
		// Token: 0x04002906 RID: 10502
		[Token(Token = "0x40021E3")]
		def13,
		// Token: 0x04002907 RID: 10503
		[Token(Token = "0x40021E4")]
		def14,
		// Token: 0x04002908 RID: 10504
		[Token(Token = "0x40021E5")]
		def15,
		// Token: 0x04002909 RID: 10505
		[Token(Token = "0x40021E6")]
		def16,
		// Token: 0x0400290A RID: 10506
		[Token(Token = "0x40021E7")]
		def17,
		// Token: 0x0400290B RID: 10507
		[Token(Token = "0x40021E8")]
		def18,
		// Token: 0x0400290C RID: 10508
		[Token(Token = "0x40021E9")]
		def19,
		// Token: 0x0400290D RID: 10509
		[Token(Token = "0x40021EA")]
		def20,
		// Token: 0x0400290E RID: 10510
		[Token(Token = "0x40021EB")]
		def21,
		// Token: 0x0400290F RID: 10511
		[Token(Token = "0x40021EC")]
		def22,
		// Token: 0x04002910 RID: 10512
		[Token(Token = "0x40021ED")]
		def23,
		// Token: 0x04002911 RID: 10513
		[Token(Token = "0x40021EE")]
		def24,
		// Token: 0x04002912 RID: 10514
		[Token(Token = "0x40021EF")]
		def25,
		// Token: 0x04002913 RID: 10515
		[Token(Token = "0x40021F0")]
		def26,
		// Token: 0x04002914 RID: 10516
		[Token(Token = "0x40021F1")]
		def27,
		// Token: 0x04002915 RID: 10517
		[Token(Token = "0x40021F2")]
		def28
	}
}
