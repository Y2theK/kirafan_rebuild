﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009FC RID: 2556
	[Token(Token = "0x200072D")]
	[StructLayout(3, Size = 4)]
	public enum eTouchStep
	{
		// Token: 0x04003AD4 RID: 15060
		[Token(Token = "0x4002A5E")]
		Begin,
		// Token: 0x04003AD5 RID: 15061
		[Token(Token = "0x4002A5F")]
		Drag,
		// Token: 0x04003AD6 RID: 15062
		[Token(Token = "0x4002A60")]
		End,
		// Token: 0x04003AD7 RID: 15063
		[Token(Token = "0x4002A61")]
		Cancel,
		// Token: 0x04003AD8 RID: 15064
		[Token(Token = "0x4002A62")]
		RayCheck
	}
}
