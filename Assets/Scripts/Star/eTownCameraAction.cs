﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AFE RID: 2814
	[Token(Token = "0x20007B0")]
	[StructLayout(3, Size = 4)]
	public enum eTownCameraAction
	{
		// Token: 0x04004132 RID: 16690
		[Token(Token = "0x4002E4D")]
		Pos,
		// Token: 0x04004133 RID: 16691
		[Token(Token = "0x4002E4E")]
		Size
	}
}
