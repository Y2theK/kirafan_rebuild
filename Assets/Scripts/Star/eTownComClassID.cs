﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200086A RID: 2154
	[Token(Token = "0x2000644")]
	[StructLayout(3, Size = 4)]
	public enum eTownComClassID
	{
		// Token: 0x040032D5 RID: 13013
		[Token(Token = "0x40025CA")]
		ComUI = 2,
		// Token: 0x040032D6 RID: 13014
		[Token(Token = "0x40025CB")]
		Build,
		// Token: 0x040032D7 RID: 13015
		[Token(Token = "0x40025CC")]
		CompleteBuild,
		// Token: 0x040032D8 RID: 13016
		[Token(Token = "0x40025CD")]
		CharaPopup,
		// Token: 0x040032D9 RID: 13017
		[Token(Token = "0x40025CE")]
		MoveDecision,
		// Token: 0x040032DA RID: 13018
		[Token(Token = "0x40025CF")]
		AddBuildComEnd
	}
}
