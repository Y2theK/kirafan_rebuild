﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000870 RID: 2160
	[Token(Token = "0x2000648")]
	[StructLayout(3, Size = 4)]
	public enum eTownCommand
	{
		// Token: 0x040032E5 RID: 13029
		[Token(Token = "0x40025D6")]
		BuildArea,
		// Token: 0x040032E6 RID: 13030
		[Token(Token = "0x40025D7")]
		BuildBuf
	}
}
