﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B97 RID: 2967
	[Token(Token = "0x2000810")]
	[StructLayout(3, Size = 4)]
	public enum eTownEffect
	{
		// Token: 0x04004427 RID: 17447
		[Token(Token = "0x4003048")]
		BuildMoney,
		// Token: 0x04004428 RID: 17448
		[Token(Token = "0x4003049")]
		BuildItem,
		// Token: 0x04004429 RID: 17449
		[Token(Token = "0x400304A")]
		TouchKRR,
		// Token: 0x0400442A RID: 17450
		[Token(Token = "0x400304B")]
		BuildLevelUp,
		// Token: 0x0400442B RID: 17451
		[Token(Token = "0x400304C")]
		AreaCreate,
		// Token: 0x0400442C RID: 17452
		[Token(Token = "0x400304D")]
		ContentChange
	}
}
