﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B2B RID: 2859
	[Token(Token = "0x20007D2")]
	[StructLayout(3, Size = 4)]
	public enum eTownEventAct
	{
		// Token: 0x040041EB RID: 16875
		[Token(Token = "0x4002ED2")]
		CompleteOpen,
		// Token: 0x040041EC RID: 16876
		[Token(Token = "0x4002ED3")]
		OpenSelectUI,
		// Token: 0x040041ED RID: 16877
		[Token(Token = "0x4002ED4")]
		CloseSelectUI,
		// Token: 0x040041EE RID: 16878
		[Token(Token = "0x4002ED5")]
		ReFlesh,
		// Token: 0x040041EF RID: 16879
		[Token(Token = "0x4002ED6")]
		ChangeObject,
		// Token: 0x040041F0 RID: 16880
		[Token(Token = "0x4002ED7")]
		ChangeModel,
		// Token: 0x040041F1 RID: 16881
		[Token(Token = "0x4002ED8")]
		StepIn,
		// Token: 0x040041F2 RID: 16882
		[Token(Token = "0x4002ED9")]
		StepOut,
		// Token: 0x040041F3 RID: 16883
		[Token(Token = "0x4002EDA")]
		StateChange,
		// Token: 0x040041F4 RID: 16884
		[Token(Token = "0x4002EDB")]
		SelectOff,
		// Token: 0x040041F5 RID: 16885
		[Token(Token = "0x4002EDC")]
		SelectTouch,
		// Token: 0x040041F6 RID: 16886
		[Token(Token = "0x4002EDD")]
		ReBuildMaker,
		// Token: 0x040041F7 RID: 16887
		[Token(Token = "0x4002EDE")]
		BuildLvInfo,
		// Token: 0x040041F8 RID: 16888
		[Token(Token = "0x4002EDF")]
		MovePos,
		// Token: 0x040041F9 RID: 16889
		[Token(Token = "0x4002EE0")]
		LinkCutPos,
		// Token: 0x040041FA RID: 16890
		[Token(Token = "0x4002EE1")]
		Rebind,
		// Token: 0x040041FB RID: 16891
		[Token(Token = "0x4002EE2")]
		MenuMode,
		// Token: 0x040041FC RID: 16892
		[Token(Token = "0x4002EE3")]
		PlayEffect
	}
}
