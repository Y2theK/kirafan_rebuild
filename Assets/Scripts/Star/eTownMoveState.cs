﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200069B RID: 1691
	[Token(Token = "0x2000566")]
	[StructLayout(3, Size = 4)]
	public enum eTownMoveState
	{
		// Token: 0x04002934 RID: 10548
		[Token(Token = "0x4002204")]
		None,
		// Token: 0x04002935 RID: 10549
		[Token(Token = "0x4002205")]
		TargetMove,
		// Token: 0x04002936 RID: 10550
		[Token(Token = "0x4002206")]
		NearmissMove,
		// Token: 0x04002937 RID: 10551
		[Token(Token = "0x4002207")]
		NonPointMove,
		// Token: 0x04002938 RID: 10552
		[Token(Token = "0x4002208")]
		SetUp
	}
}
