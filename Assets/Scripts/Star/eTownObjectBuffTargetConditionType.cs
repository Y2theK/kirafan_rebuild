﻿namespace Star {
    public enum eTownObjectBuffTargetConditionType {
        None = -1,
        TitleType,
        ClassType,
        ElementType,
        Num
    }
}
