﻿namespace Star {
    public enum eTownObjectBuffType {
        None = -1,
        Hp,
        Atk,
        Mgc,
        Def,
        MDef,
        Spd,
        Luck,
        ResistFire,
        ResistWater,
        ResistEarth,
        ResistWind,
        ResistMoon,
        ResistSun,
        Num
    }
}
