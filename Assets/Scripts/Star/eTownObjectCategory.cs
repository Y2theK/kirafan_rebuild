﻿namespace Star {
    public enum eTownObjectCategory {
        Room,
        Item,
        Money,
        Num,
        Area,
        Buf,
        Contents,
        Menu
    }
}
