﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B95 RID: 2965
	[Token(Token = "0x200080E")]
	[StructLayout(3, Size = 4)]
	public enum eTownObjectType
	{
		// Token: 0x04004411 RID: 17425
		[Token(Token = "0x4003032")]
		BUILD_ITEM_POP,
		// Token: 0x04004412 RID: 17426
		[Token(Token = "0x4003033")]
		CHARA_MODEL,
		// Token: 0x04004413 RID: 17427
		[Token(Token = "0x4003034")]
		DUMMY,
		// Token: 0x04004414 RID: 17428
		[Token(Token = "0x4003035")]
		CHARA_TOUCH_ITEM,
		// Token: 0x04004415 RID: 17429
		[Token(Token = "0x4003036")]
		BUILD_ANIME_GROUP,
		// Token: 0x04004416 RID: 17430
		[Token(Token = "0x4003037")]
		AREA_ANIME_GROUP,
		// Token: 0x04004417 RID: 17431
		[Token(Token = "0x4003038")]
		TRANING_ICON,
		// Token: 0x04004418 RID: 17432
		[Token(Token = "0x4003039")]
		CHRA_HUD,
		// Token: 0x04004419 RID: 17433
		[Token(Token = "0x400303A")]
		GET_ITEM,
		// Token: 0x0400441A RID: 17434
		[Token(Token = "0x400303B")]
		BUILDING_OBJ_00,
		// Token: 0x0400441B RID: 17435
		[Token(Token = "0x400303C")]
		BUILDING_OBJ_01,
		// Token: 0x0400441C RID: 17436
		[Token(Token = "0x400303D")]
		BUILD_LV_INFO
	}
}
