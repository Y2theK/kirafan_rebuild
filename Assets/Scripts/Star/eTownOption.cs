﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B96 RID: 2966
	[Token(Token = "0x200080F")]
	[StructLayout(3, Size = 4)]
	public enum eTownOption
	{
		// Token: 0x0400441E RID: 17438
		[Token(Token = "0x400303F")]
		Non,
		// Token: 0x0400441F RID: 17439
		[Token(Token = "0x4003040")]
		System,
		// Token: 0x04004420 RID: 17440
		[Token(Token = "0x4003041")]
		Area,
		// Token: 0x04004421 RID: 17441
		[Token(Token = "0x4003042")]
		Buf,
		// Token: 0x04004422 RID: 17442
		[Token(Token = "0x4003043")]
		Content,
		// Token: 0x04004423 RID: 17443
		[Token(Token = "0x4003044")]
		HitContent,
		// Token: 0x04004424 RID: 17444
		[Token(Token = "0x4003045")]
		HitBuf,
		// Token: 0x04004425 RID: 17445
		[Token(Token = "0x4003046")]
		Point
	}
}
