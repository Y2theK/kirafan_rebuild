﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BA1 RID: 2977
	[Token(Token = "0x2000817")]
	[StructLayout(3, Size = 4)]
	public enum eTownRequest
	{
		// Token: 0x04004459 RID: 17497
		[Token(Token = "0x4003066")]
		BuildBuf,
		// Token: 0x0400445A RID: 17498
		[Token(Token = "0x4003067")]
		BuildArea,
		// Token: 0x0400445B RID: 17499
		[Token(Token = "0x4003068")]
		ChangeBuf,
		// Token: 0x0400445C RID: 17500
		[Token(Token = "0x4003069")]
		ChangeArea,
		// Token: 0x0400445D RID: 17501
		[Token(Token = "0x400306A")]
		BackCharaBind
	}
}
