﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000876 RID: 2166
	[Token(Token = "0x200064E")]
	[StructLayout(3, Size = 4)]
	public enum eTownUICategory
	{
		// Token: 0x040032FD RID: 13053
		[Token(Token = "0x40025EE")]
		Room,
		// Token: 0x040032FE RID: 13054
		[Token(Token = "0x40025EF")]
		FreeArea,
		// Token: 0x040032FF RID: 13055
		[Token(Token = "0x40025F0")]
		FreeBuf,
		// Token: 0x04003300 RID: 13056
		[Token(Token = "0x40025F1")]
		Area,
		// Token: 0x04003301 RID: 13057
		[Token(Token = "0x40025F2")]
		Buf,
		// Token: 0x04003302 RID: 13058
		[Token(Token = "0x40025F3")]
		Content,
		// Token: 0x04003303 RID: 13059
		[Token(Token = "0x40025F4")]
		ShopView,
		// Token: 0x04003304 RID: 13060
		[Token(Token = "0x40025F5")]
		SummonView,
		// Token: 0x04003305 RID: 13061
		[Token(Token = "0x40025F6")]
		TraningView,
		// Token: 0x04003306 RID: 13062
		[Token(Token = "0x40025F7")]
		EditView,
		// Token: 0x04003307 RID: 13063
		[Token(Token = "0x40025F8")]
		QuestView,
		// Token: 0x04003308 RID: 13064
		[Token(Token = "0x40025F9")]
		MissionView,
		// Token: 0x04003309 RID: 13065
		[Token(Token = "0x40025FA")]
		CharaTraningView
	}
}
