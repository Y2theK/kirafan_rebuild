﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000602 RID: 1538
	[Token(Token = "0x20004F5")]
	[StructLayout(3, Size = 4)]
	public enum eTutorialMessageListDB
	{
		// Token: 0x04002565 RID: 9573
		[Token(Token = "0x4001ECF")]
		BTL_ATTACK = 1000,
		// Token: 0x04002566 RID: 9574
		[Token(Token = "0x4001ED0")]
		BTL_CMD_SWIPE,
		// Token: 0x04002567 RID: 9575
		[Token(Token = "0x4001ED1")]
		BTL_ENEMY_TAP,
		// Token: 0x04002568 RID: 9576
		[Token(Token = "0x4001ED2")]
		BTL_ATTACK_AFTER_ENEMY_TAP,
		// Token: 0x04002569 RID: 9577
		[Token(Token = "0x4001ED3")]
		BTL_ILLUST_SLIDE,
		// Token: 0x0400256A RID: 9578
		[Token(Token = "0x4001ED4")]
		BTL_SKILL_ICON,
		// Token: 0x0400256B RID: 9579
		[Token(Token = "0x4001ED5")]
		BTL_STUN,
		// Token: 0x0400256C RID: 9580
		[Token(Token = "0x4001ED6")]
		BTL_UNIQUESKILL,
		// Token: 0x0400256D RID: 9581
		[Token(Token = "0x4001ED7")]
		TOWN_BUILDCONTENT = 2000,
		// Token: 0x0400256E RID: 9582
		[Token(Token = "0x4001ED8")]
		TOWN_BUILDWEAPON,
		// Token: 0x0400256F RID: 9583
		[Token(Token = "0x4001ED9")]
		TOWN_BUILDWEAPONCOMPLETE,
		// Token: 0x04002570 RID: 9584
		[Token(Token = "0x4001EDA")]
		TOWN_BUILDMONEY = 2001,
		// Token: 0x04002571 RID: 9585
		[Token(Token = "0x4001EDB")]
		TOWN_BUILDMONEYCOMPLETE,
		// Token: 0x04002572 RID: 9586
		[Token(Token = "0x4001EDC")]
		ROOM_OPENSHOP = 3000,
		// Token: 0x04002573 RID: 9587
		[Token(Token = "0x4001EDD")]
		ROOM_PLACEMENT,
		// Token: 0x04002574 RID: 9588
		[Token(Token = "0x4001EDE")]
		BEGINNERS_MISSION = 4000,
		// Token: 0x04002575 RID: 9589
		[Token(Token = "0x4001EDF")]
		Max
	}
}
