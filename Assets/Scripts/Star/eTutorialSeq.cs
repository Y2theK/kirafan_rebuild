﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005DC RID: 1500
	[Token(Token = "0x20004CF")]
	[StructLayout(3, Size = 4)]
	public enum eTutorialSeq
	{
		// Token: 0x04001E84 RID: 7812
		[Token(Token = "0x40017EE")]
		Finish = -1,
		// Token: 0x04001E85 RID: 7813
		[Token(Token = "0x40017EF")]
		Unplayed,
		// Token: 0x04001E86 RID: 7814
		[Token(Token = "0x40017F0")]
		SignupComplete_NextIsPresentGet,
		// Token: 0x04001E87 RID: 7815
		[Token(Token = "0x40017F1")]
		PresentGot_NextIsADVGachaPrevPlay,
		// Token: 0x04001E88 RID: 7816
		[Token(Token = "0x40017F2")]
		ADVGachaPrevPlayed_NextIsGachaPlay,
		// Token: 0x04001E89 RID: 7817
		[Token(Token = "0x40017F3")]
		MissingNumber_GachaPlayed_NextIsGachaDecide,
		// Token: 0x04001E8A RID: 7818
		[Token(Token = "0x40017F4")]
		GachaDecided_NextIsADVGachaAfterPlay,
		// Token: 0x04001E8B RID: 7819
		[Token(Token = "0x40017F5")]
		ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play,
		// Token: 0x04001E8C RID: 7820
		[Token(Token = "0x40017F6")]
		ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play,
		// Token: 0x04001E8D RID: 7821
		[Token(Token = "0x40017F7")]
		ADVPrologueIntro2Played_NextIsQuestLogSet,
		// Token: 0x04001E8E RID: 7822
		[Token(Token = "0x40017F8")]
		QuestLogSeted_NextIsADVPrologueEnd,
		// Token: 0x04001E8F RID: 7823
		[Token(Token = "0x40017F9")]
		ADVPrologueEndPlayed
	}
}
