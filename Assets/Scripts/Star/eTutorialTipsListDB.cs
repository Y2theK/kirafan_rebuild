﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000606 RID: 1542
	[Token(Token = "0x20004F9")]
	[StructLayout(3, Size = 4)]
	public enum eTutorialTipsListDB
	{
		// Token: 0x0400257E RID: 9598
		[Token(Token = "0x4001EE8")]
		TUTORIAL_FINISH,
		// Token: 0x0400257F RID: 9599
		[Token(Token = "0x4001EE9")]
		FIRST_QUEST = 101,
		// Token: 0x04002580 RID: 9600
		[Token(Token = "0x4001EEA")]
		FIRST_PARTY_EDIT,
		// Token: 0x04002581 RID: 9601
		[Token(Token = "0x4001EEB")]
		FIRST_SUPPORT_EDIT,
		// Token: 0x04002582 RID: 9602
		[Token(Token = "0x4001EEC")]
		FIRST_CHARA_UPGRADE,
		// Token: 0x04002583 RID: 9603
		[Token(Token = "0x4001EED")]
		FIRST_CHARA_LIMITBREAK,
		// Token: 0x04002584 RID: 9604
		[Token(Token = "0x4001EEE")]
		FIRST_CHARA_EVOLUTION,
		// Token: 0x04002585 RID: 9605
		[Token(Token = "0x4001EEF")]
		FIRST_WPN_CREATE,
		// Token: 0x04002586 RID: 9606
		[Token(Token = "0x4001EF0")]
		FIRST_WPN_UPGRADE,
		// Token: 0x04002587 RID: 9607
		[Token(Token = "0x4001EF1")]
		FIRST_TRAINING = 110,
		// Token: 0x04002588 RID: 9608
		[Token(Token = "0x4001EF2")]
		FIRST_WPN_EVOLUTION,
		// Token: 0x04002589 RID: 9609
		[Token(Token = "0x4001EF3")]
		FIRST_AROUSAL,
		// Token: 0x0400258A RID: 9610
		[Token(Token = "0x4001EF4")]
		FIRST_WPN_SKILL_LVUP,
		// Token: 0x0400258B RID: 9611
		[Token(Token = "0x4001EF5")]
		FIRST_CHARA_VIEWCHANGE,
		// Token: 0x0400258C RID: 9612
		[Token(Token = "0x4001EF6")]
		FIRST_PROFILE,
		// Token: 0x0400258D RID: 9613
		[Token(Token = "0x4001EF7")]
		FIRST_BONUS_CRAFT,
		// Token: 0x0400258E RID: 9614
		[Token(Token = "0x4001EF8")]
		FIRST_ITEM_SHOP,
		// Token: 0x0400258F RID: 9615
		[Token(Token = "0x4001EF9")]
		FIRST_ADD_QUEST,
		// Token: 0x04002590 RID: 9616
		[Token(Token = "0x4001EFA")]
		FIRST_ABILITY_TREE,
		// Token: 0x04002591 RID: 9617
		[Token(Token = "0x4001EFB")]
		FIRST_ABILITY_SET,
		// Token: 0x04002592 RID: 9618
		[Token(Token = "0x4001EFC")]
		BTL_BATTLE = 1000,
		// Token: 0x04002593 RID: 9619
		[Token(Token = "0x4001EFD")]
		BTL_COMMAND,
		// Token: 0x04002594 RID: 9620
		[Token(Token = "0x4001EFE")]
		BTL_ELEMENT,
		// Token: 0x04002595 RID: 9621
		[Token(Token = "0x4001EFF")]
		BTL_MASTER,
		// Token: 0x04002596 RID: 9622
		[Token(Token = "0x4001F00")]
		BTL_CHARGE,
		// Token: 0x04002597 RID: 9623
		[Token(Token = "0x4001F01")]
		BTL_UNIQUESKILL,
		// Token: 0x04002598 RID: 9624
		[Token(Token = "0x4001F02")]
		Town_001 = 2000,
		// Token: 0x04002599 RID: 9625
		[Token(Token = "0x4001F03")]
		Town_002,
		// Token: 0x0400259A RID: 9626
		[Token(Token = "0x4001F04")]
		Town_003,
		// Token: 0x0400259B RID: 9627
		[Token(Token = "0x4001F05")]
		Town_004,
		// Token: 0x0400259C RID: 9628
		[Token(Token = "0x4001F06")]
		Room_001 = 3000,
		// Token: 0x0400259D RID: 9629
		[Token(Token = "0x4001F07")]
		Room_002,
		// Token: 0x0400259E RID: 9630
		[Token(Token = "0x4001F08")]
		Room_003,
		// Token: 0x0400259F RID: 9631
		[Token(Token = "0x4001F09")]
		ContentRoom_001 = 3100,
		// Token: 0x040025A0 RID: 9632
		[Token(Token = "0x4001F0A")]
		ContentRoom_002,
		// Token: 0x040025A1 RID: 9633
		[Token(Token = "0x4001F0B")]
		ContentRoom_003,
		// Token: 0x040025A2 RID: 9634
		[Token(Token = "0x4001F0C")]
		ContentRoom_004,
		// Token: 0x040025A3 RID: 9635
		[Token(Token = "0x4001F0D")]
		ContentRoom_005,
		// Token: 0x040025A4 RID: 9636
		[Token(Token = "0x4001F0E")]
		Event_Map_1011 = 101100,
		// Token: 0x040025A5 RID: 9637
		[Token(Token = "0x4001F0F")]
		Event_Chest_1011,
		// Token: 0x040025A6 RID: 9638
		[Token(Token = "0x4001F10")]
		FIRST_BEGINNERS_MISSION = 200000,
		// Token: 0x040025A7 RID: 9639
		[Token(Token = "0x4001F11")]
		FLAG1,
		// Token: 0x040025A8 RID: 9640
		[Token(Token = "0x4001F12")]
		FLAG2,
		// Token: 0x040025A9 RID: 9641
		[Token(Token = "0x4001F13")]
		FLAG3,
		// Token: 0x040025AA RID: 9642
		[Token(Token = "0x4001F14")]
		FLAG4,
		// Token: 0x040025AB RID: 9643
		[Token(Token = "0x4001F15")]
		FLAG5,
		// Token: 0x040025AC RID: 9644
		[Token(Token = "0x4001F16")]
		FLAG6,
		// Token: 0x040025AD RID: 9645
		[Token(Token = "0x4001F17")]
		FLAG7,
		// Token: 0x040025AE RID: 9646
		[Token(Token = "0x4001F18")]
		FLAG8,
		// Token: 0x040025AF RID: 9647
		[Token(Token = "0x4001F19")]
		FLAG9,
		// Token: 0x040025B0 RID: 9648
		[Token(Token = "0x4001F1A")]
		FLAG10,
		// Token: 0x040025B1 RID: 9649
		[Token(Token = "0x4001F1B")]
		Max
	}
}
