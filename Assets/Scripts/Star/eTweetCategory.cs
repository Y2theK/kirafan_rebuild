﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200060A RID: 1546
	[Token(Token = "0x20004FD")]
	[StructLayout(3, Size = 4)]
	public enum eTweetCategory
	{
		// Token: 0x040025BD RID: 9661
		[Token(Token = "0x4001F27")]
		None = -1,
		// Token: 0x040025BE RID: 9662
		[Token(Token = "0x4001F28")]
		HomeComment,
		// Token: 0x040025BF RID: 9663
		[Token(Token = "0x4001F29")]
		GreetWake,
		// Token: 0x040025C0 RID: 9664
		[Token(Token = "0x4001F2A")]
		GreetSleep,
		// Token: 0x040025C1 RID: 9665
		[Token(Token = "0x4001F2B")]
		GreetGoHome,
		// Token: 0x040025C2 RID: 9666
		[Token(Token = "0x4001F2C")]
		GreetGoTown,
		// Token: 0x040025C3 RID: 9667
		[Token(Token = "0x4001F2D")]
		GreetVisit,
		// Token: 0x040025C4 RID: 9668
		[Token(Token = "0x4001F2E")]
		SpecialTweet
	}
}
