﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200060B RID: 1547
	[Token(Token = "0x20004FE")]
	[StructLayout(3, Size = 4)]
	public enum eTweetIDBase
	{
		// Token: 0x040025C6 RID: 9670
		[Token(Token = "0x4001F30")]
		RoomTweet_Weekday = 21000,
		// Token: 0x040025C7 RID: 9671
		[Token(Token = "0x4001F31")]
		RoomTweet_Holiday = 22000,
		// Token: 0x040025C8 RID: 9672
		[Token(Token = "0x4001F32")]
		Greeting = 23000,
		// Token: 0x040025C9 RID: 9673
		[Token(Token = "0x4001F33")]
		TownTweet_Visit = 31000,
		// Token: 0x040025CA RID: 9674
		[Token(Token = "0x4001F34")]
		TownTweet_Visit_Overtime = 32000,
		// Token: 0x040025CB RID: 9675
		[Token(Token = "0x4001F35")]
		TownTweet_Special = 33000,
		// Token: 0x040025CC RID: 9676
		[Token(Token = "0x4001F36")]
		Training_Mission = 41000,
		// Token: 0x040025CD RID: 9677
		[Token(Token = "0x4001F37")]
		HomeComment = 51000
	}
}
