﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000816 RID: 2070
	[Token(Token = "0x2000619")]
	[StructLayout(3, Size = 4)]
	public enum eUnitRoomEvent
	{
		// Token: 0x040030AA RID: 12458
		[Token(Token = "0x40024FE")]
		ShowUI = 2,
		// Token: 0x040030AB RID: 12459
		[Token(Token = "0x40024FF")]
		HideUI,
		// Token: 0x040030AC RID: 12460
		[Token(Token = "0x4002500")]
		Max
	}
}
