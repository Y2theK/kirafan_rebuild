﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000686 RID: 1670
	[Token(Token = "0x2000555")]
	[StructLayout(3, Size = 4)]
	public enum eXlsBuildMoveCheckType
	{
		// Token: 0x040027D2 RID: 10194
		[Token(Token = "0x40020AF")]
		Normal,
		// Token: 0x040027D3 RID: 10195
		[Token(Token = "0x40020B0")]
		ResourceID,
		// Token: 0x040027D4 RID: 10196
		[Token(Token = "0x40020B1")]
		SelfClass,
		// Token: 0x040027D5 RID: 10197
		[Token(Token = "0x40020B2")]
		SelfElement,
		// Token: 0x040027D6 RID: 10198
		[Token(Token = "0x40020B3")]
		StrengBuild,
		// Token: 0x040027D7 RID: 10199
		[Token(Token = "0x40020B4")]
		ProductBuild,
		// Token: 0x040027D8 RID: 10200
		[Token(Token = "0x40020B5")]
		ClassBuild,
		// Token: 0x040027D9 RID: 10201
		[Token(Token = "0x40020B6")]
		ElementBuild
	}
}
