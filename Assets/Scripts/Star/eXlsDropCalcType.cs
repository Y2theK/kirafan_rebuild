﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000685 RID: 1669
	[Token(Token = "0x2000554")]
	[StructLayout(3, Size = 4)]
	public enum eXlsDropCalcType
	{
		// Token: 0x040027CE RID: 10190
		[Token(Token = "0x40020AB")]
		normal,
		// Token: 0x040027CF RID: 10191
		[Token(Token = "0x40020AC")]
		chara,
		// Token: 0x040027D0 RID: 10192
		[Token(Token = "0x40020AD")]
		build
	}
}
