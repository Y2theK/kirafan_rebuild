﻿namespace Star {
    public enum eXlsMissionBattleFuncType {
        QuestClear,
        Reserved1,
        Reserved2,
        QuestClearEventType,
        Reserved3,
        SpecificQuestIdClearCount
    }
}
