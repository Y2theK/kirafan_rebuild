﻿namespace Star {
    public enum eXlsMissionEditFuncType {
        BattlePartyEdit,
        SupportPartyEdit,
        CharaStorength,
        CharaLimitBreak,
        CharaEvelution,
        WeaponMake,
        WeaponStorength,
        WeaponEquipment,
        SpecificCharaRank,
        SpecificCharaLimitBreak,
        SpecificRarityCharaRank,
        SpecificRarityCharaLimitBreak,
        SpecificRarityCharaEvelution
    }
}
