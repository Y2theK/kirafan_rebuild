﻿namespace Star {
    public enum eXlsMissionRewardType {
        None,
        Money,
        Item,
        KRRPoint,
        Stamina,
        Friendship,
        LimitedGem
    }
}
