﻿namespace Star {
    public enum eXlsMissionSeg {
        System,
        Battle,
        Town,
        Room,
        Edit,
        Chara,
        Training,
        Trade
    }
}
