﻿namespace Star {
    public enum eXlsMissionTownFuncType {
        Building,
        Move,
        Remove,
        CharaTouchItem,
        ContentBuild,
        BufBuild,
        BuildTouchItem,
        IconTap,
        ProductBuildCount,
        StrengBuildCount
    }
}
