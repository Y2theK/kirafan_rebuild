﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008D6 RID: 2262
	[Token(Token = "0x200067E")]
	[StructLayout(3, Size = 4)]
	public enum eXlsPopupTiming
	{
		// Token: 0x04003536 RID: 13622
		[Token(Token = "0x4002702")]
		PopNon,
		// Token: 0x04003537 RID: 13623
		[Token(Token = "0x4002703")]
		PopSystem,
		// Token: 0x04003538 RID: 13624
		[Token(Token = "0x4002704")]
		PopRoom,
		// Token: 0x04003539 RID: 13625
		[Token(Token = "0x4002705")]
		PopTown,
		// Token: 0x0400353A RID: 13626
		[Token(Token = "0x4002706")]
		PopEdit,
		// Token: 0x0400353B RID: 13627
		[Token(Token = "0x4002707")]
		PopBattle,
		// Token: 0x0400353C RID: 13628
		[Token(Token = "0x4002708")]
		PopChara,
		// Token: 0x0400353D RID: 13629
		[Token(Token = "0x4002709")]
		PopPlayer
	}
}
