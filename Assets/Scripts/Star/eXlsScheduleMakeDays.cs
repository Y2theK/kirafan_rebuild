﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200068F RID: 1679
	[Token(Token = "0x200055E")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakeDays
	{
		// Token: 0x040028F6 RID: 10486
		[Token(Token = "0x40021D3")]
		def1,
		// Token: 0x040028F7 RID: 10487
		[Token(Token = "0x40021D4")]
		def2
	}
}
