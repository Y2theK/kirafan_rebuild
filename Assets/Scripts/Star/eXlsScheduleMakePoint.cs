﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200068D RID: 1677
	[Token(Token = "0x200055C")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakePoint
	{
		// Token: 0x040028EB RID: 10475
		[Token(Token = "0x40021C8")]
		Sleep,
		// Token: 0x040028EC RID: 10476
		[Token(Token = "0x40021C9")]
		Room,
		// Token: 0x040028ED RID: 10477
		[Token(Token = "0x40021CA")]
		Content,
		// Token: 0x040028EE RID: 10478
		[Token(Token = "0x40021CB")]
		Buf,
		// Token: 0x040028EF RID: 10479
		[Token(Token = "0x40021CC")]
		Menu
	}
}
