﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200068C RID: 1676
	[Token(Token = "0x200055B")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakeTag
	{
		// Token: 0x040028E6 RID: 10470
		[Token(Token = "0x40021C3")]
		S,
		// Token: 0x040028E7 RID: 10471
		[Token(Token = "0x40021C4")]
		A,
		// Token: 0x040028E8 RID: 10472
		[Token(Token = "0x40021C5")]
		B,
		// Token: 0x040028E9 RID: 10473
		[Token(Token = "0x40021C6")]
		C
	}
}
