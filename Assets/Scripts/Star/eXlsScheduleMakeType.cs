﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200068E RID: 1678
	[Token(Token = "0x200055D")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakeType
	{
		// Token: 0x040028F1 RID: 10481
		[Token(Token = "0x40021CE")]
		Fix,
		// Token: 0x040028F2 RID: 10482
		[Token(Token = "0x40021CF")]
		Derive,
		// Token: 0x040028F3 RID: 10483
		[Token(Token = "0x40021D0")]
		Free,
		// Token: 0x040028F4 RID: 10484
		[Token(Token = "0x40021D1")]
		Per
	}
}
