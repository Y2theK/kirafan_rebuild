﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200067B RID: 1659
	[Token(Token = "0x200054A")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleSeg
	{
		// Token: 0x04002793 RID: 10131
		[Token(Token = "0x4002070")]
		Non,
		// Token: 0x04002794 RID: 10132
		[Token(Token = "0x4002071")]
		PopupChange,
		// Token: 0x04002795 RID: 10133
		[Token(Token = "0x4002072")]
		EventWakeUp,
		// Token: 0x04002796 RID: 10134
		[Token(Token = "0x4002073")]
		IfComp,
		// Token: 0x04002797 RID: 10135
		[Token(Token = "0x4002074")]
		JumpKey,
		// Token: 0x04002798 RID: 10136
		[Token(Token = "0x4002075")]
		IFCloseness,
		// Token: 0x04002799 RID: 10137
		[Token(Token = "0x4002076")]
		IFHaveChara,
		// Token: 0x0400279A RID: 10138
		[Token(Token = "0x4002077")]
		IFHaveNamed
	}
}
