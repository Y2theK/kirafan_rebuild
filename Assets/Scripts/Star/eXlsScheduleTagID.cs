﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200068B RID: 1675
	[Token(Token = "0x200055A")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleTagID
	{
		// Token: 0x040027EE RID: 10222
		[Token(Token = "0x40020CB")]
		def1,
		// Token: 0x040027EF RID: 10223
		[Token(Token = "0x40020CC")]
		def2,
		// Token: 0x040027F0 RID: 10224
		[Token(Token = "0x40020CD")]
		def3 = 1000,
		// Token: 0x040027F1 RID: 10225
		[Token(Token = "0x40020CE")]
		def4,
		// Token: 0x040027F2 RID: 10226
		[Token(Token = "0x40020CF")]
		def5,
		// Token: 0x040027F3 RID: 10227
		[Token(Token = "0x40020D0")]
		def6,
		// Token: 0x040027F4 RID: 10228
		[Token(Token = "0x40020D1")]
		def7,
		// Token: 0x040027F5 RID: 10229
		[Token(Token = "0x40020D2")]
		def8,
		// Token: 0x040027F6 RID: 10230
		[Token(Token = "0x40020D3")]
		def9,
		// Token: 0x040027F7 RID: 10231
		[Token(Token = "0x40020D4")]
		def10,
		// Token: 0x040027F8 RID: 10232
		[Token(Token = "0x40020D5")]
		def11,
		// Token: 0x040027F9 RID: 10233
		[Token(Token = "0x40020D6")]
		def12,
		// Token: 0x040027FA RID: 10234
		[Token(Token = "0x40020D7")]
		def13,
		// Token: 0x040027FB RID: 10235
		[Token(Token = "0x40020D8")]
		def14,
		// Token: 0x040027FC RID: 10236
		[Token(Token = "0x40020D9")]
		def15,
		// Token: 0x040027FD RID: 10237
		[Token(Token = "0x40020DA")]
		def16,
		// Token: 0x040027FE RID: 10238
		[Token(Token = "0x40020DB")]
		def17,
		// Token: 0x040027FF RID: 10239
		[Token(Token = "0x40020DC")]
		def18,
		// Token: 0x04002800 RID: 10240
		[Token(Token = "0x40020DD")]
		def19,
		// Token: 0x04002801 RID: 10241
		[Token(Token = "0x40020DE")]
		def20,
		// Token: 0x04002802 RID: 10242
		[Token(Token = "0x40020DF")]
		def21,
		// Token: 0x04002803 RID: 10243
		[Token(Token = "0x40020E0")]
		def22,
		// Token: 0x04002804 RID: 10244
		[Token(Token = "0x40020E1")]
		def23,
		// Token: 0x04002805 RID: 10245
		[Token(Token = "0x40020E2")]
		def24,
		// Token: 0x04002806 RID: 10246
		[Token(Token = "0x40020E3")]
		def25,
		// Token: 0x04002807 RID: 10247
		[Token(Token = "0x40020E4")]
		def26,
		// Token: 0x04002808 RID: 10248
		[Token(Token = "0x40020E5")]
		def27,
		// Token: 0x04002809 RID: 10249
		[Token(Token = "0x40020E6")]
		def28,
		// Token: 0x0400280A RID: 10250
		[Token(Token = "0x40020E7")]
		def29,
		// Token: 0x0400280B RID: 10251
		[Token(Token = "0x40020E8")]
		def30,
		// Token: 0x0400280C RID: 10252
		[Token(Token = "0x40020E9")]
		def31,
		// Token: 0x0400280D RID: 10253
		[Token(Token = "0x40020EA")]
		def32,
		// Token: 0x0400280E RID: 10254
		[Token(Token = "0x40020EB")]
		def33,
		// Token: 0x0400280F RID: 10255
		[Token(Token = "0x40020EC")]
		def34,
		// Token: 0x04002810 RID: 10256
		[Token(Token = "0x40020ED")]
		def35 = 5000,
		// Token: 0x04002811 RID: 10257
		[Token(Token = "0x40020EE")]
		def36,
		// Token: 0x04002812 RID: 10258
		[Token(Token = "0x40020EF")]
		def37,
		// Token: 0x04002813 RID: 10259
		[Token(Token = "0x40020F0")]
		def38,
		// Token: 0x04002814 RID: 10260
		[Token(Token = "0x40020F1")]
		def39,
		// Token: 0x04002815 RID: 10261
		[Token(Token = "0x40020F2")]
		def40,
		// Token: 0x04002816 RID: 10262
		[Token(Token = "0x40020F3")]
		def41,
		// Token: 0x04002817 RID: 10263
		[Token(Token = "0x40020F4")]
		def42,
		// Token: 0x04002818 RID: 10264
		[Token(Token = "0x40020F5")]
		def43,
		// Token: 0x04002819 RID: 10265
		[Token(Token = "0x40020F6")]
		def44,
		// Token: 0x0400281A RID: 10266
		[Token(Token = "0x40020F7")]
		def45,
		// Token: 0x0400281B RID: 10267
		[Token(Token = "0x40020F8")]
		def46,
		// Token: 0x0400281C RID: 10268
		[Token(Token = "0x40020F9")]
		def47,
		// Token: 0x0400281D RID: 10269
		[Token(Token = "0x40020FA")]
		def48,
		// Token: 0x0400281E RID: 10270
		[Token(Token = "0x40020FB")]
		def49,
		// Token: 0x0400281F RID: 10271
		[Token(Token = "0x40020FC")]
		def50,
		// Token: 0x04002820 RID: 10272
		[Token(Token = "0x40020FD")]
		def51,
		// Token: 0x04002821 RID: 10273
		[Token(Token = "0x40020FE")]
		def52,
		// Token: 0x04002822 RID: 10274
		[Token(Token = "0x40020FF")]
		def53,
		// Token: 0x04002823 RID: 10275
		[Token(Token = "0x4002100")]
		def54,
		// Token: 0x04002824 RID: 10276
		[Token(Token = "0x4002101")]
		def55,
		// Token: 0x04002825 RID: 10277
		[Token(Token = "0x4002102")]
		def56,
		// Token: 0x04002826 RID: 10278
		[Token(Token = "0x4002103")]
		def57,
		// Token: 0x04002827 RID: 10279
		[Token(Token = "0x4002104")]
		def58,
		// Token: 0x04002828 RID: 10280
		[Token(Token = "0x4002105")]
		def59,
		// Token: 0x04002829 RID: 10281
		[Token(Token = "0x4002106")]
		def60,
		// Token: 0x0400282A RID: 10282
		[Token(Token = "0x4002107")]
		def61,
		// Token: 0x0400282B RID: 10283
		[Token(Token = "0x4002108")]
		def62,
		// Token: 0x0400282C RID: 10284
		[Token(Token = "0x4002109")]
		def63,
		// Token: 0x0400282D RID: 10285
		[Token(Token = "0x400210A")]
		def64,
		// Token: 0x0400282E RID: 10286
		[Token(Token = "0x400210B")]
		def65,
		// Token: 0x0400282F RID: 10287
		[Token(Token = "0x400210C")]
		def66,
		// Token: 0x04002830 RID: 10288
		[Token(Token = "0x400210D")]
		def67 = 10000,
		// Token: 0x04002831 RID: 10289
		[Token(Token = "0x400210E")]
		def68,
		// Token: 0x04002832 RID: 10290
		[Token(Token = "0x400210F")]
		def69,
		// Token: 0x04002833 RID: 10291
		[Token(Token = "0x4002110")]
		def70,
		// Token: 0x04002834 RID: 10292
		[Token(Token = "0x4002111")]
		def71,
		// Token: 0x04002835 RID: 10293
		[Token(Token = "0x4002112")]
		def72,
		// Token: 0x04002836 RID: 10294
		[Token(Token = "0x4002113")]
		def73,
		// Token: 0x04002837 RID: 10295
		[Token(Token = "0x4002114")]
		def74,
		// Token: 0x04002838 RID: 10296
		[Token(Token = "0x4002115")]
		def75,
		// Token: 0x04002839 RID: 10297
		[Token(Token = "0x4002116")]
		def76,
		// Token: 0x0400283A RID: 10298
		[Token(Token = "0x4002117")]
		def77,
		// Token: 0x0400283B RID: 10299
		[Token(Token = "0x4002118")]
		def78,
		// Token: 0x0400283C RID: 10300
		[Token(Token = "0x4002119")]
		def79,
		// Token: 0x0400283D RID: 10301
		[Token(Token = "0x400211A")]
		def80,
		// Token: 0x0400283E RID: 10302
		[Token(Token = "0x400211B")]
		def81,
		// Token: 0x0400283F RID: 10303
		[Token(Token = "0x400211C")]
		def82,
		// Token: 0x04002840 RID: 10304
		[Token(Token = "0x400211D")]
		def83,
		// Token: 0x04002841 RID: 10305
		[Token(Token = "0x400211E")]
		def84,
		// Token: 0x04002842 RID: 10306
		[Token(Token = "0x400211F")]
		def85,
		// Token: 0x04002843 RID: 10307
		[Token(Token = "0x4002120")]
		def86,
		// Token: 0x04002844 RID: 10308
		[Token(Token = "0x4002121")]
		def87,
		// Token: 0x04002845 RID: 10309
		[Token(Token = "0x4002122")]
		def88 = 1032,
		// Token: 0x04002846 RID: 10310
		[Token(Token = "0x4002123")]
		def89,
		// Token: 0x04002847 RID: 10311
		[Token(Token = "0x4002124")]
		def90,
		// Token: 0x04002848 RID: 10312
		[Token(Token = "0x4002125")]
		def91,
		// Token: 0x04002849 RID: 10313
		[Token(Token = "0x4002126")]
		def92 = 5032,
		// Token: 0x0400284A RID: 10314
		[Token(Token = "0x4002127")]
		def93,
		// Token: 0x0400284B RID: 10315
		[Token(Token = "0x4002128")]
		def94,
		// Token: 0x0400284C RID: 10316
		[Token(Token = "0x4002129")]
		def95,
		// Token: 0x0400284D RID: 10317
		[Token(Token = "0x400212A")]
		def96 = 1036,
		// Token: 0x0400284E RID: 10318
		[Token(Token = "0x400212B")]
		def97,
		// Token: 0x0400284F RID: 10319
		[Token(Token = "0x400212C")]
		def98,
		// Token: 0x04002850 RID: 10320
		[Token(Token = "0x400212D")]
		def99,
		// Token: 0x04002851 RID: 10321
		[Token(Token = "0x400212E")]
		def100 = 5036,
		// Token: 0x04002852 RID: 10322
		[Token(Token = "0x400212F")]
		def101,
		// Token: 0x04002853 RID: 10323
		[Token(Token = "0x4002130")]
		def102,
		// Token: 0x04002854 RID: 10324
		[Token(Token = "0x4002131")]
		def103,
		// Token: 0x04002855 RID: 10325
		[Token(Token = "0x4002132")]
		def104 = 1040,
		// Token: 0x04002856 RID: 10326
		[Token(Token = "0x4002133")]
		def105,
		// Token: 0x04002857 RID: 10327
		[Token(Token = "0x4002134")]
		def106,
		// Token: 0x04002858 RID: 10328
		[Token(Token = "0x4002135")]
		def107,
		// Token: 0x04002859 RID: 10329
		[Token(Token = "0x4002136")]
		def108 = 5040,
		// Token: 0x0400285A RID: 10330
		[Token(Token = "0x4002137")]
		def109,
		// Token: 0x0400285B RID: 10331
		[Token(Token = "0x4002138")]
		def110,
		// Token: 0x0400285C RID: 10332
		[Token(Token = "0x4002139")]
		def111,
		// Token: 0x0400285D RID: 10333
		[Token(Token = "0x400213A")]
		def112 = 1044,
		// Token: 0x0400285E RID: 10334
		[Token(Token = "0x400213B")]
		def113,
		// Token: 0x0400285F RID: 10335
		[Token(Token = "0x400213C")]
		def114,
		// Token: 0x04002860 RID: 10336
		[Token(Token = "0x400213D")]
		def115,
		// Token: 0x04002861 RID: 10337
		[Token(Token = "0x400213E")]
		def116 = 5044,
		// Token: 0x04002862 RID: 10338
		[Token(Token = "0x400213F")]
		def117,
		// Token: 0x04002863 RID: 10339
		[Token(Token = "0x4002140")]
		def118,
		// Token: 0x04002864 RID: 10340
		[Token(Token = "0x4002141")]
		def119,
		// Token: 0x04002865 RID: 10341
		[Token(Token = "0x4002142")]
		def120 = 1048,
		// Token: 0x04002866 RID: 10342
		[Token(Token = "0x4002143")]
		def121,
		// Token: 0x04002867 RID: 10343
		[Token(Token = "0x4002144")]
		def122,
		// Token: 0x04002868 RID: 10344
		[Token(Token = "0x4002145")]
		def123,
		// Token: 0x04002869 RID: 10345
		[Token(Token = "0x4002146")]
		def124 = 5048,
		// Token: 0x0400286A RID: 10346
		[Token(Token = "0x4002147")]
		def125,
		// Token: 0x0400286B RID: 10347
		[Token(Token = "0x4002148")]
		def126,
		// Token: 0x0400286C RID: 10348
		[Token(Token = "0x4002149")]
		def127,
		// Token: 0x0400286D RID: 10349
		[Token(Token = "0x400214A")]
		def128 = 1052,
		// Token: 0x0400286E RID: 10350
		[Token(Token = "0x400214B")]
		def129,
		// Token: 0x0400286F RID: 10351
		[Token(Token = "0x400214C")]
		def130,
		// Token: 0x04002870 RID: 10352
		[Token(Token = "0x400214D")]
		def131,
		// Token: 0x04002871 RID: 10353
		[Token(Token = "0x400214E")]
		def132 = 5052,
		// Token: 0x04002872 RID: 10354
		[Token(Token = "0x400214F")]
		def133,
		// Token: 0x04002873 RID: 10355
		[Token(Token = "0x4002150")]
		def134,
		// Token: 0x04002874 RID: 10356
		[Token(Token = "0x4002151")]
		def135,
		// Token: 0x04002875 RID: 10357
		[Token(Token = "0x4002152")]
		def136 = 1056,
		// Token: 0x04002876 RID: 10358
		[Token(Token = "0x4002153")]
		def137,
		// Token: 0x04002877 RID: 10359
		[Token(Token = "0x4002154")]
		def138,
		// Token: 0x04002878 RID: 10360
		[Token(Token = "0x4002155")]
		def139,
		// Token: 0x04002879 RID: 10361
		[Token(Token = "0x4002156")]
		def140 = 5056,
		// Token: 0x0400287A RID: 10362
		[Token(Token = "0x4002157")]
		def141,
		// Token: 0x0400287B RID: 10363
		[Token(Token = "0x4002158")]
		def142,
		// Token: 0x0400287C RID: 10364
		[Token(Token = "0x4002159")]
		def143,
		// Token: 0x0400287D RID: 10365
		[Token(Token = "0x400215A")]
		def144 = 1060,
		// Token: 0x0400287E RID: 10366
		[Token(Token = "0x400215B")]
		def145,
		// Token: 0x0400287F RID: 10367
		[Token(Token = "0x400215C")]
		def146,
		// Token: 0x04002880 RID: 10368
		[Token(Token = "0x400215D")]
		def147,
		// Token: 0x04002881 RID: 10369
		[Token(Token = "0x400215E")]
		def148 = 5060,
		// Token: 0x04002882 RID: 10370
		[Token(Token = "0x400215F")]
		def149,
		// Token: 0x04002883 RID: 10371
		[Token(Token = "0x4002160")]
		def150,
		// Token: 0x04002884 RID: 10372
		[Token(Token = "0x4002161")]
		def151,
		// Token: 0x04002885 RID: 10373
		[Token(Token = "0x4002162")]
		def152 = 1064,
		// Token: 0x04002886 RID: 10374
		[Token(Token = "0x4002163")]
		def153,
		// Token: 0x04002887 RID: 10375
		[Token(Token = "0x4002164")]
		def154,
		// Token: 0x04002888 RID: 10376
		[Token(Token = "0x4002165")]
		def155,
		// Token: 0x04002889 RID: 10377
		[Token(Token = "0x4002166")]
		def156 = 5064,
		// Token: 0x0400288A RID: 10378
		[Token(Token = "0x4002167")]
		def157,
		// Token: 0x0400288B RID: 10379
		[Token(Token = "0x4002168")]
		def158,
		// Token: 0x0400288C RID: 10380
		[Token(Token = "0x4002169")]
		def159,
		// Token: 0x0400288D RID: 10381
		[Token(Token = "0x400216A")]
		def160 = 1068,
		// Token: 0x0400288E RID: 10382
		[Token(Token = "0x400216B")]
		def161,
		// Token: 0x0400288F RID: 10383
		[Token(Token = "0x400216C")]
		def162,
		// Token: 0x04002890 RID: 10384
		[Token(Token = "0x400216D")]
		def163,
		// Token: 0x04002891 RID: 10385
		[Token(Token = "0x400216E")]
		def164 = 5068,
		// Token: 0x04002892 RID: 10386
		[Token(Token = "0x400216F")]
		def165,
		// Token: 0x04002893 RID: 10387
		[Token(Token = "0x4002170")]
		def166,
		// Token: 0x04002894 RID: 10388
		[Token(Token = "0x4002171")]
		def167,
		// Token: 0x04002895 RID: 10389
		[Token(Token = "0x4002172")]
		def168 = 1072,
		// Token: 0x04002896 RID: 10390
		[Token(Token = "0x4002173")]
		def169,
		// Token: 0x04002897 RID: 10391
		[Token(Token = "0x4002174")]
		def170,
		// Token: 0x04002898 RID: 10392
		[Token(Token = "0x4002175")]
		def171,
		// Token: 0x04002899 RID: 10393
		[Token(Token = "0x4002176")]
		def172 = 5072,
		// Token: 0x0400289A RID: 10394
		[Token(Token = "0x4002177")]
		def173,
		// Token: 0x0400289B RID: 10395
		[Token(Token = "0x4002178")]
		def174,
		// Token: 0x0400289C RID: 10396
		[Token(Token = "0x4002179")]
		def175,
		// Token: 0x0400289D RID: 10397
		[Token(Token = "0x400217A")]
		def176 = 1076,
		// Token: 0x0400289E RID: 10398
		[Token(Token = "0x400217B")]
		def177,
		// Token: 0x0400289F RID: 10399
		[Token(Token = "0x400217C")]
		def178,
		// Token: 0x040028A0 RID: 10400
		[Token(Token = "0x400217D")]
		def179,
		// Token: 0x040028A1 RID: 10401
		[Token(Token = "0x400217E")]
		def180 = 5076,
		// Token: 0x040028A2 RID: 10402
		[Token(Token = "0x400217F")]
		def181,
		// Token: 0x040028A3 RID: 10403
		[Token(Token = "0x4002180")]
		def182,
		// Token: 0x040028A4 RID: 10404
		[Token(Token = "0x4002181")]
		def183,
		// Token: 0x040028A5 RID: 10405
		[Token(Token = "0x4002182")]
		def184 = 1080,
		// Token: 0x040028A6 RID: 10406
		[Token(Token = "0x4002183")]
		def185,
		// Token: 0x040028A7 RID: 10407
		[Token(Token = "0x4002184")]
		def186,
		// Token: 0x040028A8 RID: 10408
		[Token(Token = "0x4002185")]
		def187,
		// Token: 0x040028A9 RID: 10409
		[Token(Token = "0x4002186")]
		def188 = 5080,
		// Token: 0x040028AA RID: 10410
		[Token(Token = "0x4002187")]
		def189,
		// Token: 0x040028AB RID: 10411
		[Token(Token = "0x4002188")]
		def190,
		// Token: 0x040028AC RID: 10412
		[Token(Token = "0x4002189")]
		def191,
		// Token: 0x040028AD RID: 10413
		[Token(Token = "0x400218A")]
		def192 = 1084,
		// Token: 0x040028AE RID: 10414
		[Token(Token = "0x400218B")]
		def193,
		// Token: 0x040028AF RID: 10415
		[Token(Token = "0x400218C")]
		def194,
		// Token: 0x040028B0 RID: 10416
		[Token(Token = "0x400218D")]
		def195,
		// Token: 0x040028B1 RID: 10417
		[Token(Token = "0x400218E")]
		def196 = 5084,
		// Token: 0x040028B2 RID: 10418
		[Token(Token = "0x400218F")]
		def197,
		// Token: 0x040028B3 RID: 10419
		[Token(Token = "0x4002190")]
		def198,
		// Token: 0x040028B4 RID: 10420
		[Token(Token = "0x4002191")]
		def199,
		// Token: 0x040028B5 RID: 10421
		[Token(Token = "0x4002192")]
		def200 = 1088,
		// Token: 0x040028B6 RID: 10422
		[Token(Token = "0x4002193")]
		def201,
		// Token: 0x040028B7 RID: 10423
		[Token(Token = "0x4002194")]
		def202,
		// Token: 0x040028B8 RID: 10424
		[Token(Token = "0x4002195")]
		def203,
		// Token: 0x040028B9 RID: 10425
		[Token(Token = "0x4002196")]
		def204 = 5088,
		// Token: 0x040028BA RID: 10426
		[Token(Token = "0x4002197")]
		def205,
		// Token: 0x040028BB RID: 10427
		[Token(Token = "0x4002198")]
		def206,
		// Token: 0x040028BC RID: 10428
		[Token(Token = "0x4002199")]
		def207,
		// Token: 0x040028BD RID: 10429
		[Token(Token = "0x400219A")]
		def208 = 1092,
		// Token: 0x040028BE RID: 10430
		[Token(Token = "0x400219B")]
		def209,
		// Token: 0x040028BF RID: 10431
		[Token(Token = "0x400219C")]
		def210,
		// Token: 0x040028C0 RID: 10432
		[Token(Token = "0x400219D")]
		def211,
		// Token: 0x040028C1 RID: 10433
		[Token(Token = "0x400219E")]
		def212 = 5092,
		// Token: 0x040028C2 RID: 10434
		[Token(Token = "0x400219F")]
		def213,
		// Token: 0x040028C3 RID: 10435
		[Token(Token = "0x40021A0")]
		def214,
		// Token: 0x040028C4 RID: 10436
		[Token(Token = "0x40021A1")]
		def215,
		// Token: 0x040028C5 RID: 10437
		[Token(Token = "0x40021A2")]
		def216 = 1096,
		// Token: 0x040028C6 RID: 10438
		[Token(Token = "0x40021A3")]
		def217,
		// Token: 0x040028C7 RID: 10439
		[Token(Token = "0x40021A4")]
		def218,
		// Token: 0x040028C8 RID: 10440
		[Token(Token = "0x40021A5")]
		def219,
		// Token: 0x040028C9 RID: 10441
		[Token(Token = "0x40021A6")]
		def220 = 5096,
		// Token: 0x040028CA RID: 10442
		[Token(Token = "0x40021A7")]
		def221,
		// Token: 0x040028CB RID: 10443
		[Token(Token = "0x40021A8")]
		def222,
		// Token: 0x040028CC RID: 10444
		[Token(Token = "0x40021A9")]
		def223,
		// Token: 0x040028CD RID: 10445
		[Token(Token = "0x40021AA")]
		def224 = 1100,
		// Token: 0x040028CE RID: 10446
		[Token(Token = "0x40021AB")]
		def225,
		// Token: 0x040028CF RID: 10447
		[Token(Token = "0x40021AC")]
		def226,
		// Token: 0x040028D0 RID: 10448
		[Token(Token = "0x40021AD")]
		def227,
		// Token: 0x040028D1 RID: 10449
		[Token(Token = "0x40021AE")]
		def228 = 5100,
		// Token: 0x040028D2 RID: 10450
		[Token(Token = "0x40021AF")]
		def229,
		// Token: 0x040028D3 RID: 10451
		[Token(Token = "0x40021B0")]
		def230,
		// Token: 0x040028D4 RID: 10452
		[Token(Token = "0x40021B1")]
		def231,
		// Token: 0x040028D5 RID: 10453
		[Token(Token = "0x40021B2")]
		def232 = 1104,
		// Token: 0x040028D6 RID: 10454
		[Token(Token = "0x40021B3")]
		def233,
		// Token: 0x040028D7 RID: 10455
		[Token(Token = "0x40021B4")]
		def234,
		// Token: 0x040028D8 RID: 10456
		[Token(Token = "0x40021B5")]
		def235,
		// Token: 0x040028D9 RID: 10457
		[Token(Token = "0x40021B6")]
		def236 = 5104,
		// Token: 0x040028DA RID: 10458
		[Token(Token = "0x40021B7")]
		def237,
		// Token: 0x040028DB RID: 10459
		[Token(Token = "0x40021B8")]
		def238,
		// Token: 0x040028DC RID: 10460
		[Token(Token = "0x40021B9")]
		def239,
		// Token: 0x040028DD RID: 10461
		[Token(Token = "0x40021BA")]
		def240 = 1108,
		// Token: 0x040028DE RID: 10462
		[Token(Token = "0x40021BB")]
		def241,
		// Token: 0x040028DF RID: 10463
		[Token(Token = "0x40021BC")]
		def242,
		// Token: 0x040028E0 RID: 10464
		[Token(Token = "0x40021BD")]
		def243,
		// Token: 0x040028E1 RID: 10465
		[Token(Token = "0x40021BE")]
		def244 = 5108,
		// Token: 0x040028E2 RID: 10466
		[Token(Token = "0x40021BF")]
		def245,
		// Token: 0x040028E3 RID: 10467
		[Token(Token = "0x40021C0")]
		def246,
		// Token: 0x040028E4 RID: 10468
		[Token(Token = "0x40021C1")]
		def247
	}
}
