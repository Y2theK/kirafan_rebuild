﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005F9 RID: 1529
	[Token(Token = "0x20004EC")]
	[StructLayout(3, Size = 4)]
	public enum eXlsTownActionFunc
	{
		// Token: 0x0400252B RID: 9515
		[Token(Token = "0x4001E95")]
		GoldUse,
		// Token: 0x0400252C RID: 9516
		[Token(Token = "0x4001E96")]
		AddRoom,
		// Token: 0x0400252D RID: 9517
		[Token(Token = "0x4001E97")]
		KRRPointLimitUp
	}
}
