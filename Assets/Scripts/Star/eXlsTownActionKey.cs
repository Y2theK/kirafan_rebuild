﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005F8 RID: 1528
	[Token(Token = "0x20004EB")]
	[StructLayout(3, Size = 4)]
	public enum eXlsTownActionKey
	{
		// Token: 0x04002528 RID: 9512
		[Token(Token = "0x4001E92")]
		RoomLevelUp,
		// Token: 0x04002529 RID: 9513
		[Token(Token = "0x4001E93")]
		ConntentUp
	}
}
