﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005FC RID: 1532
	[Token(Token = "0x20004EF")]
	[StructLayout(3, Size = 4)]
	public enum eXlsTownExtensionFunc
	{
		// Token: 0x04002542 RID: 9538
		[Token(Token = "0x4001EAC")]
		Non,
		// Token: 0x04002543 RID: 9539
		[Token(Token = "0x4001EAD")]
		KRRPointLimitUp
	}
}
