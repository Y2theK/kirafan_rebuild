﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005FD RID: 1533
	[Token(Token = "0x20004F0")]
	[StructLayout(3, Size = 4)]
	public enum eXlsTownUseResourceType
	{
		// Token: 0x04002545 RID: 9541
		[Token(Token = "0x4001EAF")]
		Non,
		// Token: 0x04002546 RID: 9542
		[Token(Token = "0x4001EB0")]
		ContentNum,
		// Token: 0x04002547 RID: 9543
		[Token(Token = "0x4001EB1")]
		RoomAdd
	}
}
