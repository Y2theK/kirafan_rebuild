﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000021 RID: 33
[Token(Token = "0x200001A")]
[StructLayout(3)]
public static class String_Ext
{
	// Token: 0x06000060 RID: 96 RVA: 0x00002298 File Offset: 0x00000498
	[Token(Token = "0x6000054")]
	[Address(RVA = "0x101622374", Offset = "0x1622374", VA = "0x101622374")]
	public static bool RightConvertToInt(this string self, string value, out int result)
	{
		return default(bool);
	}

	// Token: 0x06000061 RID: 97 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000055")]
	[Address(RVA = "0x10162248C", Offset = "0x162248C", VA = "0x10162248C")]
	public static string Coloring(this string str, string color)
	{
		return null;
	}

	// Token: 0x06000062 RID: 98 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000056")]
	[Address(RVA = "0x1016224F0", Offset = "0x16224F0", VA = "0x1016224F0")]
	public static string White(this string str)
	{
		return null;
	}

	// Token: 0x06000063 RID: 99 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000057")]
	[Address(RVA = "0x101622540", Offset = "0x1622540", VA = "0x101622540")]
	public static string Red(this string str)
	{
		return null;
	}

	// Token: 0x06000064 RID: 100 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000058")]
	[Address(RVA = "0x101622590", Offset = "0x1622590", VA = "0x101622590")]
	public static string Green(this string str)
	{
		return null;
	}

	// Token: 0x06000065 RID: 101 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000059")]
	[Address(RVA = "0x1016225E0", Offset = "0x16225E0", VA = "0x1016225E0")]
	public static string Blue(this string str)
	{
		return null;
	}

	// Token: 0x06000066 RID: 102 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005A")]
	[Address(RVA = "0x101622630", Offset = "0x1622630", VA = "0x101622630")]
	public static string Yellow(this string str)
	{
		return null;
	}

	// Token: 0x06000067 RID: 103 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005B")]
	[Address(RVA = "0x101622680", Offset = "0x1622680", VA = "0x101622680")]
	public static string Cyan(this string str)
	{
		return null;
	}

	// Token: 0x06000068 RID: 104 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005C")]
	[Address(RVA = "0x1016226D0", Offset = "0x16226D0", VA = "0x1016226D0")]
	public static string Magenta(this string str)
	{
		return null;
	}

	// Token: 0x06000069 RID: 105 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005D")]
	[Address(RVA = "0x101622720", Offset = "0x1622720", VA = "0x101622720")]
	public static string Orange(this string str)
	{
		return null;
	}

	// Token: 0x0600006A RID: 106 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005E")]
	[Address(RVA = "0x101622770", Offset = "0x1622770", VA = "0x101622770")]
	public static string APIColor(this string str)
	{
		return null;
	}

	// Token: 0x0600006B RID: 107 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600005F")]
	[Address(RVA = "0x1016227C0", Offset = "0x16227C0", VA = "0x1016227C0")]
	public static string Resize(this string str, int size)
	{
		return null;
	}

	// Token: 0x0600006C RID: 108 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000060")]
	[Address(RVA = "0x101622848", Offset = "0x1622848", VA = "0x101622848")]
	public static string Medium(this string str)
	{
		return null;
	}

	// Token: 0x0600006D RID: 109 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000061")]
	[Address(RVA = "0x101622850", Offset = "0x1622850", VA = "0x101622850")]
	public static string Small(this string str)
	{
		return null;
	}

	// Token: 0x0600006E RID: 110 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000062")]
	[Address(RVA = "0x101622858", Offset = "0x1622858", VA = "0x101622858")]
	public static string Large(this string str)
	{
		return null;
	}

	// Token: 0x0600006F RID: 111 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000063")]
	[Address(RVA = "0x101622860", Offset = "0x1622860", VA = "0x101622860")]
	public static string Bold(this string str)
	{
		return null;
	}

	// Token: 0x06000070 RID: 112 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000064")]
	[Address(RVA = "0x1016228B4", Offset = "0x16228B4", VA = "0x1016228B4")]
	public static string Italic(this string str)
	{
		return null;
	}
}
