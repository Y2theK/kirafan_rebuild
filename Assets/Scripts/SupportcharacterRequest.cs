﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using SupportcharacterRequestTypes;
using WWWTypes;

// Token: 0x02000079 RID: 121
[Token(Token = "0x2000052")]
[StructLayout(3)]
public static class SupportcharacterRequest
{
	// Token: 0x0600030B RID: 779 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C4")]
	[Address(RVA = "0x101622908", Offset = "0x1622908", VA = "0x101622908")]
	public static MeigewwwParam Set(Set param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600030C RID: 780 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C5")]
	[Address(RVA = "0x101622A0C", Offset = "0x1622A0C", VA = "0x101622A0C")]
	public static MeigewwwParam Set(PlayerSupport[] supportCharacters, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600030D RID: 781 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C6")]
	[Address(RVA = "0x101622B1C", Offset = "0x1622B1C", VA = "0x101622B1C")]
	public static MeigewwwParam Setname(Setname param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600030E RID: 782 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C7")]
	[Address(RVA = "0x101622C70", Offset = "0x1622C70", VA = "0x101622C70")]
	public static MeigewwwParam Setname(long managedSupportId, string name, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600030F RID: 783 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C8")]
	[Address(RVA = "0x101622DCC", Offset = "0x1622DCC", VA = "0x101622DCC")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000310 RID: 784 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C9")]
	[Address(RVA = "0x101622E90", Offset = "0x1622E90", VA = "0x101622E90")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
