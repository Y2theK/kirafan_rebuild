﻿using WWWTypes;

namespace SupportcharacterRequestTypes {
    public class Set {
        public PlayerSupport[] supportCharacters;
    }
}
