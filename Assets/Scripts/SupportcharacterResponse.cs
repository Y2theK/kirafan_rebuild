﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using SupportcharacterResponseTypes;
using WWWTypes;

// Token: 0x020000A0 RID: 160
[Token(Token = "0x2000077")]
[StructLayout(3)]
public static class SupportcharacterResponse
{
	// Token: 0x060003DC RID: 988 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000392")]
	[Address(RVA = "0x101622F6C", Offset = "0x1622F6C", VA = "0x101622F6C")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003DD RID: 989 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000393")]
	[Address(RVA = "0x101622FD4", Offset = "0x1622FD4", VA = "0x101622FD4")]
	public static Setname Setname(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003DE RID: 990 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000394")]
	[Address(RVA = "0x10162303C", Offset = "0x162303C", VA = "0x10162303C")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
