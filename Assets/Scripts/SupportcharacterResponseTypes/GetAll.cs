﻿using CommonResponseTypes;
using WWWTypes;

namespace SupportcharacterResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerSupport[] supportCharacters;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (supportCharacters == null) ? string.Empty : supportCharacters.ToString();
            return str;
        }
    }
}
