﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000006 RID: 6
[Token(Token = "0x2000005")]
[StructLayout(3)]
public class Template_AnimEventNotifier : MonoBehaviour
{
	// Token: 0x06000016 RID: 22 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000016")]
	[Address(RVA = "0x1016236A0", Offset = "0x16236A0", VA = "0x1016236A0")]
	private void Start()
	{
	}

	// Token: 0x06000017 RID: 23 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000017")]
	[Address(RVA = "0x1016236A4", Offset = "0x16236A4", VA = "0x1016236A4")]
	public void CallBack(int int0, int int2)
	{
	}

	// Token: 0x06000018 RID: 24 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000018")]
	[Address(RVA = "0x1016236A8", Offset = "0x16236A8", VA = "0x1016236A8")]
	public Template_AnimEventNotifier()
	{
	}

	// Token: 0x02000007 RID: 7
	[Token(Token = "0x2000CDC")]
	public enum eID
	{

	}
}
