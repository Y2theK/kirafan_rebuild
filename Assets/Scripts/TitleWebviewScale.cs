﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200005C RID: 92
[Token(Token = "0x2000035")]
[StructLayout(3)]
public class TitleWebviewScale : MonoBehaviour
{
	// Token: 0x060001EF RID: 495 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A8")]
	[Address(RVA = "0x1016236B0", Offset = "0x16236B0", VA = "0x1016236B0")]
	private void Start()
	{
	}

	// Token: 0x060001F0 RID: 496 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A9")]
	[Address(RVA = "0x1016237D8", Offset = "0x16237D8", VA = "0x1016237D8")]
	public TitleWebviewScale()
	{
	}

	// Token: 0x0400020A RID: 522
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000156")]
	[SerializeField]
	private RectTransform m_WebviewRect;

	// Token: 0x0400020B RID: 523
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000157")]
	[SerializeField]
	private GameObject m_GachaDetailWindow;

	// Token: 0x0400020C RID: 524
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000158")]
	private int m_BaseWidth;

	// Token: 0x0400020D RID: 525
	[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
	[Token(Token = "0x4000159")]
	private int m_BaseHeight;
}
