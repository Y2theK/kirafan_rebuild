﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TownFacilityRequestTypes;
using WWWTypes;

// Token: 0x0200007B RID: 123
[Token(Token = "0x2000054")]
[StructLayout(3)]
public static class TownFacilityRequest
{
	// Token: 0x06000313 RID: 787 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002CC")]
	[Address(RVA = "0x1016237EC", Offset = "0x16237EC", VA = "0x1016237EC")]
	public static MeigewwwParam Add(Add param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000314 RID: 788 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002CD")]
	[Address(RVA = "0x10162391C", Offset = "0x162391C", VA = "0x10162391C")]
	public static MeigewwwParam Add(string facilityId, string amount, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000315 RID: 789 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002CE")]
	[Address(RVA = "0x101623A70", Offset = "0x1623A70", VA = "0x101623A70")]
	public static MeigewwwParam Buyall(Buyall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000316 RID: 790 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002CF")]
	[Address(RVA = "0x101623B74", Offset = "0x1623B74", VA = "0x101623B74")]
	public static MeigewwwParam Buyall(TownFacilityBuyState[] townFacilityBuyStates, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000317 RID: 791 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D0")]
	[Address(RVA = "0x101623C84", Offset = "0x1623C84", VA = "0x101623C84")]
	public static MeigewwwParam Buildpointset(Buildpointset param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000318 RID: 792 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D1")]
	[Address(RVA = "0x101623EBC", Offset = "0x1623EBC", VA = "0x101623EBC")]
	public static MeigewwwParam Buildpointset(long managedTownFacilityId, int buildPointIndex, int openState, long buildTime, int actionNo, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000319 RID: 793 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D2")]
	[Address(RVA = "0x1016240EC", Offset = "0x16240EC", VA = "0x1016240EC")]
	public static MeigewwwParam Buildpointsetall(Buildpointsetall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031A RID: 794 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D3")]
	[Address(RVA = "0x1016241F0", Offset = "0x16241F0", VA = "0x1016241F0")]
	public static MeigewwwParam Buildpointsetall(TownFacilitySetState[] townFacilitySetStates, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031B RID: 795 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D4")]
	[Address(RVA = "0x101624300", Offset = "0x1624300", VA = "0x101624300")]
	public static MeigewwwParam Buildpointremove(Buildpointremove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031C RID: 796 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D5")]
	[Address(RVA = "0x101624470", Offset = "0x1624470", VA = "0x101624470")]
	public static MeigewwwParam Buildpointremove(long managedTownFacilityId, int openState, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031D RID: 797 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D6")]
	[Address(RVA = "0x1016245D0", Offset = "0x16245D0", VA = "0x1016245D0")]
	public static MeigewwwParam Preparelevelup(Preparelevelup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031E RID: 798 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D7")]
	[Address(RVA = "0x1016247C8", Offset = "0x16247C8", VA = "0x1016247C8")]
	public static MeigewwwParam Preparelevelup(long managedTownFacilityId, int nextLevel, int openState, long buildTime, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600031F RID: 799 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D8")]
	[Address(RVA = "0x1016249B0", Offset = "0x16249B0", VA = "0x1016249B0")]
	public static MeigewwwParam Openup(Openup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000320 RID: 800 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002D9")]
	[Address(RVA = "0x101624BA8", Offset = "0x1624BA8", VA = "0x101624BA8")]
	public static MeigewwwParam Openup(long managedTownFacilityId, int nextLevel, int openState, long buildTime, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000321 RID: 801 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DA")]
	[Address(RVA = "0x101624D90", Offset = "0x1624D90", VA = "0x101624D90")]
	public static MeigewwwParam Levelup(Levelup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000322 RID: 802 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DB")]
	[Address(RVA = "0x101624F88", Offset = "0x1624F88", VA = "0x101624F88")]
	public static MeigewwwParam Levelup(long managedTownFacilityId, int nextLevel, int openState, long actionTime, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000323 RID: 803 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DC")]
	[Address(RVA = "0x101625170", Offset = "0x1625170", VA = "0x101625170")]
	public static MeigewwwParam Gemlevelup(Gemlevelup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000324 RID: 804 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DD")]
	[Address(RVA = "0x1016253A8", Offset = "0x16253A8", VA = "0x1016253A8")]
	public static MeigewwwParam Gemlevelup(long managedTownFacilityId, int nextLevel, int openState, long actionTime, long remainingTime, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000325 RID: 805 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DE")]
	[Address(RVA = "0x1016255D8", Offset = "0x16255D8", VA = "0x1016255D8")]
	public static MeigewwwParam Itemup(Itemup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000326 RID: 806 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002DF")]
	[Address(RVA = "0x1016257D0", Offset = "0x16257D0", VA = "0x1016257D0")]
	public static MeigewwwParam Itemup(long managedTownFacilityId, int itemNo, int amount, long actionTime, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000327 RID: 807 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E0")]
	[Address(RVA = "0x1016259B8", Offset = "0x16259B8", VA = "0x1016259B8")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000328 RID: 808 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E1")]
	[Address(RVA = "0x101625A7C", Offset = "0x1625A7C", VA = "0x101625A7C")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000329 RID: 809 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E2")]
	[Address(RVA = "0x101625B40", Offset = "0x1625B40", VA = "0x101625B40")]
	public static MeigewwwParam SetState(SetState param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032A RID: 810 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E3")]
	[Address(RVA = "0x101625CB0", Offset = "0x1625CB0", VA = "0x101625CB0")]
	public static MeigewwwParam SetState(long managedTownFacilityId, int openState, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032B RID: 811 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E4")]
	[Address(RVA = "0x101625E10", Offset = "0x1625E10", VA = "0x101625E10")]
	public static MeigewwwParam Sale(Sale param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032C RID: 812 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E5")]
	[Address(RVA = "0x101625F14", Offset = "0x1625F14", VA = "0x101625F14")]
	public static MeigewwwParam Sale(string managedTownFacilityId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032D RID: 813 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E6")]
	[Address(RVA = "0x101626024", Offset = "0x1626024", VA = "0x101626024")]
	public static MeigewwwParam Limitadd(Limitadd param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032E RID: 814 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E7")]
	[Address(RVA = "0x10162614C", Offset = "0x162614C", VA = "0x10162614C")]
	public static MeigewwwParam Limitadd(int num, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600032F RID: 815 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E8")]
	[Address(RVA = "0x101626264", Offset = "0x1626264", VA = "0x101626264")]
	public static MeigewwwParam Remove(Remove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000330 RID: 816 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002E9")]
	[Address(RVA = "0x10162638C", Offset = "0x162638C", VA = "0x10162638C")]
	public static MeigewwwParam Remove(long managedTownFacilityId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
