﻿namespace TownFacilityRequestTypes {
    public class Buildpointset {
        public long managedTownFacilityId;
        public int buildPointIndex;
        public int openState;
        public long buildTime;
        public int actionNo;
    }
}
