﻿namespace TownFacilityRequestTypes {
    public class Gemlevelup {
        public long managedTownFacilityId;
        public int nextLevel;
        public int openState;
        public long actionTime;
        public long remainingTime;
    }
}
