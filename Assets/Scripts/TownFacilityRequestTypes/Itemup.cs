﻿namespace TownFacilityRequestTypes {
    public class Itemup {
        public long managedTownFacilityId;
        public int itemNo;
        public int amount;
        public long actionTime;
    }
}
