﻿namespace TownFacilityRequestTypes {
    public class Levelup {
        public long managedTownFacilityId;
        public int nextLevel;
        public int openState;
        public long actionTime;
    }
}
