﻿namespace TownFacilityRequestTypes {
    public class Openup {
        public long managedTownFacilityId;
        public int nextLevel;
        public int openState;
        public long buildTime;
    }
}
