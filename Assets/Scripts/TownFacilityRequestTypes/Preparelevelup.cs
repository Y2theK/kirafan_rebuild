﻿namespace TownFacilityRequestTypes {
    public class Preparelevelup {
        public long managedTownFacilityId;
        public int nextLevel;
        public int openState;
        public long buildTime;
    }
}
