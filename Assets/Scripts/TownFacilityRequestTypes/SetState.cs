﻿namespace TownFacilityRequestTypes {
    public class SetState {
        public long managedTownFacilityId;
        public int openState;
    }
}
