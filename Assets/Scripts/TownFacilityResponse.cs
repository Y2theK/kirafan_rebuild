﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TownFacilityResponseTypes;
using WWWTypes;

// Token: 0x020000A2 RID: 162
[Token(Token = "0x2000079")]
[StructLayout(3)]
public static class TownFacilityResponse
{
	// Token: 0x060003E0 RID: 992 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000396")]
	[Address(RVA = "0x10162651C", Offset = "0x162651C", VA = "0x10162651C")]
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E1 RID: 993 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000397")]
	[Address(RVA = "0x101626584", Offset = "0x1626584", VA = "0x101626584")]
	public static Buyall Buyall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E2 RID: 994 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000398")]
	[Address(RVA = "0x1016265EC", Offset = "0x16265EC", VA = "0x1016265EC")]
	public static Buildpointset Buildpointset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E3 RID: 995 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000399")]
	[Address(RVA = "0x101626654", Offset = "0x1626654", VA = "0x101626654")]
	public static Buildpointsetall Buildpointsetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E4 RID: 996 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039A")]
	[Address(RVA = "0x1016266BC", Offset = "0x16266BC", VA = "0x1016266BC")]
	public static Buildpointremove Buildpointremove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E5 RID: 997 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039B")]
	[Address(RVA = "0x101626724", Offset = "0x1626724", VA = "0x101626724")]
	public static Preparelevelup Preparelevelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E6 RID: 998 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039C")]
	[Address(RVA = "0x10162678C", Offset = "0x162678C", VA = "0x10162678C")]
	public static Openup Openup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E7 RID: 999 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039D")]
	[Address(RVA = "0x1016267F4", Offset = "0x16267F4", VA = "0x1016267F4")]
	public static Levelup Levelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E8 RID: 1000 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039E")]
	[Address(RVA = "0x10162685C", Offset = "0x162685C", VA = "0x10162685C")]
	public static Gemlevelup Gemlevelup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003E9 RID: 1001 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600039F")]
	[Address(RVA = "0x1016268C4", Offset = "0x16268C4", VA = "0x1016268C4")]
	public static Itemup Itemup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003EA RID: 1002 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A0")]
	[Address(RVA = "0x10162692C", Offset = "0x162692C", VA = "0x10162692C")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003EB RID: 1003 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A1")]
	[Address(RVA = "0x101626994", Offset = "0x1626994", VA = "0x101626994")]
	public static SetState SetState(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003EC RID: 1004 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A2")]
	[Address(RVA = "0x1016269FC", Offset = "0x16269FC", VA = "0x1016269FC")]
	public static Sale Sale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003ED RID: 1005 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A3")]
	[Address(RVA = "0x101626A64", Offset = "0x1626A64", VA = "0x101626A64")]
	public static Limitadd Limitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003EE RID: 1006 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A4")]
	[Address(RVA = "0x101626ACC", Offset = "0x1626ACC", VA = "0x101626ACC")]
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
