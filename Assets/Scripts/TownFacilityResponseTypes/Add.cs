﻿using CommonResponseTypes;

namespace TownFacilityResponseTypes {
    public class Add : CommonResponse {
        public string managedTownFacilityIds;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedTownFacilityIds == null) ? string.Empty : managedTownFacilityIds.ToString();
            return str;
        }
    }
}
