﻿using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes {
    public class Buildpointsetall : CommonResponse {
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
