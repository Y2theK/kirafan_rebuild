﻿using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes {
    public class Buyall : CommonResponse {
        public string managedTownFacilityIds;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedTownFacilityIds == null) ? string.Empty : managedTownFacilityIds.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
