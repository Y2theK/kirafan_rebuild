﻿using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerTownFacility[] managedTownFacilities;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedTownFacilities == null) ? string.Empty : managedTownFacilities.ToString();
            return str;
        }
    }
}
