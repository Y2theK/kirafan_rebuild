﻿using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes {
    public class Openup : CommonResponse {
        public Player player;
        public PlayerRoomObject[] managedRoomObjects;
        public PlayerRoom managedRoom;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            str += (managedRoom == null) ? string.Empty : managedRoom.ToString();
            return str;
        }
    }
}
