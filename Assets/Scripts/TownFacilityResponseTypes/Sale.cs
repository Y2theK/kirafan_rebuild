﻿using CommonResponseTypes;
using WWWTypes;

namespace TownFacilityResponseTypes {
    public class Sale : CommonResponse {
        public PlayerTownFacility[] managedTownFacilities;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedTownFacilities == null) ? string.Empty : managedTownFacilities.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
