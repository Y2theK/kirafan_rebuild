﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TownRequestTypes;

// Token: 0x0200007C RID: 124
[Token(Token = "0x2000055")]
[StructLayout(3)]
public static class TownRequest
{
	// Token: 0x06000331 RID: 817 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002EA")]
	[Address(RVA = "0x101627264", Offset = "0x1627264", VA = "0x101627264")]
	public static MeigewwwParam Set(Set param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000332 RID: 818 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002EB")]
	[Address(RVA = "0x1016273B8", Offset = "0x16273B8", VA = "0x1016273B8")]
	public static MeigewwwParam Set(long managedTownId, string gridData, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000333 RID: 819 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002EC")]
	[Address(RVA = "0x101627514", Offset = "0x1627514", VA = "0x101627514")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000334 RID: 820 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002ED")]
	[Address(RVA = "0x10162763C", Offset = "0x162763C", VA = "0x10162763C")]
	public static MeigewwwParam GetAll(long playerId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000335 RID: 821 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002EE")]
	[Address(RVA = "0x101627754", Offset = "0x1627754", VA = "0x101627754")]
	public static MeigewwwParam Remove(Remove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000336 RID: 822 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002EF")]
	[Address(RVA = "0x10162787C", Offset = "0x162787C", VA = "0x10162787C")]
	public static MeigewwwParam Remove(long managedTownId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
