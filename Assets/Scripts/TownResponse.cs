﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TownResponseTypes;
using WWWTypes;

// Token: 0x020000A3 RID: 163
[Token(Token = "0x200007A")]
[StructLayout(3)]
public static class TownResponse
{
	// Token: 0x060003EF RID: 1007 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A5")]
	[Address(RVA = "0x1016279AC", Offset = "0x16279AC", VA = "0x1016279AC")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F0 RID: 1008 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A6")]
	[Address(RVA = "0x101627A14", Offset = "0x1627A14", VA = "0x101627A14")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F1 RID: 1009 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A7")]
	[Address(RVA = "0x101627A7C", Offset = "0x1627A7C", VA = "0x101627A7C")]
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
