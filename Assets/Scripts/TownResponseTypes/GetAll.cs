﻿using CommonResponseTypes;
using WWWTypes;

namespace TownResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerTown[] managedTowns;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedTowns == null) ? string.Empty : managedTowns.ToString();
            return str;
        }
    }
}
