﻿using CommonResponseTypes;

namespace TownResponseTypes {
    public class Set : CommonResponse {
        public long managedTownId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += managedTownId.ToString();
            return str;
        }
    }
}
