﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TrainingRequestTypes;
using WWWTypes;

// Token: 0x0200007D RID: 125
[Token(Token = "0x2000056")]
[StructLayout(3)]
public static class TrainingRequest
{
	// Token: 0x06000337 RID: 823 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F0")]
	[Address(RVA = "0x101627BC4", Offset = "0x1627BC4", VA = "0x101627BC4")]
	public static MeigewwwParam GetList(GetList param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000338 RID: 824 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F1")]
	[Address(RVA = "0x101627C88", Offset = "0x1627C88", VA = "0x101627C88")]
	public static MeigewwwParam GetList([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000339 RID: 825 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F2")]
	[Address(RVA = "0x101627D4C", Offset = "0x1627D4C", VA = "0x101627D4C")]
	public static MeigewwwParam Orders(Orders param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600033A RID: 826 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F3")]
	[Address(RVA = "0x101627E50", Offset = "0x1627E50", VA = "0x101627E50")]
	public static MeigewwwParam Orders(TrainingOrderParam[] trainingOrderParams, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600033B RID: 827 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F4")]
	[Address(RVA = "0x101627F60", Offset = "0x1627F60", VA = "0x101627F60")]
	public static MeigewwwParam Cancel(Cancel param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600033C RID: 828 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F5")]
	[Address(RVA = "0x101628088", Offset = "0x1628088", VA = "0x101628088")]
	public static MeigewwwParam Cancel(long orderId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600033D RID: 829 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F6")]
	[Address(RVA = "0x1016281A0", Offset = "0x16281A0", VA = "0x1016281A0")]
	public static MeigewwwParam Completes(Completes param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600033E RID: 830 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002F7")]
	[Address(RVA = "0x1016282A4", Offset = "0x16282A4", VA = "0x1016282A4")]
	public static MeigewwwParam Completes(TrainingCompleteParam[] trainingCompleteParams, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
