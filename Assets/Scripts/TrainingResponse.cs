﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using TrainingResponseTypes;
using WWWTypes;

// Token: 0x020000A4 RID: 164
[Token(Token = "0x200007B")]
[StructLayout(3)]
public static class TrainingResponse
{
	// Token: 0x060003F2 RID: 1010 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A8")]
	[Address(RVA = "0x1016283D4", Offset = "0x16283D4", VA = "0x1016283D4")]
	public static GetList GetList(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F3 RID: 1011 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003A9")]
	[Address(RVA = "0x10162843C", Offset = "0x162843C", VA = "0x10162843C")]
	public static Orders Orders(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F4 RID: 1012 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003AA")]
	[Address(RVA = "0x1016284A4", Offset = "0x16284A4", VA = "0x1016284A4")]
	public static Cancel Cancel(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F5 RID: 1013 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003AB")]
	[Address(RVA = "0x10162850C", Offset = "0x162850C", VA = "0x10162850C")]
	public static Completes Completes(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
