﻿using CommonResponseTypes;
using WWWTypes;

namespace TrainingResponseTypes {
    public class Completes : CommonResponse {
        public Player player;
        public TrainingSlotInfo[] slotInfo;
        public TrainingInfo[] trainingInfo;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public PlayerItemSummary[] itemSummary;
        public TrainingCompleteReward[] trainingCompleteRewards;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (slotInfo == null) ? string.Empty : slotInfo.ToString();
            str += (trainingInfo == null) ? string.Empty : trainingInfo.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (trainingCompleteRewards == null) ? string.Empty : trainingCompleteRewards.ToString();
            return str;
        }
    }
}
