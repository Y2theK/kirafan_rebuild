﻿using CommonResponseTypes;
using WWWTypes;

namespace TrainingResponseTypes {
    public class GetList : CommonResponse {
        public TrainingSlotInfo[] slotInfo;
        public TrainingInfo[] trainingInfo;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (slotInfo == null) ? string.Empty : slotInfo.ToString();
            str += (trainingInfo == null) ? string.Empty : trainingInfo.ToString();
            return str;
        }
    }
}
