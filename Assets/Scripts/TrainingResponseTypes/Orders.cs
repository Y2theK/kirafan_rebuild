﻿using CommonResponseTypes;
using WWWTypes;

namespace TrainingResponseTypes {
    public class Orders : CommonResponse {
        public Player player;
        public TrainingSlotInfo[] slotInfo;
        public TrainingInfo[] trainingInfo;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (slotInfo == null) ? string.Empty : slotInfo.ToString();
            str += (trainingInfo == null) ? string.Empty : trainingInfo.ToString();
            return str;
        }
    }
}
