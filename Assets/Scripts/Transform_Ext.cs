﻿using UnityEngine;

public static class Transform_Ext {
    public static void positionX(this Transform self, float x) {
        self.position = new Vector3(x, self.position.y, self.position.z);
    }

    public static void positionY(this Transform self, float y) {
        self.position = new Vector3(self.position.x, y, self.position.z);
    }

    public static void positionZ(this Transform self, float z) {
        self.position = new Vector3(self.position.x, self.position.y, z);
    }

    public static void localPosX(this Transform self, float x) {
        self.localPosition = new Vector3(x, self.localPosition.y, self.localPosition.z);
    }

    public static void localPosY(this Transform self, float y) {
        self.localPosition = new Vector3(self.localPosition.x, y, self.localPosition.z);
    }

    public static void localPosZ(this Transform self, float z) {
        self.localPosition = new Vector3(self.localPosition.x, self.localPosition.y, z);
    }

    public static void localScaleX(this Transform self, float x) {
        self.localScale = new Vector3(x, self.localScale.y, self.localScale.z);
    }

    public static void localScaleY(this Transform self, float y) {
        self.localScale = new Vector3(self.localScale.x, y, self.localScale.z);
    }

    public static void localScaleZ(this Transform self, float z) {
        self.localScale = new Vector3(self.localScale.x, self.localScale.y, z);
    }

    public static Transform SearchParent(this Transform self) {
        return parentObject(self);
    }

    public static Transform SearchParent(this Transform self, string tag) {
        return parentObject(self, tag);
    }

    private static Transform parentObject(Transform obj) {
        if (obj.parent == null) {
            return obj;
        }
        return parentObject(obj.parent);
    }

    private static Transform parentObject(Transform obj, string tag) {
        if (obj == null || obj.CompareTag(tag)) {
            return obj;
        }
        return parentObject(obj.parent, tag);
    }
}
