﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000DA RID: 218
[Token(Token = "0x20000A3")]
[StructLayout(3)]
public class UnicodeCSVReader
{
	// Token: 0x0600056F RID: 1391 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000514")]
	[Address(RVA = "0x1016291BC", Offset = "0x16291BC", VA = "0x1016291BC")]
	public UnicodeCSVReader(string path)
	{
	}

	// Token: 0x06000570 RID: 1392 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000515")]
	[Address(RVA = "0x101629260", Offset = "0x1629260", VA = "0x101629260")]
	public void parse(string path)
	{
	}

	// Token: 0x06000571 RID: 1393 RVA: 0x000035B8 File Offset: 0x000017B8
	[Token(Token = "0x6000516")]
	[Address(RVA = "0x101629480", Offset = "0x1629480", VA = "0x101629480")]
	public bool IsAvailable()
	{
		return default(bool);
	}

	// Token: 0x06000572 RID: 1394 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000517")]
	[Address(RVA = "0x101629490", Offset = "0x1629490", VA = "0x101629490")]
	public string get(int row, int col)
	{
		return null;
	}

	// Token: 0x06000573 RID: 1395 RVA: 0x000035D0 File Offset: 0x000017D0
	[Token(Token = "0x6000518")]
	[Address(RVA = "0x101629610", Offset = "0x1629610", VA = "0x101629610")]
	public int getRowSize()
	{
		return 0;
	}

	// Token: 0x06000574 RID: 1396 RVA: 0x000035E8 File Offset: 0x000017E8
	[Token(Token = "0x6000519")]
	[Address(RVA = "0x101629648", Offset = "0x1629648", VA = "0x101629648")]
	public int getColumnSize(int row)
	{
		return 0;
	}

	// Token: 0x06000575 RID: 1397 RVA: 0x00003600 File Offset: 0x00001800
	[Token(Token = "0x600051A")]
	[Address(RVA = "0x1016297E8", Offset = "0x16297E8", VA = "0x1016297E8")]
	public int getInt(int row, int col)
	{
		return 0;
	}

	// Token: 0x06000576 RID: 1398 RVA: 0x00003618 File Offset: 0x00001818
	[Token(Token = "0x600051B")]
	[Address(RVA = "0x101629818", Offset = "0x1629818", VA = "0x101629818")]
	public float getFloat(int row, int col)
	{
		return 0f;
	}

	// Token: 0x06000577 RID: 1399 RVA: 0x00003630 File Offset: 0x00001830
	[Token(Token = "0x600051C")]
	[Address(RVA = "0x101629848", Offset = "0x1629848", VA = "0x101629848")]
	public double getDouble(int row, int col)
	{
		return 0.0;
	}

	// Token: 0x04000308 RID: 776
	[Token(Token = "0x40001D6")]
	private const char TAB_SEPARATOR = '\t';

	// Token: 0x04000309 RID: 777
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x40001D7")]
	private string m_path;

	// Token: 0x0400030A RID: 778
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40001D8")]
	private ArrayList m_strings;

	// Token: 0x0400030B RID: 779
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x40001D9")]
	private char[] m_Separator;
}
