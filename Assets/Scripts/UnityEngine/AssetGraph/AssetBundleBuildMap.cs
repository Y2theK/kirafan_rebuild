﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace UnityEngine.AssetGraph
{
	// Token: 0x020000EF RID: 239
	[Token(Token = "0x20000AC")]
	[StructLayout(3)]
	public class AssetBundleBuildMap : ScriptableObject
	{
		// Token: 0x060005C6 RID: 1478 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000558")]
		[Address(RVA = "0x1016298EC", Offset = "0x16298EC", VA = "0x1016298EC")]
		public static AssetBundleBuildMap GetBuildMap()
		{
			return null;
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x00003708 File Offset: 0x00001908
		[Token(Token = "0x6000559")]
		[Address(RVA = "0x1016299E0", Offset = "0x16299E0", VA = "0x1016299E0")]
		private static bool Load()
		{
			return default(bool);
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600055A")]
		[Address(RVA = "0x1016299E8", Offset = "0x16299E8", VA = "0x1016299E8")]
		public static void SetMapDirty()
		{
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600055B")]
		[Address(RVA = "0x1016299EC", Offset = "0x16299EC", VA = "0x1016299EC")]
		internal static string MakeFullName(string assetBundleName, string variantName)
		{
			return null;
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600055C")]
		[Address(RVA = "0x101629AE4", Offset = "0x1629AE4", VA = "0x101629AE4")]
		internal static string[] FullNameToNameAndVariant(string assetBundleFullName)
		{
			return null;
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600055D")]
		[Address(RVA = "0x101629D64", Offset = "0x1629D64", VA = "0x101629D64")]
		public AssetBundleBuildMap.AssetBundleEntry GetAssetBundle(string registererId, string assetBundleFullName)
		{
			return null;
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600055E")]
		[Address(RVA = "0x101629FD0", Offset = "0x1629FD0", VA = "0x101629FD0")]
		public void Clear()
		{
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600055F")]
		[Address(RVA = "0x10162A030", Offset = "0x162A030", VA = "0x10162A030")]
		public void ClearFromId(string id)
		{
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000560")]
		[Address(RVA = "0x10162A108", Offset = "0x162A108", VA = "0x10162A108")]
		public AssetBundleBuildMap.AssetBundleEntry GetAssetBundleWithNameAndVariant(string registererId, string assetBundleName, string variantName)
		{
			return null;
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000561")]
		[Address(RVA = "0x10162A140", Offset = "0x162A140", VA = "0x10162A140")]
		public string[] GetAssetPathsFromAssetBundleAndAssetName(string assetbundleName, string assetName)
		{
			return null;
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000562")]
		[Address(RVA = "0x10162A2D8", Offset = "0x162A2D8", VA = "0x10162A2D8")]
		public string[] GetAssetPathsFromAssetBundle(string assetBundleName)
		{
			return null;
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000563")]
		[Address(RVA = "0x10162A550", Offset = "0x162A550", VA = "0x10162A550")]
		public string GetAssetBundleName(string assetPath)
		{
			return null;
		}

		// Token: 0x060005D2 RID: 1490 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000564")]
		[Address(RVA = "0x10162A64C", Offset = "0x162A64C", VA = "0x10162A64C")]
		public string GetImplicitAssetBundleName(string assetPath)
		{
			return null;
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000565")]
		[Address(RVA = "0x10162A650", Offset = "0x162A650", VA = "0x10162A650")]
		public string[] GetAllAssetBundleNames()
		{
			return null;
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000566")]
		[Address(RVA = "0x10162A750", Offset = "0x162A750", VA = "0x10162A750")]
		public AssetBundleBuildMap()
		{
		}

		// Token: 0x04000399 RID: 921
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400020C")]
		[SerializeField]
		private List<AssetBundleBuildMap.AssetBundleEntry> m_assetBundles;

		// Token: 0x0400039A RID: 922
		[Token(Token = "0x400020D")]
		private static AssetBundleBuildMap s_map;

		// Token: 0x020000F0 RID: 240
		[Token(Token = "0x2000D1D")]
		[Serializable]
		public class AssetBundleEntry
		{
			// Token: 0x17000064 RID: 100
			// (get) Token: 0x060005D5 RID: 1493 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x17000682")]
			public string Name
			{
				[Token(Token = "0x6005D3A")]
				[Address(RVA = "0x10162ABDC", Offset = "0x162ABDC", VA = "0x10162ABDC")]
				get
				{
					return null;
				}
			}

			// Token: 0x17000065 RID: 101
			// (get) Token: 0x060005D6 RID: 1494 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x17000683")]
			public string Variant
			{
				[Token(Token = "0x6005D3B")]
				[Address(RVA = "0x10162ABE4", Offset = "0x162ABE4", VA = "0x10162ABE4")]
				get
				{
					return null;
				}
			}

			// Token: 0x17000066 RID: 102
			// (get) Token: 0x060005D7 RID: 1495 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x17000684")]
			public string FullName
			{
				[Token(Token = "0x6005D3C")]
				[Address(RVA = "0x10162ABEC", Offset = "0x162ABEC", VA = "0x10162ABEC")]
				get
				{
					return null;
				}
			}

			// Token: 0x060005D8 RID: 1496 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D3D")]
			[Address(RVA = "0x101629EF4", Offset = "0x1629EF4", VA = "0x101629EF4")]
			public AssetBundleEntry(string registererId, string assetBundleName, string variantName)
			{
			}

			// Token: 0x060005D9 RID: 1497 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D3E")]
			[Address(RVA = "0x10162ABF4", Offset = "0x162ABF4", VA = "0x10162ABF4")]
			public void Clear()
			{
			}

			// Token: 0x060005DA RID: 1498 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D3F")]
			[Address(RVA = "0x10162AC54", Offset = "0x162AC54", VA = "0x10162AC54")]
			public void AddAssets(string id, IEnumerable<string> assets)
			{
			}

			// Token: 0x060005DB RID: 1499 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D40")]
			[Address(RVA = "0x10162A8C8", Offset = "0x162A8C8", VA = "0x10162A8C8")]
			public IEnumerable<string> GetAssetFromAssetName(string assetName)
			{
				return null;
			}

			// Token: 0x0400039B RID: 923
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400551C")]
			[SerializeField]
			internal string m_assetBundleName;

			// Token: 0x0400039C RID: 924
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400551D")]
			[SerializeField]
			internal string m_assetBundleVariantName;

			// Token: 0x0400039D RID: 925
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400551E")]
			[SerializeField]
			internal string m_fullName;

			// Token: 0x0400039E RID: 926
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400551F")]
			[SerializeField]
			internal List<AssetBundleBuildMap.AssetBundleEntry.AssetPathString> m_assets;

			// Token: 0x0400039F RID: 927
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005520")]
			[SerializeField]
			public string m_registererId;

			// Token: 0x020000F1 RID: 241
			[Token(Token = "0x200133C")]
			[Serializable]
			internal struct AssetPathString
			{
				// Token: 0x060005DC RID: 1500 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006502")]
				[Address(RVA = "0x10003987C", Offset = "0x3987C", VA = "0x10003987C")]
				internal AssetPathString(string s)
				{
				}

				// Token: 0x040003A0 RID: 928
				[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
				[Token(Token = "0x4007575")]
				[SerializeField]
				public string original;

				// Token: 0x040003A1 RID: 929
				[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
				[Token(Token = "0x4007576")]
				[SerializeField]
				public string lower;
			}
		}
	}
}
