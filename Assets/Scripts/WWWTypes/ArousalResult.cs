﻿namespace WWWTypes {
    public class ArousalResult {
        public int characterId;
        public int arousalLevel;
        public int duplicatedCount;
        public int arousalItemId;
        public int arousalItemAmount;
    }
}
