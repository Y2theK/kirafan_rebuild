﻿namespace WWWTypes {
    public class Bonus {
        public int bonusId;
        public int imageId;
        public int type;
        public int dayIndex;
        public BonusDay[] bonusDays;
    }
}
