﻿namespace WWWTypes {
    public class ChangeScheduleMember {
        public long managedPartyMemberId;
        public int scheduleId;
        public int scheduleTag;
        public long managedFacilityId;
        public int touchItemResultNo;
        public int flag;
        public string scheduleTable;
        public DropPresent[] dropPresent;
    }
}
