﻿namespace WWWTypes {
    public class Character {
        public int id;
        public string name;
        public int namedType;
        public int titleType;
        public int friendshipExpTableId;
        public int rarity;
        public int classType;
        public int elementType;
        public int cost;
        public int level;
        public int levelLimit;
        public int hp;
        public int attack;
        public int magic;
        public int defence;
        public int magicDefence;
        public int speed;
        public int luck;
        public int skillLevelLimit;
        public int charSkillId;
        public int charSkillExpTableId;
        public int classSkillId1;
        public int classSkillExpTableId1;
        public int classSkillId2;
        public int classSkillExpTableId2;
    }
}
