﻿namespace WWWTypes {
    public class CharacterEvolution {
        public int recipeId;
        public int srcCharacterId;
        public int dstCharacterId;
        public int amount;
        public int itemId0;
        public int num0;
        public int itemId1;
        public int num1;
        public int itemId2;
        public int num2;
        public int itemId3;
        public int num3;
        public int itemId4;
        public int num4;
    }
}
