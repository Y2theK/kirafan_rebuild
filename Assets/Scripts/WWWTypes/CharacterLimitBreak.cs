﻿namespace WWWTypes {
    public class CharacterLimitBreak {
        public int recipeId;
        public int rarity;
        public int classType;
        public int costGold;
        public int costItemId;
        public int costItemAmount;
        public int costTitleItemAmount;
        public int levelUpMax1;
        public int skillUpMax1;
        public int levelUpMax2;
        public int skillUpMax2;
        public int levelUpMax3;
        public int skillUpMax3;
        public int levelUpMax4;
        public int skillUpMax4;
    }
}
