﻿using System;

namespace WWWTypes {
    public class ChestInfo {
        public int id;
        public string name;
        public string bannerName;
        public string bgName;
        public int eventType;
        public int tradeGroupId;
        public DateTime startAt;
        public DateTime endAt;
        public DateTime dispStartAt;
        public DateTime dispEndAt;
        public int costItemId;
        public int costItemAmount;
        public int tutorialTipsId;
        public int enableReset;
        public int currentStock;
        public int totalStock;
        public int currentStep;
        public int maxStep;
        public ChestPrize[] resetChestPrizes;
    }
}
