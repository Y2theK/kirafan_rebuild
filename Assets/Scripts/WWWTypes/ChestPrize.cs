﻿namespace WWWTypes {
    public class ChestPrize {
        public int rewardType;
        public int rewardId;
        public int rewardAmount;
        public int resetTarget;
        public int currentStock;
        public int maxStock;
    }
}
