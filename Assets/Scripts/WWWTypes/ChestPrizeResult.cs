﻿namespace WWWTypes {
    public class ChestPrizeResult {
        public int rewardType;
        public int rewardId;
        public int rewardAmount;
        public int resetTarget;
    }
}
