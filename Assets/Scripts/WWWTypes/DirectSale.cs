﻿using System;

namespace WWWTypes {
    public class DirectSale {
        public long id;
        public int restockType;
        public int stock;
        public int rewardType;
        public int rewardId;
        public int rewardAmount;
        public string detailText;
        public int badgeType;
        public DateTime? restockAt;
    }
}
