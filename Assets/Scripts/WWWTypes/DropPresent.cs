﻿namespace WWWTypes {
    public class DropPresent {
        public int itemNo;
        public long presentAt;
        public float magnification;
    }
}
