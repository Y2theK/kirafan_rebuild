﻿using System;

namespace WWWTypes {
    public class EventBanner {
        public int id;
        public int category;
        public string imgId;
        public float offsetX;
        public float offsetY;
        public DateTime startAt;
        public DateTime endAt;
        public int[] pickupCharacterIds;
        public DateTime dispEndAt;
    }
}
