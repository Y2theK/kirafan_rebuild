﻿using System;

namespace WWWTypes {
    public class EventQuest {
        public int id;
        public int eventType;
        public int groupId;
        public string groupName;
        public int groupSkip;
        public int mapFlg;
        public int tradeGroupId;
        public int chestId;
        public Quest quest;
        public DateTime startAt;
        public DateTime endAt;
        public DateTime? lossTimeEndAt;
        public int orderLimit;
        public int freq;
        public int interval;
        public int month;
        public int day;
        public int sun;
        public int mon;
        public int tue;
        public int wed;
        public int thu;
        public int fri;
        public int sat;
        public int[] condEventQuestIds;
        public int exTitle;
        public int exName;
        public int exRarity;
        public int exCost;
        public int exFire;
        public int exWater;
        public int exEarth;
        public int exWind;
        public int exMoon;
        public int exSun;
        public int exFighter;
        public int exMagician;
        public int exPriest;
        public int exKnight;
        public int exAlchemist;
    }
}
