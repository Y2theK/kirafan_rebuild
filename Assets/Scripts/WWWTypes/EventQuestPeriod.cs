﻿using System;

namespace WWWTypes {
    public class EventQuestPeriod {
        public int eventType;
        public DateTime startAt;
        public DateTime endAt;
        public DateTime? lossTimeEndAt;
        public EventQuestPeriodGroup[] eventQuestPeriodGroups;
    }
}
