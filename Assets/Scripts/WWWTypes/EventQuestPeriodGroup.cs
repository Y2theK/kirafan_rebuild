﻿using System;

namespace WWWTypes {
    public class EventQuestPeriodGroup {
        public int groupId;
        public DateTime startAt;
        public DateTime endAt;
        public bool unlocked;
        public EventQuestPeriodGroupQuest[] eventQuestPeriodGroupQuests;
    }
}
