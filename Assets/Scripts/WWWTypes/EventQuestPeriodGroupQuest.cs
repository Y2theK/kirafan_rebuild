﻿using System;

namespace WWWTypes {
    public class EventQuestPeriodGroupQuest {
        public int questId;
        public int eventQuestId;
        public int[] condEventQuestIds;
        public DateTime startAt;
        public DateTime endAt;
        public bool unlocked;
    }
}
