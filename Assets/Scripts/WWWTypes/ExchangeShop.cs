﻿using System;

namespace WWWTypes {
    public class ExchangeShop {
        public long id;
        public DateTime? startAt;
        public DateTime? endAt;
        public int restockType;
        public int stock;
        public int uiPriority;
        public int srcType;
        public int srcId;
        public int srcAmount;
        public int rewardType;
        public int rewardId;
        public int rewardAmount;
        public string description;
        public string detailText;
        public int buyCount;
        public int badgeType;
        public DateTime? restockAt;
    }
}
