﻿using System;

namespace WWWTypes {
    public class FriendList {
        public long managedFriendId;
        public int state;
        public int direction;
        public long playerId;
        public string name;
        public int supportLimit;
        public string comment;
        public string myCode;
        public int currentAchievementId;
        public int level;
        public long totalExp;
        public DateTime lastLoginAt;
        public string supportName;
        public SupportCharacter[] supportCharacters;
        public SupportCharacter firstFavoriteMember;
    }
}
