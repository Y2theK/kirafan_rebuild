﻿using System;

namespace WWWTypes {
    public class Gacha {
        public int id;
        public string name;
        public string bannerId;
        public string mvName;
        public int type;
        public int unlimitedGem;
        public int unlimitedGemDrawCount;
        public int gem1;
        public int unlimitedGem1Flag;
        public int gem10;
        public int first10;
        public int unlimitedGem10Flag;
        public int itemId;
        public int itemAmount;
        public int rateUpTicketId;
        public bool hasPeriod;
        public DateTime startAt;
        public DateTime endAt;
        public DateTime dispStartAt;
        public DateTime dispEndAt;
        public int sort;
        public int sun;
        public int mon;
        public int tue;
        public int wed;
        public int thu;
        public int fri;
        public int sat;
        public int reDrawItemId;
        public int drawLimit;
        public int box1Id;
        public string webViewUrl;
        public GachaStep[] gachaSteps;
    }
}
