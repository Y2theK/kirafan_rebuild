﻿namespace WWWTypes {
    public class GachaList {
        public Gacha gacha;
        public int uGem1Total;
        public int uGem1Daily;
        public int gem1Total;
        public int gem1Daily;
        public int gem1FreeDrawCount;
        public int gem10Total;
        public int gem10Daily;
        public int gem10CurrentStep;
        public int gem10FreeDrawCount;
        public int itemTotal;
        public int itemDaily;
        public int[] selectionCharacterIds;
        public bool highRarity;
        public DrawPoint[] drawPoints;
        public int playerDrawPoint;
    }
}
