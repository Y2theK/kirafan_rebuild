﻿namespace WWWTypes {
    public class GachaOneStep {
        public int limitedGem;
        public int unlimitedGem;
        public int lotteryBoxId;
        public int count;
        public int exType;
        public int exId;
        public int exAmount;
    }
}
