﻿using System;

namespace WWWTypes {
    public class Information {
        public int id;
        public string url;
        public int isFeatured;
        public string imgId;
        public int platform;
        public DateTime startAt;
        public DateTime endAt;
        public DateTime dispStartAt;
        public DateTime dispEndAt;
        public int sort;
    }
}
