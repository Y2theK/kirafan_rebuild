﻿namespace WWWTypes {
    public class Item {
        public int id;
        public string name;
        public int rarity;
        public int saleAmount;
        public int type;
        public int typeArg0;
        public int typeArg1;
        public int typeArg2;
        public int typeArg3;
    }
}
