﻿using System;

namespace WWWTypes {
    public class ItemTradeRecipe {
        public int id;
        public int limitedFlag;
        public int dstType;
        public int dstId;
        public int dstAmount;
        public int altType;
        public int altId;
        public int altAmount;
        public int messageMakeId;
        public int srcType1;
        public int srcId1;
        public int srcAmount1;
        public int srcType2;
        public int srcId2;
        public int srcAmount2;
        public DateTime? startAt;
        public DateTime? endAt;
        public int limitCount;
        public int tradeMax;
        public int resetFlag;
        public int totalTradeCount;
        public int monthlyTradeCount;
        public int groupId;
        public int eventType;
        public int sort;
        public int labelId;
    }
}
