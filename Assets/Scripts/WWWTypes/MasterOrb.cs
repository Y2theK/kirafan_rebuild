﻿namespace WWWTypes {
    public class MasterOrb {
        public int id;
        public string name;
        public int classType;
        public int expTableId;
        public int levelLimit;
    }
}
