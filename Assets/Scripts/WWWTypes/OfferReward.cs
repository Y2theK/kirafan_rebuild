﻿namespace WWWTypes {
    public class OfferReward {
        public int id;
        public int contentType;
        public int contentId;
        public int contentAmount;
        public string contentName;
    }
}
