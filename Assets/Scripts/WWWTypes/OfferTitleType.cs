﻿namespace WWWTypes {
    public class OfferTitleType {
        public int titleType;
        public int contentRoomFlg;
        public int category;
        public int offerIndex;
        public int offerPoint;
        public int offerMaxPoint;
        public int state;
        public int shown;
        public int subState;
        public int subState1;
        public int subState2;
        public int subState3;
    }
}
