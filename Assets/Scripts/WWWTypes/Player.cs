﻿using System;

namespace WWWTypes {
    public class Player {
        public long id;
        public string name;
        public string comment;
        public int age;
        public string myCode;
        public int currentAchievementId;
        public int level;
        public long levelExp;
        public long totalExp;
        public long gold;
        public long kirara;
        public long unlimitedGem;
        public long limitedGem;
        public long lotteryTicket;
        public int partyCost;
        public long stamina;
        public long staminaMax;
        public DateTime staminaUpdatedAt;
        public int recastTime;
        public int recastTimeMax;
        public long kiraraLimit;
        public int weaponLimit;
        public int weaponLimitCount;
        public int characterWeaponCount;
        public int facilityLimit;
        public int facilityLimitCount;
        public int roomObjectLimit;
        public int roomObjectLimitCount;
        public int characterLimit;
        public int itemLimit;
        public int friendLimit;
        public int supportLimit;
        public int loginCount;
        public int loginDays;
        public int continuousDays;
        public DateTime lastLoginAt;
        public string ipAddr;
        public string userAgent;
    }
}
