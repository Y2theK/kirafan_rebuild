﻿namespace WWWTypes {
    public class PlayerAbilityBoard {
        public long managedAbilityBoardId;
        public int abilityBoardId;
        public long managedCharacterId;
        public int[] equipItemIds;
    }
}
