﻿namespace WWWTypes {
    public class PlayerAchievement {
        public int id;
        public string name;
        public int enable;
        public int isNew;
        public int titleType;
        public int sortId;
        public string description;
    }
}
