﻿namespace WWWTypes {
    public class PlayerBattleParty {
        public long managedBattlePartyId;
        public string name;
        public long[] managedCharacterIds;
        public long[] managedWeaponIds;
        public int masterOrbId;
    }
}
