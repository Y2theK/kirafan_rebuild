﻿namespace WWWTypes {
    public class PlayerBattlePartyMember {
        public long managedBattlePartyId;
        public long[] managedCharacterIds;
        public long[] managedWeaponIds;
        public int masterOrbId;
    }
}
