﻿namespace WWWTypes {
    public class PlayerCharacter {
        public long managedCharacterId;
        public int level;
        public int levelLimit;
        public long exp;
        public int levelBreak;
        public int skillLevel1;
        public int skillLevelLimit1;
        public long skillExp1;
        public int skillLevel2;
        public int skillLevelLimit2;
        public long skillExp2;
        public int skillLevel3;
        public int skillLevelLimit3;
        public long skillExp3;
        public int characterId;
        public int shown;
        public int duplicatedCount;
        public int arousalLevel;
        public int viewCharacterId;
    }
}
