﻿namespace WWWTypes {
    public class PlayerCharacterQuest {
        public Quest quest;
        public int characterId;
        public int isRead;
        public int clearRank;
    }
}
