﻿namespace WWWTypes {
    public class PlayerEventQuest {
        public EventQuest eventQuest;
        public int clearRank;
        public int orderTotal;
        public int orderDaily;
        public int chestCostItemId;
        public int chestCostItemAmount;
        public long pointEventId;
        public int isRead;
    }
}
