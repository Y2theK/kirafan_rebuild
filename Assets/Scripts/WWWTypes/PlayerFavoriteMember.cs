﻿namespace WWWTypes {
    public class PlayerFavoriteMember {
        public int favoriteIndex;
        public long managedCharacterId;
        public int characterId;
        public int arousalLevel;
    }
}
