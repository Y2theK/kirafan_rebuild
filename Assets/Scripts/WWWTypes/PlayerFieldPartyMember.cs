﻿namespace WWWTypes {
    public class PlayerFieldPartyMember {
        public long managedPartyMemberId;
        public long managedCharacterId;
        public int characterId;
        public int scheduleId;
        public int scheduleTag;
        public long managedFacilityId;
        public int roomId;
        public int liveIdx;
        public int touchItemResultNo;
        public int flag;
        public string scheduleTable;
        public int arousalLevel;
    }
}
