﻿using System;

namespace WWWTypes {
    public class PlayerGachaLog {
        public long playerId;
        public int gachaId;
        public int drawType;
        public DateTime drawAt;
        public GachaResult[] gachaResults;
    }
}
