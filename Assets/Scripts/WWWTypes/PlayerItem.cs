﻿using System;

namespace WWWTypes {
    public class PlayerItem {
        public long managedItemId;
        public int state;
        public DateTime usedAt;
        public DateTime soldAt;
        public int itemId;
    }
}
