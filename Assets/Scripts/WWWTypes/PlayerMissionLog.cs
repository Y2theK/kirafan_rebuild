﻿using System;

namespace WWWTypes {
    public class PlayerMissionLog {
        public long managedMissionId;
        public int missionID;
        public long subCode;
        public int rate;
        public int rateMax;
        public int state;
        public MissionReward[] reward;
        public DateTime limitTime;
        public string targetMessage;
        public int category;
        public int missionSegType;
        public int missionFuncType;
        public int transitScene;
        public int transitParam;
        public int uiPriority;
    }
}
