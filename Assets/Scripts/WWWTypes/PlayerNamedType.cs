﻿namespace WWWTypes {
    public class PlayerNamedType {
        public long managedNamedTypeId;
        public int namedType;
        public int titleType;
        public int level;
        public long exp;
    }
}
