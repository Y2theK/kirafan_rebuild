﻿namespace WWWTypes {
    public class PlayerOffer {
        public long id;
        public int offerId;
        public int titleType;
        public int category;
        public int offerIndex;
        public int progress;
        public int maxProgress;
        public int offerPoint;
        public int offerMaxPoint;
        public string title;
        public string body;
        public string clientName;
        public int state;
        public int transitQuestId;
        public string transitQuestName;
        public int funcType;
        public int funcId;
        public OfferReward[] offerRewards;
    }
}
