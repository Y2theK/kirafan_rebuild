﻿namespace WWWTypes {
    public class PlayerOfferQuest {
        public Quest quest;
        public int offerId;
        public int titleType;
        public int category;
        public int isRead;
        public int isOrder;
        public int clearRank;
    }
}
