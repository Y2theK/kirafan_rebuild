﻿using System;

namespace WWWTypes {
    public class PlayerPresent {
        public long managedPresentId;
        public int type;
        public int objectId;
        public int amount;
        public string options;
        public string title;
        public string message;
        public bool deadlineFlag;
        public DateTime createdAt;
        public DateTime deadlineAt;
        public DateTime receivedAt;
    }
}
