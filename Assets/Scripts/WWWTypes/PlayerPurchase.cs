﻿using System;

namespace WWWTypes {
    public class PlayerPurchase {
        public long id;
        public int platform;
        public string receiptNo;
        public string sku;
        public long price;
        public string name;
        public int state;
        public DateTime purchasedAt;
        public int premiumId;
        public DateTime? expiredAt;
    }
}
