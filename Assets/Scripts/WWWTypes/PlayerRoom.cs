﻿namespace WWWTypes {
    public class PlayerRoom {
        public long managedRoomId;
        public int floorId;
        public int groupId;
        public PlayerRoomArrangement[] arrangeData;
        public int active;
    }
}
