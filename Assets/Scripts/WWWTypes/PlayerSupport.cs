﻿namespace WWWTypes {
    public class PlayerSupport {
        public long managedSupportId;
        public string name;
        public int active;
        public long[] managedCharacterIds;
        public long[] managedWeaponIds;
    }
}
