﻿namespace WWWTypes {
    public class PlayerTownFacility {
        public long managedTownFacilityId;
        public int facilityId;
        public int buildPointIndex;
        public int level;
        public int openState;
        public long actionTime;
        public long buildTime;
    }
}
