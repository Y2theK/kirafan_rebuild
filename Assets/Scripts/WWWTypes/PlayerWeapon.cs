﻿namespace WWWTypes {
    public class PlayerWeapon {
        public long managedWeaponId;
        public int level;
        public long exp;
        public int skillLevel;
        public long skillExp;
        public int weaponId;
    }
}
