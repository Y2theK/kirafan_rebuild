﻿namespace WWWTypes {
    public class PointEventResult {
        public long playerGainedPoint;
        public long playerTotalPoint;
    }
}
