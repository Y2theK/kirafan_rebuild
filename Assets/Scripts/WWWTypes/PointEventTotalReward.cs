﻿namespace WWWTypes {
    public class PointEventTotalReward {
        public long id;
        public long pointEventId;
        public long point;
        public int contentType;
        public int contentId;
        public int contentAmount;
    }
}
