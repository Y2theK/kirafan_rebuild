﻿using System;

namespace WWWTypes {
    public class Premium {
        public int id;
        public string name;
        public int expiredDay;
        public PremiumReward[] rewards;
        public string bgName;
        public string bannerName;
        public string helpBannerName;
        public string summary;
        public string notice;
        public DateTime? expiredAt;
        public int extendDay;
        public int remindType;
    }
}
