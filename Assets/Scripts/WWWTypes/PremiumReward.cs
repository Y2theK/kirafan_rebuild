﻿namespace WWWTypes {
    public class PremiumReward {
        public int itemId;
        public int itemAmount;
        public int premiumId;
        public int grantType;
        public int contentType;
    }
}
