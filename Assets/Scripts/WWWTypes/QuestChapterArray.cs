﻿using System;

namespace WWWTypes {
    public class QuestChapterArray {
        public int id;
        public int difficulty;
        public int part;
        public string name;
        public int[] questIds;
        public int trialQuestId;
        public int collectQuestId;
        public DateTime? newDispEndAt;
    }
}
