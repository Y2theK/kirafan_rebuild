﻿namespace WWWTypes {
    public class QuestNpc {
        public long id;
        public int characterId;
        public int characterLevel;
        public int characterLimitBreak;
        public int characterSkillLevel;
        public int weaponId;
        public int weaponLevel;
        public int weaponSkillLevel;
    }
}
