﻿namespace WWWTypes {
    public class QuestReward {
        public long gold;
        public long gem;
        public int itemId1;
        public int amount1;
        public int itemId2;
        public int amount2;
        public int itemId3;
        public int amount3;
        public int roomObjectId;
        public int roomObjectAmount;
        public int weaponId;
        public int weaponAmount;
    }
}
