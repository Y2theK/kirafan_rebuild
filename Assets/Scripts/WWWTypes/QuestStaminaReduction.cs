﻿using System;

namespace WWWTypes {
    public class QuestStaminaReduction {
        public int questCategory;
        public int targetId;
        public int difficulty;
        public DateTime startAt;
        public DateTime endAt;
    }
}
