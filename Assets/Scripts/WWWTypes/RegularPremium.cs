﻿namespace WWWTypes {
    public class RegularPremium {
        public int premiumId;
        public string premiumName;
        public PremiumReward[] premiumRewards;
    }
}
