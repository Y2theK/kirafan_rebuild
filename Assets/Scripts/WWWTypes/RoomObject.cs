﻿using System;

namespace WWWTypes {
    public class RoomObject {
        public int id;
        public string name;
        public int type;
        public int buyAmount;
        public int saleAmount;
        public int objectLimit;
        public DateTime dispStartAt;
        public DateTime dispEndAt;
        public int bargainFlag;
        public DateTime bargainStartAt;
        public DateTime bargainEndAt;
        public int bargainBuyAmount;
    }
}
