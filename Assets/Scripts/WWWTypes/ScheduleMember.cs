﻿namespace WWWTypes {
    public class ScheduleMember {
        public long managedPartyMemberId;
        public long managedCharacterId;
        public int scheduleId;
        public int scheduleTag;
        public long managedFacilityId;
        public int flag;
        public string scheduleTable;
    }
}
