﻿namespace WWWTypes {
    public class ScheduleMemberParam {
        public long managedPartyMemberId;
        public long managedCharacterId;
        public int roomId;
        public int liveIdx;
    }
}
