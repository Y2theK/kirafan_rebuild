﻿using System;

namespace WWWTypes {
    public class StoreBonus {
        public int contentId;
        public int contentAmount;
        public DateTime? expireAt;
    }
}
