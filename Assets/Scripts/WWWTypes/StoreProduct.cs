﻿using System;

namespace WWWTypes {
    public class StoreProduct {
        public int id;
        public int platform;
        public int env;
        public string sku;
        public int amount1;
        public int amount2;
        public bool isNew;
        public bool alreadyShown;
        public DateTime? dispStartAt;
        public DateTime? dispEndAt;
        public long price;
        public int limitNum;
        public int linkId;
        public int uiPriority;
        public int uiType;
        public string name;
        public string description;
        public Premium premium;
        public StoreBonus[] storeBonuses;
        public int purchaseCount;
        public int premiumId;
        public int tabId;
        public int prefabType;
        public int passType;
        public DirectSale directSale;
    }
}
