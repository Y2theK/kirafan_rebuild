﻿namespace WWWTypes {
    public class SupportCharacter {
        public long managedCharacterId;
        public int characterId;
        public int level;
        public long exp;
        public int levelBreak;
        public int skillLevel1;
        public int skillLevel2;
        public int skillLevel3;
        public int weaponId;
        public int weaponLevel;
        public int weaponSkillLevel;
        public long weaponSkillExp;
        public int namedLevel;
        public long namedExp;
        public int duplicatedCount;
        public int arousalLevel;
        public int abilityBoardId;
        public int[] equipItemIds;
    }
}
