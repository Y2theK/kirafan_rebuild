﻿namespace WWWTypes {
    public class TownFacilityBuyState {
        public int facilityId;
        public int actionNo;
        public int openState;
        public int nextLevel;
        public int buildPointIndex;
        public long actionTime;
        public long buildTime;
    }
}
