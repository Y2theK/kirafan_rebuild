﻿namespace WWWTypes {
    public class TownFacilitySetState {
        public long managedTownFacilityId;
        public int buildPointIndex;
        public int openState;
        public long buildTime;
        public int actionNo;
    }
}
