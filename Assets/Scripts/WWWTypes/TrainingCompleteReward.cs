﻿namespace WWWTypes {
    public class TrainingCompleteReward {
        public int slotId;
        public TrainingRewardItem[] trainingRewardItems;
        public int rewardCharaExp;
        public int rewardFriendship;
        public int rewardGold;
        public int rewardKRRPoint;
        public int firstGem;
    }
}
