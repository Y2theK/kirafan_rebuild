﻿namespace WWWTypes {
    public class TrainingInfo {
        public int trainingId;
        public int clear;
        public string name;
        public int category;
        public int openRank;
        public int num;
        public int limitNum;
        public int costType;
        public int cost;
        public int clearTime;
        public int skipGem;
        public int condCharaLv;
        public int condPartyNum;
        public int firstGem;
        public int rewardCharaExp;
        public int rewardFriendship;
        public int rewardCurrencyType;
        public int rewardCurrencyAmount;
        public TrainingRewardInfo[] rewards;
    }
}
