﻿namespace WWWTypes {
    public class TrainingOrderParam {
        public int slotId;
        public int trainingId;
        public long[] managedCharacterIds;
    }
}
