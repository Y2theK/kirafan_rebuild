﻿using System;

namespace WWWTypes {
    public class TrainingSlotInfo {
        public int slotId;
        public long orderId;
        public TrainingInfo trainingData;
        public DateTime orderAt;
        public int openState;
        public int needRank;
        public long[] managedCharacterIds;
    }
}
