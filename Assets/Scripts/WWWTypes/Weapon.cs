﻿namespace WWWTypes {
    public class Weapon {
        public int id;
        public string name;
        public int classType;
        public int cost;
        public int expTableId;
        public int levelLimit;
        public int attack;
        public int magic;
        public int defense;
        public int magicDefence;
        public int attackMax;
        public int magicMax;
        public int defenseMax;
        public int magicDefenceMax;
        public int skillId;
        public int skillLevelLimit;
        public int saleAmount;
        public int[] bonuses;
        public int canSell;
        public int evolvedCount;
        public int passiveSkillId;
        public int equipableCharacterId;
    }
}
