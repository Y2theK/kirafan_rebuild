﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WeaponRecipeRequestTypes;

// Token: 0x0200007F RID: 127
[Token(Token = "0x2000058")]
[StructLayout(3)]
public static class WeaponRecipeRequest
{
	// Token: 0x06000341 RID: 833 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FA")]
	[Address(RVA = "0x10162B240", Offset = "0x162B240", VA = "0x10162B240")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000342 RID: 834 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FB")]
	[Address(RVA = "0x10162B2CC", Offset = "0x162B2CC", VA = "0x10162B2CC")]
	public static MeigewwwParam Getall([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000343 RID: 835 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FC")]
	[Address(RVA = "0x10162B358", Offset = "0x162B358", VA = "0x10162B358")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000344 RID: 836 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FD")]
	[Address(RVA = "0x10162B428", Offset = "0x162B428", VA = "0x10162B428")]
	public static MeigewwwParam Get(string weaponId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
