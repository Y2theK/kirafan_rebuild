﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WeaponRecipeResponseTypes;
using WWWTypes;

// Token: 0x020000A6 RID: 166
[Token(Token = "0x200007D")]
[StructLayout(3)]
public static class WeaponRecipeResponse
{
	// Token: 0x060003F7 RID: 1015 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003AD")]
	[Address(RVA = "0x10162B518", Offset = "0x162B518", VA = "0x10162B518")]
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003F8 RID: 1016 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003AE")]
	[Address(RVA = "0x10162B580", Offset = "0x162B580", VA = "0x10162B580")]
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
