﻿using CommonResponseTypes;
using WWWTypes;

namespace WeaponRecipeResponseTypes {
    public class Getall : CommonResponse {
        public WeaponRecipe[] weaponRecipes;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (weaponRecipes == null) ? string.Empty : weaponRecipes.ToString();
            return str;
        }
    }
}
