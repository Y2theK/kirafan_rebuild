﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WeaponRequestTypes;

// Token: 0x02000080 RID: 128
[Token(Token = "0x2000059")]
[StructLayout(3)]
public static class WeaponRequest
{
	// Token: 0x06000345 RID: 837 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FE")]
	[Address(RVA = "0x10162B6F8", Offset = "0x162B6F8", VA = "0x10162B6F8")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000346 RID: 838 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002FF")]
	[Address(RVA = "0x10162B784", Offset = "0x162B784", VA = "0x10162B784")]
	public static MeigewwwParam Getall([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000347 RID: 839 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000300")]
	[Address(RVA = "0x10162B810", Offset = "0x162B810", VA = "0x10162B810")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000348 RID: 840 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000301")]
	[Address(RVA = "0x10162B8E0", Offset = "0x162B8E0", VA = "0x10162B8E0")]
	public static MeigewwwParam Get(string weaponId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
