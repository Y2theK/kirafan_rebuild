﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WeaponResponseTypes;
using WWWTypes;

// Token: 0x020000A7 RID: 167
[Token(Token = "0x200007E")]
[StructLayout(3)]
public static class WeaponResponse
{
	// Token: 0x060003F9 RID: 1017 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003AF")]
	[Address(RVA = "0x10162B9D0", Offset = "0x162B9D0", VA = "0x10162B9D0")]
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003FA RID: 1018 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003B0")]
	[Address(RVA = "0x10162BA38", Offset = "0x162BA38", VA = "0x10162BA38")]
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
