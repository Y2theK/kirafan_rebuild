﻿using CommonResponseTypes;
using WWWTypes;

namespace WeaponResponseTypes {
    public class Getall : CommonResponse {
        public Weapon[] weapons;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (weapons == null) ? string.Empty : weapons.ToString();
            return str;
        }
    }
}
