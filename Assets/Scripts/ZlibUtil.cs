﻿using System;
using System.IO;
using zlib;

public class ZlibUtil {
    public const int bufferSize = 2048;
    public static int bits = 0xF;

    public static byte[] compress(byte[] inbuf, int level = 6) {
        byte[] outbuf = null;
        ZStream zStream = new ZStream();
        MemoryStream memoryStream= new MemoryStream();

        try {
            if (zStream.deflateInit(level, bits) == 0) {
                byte[] buffer = new byte[bufferSize];
                zStream.next_in = inbuf;
                zStream.next_in_index = 0;
                zStream.avail_in = inbuf.Length;
                zStream.next_out = buffer;

                bool done = false;
                while (!done) {
                    zStream.next_out_index = 0;
                    zStream.avail_out = buffer.Length;
                    while (zStream.avail_out != 0) {
                        int flag = zStream.deflate(4);
                        if (flag != 0) {
                            done = true;
                            if (flag == 1) {
                                if (zStream.avail_out != buffer.Length) {
                                    memoryStream.Write(buffer, 0, buffer.Length - zStream.avail_out);
                                }
                                if (zStream.deflateEnd() == 0) {
                                    outbuf = memoryStream.ToArray();
                                }
                            }
                            break;
                        }
                    }
                    memoryStream.Write(buffer, 0, buffer.Length);
                    zStream.next_out = buffer;
                }
            }
        } catch (Exception ex) {
            Console.Write("ZlibUtil.compress error : " + ex.ToString());
        } finally {
            zStream.free();
            memoryStream.Close();
        }
        return outbuf;
    }

    public static byte[] uncompress(byte[] inbuf) {
        byte[] outbuf = null;
        ZStream zStream = new ZStream();
        MemoryStream memoryStream = new MemoryStream();

        try {
            if (zStream.inflateInit(bits) == 0) {
                byte[] buffer = new byte[bufferSize];
                zStream.next_in = inbuf;
                zStream.next_in_index = 0;
                zStream.avail_in = inbuf.Length;
                zStream.next_out = buffer;

                bool done = false;
                while (!done) {
                    zStream.next_out_index = 0;
                    zStream.avail_out = buffer.Length;
                    while (zStream.avail_out != 0) {
                        int flag = zStream.inflate(0);
                        if (flag != 0) {
                            done = true;
                            if (flag == 1) {
                                if (zStream.avail_out != buffer.Length) {
                                    memoryStream.Write(buffer, 0, buffer.Length - zStream.avail_out);
                                }
                                if (zStream.inflateEnd() == 0) {
                                    outbuf = memoryStream.ToArray();
                                }
                            }
                            break;
                        }
                    }
                    memoryStream.Write(buffer, 0, buffer.Length);
                    zStream.next_out = buffer;
                }
            }
        } catch (Exception ex) {
            Console.Write("ZlibUtil.uncompress error : " + ex.ToString());
        } finally {
            zStream.free();
            memoryStream.Close();
        }
        return outbuf;
    }

    public byte[] compressProcess(byte[] inbuf, int level = 6) {
        byte[] outbuf = null;
        ZStream zStream = new ZStream();
        MemoryStream memoryStream= new MemoryStream();

        try {
            if (zStream.deflateInit(level, bits) == 0) {
                byte[] buffer = new byte[bufferSize];
                zStream.next_in = inbuf;
                zStream.next_in_index = 0;
                zStream.avail_in = inbuf.Length;
                zStream.next_out = buffer;

                bool done = false;
                while (!done) {
                    zStream.next_out_index = 0;
                    zStream.avail_out = buffer.Length;
                    while (zStream.avail_out != 0) {
                        int flag = zStream.deflate(4);
                        if (flag != 0) {
                            done = true;
                            if (flag == 1) {
                                if (zStream.avail_out != buffer.Length) {
                                    memoryStream.Write(buffer, 0, buffer.Length - zStream.avail_out);
                                }
                                if (zStream.deflateEnd() == 0) {
                                    outbuf = memoryStream.ToArray();
                                }
                            }
                            break;
                        }
                    }
                    memoryStream.Write(buffer, 0, buffer.Length);
                    zStream.next_out = buffer;
                }
            }
        } catch (Exception ex) {
            Console.Write("ZlibUtil.compress error : " + ex.ToString());
        } finally {
            zStream.free();
            memoryStream.Close();
        }
        return outbuf;
    }

    public byte[] uncompressProcess(byte[] inbuf) {
        byte[] outbuf = null;
        ZStream zStream = new ZStream();
        MemoryStream memoryStream = new MemoryStream();

        try {
            if (zStream.inflateInit(bits) == 0) {
                byte[] buffer = new byte[bufferSize];
                zStream.next_in = inbuf;
                zStream.next_in_index = 0;
                zStream.avail_in = inbuf.Length;
                zStream.next_out = buffer;

                bool done = false;
                while (!done) {
                    zStream.next_out_index = 0;
                    zStream.avail_out = buffer.Length;
                    while (zStream.avail_out != 0) {
                        int flag = zStream.inflate(0);
                        if (flag != 0) {
                            done = true;
                            if (flag == 1) {
                                if (zStream.avail_out != buffer.Length) {
                                    memoryStream.Write(buffer, 0, buffer.Length - zStream.avail_out);
                                }
                                if (zStream.inflateEnd() == 0) {
                                    outbuf = memoryStream.ToArray();
                                }
                            }
                            break;
                        }
                    }
                    memoryStream.Write(buffer, 0, buffer.Length);
                    zStream.next_out = buffer;
                }
            }
        } catch (Exception ex) {
            Console.Write("ZlibUtil.uncompress error : " + ex.ToString());
        } finally {
            zStream.free();
            memoryStream.Close();
        }
        return outbuf;
    }
}
