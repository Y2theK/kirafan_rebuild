﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000015 RID: 21
[Token(Token = "0x200000E")]
[StructLayout(3, Size = 4)]
public enum eADVAnchor
{
	// Token: 0x040000A9 RID: 169
	[Token(Token = "0x400005F")]
	UpperLeft,
	// Token: 0x040000AA RID: 170
	[Token(Token = "0x4000060")]
	UpperCenter,
	// Token: 0x040000AB RID: 171
	[Token(Token = "0x4000061")]
	UpperRight,
	// Token: 0x040000AC RID: 172
	[Token(Token = "0x4000062")]
	MiddleLeft,
	// Token: 0x040000AD RID: 173
	[Token(Token = "0x4000063")]
	MiddleCenter,
	// Token: 0x040000AE RID: 174
	[Token(Token = "0x4000064")]
	MiddleRight,
	// Token: 0x040000AF RID: 175
	[Token(Token = "0x4000065")]
	LowerLeft,
	// Token: 0x040000B0 RID: 176
	[Token(Token = "0x4000066")]
	LowerCenter,
	// Token: 0x040000B1 RID: 177
	[Token(Token = "0x4000067")]
	LowerRight
}
