﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000017 RID: 23
[Token(Token = "0x2000010")]
[StructLayout(3, Size = 4)]
public enum eADVCharaAnchor
{
	// Token: 0x040000B7 RID: 183
	[Token(Token = "0x400006D")]
	Center,
	// Token: 0x040000B8 RID: 184
	[Token(Token = "0x400006E")]
	Face,
	// Token: 0x040000B9 RID: 185
	[Token(Token = "0x400006F")]
	Bottom
}
