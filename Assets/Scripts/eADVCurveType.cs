﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000014 RID: 20
[Token(Token = "0x200000D")]
[StructLayout(3, Size = 4)]
public enum eADVCurveType
{
	// Token: 0x040000A1 RID: 161
	[Token(Token = "0x4000057")]
	Linear,
	// Token: 0x040000A2 RID: 162
	[Token(Token = "0x4000058")]
	InQuad,
	// Token: 0x040000A3 RID: 163
	[Token(Token = "0x4000059")]
	OutQuad,
	// Token: 0x040000A4 RID: 164
	[Token(Token = "0x400005A")]
	InOutQuad,
	// Token: 0x040000A5 RID: 165
	[Token(Token = "0x400005B")]
	InCubic,
	// Token: 0x040000A6 RID: 166
	[Token(Token = "0x400005C")]
	OutCubic,
	// Token: 0x040000A7 RID: 167
	[Token(Token = "0x400005D")]
	InOutCubic
}
