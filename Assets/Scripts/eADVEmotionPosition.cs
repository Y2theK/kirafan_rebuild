﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000012 RID: 18
[Token(Token = "0x200000B")]
[StructLayout(3, Size = 4)]
public enum eADVEmotionPosition
{
	// Token: 0x04000091 RID: 145
	[Token(Token = "0x4000047")]
	CharaDefault,
	// Token: 0x04000092 RID: 146
	[Token(Token = "0x4000048")]
	CharaFace,
	// Token: 0x04000093 RID: 147
	[Token(Token = "0x4000049")]
	CharaFaceLeft,
	// Token: 0x04000094 RID: 148
	[Token(Token = "0x400004A")]
	CharaFaceRight,
	// Token: 0x04000095 RID: 149
	[Token(Token = "0x400004B")]
	CharaBottom
}
