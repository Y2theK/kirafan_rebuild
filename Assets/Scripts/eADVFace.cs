﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000011 RID: 17
[Token(Token = "0x200000A")]
[StructLayout(3, Size = 4)]
public enum eADVFace
{
	// Token: 0x0400007B RID: 123
	[Token(Token = "0x4000031")]
	Default,
	// Token: 0x0400007C RID: 124
	[Token(Token = "0x4000032")]
	Joy,
	// Token: 0x0400007D RID: 125
	[Token(Token = "0x4000033")]
	Angry,
	// Token: 0x0400007E RID: 126
	[Token(Token = "0x4000034")]
	Sorrow,
	// Token: 0x0400007F RID: 127
	[Token(Token = "0x4000035")]
	Happy,
	// Token: 0x04000080 RID: 128
	[Token(Token = "0x4000036")]
	Shy,
	// Token: 0x04000081 RID: 129
	[Token(Token = "0x4000037")]
	Surprise,
	// Token: 0x04000082 RID: 130
	[Token(Token = "0x4000038")]
	Unique1,
	// Token: 0x04000083 RID: 131
	[Token(Token = "0x4000039")]
	Unique2,
	// Token: 0x04000084 RID: 132
	[Token(Token = "0x400003A")]
	Unique3,
	// Token: 0x04000085 RID: 133
	[Token(Token = "0x400003B")]
	Unique4,
	// Token: 0x04000086 RID: 134
	[Token(Token = "0x400003C")]
	Unique5,
	// Token: 0x04000087 RID: 135
	[Token(Token = "0x400003D")]
	Unique6,
	// Token: 0x04000088 RID: 136
	[Token(Token = "0x400003E")]
	Unique7,
	// Token: 0x04000089 RID: 137
	[Token(Token = "0x400003F")]
	Unique8,
	// Token: 0x0400008A RID: 138
	[Token(Token = "0x4000040")]
	Unique9,
	// Token: 0x0400008B RID: 139
	[Token(Token = "0x4000041")]
	Unique10,
	// Token: 0x0400008C RID: 140
	[Token(Token = "0x4000042")]
	Unique11,
	// Token: 0x0400008D RID: 141
	[Token(Token = "0x4000043")]
	Unique12,
	// Token: 0x0400008E RID: 142
	[Token(Token = "0x4000044")]
	Unique13,
	// Token: 0x0400008F RID: 143
	[Token(Token = "0x4000045")]
	Num
}
