﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000016 RID: 22
[Token(Token = "0x200000F")]
[StructLayout(3, Size = 4)]
public enum eADVScreenAnchor
{
	// Token: 0x040000B3 RID: 179
	[Token(Token = "0x4000069")]
	Middle,
	// Token: 0x040000B4 RID: 180
	[Token(Token = "0x400006A")]
	Upper,
	// Token: 0x040000B5 RID: 181
	[Token(Token = "0x400006B")]
	Lower
}
