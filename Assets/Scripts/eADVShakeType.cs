﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000013 RID: 19
[Token(Token = "0x200000C")]
[StructLayout(3, Size = 4)]
public enum eADVShakeType
{
	// Token: 0x04000097 RID: 151
	[Token(Token = "0x400004D")]
	RandomS,
	// Token: 0x04000098 RID: 152
	[Token(Token = "0x400004E")]
	RandomM,
	// Token: 0x04000099 RID: 153
	[Token(Token = "0x400004F")]
	RandomL,
	// Token: 0x0400009A RID: 154
	[Token(Token = "0x4000050")]
	HorizontalS,
	// Token: 0x0400009B RID: 155
	[Token(Token = "0x4000051")]
	HorizontalM,
	// Token: 0x0400009C RID: 156
	[Token(Token = "0x4000052")]
	HorizontalL,
	// Token: 0x0400009D RID: 157
	[Token(Token = "0x4000053")]
	VerticalS,
	// Token: 0x0400009E RID: 158
	[Token(Token = "0x4000054")]
	VerticalM,
	// Token: 0x0400009F RID: 159
	[Token(Token = "0x4000055")]
	VerticalL
}
