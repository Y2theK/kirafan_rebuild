﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000019 RID: 25
[Token(Token = "0x2000012")]
[StructLayout(3, Size = 4)]
public enum eADVSidePosition
{
	// Token: 0x040000C8 RID: 200
	[Token(Token = "0x400007E")]
	LeftSide,
	// Token: 0x040000C9 RID: 201
	[Token(Token = "0x400007F")]
	RightSide
}
