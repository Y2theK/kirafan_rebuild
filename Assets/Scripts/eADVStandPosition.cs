﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000018 RID: 24
[Token(Token = "0x2000011")]
[StructLayout(3, Size = 4)]
public enum eADVStandPosition
{
	// Token: 0x040000BB RID: 187
	[Token(Token = "0x4000071")]
	Left,
	// Token: 0x040000BC RID: 188
	[Token(Token = "0x4000072")]
	CenterLeft,
	// Token: 0x040000BD RID: 189
	[Token(Token = "0x4000073")]
	Center,
	// Token: 0x040000BE RID: 190
	[Token(Token = "0x4000074")]
	CenterRight,
	// Token: 0x040000BF RID: 191
	[Token(Token = "0x4000075")]
	Right,
	// Token: 0x040000C0 RID: 192
	[Token(Token = "0x4000076")]
	LeftLeft,
	// Token: 0x040000C1 RID: 193
	[Token(Token = "0x4000077")]
	CenterLeftLeft,
	// Token: 0x040000C2 RID: 194
	[Token(Token = "0x4000078")]
	CenterHalfLeft,
	// Token: 0x040000C3 RID: 195
	[Token(Token = "0x4000079")]
	CenterHalfRight,
	// Token: 0x040000C4 RID: 196
	[Token(Token = "0x400007A")]
	CenterRightRight,
	// Token: 0x040000C5 RID: 197
	[Token(Token = "0x400007B")]
	RightRight,
	// Token: 0x040000C6 RID: 198
	[Token(Token = "0x400007C")]
	Num
}
