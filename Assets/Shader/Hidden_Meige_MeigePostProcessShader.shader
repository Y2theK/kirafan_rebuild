Shader "Hidden/Meige/MeigePostProcessShader" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_BloomTex ("Bloom Texture", 2D) = "black" {}
		_AdaptedLuminunceTex ("AdaptedLuminunce Texture", 2D) = "black" {}
		[KeywordEnum(Disable,Enable)] _EnableToneCurve ("Tone Curve", Float) = 0
		[KeywordEnum(Disable,Enable)] _EnableContrast ("Contrast", Float) = 0
		[KeywordEnum(Disable,Enable)] _EnableBrightness ("Brightness", Float) = 0
		[KeywordEnum(Disable,Enable)] _EnableChroma ("Chroma", Float) = 0
		[KeywordEnum(Disable,Enable)] _EnableColorBlend ("ColorBlend", Float) = 0
		[KeywordEnum(Disable,Enable)] _EnableBloom ("Bloom", Float) = 0
		_ToneCurve_Param ("_ToneCurve_Param", Vector) = (1,1,1,1)
		_ToneCurve_Coeff ("ToneCurve_Coeff", Float) = 0
		_ToneCurve_WhitePointValue ("ToneCurve_WhitePoint", Float) = 1
		_CorrectContrastRate ("CorrectBlend_ContrastRate", Float) = 0
		_CorrectBrightnessRate ("CorrectBlend_BrightnessRate", Float) = 0
		_CorrectChromaRate ("CorrectBlend_ChromaRate", Float) = 0
		_CorrectBlendRate ("CorrectBlend_BlendRate", Float) = 0
		_CorrectBlendColor ("CorrectBlend_Color", Vector) = (1,1,1,1)
		_Bloomintensity ("Bloom_Intensity", Float) = 1
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		sampler2D _MainTex;
		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
}