Shader "Meige/MeigeStandardShader" {
	Properties {
		[KeywordEnum(Unlight,BlinnPhong,Toon)] _ShadingType ("Shading Type", Float) = 0
		[HideInInspector] _BlendMode ("Alpha Blend Mode", Float) = 1
		[HideInInspector] _BlendSrc ("AlphaBlend Src", Float) = 5
		[HideInInspector] _BlendDst ("AlphaBlend Dst", Float) = 10
		[Enum(Add,0,Sub,1,RevSub,2,Min,3,Max,4)] [HideInInspector] _BlendOpColor ("AlphaBlend Color Op", Float) = 0
		[Enum(Add,0,Sub,1,RevSub,2,Min,3,Max,4)] [HideInInspector] _BlendOpAlpha ("AlphaBlend Alpha Op", Float) = 0
		[Enum(Less,2,LEqual,4,Equal,3,NotEqual,6,GEqual,7,Greater,5,Always,8)] _DepthTest ("Depth Test", Float) = 2
		[Enum(Off,0,On,1)] _DepthWrite ("Depth Write", Float) = 1
		_DepthOffsetFactor ("Depth Offset Factor", Float) = 0
		_DepthOffsetUnits ("Depth Offset Units", Float) = 0
		[KeywordEnum(Disable,Enable)] _Using_AlphaTest ("Use AlphaTest", Float) = 0
		[Enum(Off,0,Front,1,Back,2)] _CullMode ("Cull Mode", Float) = 2
		[KeywordEnum(Disable,Enable)] _Using_Fog ("Use Fog", Float) = 0
		_MeshColor ("Mesh Color", Vector) = (1,1,1,1)
		_HDRFactor ("HDR Factor", Range(0.01, 1)) = 1
		_AlphaTestRefValue ("AlphaTest Reference Value", Range(0, 1)) = 0.01
		[Header(Texture_Albedo)] [HideInInspector] [NoScaleOffset] _Texture_Albedo ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_Albedo ("MaterialColor", Vector) = (1,1,1,1)
		[Header(Texture_AlbedoLayer)] [HideInInspector] [NoScaleOffset] _Texture_AlbedoLayer ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_AlbedoLayer ("MaterialColor", Vector) = (1,1,1,1)
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerColorBlendMode_Albedo ("LayerBlend", Float) = 1
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerAlphaBlendMode_Albedo ("LayerBlendAlpha", Float) = 1
		[Header(Texture_Normal)] [HideInInspector] [NoScaleOffset] _Texture_Normal ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_Normal ("MaterialColor", Vector) = (1,1,1,1)
		[Header(Texture_Normal Layer)] [HideInInspector] [NoScaleOffset] _Texture_NormalLayer ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_NormalLayer ("MaterialColor", Vector) = (1,1,1,1)
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerColorBlendMode_Normal ("LayerBlend", Float) = 1
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerAlphaBlendMode_Normal ("LayerBlendAlpha", Float) = 1
		[Header(Texture_Specular)] [HideInInspector] [NoScaleOffset] _Texture_Specular ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_Specular ("MaterialColor", Vector) = (1,1,1,1)
		[Header(Texture_Specular Layer)] [HideInInspector] [NoScaleOffset] _Texture_SpecularLayer ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_SpecularLayer ("MaterialColor", Vector) = (1,1,1,1)
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerColorBlendMode_Specular ("LayerBlend", Float) = 1
		[Enum(Std,1,Add,2,Sub,3,SrcOne,5,DstOne,6,Mul,7)] [HideInInspector] _LayerAlphaBlendMode_Specular ("LayerBlendAlpha", Float) = 1
		[Header(Texture_Additional0)] [HideInInspector] [NoScaleOffset] _Texture_Additional0 ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_Additional0 ("MaterialColor", Vector) = (1,1,1,1)
		[Header(Texture_Additional0 Layer)] [HideInInspector] [NoScaleOffset] _Texture_Additional0Layer ("Texture", 2D) = "" {}
		[HideInInspector] _MatColor_Additional0Layer ("MaterialColor", Vector) = (1,1,1,1)
		[KeywordEnum(None,Single,Layer)] [HideInInspector] _Using_Albedo_Texture ("Use AlbedoTexture", Float) = 0
		[KeywordEnum(None,Single,Layer)] [HideInInspector] _Using_Normal_Texture ("Use NormalTexture", Float) = 0
		[KeywordEnum(None,Single,Layer)] [HideInInspector] _Using_Specular_Texture ("Use SpecularTexture", Float) = 0
		[KeywordEnum(None,Single,Layer)] [HideInInspector] _Using_Additional0_Texture ("Use Additional0Texture", Float) = 0
		[Enum(Less,2,Equal,3,LessEqual,4,Greater,5,NotEqual,6,GreaterEqual,7,Always,8)] _StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		[Enum(Keep,0,Zero,1,Replace,2,IncrSat,3,DecrSat,4,Invert,5)] _StencilOp ("Stencil Operation", Float) = 0
		[Enum(Keep,0,Zero,1,Replace,2,IncrSat,3,DecrSat,4,Invert,5)] _StencilOpFail ("Stencil Operation Fail", Float) = 0
		[Enum(Keep,0,Zero,1,Replace,2,IncrSat,3,DecrSat,4,Invert,5)] _StencilOpZFail ("Stencil Operation ZFail", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = 1;
		}
		ENDCG
	}
	//CustomEditor "MeigeShaderEditor"
}