# Kirafan Rebuild

A project to re-create きららファンタジア in Unity from ARM bytecode.

Unity version used: `2021.3.10f1`
</br></br>
## Current goal

- [ ] Reach title screen
  - [x] Title states
    - [x] Main classes
    - [x] Referenced classes
  - [ ] GameSystem
    - [x] Main class
    - [ ] Referenced classes
      - [ ] Managers
      - [ ] Meige
  - [x] Title UI classes
</br></br>
## Notes

Nothing at the moment.